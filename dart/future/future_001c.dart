void main() {
  printDelayed(5);
  printDelayed(3);
  print('Go go go');
}

Future<void> printDelayed(int s) async {
  Future.delayed(Duration(seconds: s), () => print('after $s seconds'));
}
