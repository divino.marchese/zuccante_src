Future<String> myAsync(int n) async {
  return Future<String>.delayed(const Duration(seconds: 2), () {
    return "$n : async";
  });
}

// a blocking approach
void main() async {
  String s1 = await myAsync(1);
  print(s1);
  String s2 = await myAsync(2);
  print(s2);
  String s3 = await myAsync(3);
  print(s3);
  print('done');
}
