void main() {
  printDelayed(3);
  print('Go go go');
}

void printDelayed(int s) async {
  await Future.delayed(Duration(seconds: s), () => print('after $s seconds'));
}
