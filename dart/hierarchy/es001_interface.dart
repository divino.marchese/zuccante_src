class A {
  void method() {
    print("A method");
  }
}

class B {
  int n = 4;
  void method() {
    print("B method");
  }
}

class C implements A, B {
  @override
  int n = 10; // try to comment

  @override
  void method() {
    print("C method");
  }
}

main(List<String> args) {
  A a = A();
  B b = B();
  C c = C();
  a.method();
  b.method();
  c.method();
}
