import 'dart:io';

void main() {
  RawDatagramSocket.bind(InternetAddress.anyIPv4, 0)
      .then((RawDatagramSocket socket) {
    print('Sending from ${socket.address.address}:${socket.port}');
    int port = 3000;
    // convert to UTF16 codes List<int>
    socket.send(
        'Hello from UDP land!\n'.codeUnits, InternetAddress.loopbackIPv4, port);
  });
}
