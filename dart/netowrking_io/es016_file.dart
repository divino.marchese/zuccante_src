import 'dart:convert';
import 'dart:io';

Future<void> main(List<String> args) async {
  var file = File('data.txt');
  StringBuffer contents = StringBuffer();
  if (await file.exists()) {
    // Read file
    contents = StringBuffer();
    var contentStream = file.openRead();

    contentStream.transform(Utf8Decoder()).transform(LineSplitter()).listen(
        (String line) =>
            contents.write(line), // Add line to our StringBuffer object
        onDone: () => print(contents
            .toString()), // Call toString() method to receive the complete data
        onError: (e) => print('[Problems]: $e'));
  }
}
