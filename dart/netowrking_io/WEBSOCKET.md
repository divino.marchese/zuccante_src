# appunti web socket
Qui di seguito tracciamo le linee per l'*handshake* fra cliente e erver. Si rimanda anche a Wikipedia(en) [qui](https://en.wikipedia.org/wiki/WebSocket).

## il client

Passa i seguenti *header* (su MDN i dettagli).

- `Connection: Upgrade` *switch to another protocol* [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Connection).
- `Upgrade: websocket` aggiorna una connessione già stabilita [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Upgrade).
- `Sec-WebSocket-Key` passa la chiave 16B codificati in `base64`. vedi [qui](https://it.wikipedia.org/wiki/Base64) (Wikipedia(it)).

Creiamo una chiave di 16B random, nel nostro caso 
```
echo -n '1234567890abcdef' | base64
```
ed ecco il nostro *header*
```
Sec-WebSocket-Key:  MTIzNDU2Nzg5MGFiY2RlZg==
```

- `Sec-WebSocket-Version: 13`: solo questa versione è accetaa!

Vi possono essere quindi altri header (vedi i riferimenti esterni)! Di solito si passa anche 

- `Origin: ` per i dettagli vedi [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Origin).

## il server risponde

QUindi ecco la risposta del server.

- `Upgrade: websocket` 
- `Connection: Upgrade` 
- `Sec-WebSocket-Accept` la chiave costruita come segue.

Abbiamo
```
UID: 258EAFA5-E914-47DA-95CA-C5AB0DC85B11
chiave client: YW1iYXJhYmFjY2ljaWNvCg==
```
come [qui](https://datatracker.ietf.org/doc/html/rfc6455) otteniamo la concatenazione 
```
YW1iYXJhYmFjY2ljaWNvCg==258EAFA5-E914-47DA-95CA-C5AB0DC85B11
```
con la shell facciamo 2 passaggi: primo **sha1**
```
echo -n YW1iYXJhYmFjY2ljaWNvCg==258EAFA5-E914-47DA-95CA-C5AB0DC85B11 | sha1sum
```
otteniamo
```
5d4da3882be0d93c5452807bd2e9fc023f14aaa6  -
```
a noi interessa solo la prima stringa
```
echo -n 5d4da3882be0d93c5452807bd2e9fc023f14aaa6 | xxd -r -p | base64
```
otteniamo
```
XU2jiCvg2TxUUoB70un8Aj8UqqY=
```
che è il valore che darà il server per `Sec-WebSocket-Accept`.

