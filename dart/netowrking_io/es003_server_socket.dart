import 'dart:io';

// use netcat 127.0.0.1 3000 (client)

void main() {
  ServerSocket.bind(InternetAddress.anyIPv4, 3000).then((ServerSocket server) {
    server.listen(handleClient);
  });
}

void handleClient(Socket client) {
  print('Connection from ' +
      '${client.remoteAddress.address}:${client.remotePort}');
  client.write("Hello from simple server!\n");
  client.close();
}
