import 'dart:io';

// use netcat -l 3000

void main() {
  Socket.connect("127.0.0.1", 3000).then((socket) {
    print('Connected to: '
        '${socket.remoteAddress.address}:${socket.remotePort}');
    socket.destroy();
  }).catchError((e) {
    // server down
    if (e is SocketException) print('SocketException => $e');
  });
}
