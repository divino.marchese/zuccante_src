import 'dart:io';

void main(List args){
  InternetAddress multicastAddress = InternetAddress("224.0.0.0");
  int multicastPort = 3000;
  RawDatagramSocket.bind(InternetAddress.anyIPv4, multicastPort)
    .then((RawDatagramSocket socket){
       print('Datagram socket ready to receive');
       print('${socket.address.address}:${socket.port}');
       // join multicast group
       socket.joinMulticast(multicastAddress);
       print('Multicast group joined');
       socket.listen((RawSocketEvent e){
         Datagram? d = socket.receive();
         if (d == null) return;

         String message = String.fromCharCodes(d.data).trim();
         print('Datagram from ${d.address.address}:${d.port}: ${message}');
       });
  });
}