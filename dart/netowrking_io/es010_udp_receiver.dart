import 'dart:io';

/* UDP

#include <stdint.h>

typedef struct {
   uint16_t sourcePort;
   uint16_t destPort;
   uint16_t length;
   uint16_t checksum;
} UDPHeader_t;
 */

// netcat -u 127.0.0.1 3000 PROVA!!!!

void main(List<String> args) {
  RawDatagramSocket.bind(InternetAddress.anyIPv4, 3000)
      // socket is listening for any incoming datagram 0.0.0.0
      .then((RawDatagramSocket socket) {
    print('Datagram socket ready to receive');
    print('${socket.address.address}:${socket.port}');
    socket.listen((RawSocketEvent e) {
      Datagram? d = socket.receive();
      if (d == null) return;
      String message = String.fromCharCodes(d.data).trim();
      print('Datagram from ${d.address.address}:${d.port}: ${message}');
    });
  });
}
