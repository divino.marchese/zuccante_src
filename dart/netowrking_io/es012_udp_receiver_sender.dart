import 'dart:io';

// use: netcat -u localhost 3000

void main(List<String> args){
  RawDatagramSocket.bind(InternetAddress.anyIPv4, 3000).then((RawDatagramSocket socket){
    print('UDP Echo ready to receive');
    print('${socket.address.address}:${socket.port}');
    socket.listen((RawSocketEvent e){
      Datagram? d = socket.receive();
      if (d == null) return;

      String message = String.fromCharCodes(d.data);
      print('Datagram from ${d.address.address}:${d.port}: ${message.trim()}');
      message = 'echo: ' + message;
      socket.send(message.codeUnits, d.address, d.port);
    });
  });
}