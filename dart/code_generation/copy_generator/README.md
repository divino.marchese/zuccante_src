# copy generator

Mettiamo in `pubspec.yaml` i seguenti pacchetti
- `buld` [home](https://pub.dev/packages/build) per definire la ricetta che ci permette di geneare il codice,
- `build_config` [home](https://pub.dev/packages/build_config) per creare il file di configurazione `pubspec.yaml` e
- `build_runner` [home](https://pub.dev/packages/build_runner) per operare la generazione.

## come lavora `build_runner`

Parttiamo dal file di configurazione.

## `build.yaml`

La scrittura di un tale file risulta delicata, [qui](https://github.com/dart-lang/build/blob/master/docs/build_yaml_format.md) le regole, esso è un file `yaml`, per il dettaglio vedi [1] in particolare [qui](https://yaml.org/spec/1.2.2/) per la documentazione uffiviale.). Il file viene diviso in due parti:  
i) la definizione dei **target** e dei **builder** associati, vedi `targets` e  
ii) il dettaglio dei vari **builder**, vedi `builders`

### target

[API](https://github.com/dart-lang/build/blob/master/docs/build_yaml_format.md#buildtarget) ecco i target nel nostro file
```yaml
targets:
  # target name
  $default:
    builders:
      # builder
      copy_generator:my_builder:
        # enabled is default true
        enabled: true
        # List<STring>
        generate_for:
          - "lib/*.txt"
```
Qui ne esiste uno solo, l'obiettivo di *default* indicato con `$default`. Vengono dichiarati nella forma `$packageName:$builderName`, il target `$default` indicherebbe un tqarget omonimo al pacchetto.

### builder

[API](https://github.com/dart-lang/build/blob/master/docs/build_yaml_format.md#builderdefinition)
```yaml
builders:
  my_builder:
    import: "package:copy_generator/copy_generator.dart"
    # factory method
    builder_factories: ["copyBuilder"]
    # see Builder class
    build_extensions: {".txt": [".copy.txt"]}
    # cache is default
    build_to: source
```
Per prima cosa diamo la risorsa usando la sintassi `package:uri`, poi indichiamo i metodi in una lista di *factory*, nel nostro caso è solo in `copy_generator.dart`
```dart
Builder copyBuilder(BuilderOptions options) => CopyBuilder();
```
ed infine le gastione delle estensioni dei file da creare (nel nostro caso uno solo). 

# la classe `Builder`

È quello della documentazione uffucuale.
```dart
class CopyBuilder implements Builder {
  @override
  final buildExtensions = const {
    '.txt': ['.copy.txt']
  };

  @override
  Future<void> build(BuildStep buildStep) async {
    // each `buildStep` has a single input.
    var inputId = buildStep.inputId;

    // create a new target `AssetId` based on the old one.
    var copy = inputId.changeExtension('.copy.txt');
    var contents = await buildStep.readAsString(inputId);

    // write out the new asset.
    await buildStep.writeAsString(copy, contents);
  }
}
```
La troviamo in `builder.dart`, [API](https://pub.dev/documentation/build/latest/build/Builder-class.html), 
```dart
var inputId = buildStep.inputId;
```
ci permette di recuperare un `AssetId`, [API](https://pub.dev/documentation/build/latest/build/AssetId-class.html), ovvero una risorsa del pacchetto (un file), 
esso possiede, fra le varie:
- un'estensione,
- un path,
- un nome e
- un pacchetto di appartenenza; il contenuto lo leggiamo
```dart
var contents = await buildStep.readAsString(inputId);
```
[API](https://pub.dev/documentation/build/latest/build/AssetReader/readAsString.html) e lo scriviamo nel nuovo asset
```dart
await buildStep.writeAsString(copy, contents);
```
[API](https://pub.dev/documentation/build/latest/build/BuildStep/writeAsString.html).


## generazione

Qui entra in gioco `build_runner`, abbiamo un aiuto con
```
dart run build_runner help
```
vediamo se tutto è scritto bene con
```
dart run build_runner doctor
```
e generaimo con
```
dart run build_runner build
```
possiamo "cancellare" con
```
dart run build_runner clean
```

## altri materiali

[1] "Official Yaml" [qui](https://yaml.org/).  
[2] "Builder Definition" [qui](https://github.com/dart-lang/build/blob/master/build_config/README.md)
