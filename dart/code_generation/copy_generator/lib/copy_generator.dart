import 'package:build/build.dart';
import 'package:copy_generator/builder.dart';

Builder copyBuilder(BuilderOptions options) => CopyBuilder();
