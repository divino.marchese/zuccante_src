import 'package:build/build.dart';

class CopyBuilder implements Builder {
  @override
  final buildExtensions = const {
    '.txt': ['.copy.txt']
  };

  @override
  Future<void> build(BuildStep buildStep) async {
    var inputId = buildStep.inputId;

    var copy = inputId.changeExtension('.copy.txt');
    var contents = await buildStep.readAsString(inputId);

    await buildStep.writeAsString(copy, contents);
  }
}
