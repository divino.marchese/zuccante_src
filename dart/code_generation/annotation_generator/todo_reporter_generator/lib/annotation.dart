class Todo {
  final String name;
  final String todoUrl;
  const Todo({required this.name, required this.todoUrl});
}
