import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';
import 'package:todo_reporter_generator/src/builder.dart';

Builder annotationBuilder(BuilderOptions options) =>
    LibraryBuilder(TodoReporterGenerator(), generatedExtension: '.g.dart');
