import 'dart:async';

import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import '../annotation.dart';

class TodoReporterGenerator extends GeneratorForAnnotation<Todo> {
  @override
  FutureOr<String> generateForAnnotatedElement(
      Element element, ConstantReader annotation, BuildStep buildStep) {
    String name = annotation.peek('name')?.stringValue ?? '';
    String todoUrl = annotation.peek('todoUrl')?.stringValue ?? '';
    String json = '{ "name": "$name", "tdoUrl": "$todoUrl" }';
    return 'var json = \'$json\';';
  }
}
