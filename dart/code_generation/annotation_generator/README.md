# todo reporter generator e pacchetto di test

L'idea, con modifiche di sostanza, è stat tratta da un articolo su Medium [qui](https://medium.com/flutter-community/part-2-code-generation-in-dart-annotations-source-gen-and-build-runner-bbceee28697b).

Rimandiamo al precedente esempio di cui questo costituisce una prosecuzione.


## `pubspect.yaml` i pacchetti importati

- `build_runner` lo useremo nel pacchetto di test [home](https://pub.dev/packages/build_runner),
- `build_config` ci permette di creare il file di configurazione per i vari *target*, [home](https://pub.dev/packages/build_config),
- `build` [home](https://pub.dev/packages/build),
- `source_gen` [home](https://pub.dev/packages/source_gen) oltre a `build` ci permette di lavorare a grana fine usando le *annotation* e
- `analyzer` ci permette di analizzare del codice Dart [home](https://pub.dev/packages/analyzer).


## il *generator*

Nella cartella `src` (cartella privata per un *package*) abbiamo messo la classe per generare il codice
```dart
class TodoReporterGenerator extends GeneratorForAnnotation<Todo> {
  @override
  FutureOr<String> generateForAnnotatedElement(
      Element element, ConstantReader annotation, BuildStep buildStep) {
    String name = annotation.peek('name')?.stringValue ?? '';
    String todoUrl = annotation.peek('todoUrl')?.stringValue ?? '';
    String json = '{ "name": "$name", "tdoUrl": "$todoUrl" }';
    return 'var json = \'$json\';';
  }
}
```
`GeneratorForAnnotation<T>` [API](https://pub.dev/documentation/source_gen/latest/source_gen/GeneratorForAnnotation-class.html) crea un `Generator` [API](https://pub.dev/documentation/source_gen/latest/source_gen/Generator-class.html), pensato per le *annotation*, vedi *generic* `T`. Una *annotation* in Dart banalmente la si costruisce a partire da una classe per un PODO - *Plain Old Dart Object* - tale tipologia di oggetti a campi "semplici" (tipi primitivi o collezioni *built-in*) la incontriamo in special modo durante la *serialiation* e la *de-serialization*.

Il metodo `generateForAnnotatedElement` [API](https://pub.dev/documentation/source_gen/latest/source_gen/GeneratorForAnnotation/generateForAnnotatedElement.html) ha tre parametri:
- `Element` del pacchetto `analyzer` della libreria `element` [API](https://pub.dev/documentation/analyzer/latest/dart_element_element/dart_element_element-library.html), fa riferimento sia agli oggetti che ai tipi, cioè alle classi, è ciò che viene annotato,`Element` è una classe [API](https://pub.dev/documentation/analyzer/latest/dart_element_element/Element-class.html) e ad esempio abbiamo `element.displayName` ci permette di recuperare il nome della classe (nel nostro caso),
- `ConstantReader` è il modo con cui viene fornita l'*annotation* e grazie a cui possiamo indagarne i valori eventuali come nel nostro caso [API](https://pub.dev/documentation/source_gen/latest/source_gen/ConstantReader-class.html) si basa su `DartObject` 
- `BuildStep` già incontrato nell'esempio precedente [API](https://pub.dev/documentation/build/latest/build/BuildStep-class.html) rappresenta un singolo step nel processo di costruzione, in particolare grazie ad esso risaliamo a `inputId` (sua *property*) che è un asset, tipicamente un file (*library*) che andiamo ad analizzare nel nostro processo di *building* del codice.

In particolare il metodo 
```dart
peek(String field) → ConstantReader?
``` 
ci permette di recuperare i campi (*property*) dell'annotation.

## la configurazione in `build.yaml`

Rimandiamo all'esempio precedente. Riproponiamo una documentazione di sintesi [qui](https://github.com/dart-lang/build/blob/master/build_config/README.md).
```yaml
targets:
  $default:
    builders:
      todo_builder:
        enabled: true

builders:
  todo_builder:
    import: 'package:todo_reporter_generator/todo_reporter_generator.dart'
    builder_factories: ['annotationBuilder']
    build_extensions: { '.dart': ['.g.dart'] }
    build_to: source
    auto_apply: root_package
```
la novità, bella descrizione del *builder* sta in `auto_apply` dove indichiamo che il processo di generazione prende in considerazione solo gli asset di questo pacchetto! L'assenza di tale opzione, dando
```
dart run build_runner build
```
crea problemi (manca parte della configurazione che andrebbe manualmente fornita). Ricordiamo anche l'utilità dei comandi
```
dart run build_runner doctor
```
per tesater la correttezza sintattica e
```
dart run build_runner clean
```
per cancellare il lavoro di `build_runner` e ripartire da zero, da chiamare una volta modificato il pacchetto per l'autoegerazione quandi si lavra nel pacchetto di test.