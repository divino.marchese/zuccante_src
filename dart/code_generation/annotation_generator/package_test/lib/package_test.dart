import 'package:todo_reporter_generator/annotation.dart';

@Todo(name: "memoa", todoUrl: "file://my_memops/memoa.txt")
class Model {
  final String name = "Ciccio";
  final int age = 23;
}
