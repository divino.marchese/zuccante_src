# code_gen_demo

La novità rispetto all'esempio precedente è l'uso dei pacchetti
- `source_gen`, [qui](https://pub.dev/packages/source_gen), invitiamo alla lettura delle FAQ per vedere le differenze col pacchetto `build`.
- `build` [qui](https://pub.dev/packages/build) usato dal precedente e non solo.
- `build_config` [qui](https://pub.dev/packages/build_config/install) per generare il file di configurazione `build.yaml`.
- `build_runner` [qui](https://pub.dev/packages/build_runner) che ci fornisce una CLI per generare file.

Il progetto viene creato con `dart create -t console-full code_gen_demo`.

Qui lavoriamo con un singolo pacchetto, questo, per poi utilizzarlo su di un esmpio, `example` e, finalmente, introduciamo all'uso delle *annotation*. Prepariamo la classe **PODO** (Plain Old Dart Object) per le nsotre annotation
``` dart
class MyAnnotation {
  final String prop;
  const MyAnnotation({this.prop});
}
```

## generatore

Sempre analogamente all'esempiom precedente prepariamo il *builder* che qui si chiama *generator*, per la classe vedi [qui](https://pub.dev/documentation/source_gen/latest/source_gen/GeneratorForAnnotation-class.html)
``` dart
class AnnotationGenerator extends GeneratorForAnnotation<MyAnnotation> {
  @override
  generateForAnnotatedElement(Element element, ConstantReader annotation, BuildStep buildStep) {
    String className = element.displayName;
    String path = buildStep.inputId.path;
    String prop = annotation.peek('prop').stringValue;
    return '''
    // ${DateTime.now()}
    // =====================

    // classname: $className
    // path: $path
    // prop: $prop

    print('Hello MyAnnotation');
    '''
    ;
  } 
}
```
il metodo `generateForAnnotatedElement(..)`, vedi [qui](https://pub.dev/documentation/source_gen/latest/source_gen/GeneratorForAnnotation/generateForAnnotatedElement.html), 


`GeneratorForAnnotation` estende `Generator` pensato appunto per generare codice Dart basandosi su di una libreria. Vediamo quindi di analizzare il metodo
``` dart
@override
generateForAnnotatedElement(Element element, ConstantReader annotation, BuildStep buildStep) 
```
Per quanto riguarda `Element`
> The element model describes the semantic (as opposed to syntactic) structure of Dart code. Generally speaking, an element represents something that is declared in the
code, such as a class, method, or variable.

Ecco che troviamo `ClassElemenet`, `ClassMemberElement`, `ConstructorElement` ed infine `Element`. 

`BuildSTep` già lo conosciamo e rappresemta un passo nella costruzione. 

`CostantReader` permette di indagare valori ed altro .. `peek` ci permette di recuperare un oggetto di tipo `CostantReader` il quale ci permette di recuperare il tipo ed il vallre, nel nostro caso come stringa visto che poi dobbiamo scriverla.

## Configurazione

Il formato del file `build.yaml` è decsritto nel dettaglio [qui](https://github.com/dart-lang/build/blob/master/docs/build_yaml_format.md).

Per prima cosa guardiamo il *dictionary* dei *builders*
```
builders:
  annotation_builder:
    import: 'package:code_gen_demo/builder.dart'
    builder_factories: ['annotationBuilder']
    build_extensions: { '.dart': ['.my_annotation.dart'] }
    auto_apply: root_package
    build_to: source
```
soffermiamoci solo su alcuni:
- `auto_apply` `root_package`: il ]builder] viene applicato solo al pacchetto in cui viene importato il pacchetto generatore (quello che stiamo scrivendo).
- `build_to` è `source` se il sorgente viene generato non in *cache* ma nell`albero primario. Per i target abbiamo
```
targets:
  $default:
    builders:
      code_gen_demo|annotation_builder:
        enabled: true
```   

## il test

Dato eventualmente un `dart pub get` procediamo con 
```
dart pub run build_runner build
```

