import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';
import 'src/annotation_builder.dart';

Builder annotationBuilder(BuilderOptions options) =>
    LibraryBuilder(AnnotationGenerator(),
        generatedExtension: '.my_annotation.dart');
