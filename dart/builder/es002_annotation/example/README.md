# example

Con questo esempio mettiamo alla prova `code_gen_demo` che importiamo come pacchetto nel solito `pubspec.yaml`
```
dependencies:
  ...
  code_gen_demo:
    path: ../code_gen_demo/
```
rimandiamo ovviamente al `README.md` di quel pacchetto. In dart lo testeremo per prodeurre da `example.dart` il file `example.my_annotation.dart`. Il comando da dare è
```