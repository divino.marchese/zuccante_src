main() async {
  Stream<int> stream =
      Stream<int>.periodic(const Duration(seconds: 1), transform);
  // added this statement
  stream = stream.take(5);
  await for (int i in stream) {
    print(i);
  }
}

int transform(int x) {
  return (x + 1) * 2;
}
