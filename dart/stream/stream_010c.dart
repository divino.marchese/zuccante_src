import 'dart:async';

main() {
  late StreamSubscription sub;
  sub = Stream<int>.periodic(const Duration(seconds: 1), (value) => value + 1)
      .take(20)
      .listen((data) {
    print('recieved: $data');
    if (data == 10) {
      sub.pause(Future.delayed(const Duration(seconds: 3)));
      // sub.pause(Future.delayed(const Duration(seconds: 3), () => sub.resume()));
    }
  }, onDone: () {
    print("FINE");
  });
}
