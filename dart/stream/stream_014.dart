import 'dart:math';
import 'dart:async';

main() {
  late StreamSubscription<int> sub;
  Stream<int> stream = getRandomValues(6);
  sub = stream.listen((val) async {
    print(val);
    if (val == 4) {
      await sub.cancel();
      print('cancel');
    }
  }, onDone: () {
    print('done');
  });
}

Stream<int> getRandomValues(int goal) async* {
  var random = Random();
  while (true) {
    int num = random.nextInt(6) + 1;
    await Future.delayed(Duration(milliseconds: 500));
    yield num;
    if (num == goal) break;
  }
}
