void main() async {
  Stream<int> stream = timedCounter(Duration(seconds: 1), 10);
  await for (var data in stream) {
    print('yeld: $data');
  }
}

Stream<int> timedCounter(Duration interval, [int? maxCount]) async* {
  // optional could be null
  int i = 0;
  while (true) {
    await Future.delayed(interval);
    yield i++;
    if (i == maxCount) break;
  }
}
