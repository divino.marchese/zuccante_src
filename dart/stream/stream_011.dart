import 'dart:async';

main() {
  var streamController = StreamController.broadcast();
  Stream stream =
      Stream.periodic(const Duration(seconds: 1), (value) => value + 1).take(8);

  late StreamSubscription sub1;

  sub1 = streamController.stream.listen((data) {
    print('recieved1: $data');
    if (data == 2) {
      sub1.pause(
          Future.delayed(const Duration(seconds: 3), () => print('fire')));
      // try to uncomment
      // sub1.cancel();
    }
  });

  streamController.stream.listen((data) {
    print('recieved2: $data');
  });

  streamController.addStream(stream);
}
