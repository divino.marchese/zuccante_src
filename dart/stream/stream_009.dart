import 'dart:async';

int transform(int x) => (x + 1) * 2;

main() {
  print("Creating a StreamController...");

  StreamController<int> streamController = StreamController.broadcast();
  Stream<int> stream =
      Stream<int>.periodic(const Duration(seconds: 1), transform);
  stream = stream.take(5);

  // streamController.addStream(stream);

  streamController.stream.listen((data) {
    print("DataReceived1: $data");
  }, onDone: () {
    print("Task Done1");
  }, onError: (error) {
    print("Some Error1");
  });

  streamController.stream.listen((data) {
    print("DataReceived2: $data");
  }, onDone: () {
    print("Task Done2");
  }, onError: (error) {
    print("Some Error2");
  });

  streamController.addStream(stream);

  print("code controller is here");
}
