main() {
  Stream<int> stream =
      Stream<int>.periodic(const Duration(seconds: 1), transform);
  // added this statement
  stream = stream.take(5);
  stream.listen((data) => print('yeld: $data'));
}

int transform(int x) {
  return (x + 1) * 2;
}
