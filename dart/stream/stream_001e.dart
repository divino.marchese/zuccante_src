void main() {
  timedCounter(Duration(seconds: 1), 10).handleError((e) {
    print(e.toString());
  }).forEach((data) => print('yeld: $data'));
}

Stream<int> timedCounter(Duration interval, [int? maxCount]) async* {
  int i = 0;
  while (true) {
    await Future.delayed(interval);
    yield i++;
    if (i == maxCount) break;
    if (i == 4) throw Exception('go sbaja');
  }
}
