void main(List<String> args) {
  Stream.periodic(const Duration(seconds: 1), (count) {
    if (count == 2) {
      throw Exception('Exceptional event');
    }
    return count;
  }).take(4).handleError(print).forEach(print);
}
