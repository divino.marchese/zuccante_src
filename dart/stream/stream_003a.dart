import 'dart:async';

void main() async {
  // final StreamController<dynamic>
  final StreamController ctrl = StreamController();

  final Stream stream = ctrl.stream;
  stream.listen((data) => print('$data'), onDone: () => print("done"));

  ctrl.sink.add('my name');
  // try to remove await ... await is better
  await Future.delayed(Duration(seconds: 2), () => ctrl.sink.add(1234));
  await Future.delayed(Duration(seconds: 2),
      () => ctrl.sink.add({'a': 'element A', 'b': 'element B'}));
  await Future.delayed(Duration(seconds: 2), () => ctrl.sink.add(123.45));
  ctrl.close();
}
