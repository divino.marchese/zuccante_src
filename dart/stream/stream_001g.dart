void main() {
  Stream<int> stream = timer(10);
  stream.listen((data) => print('yeld: $data'));
}

Stream<int> timer(int n) async* {
  if (n > 0) {
    await Future.delayed(Duration(seconds: 1));
    yield n;
    // recursion
    yield* timer(n - 1);
  }
}
