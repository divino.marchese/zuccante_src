import 'dart:async';

main() {
  late StreamSubscription sub;
  sub = Stream<int>.periodic(const Duration(seconds: 1), (value) => value + 1)
      .take(20)
      .listen((data) async {
    print('recieved: $data');
    if (data == 10) {
      sub.pause();
      await Future.delayed(const Duration(seconds: 2));
      print('fire');
      sub.resume();
    }
  }, onDone: () {
    print("FINE");
  });
}
