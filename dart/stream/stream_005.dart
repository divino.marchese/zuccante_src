import 'dart:io';

main() async {
  Stream<int> stream = Stream<int>.periodic(Duration(seconds: 2), callback);
  await for (int i in stream) {
    if (i > 12) exit(0);
    print(i);
  }
}

int callback(int value) => (value + 1) * 2;
