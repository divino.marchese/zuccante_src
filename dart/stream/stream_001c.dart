void main() {
  Stream<int> stream = timedCounter(Duration(seconds: 1), 10);
  stream.listen(
    (data) => print('yeld: $data'),
    onDone: () => print('xe finia'),
  );
}

Stream<int> timedCounter(Duration interval, [int? maxCount]) async* {
  int i = 0;
  while (true) {
    await Future.delayed(interval);
    yield i++;
    if (i == maxCount) break;
  }
}
