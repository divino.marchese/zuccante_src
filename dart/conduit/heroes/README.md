[[_TOC_]]

# heroes

## Running the Application Locally

Run `conduit serve` from this directory to run the application. For running within an IDE, run `bin/main.dart`. By default, a configuration file named `config.yaml` will be used.

To generate a SwaggerUI client, run `conduit document client`.

## Running Application Tests

To run all tests for this application, run the following in this directory:

```
pub run test
```

The default configuration file used when testing is `config.src.yaml`. This file should be checked into version control. It also the template for configuration files used in deployment.

## Deploying an Application

See the documentation for [Deployment](https://conduit.io/docs/deploy/).

# Cosa è Conduit e come si installa

Riprendiamo la documentazione esterna qui integrandola col nostro tutorial.

Con la *null safety* Aqueduct aveva bisogno di un fork, ed ecco Conduict: [qui](https://www.theconduit.dev/). Per installarlo si procede nel modo seguente (con l'accortezza di avere `pub` nel `PATH`).
```
pub global activate conduit
```
installati i paccheti ci viene suggerito
```
export PATH="$PATH":"$HOME/.pub-cache/bin"
```
Portare il comando nel proprio `PATH`, ad esempio
```
ln -s /home/genji/.pub-cache/bin/conduit conduit
```
Dando quindi `source .profile` per ricaricare il `PATH`. Seguiamo la documentazione ufficiale [qui](https://gitbook.theconduit.dev/).

# Installazione di PostgreSQL e primi passi con la sua gestione

Qui ci riferiamo all'installazione su Ubuntu 18.04. Una volta installati i pacchetti possiamo passare all'untente di PostgreSQL
```
sudo su postgres
```
ovvero
``` 
sudo -i -u postgres
```
(l'utente infatti non è attivato) e quindi, solo ora, entrare nella shell.
```
psql
```
Ci spuò connettere nel dattaglio come (dare `man psql`). Possiamo dare
```
SELECT version();
```
Per creare un db si fa come al solito (stando nella shell)
```
CREATE DATABASE conduit01;
```
quindi creiamo l'utente con la sua password criptata MD5, [qui](https://www.postgresql.org/docs/8.0/sql-createuser.html) per la documentazione
```
CREATE USER conduit WITH ENCRYPTED PASSWORD 'zuccante@2021';
```
con `\du` vediamo il risultato, un utente è un ruolo - `ROLE` - con permessi di login. Possiamo anche (come utente Linux `postgres`)
```
postgres=# SELECT usename FROM pg_user;
 usename  
----------
 postgres
 conduit
(2 rows)
```
Per cancellare un utente (vedi MySQL): `DROP USER ...`. Garatiamo tutti i privilegi sul nostro db
```
GRANT ALL PRIVILEGES ON DATABASE conduit01 to conduit;
```
Vediamo cosa è successo
```
postgres=# \l conduit01
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges   
-----------+----------+----------+-------------+-------------+-----------------------
 conduit01 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/postgres         +
           |          |          |             |             | postgres=CTc/postgres+
           |          |          |             |             | conduit=CTc/postgres
(1 row)
```
nella documentazione i dettagli per leggere l'informazione di cui sopra [qui](https://www.postgresql.org/docs/12/ddl-priv.html), in particolare `c` indica la possibilità di connessione.  
Usciamo `\q`.  
A uqesto punto ci connettiamo
```
psql -h 127.0.0.1 -U conduit -d conduit01
```
Usciamo di nuovo `\q` e spostiamoci in `/etc/postgresql/12/main/` (eventualmente aggiustando la versione, in WIndows vi sarà un percorso analogo) ne capiamo il motivo andando a vedere la riga per `md5` (solo `127.0.0.1` è ammesso) del file `pg_hba.conf`.
Se si è entrati in `psql` si può dare
```
\c dbname username
```
Altre modalità grafiche di connessione sono `pgadmin` (web, da installare). Alcuni comandi utili sono:  
- `\l` per il list dei database disponibili
- `\l db` per vedere le caratteristiche di `db`
- `\dt` lista delle tabelle del db in uso
- `\du` per vedere utenti e regole
- `\c dbname` per usare il db
- `\d table_name` per descrivere la tabella
- `\dn` per la lista desgli *schema*
- `\i file` esegue i coman\h ALTER TABLEdi da un file
- `\h ALTER TABLE` help su di un comando `sql`

# L'applicazione in Conduit

Il nostro rkiferimento resta la documentazione del framework [qui](https://gitbook.theconduit.dev/); iniziamo il tutorial [qui](https://gitbook.theconduit.dev/tut). Mancando `.giotignore` lo prepariamo con `gitignore.io` segnando `Dart` e `Visual Studio Code`.

Creato il progetto, entrando nella directory del medesimo, dare
```
conduit serve
```
da terminale per avviare l'applicazione, e provare, ache su di un browser o con `curl`
```
http://localhost:8888/example
```
per fermare il server `CTRL+C`, se non si riuscisse ad uccidere il server provare con
```
fuser 8888/tcp
```
trovare il `PID` del processo e bdare `kill -9 <PID>`. 

## Basic Controller

Mettendo in `channel.dart`
```dart
router.route('/heroes').link(() => HeroesController());
```
scriviamo la classe `Controller`
```dart
class HeroesController extends Controller {
  final _heroes = [
    {'id': 11, 'name': 'Mr. Nice'},
    {'id': 12, 'name': 'Narco'},
    {'id': 13, 'name': 'Bombasto'},
    {'id': 14, 'name': 'Celeritas'},
    {'id': 15, 'name': 'Magneta'},
  ];

  @override
  Future<RequestOrResponse> handle(Request request) async {
    return Response.ok(_heroes);
  }
}
```

## with param id

```dart
router.route('/heroes/[:id]').link(() => HeroesController());
```
con
```dart
@override
  Future<RequestOrResponse> handle(Request request) async {
    if (request.path.variables.containsKey('id')) {
      final id = int.parse(request.path.variables['id']!);
      final Map<String, Object> hero = _heroes.firstWhere(
          (hero) => hero['id'] == id,
          orElse: () => {"error": "hero not found"});
      if (hero["error"] != null) {
        return Response.notFound();
      }
      return Response.ok(hero);
    }
    return Response.ok(_heroes);
  }
```
in modo più pulito possiamo scrivere
```dart
class HeroesController extends ResourceController {
  final _heroes = [
    ...  
  ];

  @Operation.get()
  Future<Response> getAllHeroes() async {
    return Response.ok(_heroes);
  }

  @Operation.get('id')
  Future<Response> getHeroByID() async {
    final id = int.parse(request!.path.variables['id']!);
    final hero = _heroes.firstWhere((hero) => hero['id'] == id,
        orElse: () => {"error": "hero not found"});
    if (hero["error"] != null) {
      return Response.notFound();
    }
    return Response.ok(hero);
  }
}
```
o ancora meglio con una *request binding* usando opportuna *annotation* 
```dart
@Operation.get('id')
  Future<Response> getHeroByID(@Bind.path('id') int id) async {
    final hero = _heroes.firstWhere((hero) => hero['id'] == id,
        orElse: () => {"error": "hero not found"});
    if (hero["error"] != null) {
      return Response.notFound();
    }
    return Response.ok(hero);
  }
```

## il database

Per preparare il db sfruttiamo l'ORM interno 

## model

Scriviamo il *model*
```dart
class Hero extends ManagedObject<_Hero> implements _Hero {}

class _Hero {
  @primaryKey
  late int id;

  @Column(unique: true)
  late String name;
}
```
`Hero` viene detto un *instance type*: `Hero` fa riferimento allóggetto ricavato dalla tabella del db che si chiamerà `_Hero`; nella classe `Hero` non c'è nulla ma potremmo mettere *property* come *getter* che non appaiono come campi della tabella. 

## la connessione ed il routing

Il prossimo step prevede la definizione della connessione.
```dart
class HeroesChannel extends ApplicationChannel {
  ManagedContext context;
​
  @override
  Future prepare() async {
    logger.onRecord.listen((rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));
​
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final persistentStore = PostgreSQLPersistentStore.fromConnectionInfo(
      "heroes_user", "password", "localhost", 5432, "heroes");
​
    context = ManagedContext(dataModel, persistentStore);
  }
​
  @override
  Controller get entryPoint {
...
```
Ora `ManagedDataModel.fromCurrentMirrorSystem();` trova tutti i *model* ovvero `ManagedObject<T>`;
un `ManagedContext` ci permetterà di eseguire le *query* sul nostro db. A questo punto dobbiamo riscrivere il *controller*.  
Per il *routing* possiamo modificare il codice nel modo che segue
```dart
router.route("/heroes/[:id]").link(() => HeroesController(context));
```

## file di migrazione e creazione del db

Una vlta definito il *model* e quindi la connessione possiamo dare
```
conduit db generate
```
Viene creato un file di migrazione, questo ci farà da *blueprint* per il nostro db. Diamo quindi
```
conduit db upgrade --connect postgres://user:pwd@localhost:5432/db-name
```
Oppure, preparando il file `database.yaml` con dentro
```
username: conduit
password: zuccante@2021
host: 127.0.0.1
port: 5432
databaseName: conduit01
```
diamo a terminale
```
conduit db upgrade
```
A riprova entriamo con `psql` (utente del nostro db ecc., vedi sopra)
```
conduit01=> \dt
                 List of relations
 Schema |          Name          | Type  |  Owner  
--------+------------------------+-------+---------
 public | _conduit_version_pgsql | table | conduit
 public | _hero                  | table | conduit
(2 rows)
```
e per conferma
```
conduit01=> \d _hero
                            Table "public._hero"
 Column |  Type  | Collation | Nullable |              Default              
--------+--------+-----------+----------+-----------------------------------
 id     | bigint |           | not null | nextval('_hero_id_seq'::regclass)
 name   | text   |           | not null | 
Indexes:
    "_hero_pkey" PRIMARY KEY, btree (id)
    "_hero_name_key" UNIQUE CONSTRAINT, btree (name)
```
Con `\d+` otterremmo più informazioni!

## Il controller

Resta il *controller*, riscriviamo il `GET`
```dart
class HeroesController extends ResourceController {
  HeroesController(this.context);

  final ManagedContext context;

  @Operation.get('id')
  Future<Response> getHeroByID(@Bind.path('id') int id) async {
    final heroQuery = Query<Hero>(context)..where((h) => h.id).equalTo(id);
    final hero = await heroQuery.fetchOne();
    if (hero == null) {
      return Response.notFound();
    }
    return Response.ok(hero);
  }

  @Operation.get()
  Future<Response> getAllHeroes({@Bind.query('name') String? name}) async {
    final heroQuery = Query<Hero>(context);
    if (name != null) {
      heroQuery.where((h) => h.name).contains(name, caseSensitive: false);
    }
    final heroes = await heroQuery.fetch();

    return Response.ok(heroes);
  }

}
```

## .. inserimento dati

Il `body` si presenta, grazie alla conversione, come mappa o come lista (vedi il classico esempio del client rest): ecco il `POST`
```dart
@Operation.post()
Future<Response> createHero(@Bind.body(ignore: ["id"]) Hero inputHero) async {
  final query = Query<Hero>(context)..values = inputHero;

  final insertedHero = await query.insert();
  return Response.ok(insertedHero);
}
```
La conversione prevede il passaggio dalle *property* della *map* a quelle di `Hero` mediante `@Bind`.
Per il **rest** completo rimandiamo alla documentazione [qui](https://gitbook.theconduit.dev/http).

## .. l'ORM e completamento del REST

Per la documentazione[qui](https://gitbook.theconduit.dev/core_concepts/tour#postgresql-orm), essendo
`conduit`un fork di àqueduct` va bene anche [qui](https://aqueduct.io/docs/db/).
```dart
// POST
@Operation.post()
Future<Response> createHero(@Bind.body(ignore: ["id"]) Hero inputHero) async {
  final query = Query<Hero>(context)..values = inputHero;

  final insertedHero = await query.insert();
  return Response.ok(insertedHero);
}

// PUT
@Operation.put()
Future<Response> updateHeroById(@Bind.body() Hero hero) async {
  final heroQuery = Query<Hero>(context)..where((h) => h.id).equalTo(hero.id);
  final heroFound = await heroQuery.fetchOne();
  // .. no
  if (heroFound == null) {
    return Response.ok({"error": "no_update"});
  }
  // ... yes
  final query = Query<Hero>(context)
    ..values.name = hero.name
    ..where((u) => u.id).equalTo(hero.id);
  await query.update();
  return Response.ok(hero);
}

// DELETE
@Operation.delete()
Future<Response> deleteHeroByName(
    {@Bind.query('name') required String name}) async {
  final query = Query<Hero>(context)
    ..where((h) => h.name).contains(name, caseSensitive: false);
  final hero = await query.fetchOne();
  if (hero == null) {
    // return Response.notFound();
    return Response.ok({"error": "no_deleted"});
  }
  final int? heroesDeleted = await query.delete();
  return Response.ok({"herosesDeleted": heroesDeleted});
}

@Operation.delete('id')
Future<Response> deleteHeroById(@Bind.path('id') int id) async {
  final query = Query<Hero>(context)..where((h) => h.id).equalTo(id);
  final hero = await query.fetchOne();
  if (hero == null) {
    // return Response.notFound();
    return Response.ok({"error": "no_deleted"});
  }
  final int? heroesDeleted = await query.delete();
  return Response.ok({"herosesDeleted": heroesDeleted});
}
```

# REST con curl

Usiamo`curl` ([qui](https://curl.haxx.se/docs/httpscripting.html) un tutorial come tanti): per il `POST` scriviamo
```
curl -X POST -H "Content-Type: application/json" -d '{"name":"alghost"}' localhost:8888/heroes
```
per il `PUT`
```
curl -X PUT -H "Content-Type: application/json" -d '{"id":1, "name":"alghost"}' localhost:8888/heroes
```
per il `DELETE`
```
curl -X DELETE localhost:8888/heroes/1
```