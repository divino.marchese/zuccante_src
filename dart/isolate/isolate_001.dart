import 'dart:async';
import 'dart:isolate';
import 'dart:io';

// run in terminal!

main() async {
  print('type RETURN to stop');
  var receivePort = ReceivePort();
  var isolate = await Isolate.spawn(timer, receivePort.sendPort);
  receivePort.listen((data) => print('recieve: $data'));
  await stdin.first;
  stop(isolate);
  exit(0);
}

// periodic message
void timer(SendPort sendPort) {
  int n = 1;
  Timer.periodic(Duration(seconds: 1), (Timer t) {
    var msg = 'counter is: $n';
    n++;
    sendPort.send(msg);
    if (n == 11) t.cancel();
  });
}

void stop(Isolate isolate) {
  stdout.writeln('killing isolate');
  isolate.kill(priority: Isolate.immediate);
}
