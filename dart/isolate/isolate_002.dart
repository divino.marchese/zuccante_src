import 'dart:async';
import 'dart:isolate';

main() async {
  var receivePort = ReceivePort();
  await Isolate.spawn(echo, receivePort.sendPort);
  // handshake: SendPort as first message (**)
  var sendPort = await receivePort.first;
  // send two messages
  await sendReceive(sendPort, "foo");
  await sendReceive(sendPort, "bar");
}

// the isolate entry point
echo(SendPort sendPort) async {
  var port = new ReceivePort();
  // send SendPort (**)
  sendPort.send(port.sendPort);
  await for (var msg in port) {
    // print(msg);
    String data = msg[0];
    print('from isolate: received ${msg[0]}');
    SendPort replyTo = msg[1];
    print(
        'from isolate: send "${msg[0]}" to port identified by hashCode ${msg[1].hashCode}');
    replyTo.send(data);
    if (data == "bar") port.close();
  }
}

// main sends a message and a SendPort,
// and returns the received message
Future sendReceive(SendPort port, msg) {
  ReceivePort receivePort = ReceivePort();
  port.send([msg, receivePort.sendPort]);
  return receivePort.first;
}
