import 'dart:async';
import 'dart:io';
import 'dart:isolate';

main() {
  ReceivePort receivePort = ReceivePort();
  Isolate.spawn(entryPoint, receivePort.sendPort).then((isolate) {
    print('isolate: $isolate');
    receivePort.listen((message) {
      print('from isolate: ${message}');
    });
  });
}

void entryPoint(SendPort sendPort) {
  int i = 0;
  Timer.periodic(new Duration(seconds: 2), (Timer t) {
    if (i == 5) exit(0);
    i += 1;
    sendPort.send('cazzabubbolo n.${i}');
  });
}
