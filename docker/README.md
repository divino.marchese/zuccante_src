# esercitazioni con docker per il quinto anno

[TOC]

## 240629_postgres

Il file di configurazione per *compose*
```
name: '240629postgres'
services:
  postgres:
    image: 'postgres:latest'
    ports:
      - "5432:5432"
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
```
Per prima cosa lanciamo in modo *detach*
```
docker compose up -d
```
Per i dettagli delle variabili di ambiente si rimanda ad [q]: non specificando il nome del DB esso sarà quello dell'utente `postgres` utente amministratore. Vediamo le immagini scaricate con
```
docker image ls
```
Vediamo i *container* in esecuzione
```
docker ps
```
A questo punto possiamo entrare nella *shell* del *container* (usando l'id del nostro *container*) ed entriamo in PostgreSQL con l'utente `postgres`
```
docker exec -it 917a2db9130c bash
psql -U postgres
```
In alternativa, entrati nel *container* avremmo potuto dare
```
su posttgres
psql
```

**NB** In [1] ci viene detto che l'autenticazione è in *trust mode* pertanto, lavorando nella "macchina", non inseriamo la password. Entrati diamo
```
SELECT current_database();
```
Vedendo che già ci troviamo nel DB `postgres`. Creiamo quindi direttamente una nostra tabella
```
postgres(# user_id SERIAL PRIMARY KEY,
postgres(# username VARCHAR (50) UNIQUE NOT NULL,
postgres(# password VARCHAR (50) NOT NULL,
postgres(# email VARCHAR (255) UNIQUE NOT NULL
postgres(# );
```
Inseriamo un utente
```
insert into accounts(username, password, email) values ('andrea', 'zuccante@2024', 'andrea.morettin@itiszuccante.edu.it');
```
Fatto ciò, dalla nostra macchina ospite facciamo il backup completo
```
docker exec -t 917a2db9130c pg_dumpall -c -U postgres > dump_`date +%Y-%m-%d"_"%H_%M_%S`.sql
```
Finita la nostra sessione di lavoro dopo il backup possiamo dare
```
docker compose down
```
Per ripristinare il backup
```
cat dump_2024-06-29_09_09_31.sql | docker exec -i 72fefec2ecb1 psql -U postgres
```
E verifichiamo dopo essere entrati in `psql` del *container*
```
postgres=# \dt
          List of relations
 Schema |   Name   | Type  |  Owner
--------+----------+-------+----------
 public | accounts | table | postgres
(1 row)
```
diamo
```
selct * from accounts;
        ^
postgres=# select * from accounts;
 user_id | username |   password    |                email 
---------+----------+---------------+-------------------------------------
       1 | andrea   | zuccante@2024 | andrea.morettin@itiszuccante.edu.it
       2 | maria    | maria@2024    | maria23@gmail.com
(2 rows)
```
Di nuovo chiudiamo tutto (ĉontainer* e *network*)
```
docker compose down
```

## 240701_postgres

Un esempio più articolato che usa PgAdmin [2]
```
name: '240701postgres_pgasmin'
services:
  database:
    image: 'postgres:latest'
    ports:
      - 5432:5432
    env_file:
      - .env
    networks:
      - postgres-network
    volumes:
      - ./db-data/:/var/lib/postgresql/data/
      - ./init.sql:/docker-entrypoint-initdb.d/init.sql
  
  pgadmin:
    image: 'dpage/pgadmin4:latest'
    ports:
      - 5433:80
    env_file:
      - .env
    depends_on:
      - database
    networks:
      - postgres-network
    volumes:
      - ./pgadmin-data/:/var/lib/pgadmin/

networks: 
  postgres-network:
    driver: bridge
```

Premesso che il il *network* non è necessario, abbiamo riportato un un file
molto simile alla [fonte](https://github.com/felipewom/docker-compose-postgres). Interessante soprattutto la possibilità di far eseguire un sile SQL di inizializzazione per il *conteiner* PostgreSQL.

## 240703_nginx_php

Questo approccio, vedi [6], usa due immagini e quindi è esoso dal punto di vista dell'uso del dusci, ma semplice e diretto, non richiede la creazione di un file dcoker in cui, su di un'immagine `alpine` (Linux), si installino i pacchetti!. Procediamo per passi
```
name: 240703_nginx_php
services:
  web:
    image: nginx:latest
    ports:
      - '80:80'
    volumes:
      - ./src:/var/www/html
```
diamo `docker compose up -d` e su `localhost` vediamo il server web in esecuzione! 
Terminiamo con `docker compose down`. Procediamo con la prima configurazione del server
```
server {
    index index.php index.html;
    server_name phpfpm.local;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /var/www/html;
}
```
e testiamo il tutto. Terminando completiamo `default.conf` (vedi file). Resta da inserire (con opportuni permessi) `index.php` in `serc/` e provare.

## 340709_shinsenter

Abbiamo trovato un'univa immagine funzionante per PHP e NGinx. Copiato il file `index.php` in `myproject` dare (vedi [9])
```
docker run -d -v ./myproject:/var/www/html -p 80:80 -p 443:443 shinsenter/php:8.3-fpm-nginx
```
immagine molto leggera!

## 240709_mongo

Seguendo [7]
```
docker pull mongodb/mongodb-community-server:latest
```
diamo
```
genji@P330:~$ docker image ls
REPOSITORY                         TAG       IMAGE ID       CREATED        SIZE
mongodb/mongodb-community-server   latest    b5ced38f067f   4 days ago     1.21GB
```
e quindi
```
docker run --name mongodb -p 27017:27017 -d b5ced38f067f
```
entriamo quindi con la Bash nel *container*
```
docker exec -it mongodb bash
```
con `mongosh` entriamo nella shell, `shodbs` ci mostra i DB, `use mydb` permette di usare `mm̀ydb`e come *side effect* crea il database.
```
mydb> db.user.insert({name: "Andrea Morettin", email: "andrea.morettin@itiszuccante.edu.it", age: 53})
DeprecationWarning: Collection.insert() is deprecated. Use insertOne, insertMany, or bulkWrite.
{
  acknowledged: true,
  insertedIds: { '0': ObjectId('668d22322ec2130823149f48') }
}
```
abbiamo creato la *collection* `user`, basta dare `show collections` ed infine
```
mydb> db.user.find()
[
  {
    _id: ObjectId('668d22322ec2130823149f48'),
    name: 'Andrea Morettin',
    email: 'andrea.morettin@itiszuccante.edu.it',
    age: 53
  }
]
```
Ora archiviamo il nostro DB con un *dump*
```
docker exec mongodb sh -c 'exec mongodump --db mydb --archive' > mydb.dump
```
per il recupero (eliminato il *container*)
```
docker exec -i mongodb sh -c 'mongorestore --archive' < mydb.dump
```

## 241010_mariadb

```
name: '241010mariadb'
services:
  mariadb:
    container_name: mariadb
    image: mariadb:latest
    ports:
      - "3306:3306"
    environment:
      MARIADB_ROOT_PASSWORD: zuccante
```
possiamo quindi entrare con `docker exec -it mariadb bash`, `mysql` (client testuale)
non è installato, ma abbiamo
```
mariadb -u root -p
```
a questo punto possiamo creare un utente ed un db e rifare la connessione oppure usare
un client grafico tipo BeeKeeper (community) [qui](https://www.beekeeperstudio.io/). 

Per il *backup*, vedi documentazione [qui](https://mariadb.com/kb/en/mariadb-dump/)
```
docker exec <container> mariadb-dump -u root -p password <db> > backup-$(date +%Y%m%d).sql
```
il *restore*
```
docker exec <container> mariadb -u root  -p zuccante < mysql-backup.sql
```
Anche una cosa simile (tratto da [10])
```
version: '3.1'

services:

  db:
    image: mariadb
    restart: always
    environment:
      MARIADB_ROOT_PASSWORD: example

  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080

  ```
  `docker compose down` per uscire.

## 241118_TPSIT

Questa è una prima versione di test per i miei studenti di 3ID. Creeremo un `Dockerfile`.

**NB&** Se si ha VPN potrebbe rallentare il tutto!

Creiamo il file `Dockerfile` col segunete contenuto
```
# pull base image.
FROM ubuntu:latest

# install package
RUN apt update 
RUN apt install -y git
```
a questo punto prepariamo l'immagine
```
docker build --network host -t ubuntu/tpsit .
```
dando `docker image ls` vedremo la nostra immagine! Ora si tratta di entrare in `bash` come amministratore
```
docker run -it ubuntu/tpsit /bin/bash
```
verrà creato un container (`run`) e vi entriamo con `bash`. Il pacchetto `git` è già installato. Dando
```
docker ps -a
```
vediamo i *container* fermati (*stopped*). SOlo quando fermi i *container* possono essere rimossi con
```
docker container rm 78c1c89966e0
```
le immagini si rimuovono più agevolmente col loro come o con l'ID dando `docker image rm`.
 

## materiali

[1] "Immagini ufficiali di PostgreSQL" [qui](https://hub.docker.com/_/postgres/).  
[2] "pgadmin immagini" [qui](https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html).  
[3] "Docker compose Cli" [qui](https://docs.docker.com/compose/reference/).  
[4] "Docker compose postgres" un esempio [qui](https://github.com/felipewom/docker-compose-postgres).  
[5] "nginx offidial docker images" [qui](https://hub.docker.com/_/nginx/).  
[6] "Dockerize your PHP application with Nginx and PHP8-FPM" un articolo [qui](https://marc.it/dockerize-application-with-nginx-and-php8/).  
[7] "Install MongoDB Community with Docker" [qui](https://www.mongodb.com/docs/manual/tutorial/install-mongodb-community-with-docker/).  
[8] "mongoDB official DOcker images" [qui](https://hub.docker.com/_/mongo/).  
[9] "shinsenter PHP image" [qui](https://hub.docker.com/r/shinsenter/php).  
[10] "mariadb official images" [qui](https://hub.docker.com/_/mariadb).
