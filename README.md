## src per ITIS Zuccante

Qui sono contenuti guide e materiali per i miei anni di insegnamento all'ITIS Zuccante: il materiale è in costante aggiornamento.
- Progreammazioni e materiali organizzati in classi e materie [qui](https://gitlab.com/divino.marchese/zuccante_src/-/wikis/home).
- Appunti su GIT [qui](https://gitlab.com/divino.marchese/zuccante_src/-/wikis/git/home).
- Appunti su Dart [qui](https://gitlab.com/divino.marchese/zuccante_src/-/wikis/dart/home).
- Appunti su NodeJs [qui](https://gitlab.com/divino.marchese/zuccante_src/-/wikis/node).
- Appunti su PostgreSQL [qui](https://gitlab.com/divino.marchese/zuccante_src/-/wikis/PostgreSQL).
