import java.awt.*;
import java.awt.event.*;

public class AWT10 extends Frame {
    public AWT10(String title) {
        super(title);
        setLocation(400, 200);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbcnt = new GridBagConstraints();
        setLayout(gbl);

        gbcnt.fill = GridBagConstraints.HORIZONTAL;
        
        gbcnt.gridx = 0;
        gbcnt.gridy = 0;
        add(new Button("Linux"), gbcnt);

        gbcnt.gridx = 1;
        gbcnt.gridy = 0;
        add(new Button("Windows"), gbcnt);
        
        gbcnt.ipady = 20;
        gbcnt.gridx = 0;
        gbcnt.gridy = 1;
        add(new Button("UNIX"), gbcnt);
        
        gbcnt.gridx = 1;
        gbcnt.gridy = 1;
        add(new Button("DOS"), gbcnt);
        
        gbcnt.gridx = 0;
        gbcnt.gridy = 2;
        gbcnt.gridwidth = 2;
        add(new Button("Operation System"), gbcnt);
        
        pack();
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        }); 
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new AWT10("AWT10"));
    }
}
