import java.awt.Frame;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AWT03 extends Frame {

    public AWT03(String title) {
        super(title);
        setSize(500,500);
        Label lab1,lab2;  
        lab1 = new Label("Welcome to studytonight.com");  
        lab1.setBounds(50,50,200,30); 
        lab2 = new Label("This Tutorial is of Java");  
        lab2.setBounds(50,100,200,30);  
        add(lab1); 
        add(lab2);  
        // absolute positioning
        setLayout(null);  
        setVisible(true);  
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> (new AWT03("AWT03")).setVisible(true));
    }
}
