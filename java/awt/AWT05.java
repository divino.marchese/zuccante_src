import java.awt.*;
import java.awt.event.*;

public class AWT05 extends Frame {
    public AWT05(String title) {
        super(title);
        setSize(400, 400);
        Panel panel = new Panel();
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(new Color(226, 211, 70));
        Button btn1 = new Button("On");
        btn1.setBounds(50, 100, 80, 30);
        btn1.setBackground(new Color(229, 103, 78));
        Button btn2 = new Button("Off");
        btn2.setBounds(100, 100, 80, 30);
        btn2.setBackground(new Color(78, 173, 41));
        panel.add(btn1);
        panel.add(btn2);
        add(panel);
        // absolute positioning
        setLayout(null);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> (new AWT05("AWT05")).setVisible(true));
    }
}
