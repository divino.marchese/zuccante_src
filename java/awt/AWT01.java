import java.awt.Frame;
import java.awt.EventQueue;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AWT01 extends Frame {

    public AWT01(String title) {
        super(title);
        setSize(300, 300);
        setBackground(new Color(192, 83, 199));
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
        setLocationByPlatform(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            System.out.println("run windows 1");
            (new AWT01("AWT01: 1")).setVisible(true);
            System.out.println("run windows 1");
            (new AWT01("AWT01: 2")).setVisible(true);
        });
    }
}
