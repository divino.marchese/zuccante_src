# AWT puro

## AWT06 -- BordeLayout

## AWT07 -- BlockLayout da Swing

## AWT08 -- FlowLayout

## AWT08 -- GridBagLayout


## Riferimenti

[] API [qui](https://docs.oracle.com/javase/8/docs/api/overview-summary.html)
.[] `BorderLayout` [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/border.html).
[] `BlockLayout` da `swing` [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/box.html).
[] `FlowLayout` [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/flow.html).
[] `GridLayout` [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/card.html).
[] `GridBagLayout` [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/gridbag.html).
