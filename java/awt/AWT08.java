import java.awt.*;
import java.awt.event.*;

public class AWT08 extends Frame {

    public AWT08(String title) {
        super(title);
        setSize(200, 35);
        setLayout(new FlowLayout());
        Button btn = new Button("one");
        add(btn);
        btn = new Button("two");
        add(btn);
        btn = new Button("three");
        add(btn);
        btn = new Button("four");
        add(btn);
        // pack();
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });   
    }
    
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new AWT08("AWT08"));
    }
}
