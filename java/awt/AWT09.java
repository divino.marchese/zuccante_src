import java.awt.*;
import java.awt.event.*;

public class AWT09 extends Frame {

    public AWT09(String title) {
        super(title);
        setSize(200, 200);
        setLayout(new GridLayout(3, 2));
        add(new Button("Button A"));
        add(new Button("Button B"));
        add(new Button("Button C"));
        add(new Button("Button D"));
        add(new Button("Button E"));
        add(new Button("Button F"));
        // NO: frame.pack();
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });  
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new AWT09("AWT09"));
    }

}
