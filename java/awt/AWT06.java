import java.awt.*;
import java.awt.event.*;

public class AWT06 extends Frame {
    public AWT06(String title) {
        super(title);
        Button button = new Button("Button 1 (PAGE_START)");
        add(button, BorderLayout.PAGE_START);
        button = new Button("Button 2 (CENTER)");
        button.setPreferredSize(new Dimension(200, 100));
        add(button, BorderLayout.CENTER);
        button = new Button("Button 3 (LINE_START)");
        add(button, BorderLayout.LINE_START);
        button = new Button("Long-Named Button 4 (PAGE_END)");
        add(button, BorderLayout.PAGE_END);
        button = new Button("5 (LINE_END)");
        add(button, BorderLayout.LINE_END);
        // size from content
        pack();
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new AWT06("AWT06"));
    }

}
