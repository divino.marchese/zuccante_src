import java.awt.*;
import java.awt.event.*;

public class AWT11 extends Frame implements ActionListener {

    private String text;
    private Label lName = new Label("name");
    private TextField tName = new TextField("my name");
    private Label lSurname = new Label("surname");
    private TextField tSurname = new TextField("my surname");
    private Checkbox male = new Checkbox("male");
    private Button sub = new Button("submit");
    private TextArea area = new TextArea();
    // private Font font =new Font("Clear Sans", Font.PLAIN, 13);
    private Font font = new Font("Comic Neue", Font.PLAIN, 13);


    public AWT11(String title) {
        super(title);
        setSize(200, 200);
        setResizable(false);

        lName.setBounds(10, 10, 60, 20);
        lName.setFont(font);
        add(lName);

        tName.setBounds(70, 10, 100, 20);
        tName.setFont(font);
        add(tName);

        lSurname.setBounds(10, 30, 60, 20);
        lSurname.setFont(font);
        add(lSurname);

        tSurname.setBounds(70, 30, 100, 20);
        tSurname.setFont(font);
        add(tSurname);

        male.setBounds(10, 60, 70, 20);
        male.setState(true);
        add(male);

        sub.setBounds(10, 90, 50, 20);
        sub.setFont(font);
        sub.addActionListener(this);
        add(sub);

        area.setBounds(10, 120, 180, 70);
        area.setFont(font);
        area.setEditable(false);
        add(area);

        // absolute positioning
        setLayout(null);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        }); 
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == sub) {
            // tName.setEditable(false);
            // tSurname.setEditable(false);
            System.out.println("submit");
            if(male.getState()) {
                text = "";
                text += "Mr\n";
            } else {
                text += "Mrs\n";
            }
            text += "name: " + tName.getText();
            text += "\n";
            text += "surname: " + tSurname.getText();
            area.setText(text);
        }

    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new AWT11("AWT11"));
    }
}