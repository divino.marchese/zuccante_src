import java.awt.Frame;
import java.awt.Label;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AWT02 extends Frame {
    public AWT02(String title) {
        super(title);
        setSize(300, 100);
        Label label = new Label("Hello World");
        label.setAlignment(Label.CENTER); 
        add(label);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
        setLocationByPlatform(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            System.out.println("AWT02");
            (new AWT02("AWT02")).setVisible(true);
        });
    }

    
}