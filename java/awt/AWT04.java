import java.awt.*;
import java.awt.event.*;

public class AWT04 extends Frame {
    public AWT04(String title) {
        super(title);
        setSize(300, 70);
        Button btn = new Button("Hello World");
        // use setPreferred size when a parent layout exists
        // btn.setPreferredSize(new Dimension(200, 50));
        Label label = new Label("Hello World", Label.CENTER);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("click");
                label.setText("click");
            }
        });
        add(btn, BorderLayout.PAGE_START);
        add(label, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> (new AWT04("AWT04")).setVisible(true));
    }

}
