import java.awt.*;
import java.awt.event.*;

import javax.swing.BoxLayout;

public class AWT07 extends Frame {
    public AWT07(String title) {
        super(title);
        setSize(300, 300);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        Button button = new Button("one");
        add(button);
        button = new Button("two");
        add(button);
        button = new Button("three");
        add(button);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new AWT07("AWT07"));
    }

}
