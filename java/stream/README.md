# I/O Stream in Java

Il materiale qui proposto è sempre in costante aggiornamento.

## Stream a caratteri, stream a btte e buffered stream

In questo repository sono contenuti i sorgenti per le lezioni e le esercitazioni nella programmazione con gli Stream
, programmazione di tipo ""sincrono".
```
CopyBytes.java
CopyCharacters.java
```
In questi due primi esempi vediamo come copiare byte o caratteri da file a file.
In particolare si consideri l'esempio, vediamo qui un **filter stream**
```
CopyCharactersBuffered.java
```
e l'uso del metodo flush. Si vada a leggere la documentazione per l'interfaccia `AutoCloseable` (Java 8) e pure l'interfaccia `Closeable`.
```
Copy.java
PipeExample.java
```
si muovono sulla stessa lunghezza d'onda.
```
TestFlushAgain.java
```
un altro esempio con `flush()`.

## Criptografia

Dalla rete, riadattando e commentando, criptiamo e decriptiamo usando AES criptografia simmetrica ove usiamo la stessa chiave per criptare e decriptare)
```
CryptoException.java
CryptoUtils.java
CryptoUtilsTest.java
```

## Guide ed approfondimenti

[1] [Java I/O tutorial](http://tutorials.jenkov.com/java-io/index.html) di J.Jenkov.  
[2] [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) da Wikipedia(en).
