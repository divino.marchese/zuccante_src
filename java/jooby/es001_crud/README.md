# es001_crud

Welcome to es001_crud!!

## running and building

Dalla documentazione ufficiale con  `./gradlew joobyRun`.

## DAO

Un *singleton* per manipolare la base di dati.

## `App.java`

Abbiamo usato la sintassi che fa uso della *anonymous inner class*.

## `GET`

Il secondo lo usiamo dopo aver inserito i `3` utenti.
```bash
curl localhost:8080/users
curl localhost:8080/users/1
```

## `POST`

Inseriamo `3` utenti
```bash
curl -X POST -d '{"id":0, "name":"toni bueghin", "email":"toni@gmail.com"}' -H 'Content-type: application/json' localhost:8080/users
curl -X POST -d '{"id":0, "name":"maria svampia", "email":"tmary@gmail.com"}' -H 'Content-type: application/json' localhost:8080/users
curl -X POST -d '{"id":0, "name":"berto suppia", "email":"bertos@gmail.com"}' -H 'Content-type: application/json' localhost:8080/users
```

## `PUT`

Aggiorniamo.
```bash
curl -X PUT -d '{"id":0, "name":"maria svampia", "email":"tmary.svamp@gmail.com"}' -H 'Content-type: application/json' localhost:8080/users/2
```

## `DELETE`

```bash
curl -X DELETE localhost:8080/users/1
```
## riferimenti

[1] "jooby, pagina ufficiale" [qui](https://jooby.io).  
[2] "curl man page" [qui](https://curl.se/docs/manpage.html).





