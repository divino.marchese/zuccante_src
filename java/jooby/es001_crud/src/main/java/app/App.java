package app;

import com.google.gson.Gson;
import io.jooby.Jooby;
import io.jooby.MediaType;
import io.jooby.netty.NettyServer;

import static app.DAO.dao;

public class App extends Jooby {

    {
        install(new NettyServer());
        get("/", ctx -> "Welcome to Jooby!");

        // get all users
        get("/users", ctx -> {
            ctx.setResponseHeader("Content-type", "application/json");
            Gson gson = new Gson();
            return gson.toJson(dao.getAllUsers());
        });

        // get user by id
        get("/users/{id:[0-9]+}", ctx -> {
            ctx.setResponseHeader("Content-type", "application/json");
            int id = Integer.parseInt(ctx.path("id").value());
            User user = dao.findUserById(id);
            Gson gson = new Gson();
            if(user != null) {
                return gson.toJson(user);
            } else {
                return gson.toJson(dao.getAllUsers());
            }
        });

        // add user
        post("/users", ctx -> {
            ctx.setResponseHeader("Content-type", "application/json");
            Gson gson = new Gson();
            if(ctx.getRequestType() != MediaType.json) {
                return gson.toJson(dao.getAllUsers());
            }
            String body = ctx.body().value();
            User user = gson.fromJson(body, User.class);
            dao.addUser(user.getName(), user.getEmail());
            return gson.toJson(dao.getAllUsers());
        });

        // update user
        put("/users/{id:[0-9]+}", ctx -> {
            ctx.setResponseHeader("Content-type", "application/json");
            Gson gson = new Gson();
            if(ctx.getRequestType() != MediaType.json) {
                return gson.toJson(dao.getAllUsers());
            }
            int id = Integer.parseInt(ctx.path("id").value());
            String body = ctx.body().value();
            User user = gson.fromJson(body, User.class);
            dao.updateUserById(id, user.getName(), user.getEmail());
            return gson.toJson(dao.getAllUsers());
        });

        // delete user
        delete("/users/{id:[0-9]+}", ctx -> {
            ctx.setResponseHeader("Content-type", "application/json");
            int id = Integer.parseInt(ctx.path("id").value());
            dao.deleteUserById(id);
            Gson gson = new Gson();
            return gson.toJson(dao.getAllUsers());
        });
    }

    public static void main(final String[] args) {
        runApp(args, App::new);
    }

}
