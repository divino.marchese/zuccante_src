package app;

import java.util.ArrayList;
import java.util.List;

public class DAO {

    public static DAO dao = new DAO();

    private static List<User> users = new ArrayList<>();

    private DAO(){}

    public List<User> getAllUsers() {
        return users;
    }

    public void addUser(String name, String email) {
        User user = new User(name, email);
        users.add(user);
    }

    public User findUserById(int id) {
        return users.stream().
                filter(user -> user.getId() == id).
                findFirst().orElse(null);
    }

    public void updateUserById(int id, String name, String email) {
        User user = findUserById(id);
        if(user != null) {
            user.setName(name);
            user.setEmail(email);
        }
    }

    public void deleteUserById(int id) {
        User user = findUserById(id);
        if(user != null) {
            users.remove(user);
        }
    }


}
