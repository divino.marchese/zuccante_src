# es000_starting_rest

Welcome to es000_starting_rest!!

## run & build

Documentazione già presente di *default*
```
./gradlew joobyRun
./gradlew build
```

## creazione del progetto

Abbiamo creato un progetto Gradle dopo aver installato la `cli`
```
jooby> create --gradle es000_starting_rest
Try it! Open a terminal and type: 
  cd /home/genji/SCUOLA/2024-2025_zuccante/materiali/java/jooby/es000_starting_rest
  ./gradlew joobyRun
```
Già possiamo testare la nostra applicazione.

## hot reload

Possiamo sistemare Gradle (file `build.gradle`)
```groovy
joobyRun {
  mainClass = "app.App"
  restartExtensions = ["conf", "properties", "class"]
  compileExtensions = ["java"]
  port = 8080
}
```
eseguendo questo *task* abbiamo l'*hor reload* (vedi sopra)!

# Gson

Nota libreria di Google, vedi [2] in `build.gradle` aggiungiamo
```groovy
dependencies {
    ...
    implementation 'com.google.code.gson:gson:2.11.0'
}
```

## materiali 

[1] "jooby" pagina uffuiciale [qui](https://jooby.io/).  
[2] "Gson" libreria di Google [qui](https://google.github.io/gson/).



    

