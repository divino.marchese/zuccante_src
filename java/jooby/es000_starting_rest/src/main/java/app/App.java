package app;

import com.google.gson.Gson;

import java.util.List;

import static app.MyObject.getObject;
import static app.MyObject.getObjectList;
import static io.jooby.Jooby.runApp;

public class App {
    public static void main(String[] args) {
        runApp(args, app -> {
            final Gson gson = new Gson();
            // helo world
            app.get("/", ctx -> "Hello World");
            // return object
            app.get("/object", ctx -> {
                ctx.setResponseHeader("Content-type", "application/json");
                return gson.toJson(getObject());
            });
            // return list
            app.get("/list", ctx -> {
                ctx.setResponseHeader("Content-type", "application/json");
                return gson.toJson(getObjectList());
            });
            // variable
            app.get("/list/{id}", ctx -> {
                ctx.setResponseHeader("Content-type", "application/json");
                int id = ctx.path("id").intValue();
                List<MyObject> list = getObjectList();
                if(id > list.toArray().length)
                    return "{\"msg\" : \"error\"}";
                return gson.toJson(list.get(id));
            });
        });
    }
}
