package app;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MyObject {

    private static final Gson gson = new Gson();

    private String s;
    private int n;
    private float f;
    private boolean b;

    public static MyObject getObject() {
        MyObject obj = new MyObject("ciao", 666, 0.2f, false);
        return obj;
    }

    public static List<MyObject> getObjectList() {
        List<MyObject> list = new ArrayList<>();
        list.add(new MyObject("miao", 13, 0.3f, true));
        list.add(new MyObject("mbau", 666, 0.6f, false));
        list.add(new MyObject("cip", 17, 0.9f, true));
        list.add(new MyObject("muu", 77, 1.2f, false));
        return list;
    }

    public MyObject(String s, int n, float f, boolean b) {
        this.s = s;
        this.n = n;
        this.f = f;
        this.b = b;
    }

}
