public class Card {

    private char suit;
    private int value;

    public Card(char s, int v) {
        this.suit = s;
        this.value = v;
    }
}
