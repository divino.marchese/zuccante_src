import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FileClock implements Runnable {
    
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.printf("%s\n", new Date());
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                Thread current = Thread.currentThread();
                System.out.printf("%s is interrupted\n", current.getName());
                System.out.printf("%s status: %s\n", current.getName(), current.getState());
            }
        }
    }
}
