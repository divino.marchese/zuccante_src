# Thread

Imparare i thread leggendo i sorgenti, ecco la proposta per un percorso. Invitiamo ad una lettura attenta delle [api](https://docs.oracle.com/en/java/javase/22/docs/api/java.base/java/lang/Thread.html).

## Primi esempi

*Task*, `Runnable` e `Thread`.
```
HelloThread
HelloRunnable
HelloRunnable2
```
nell'ultimo esempio usiamo la sintassi *lambda*; per un'introduzione a tale sintassi rimandiamo, ad esempio, al [tutorial](http://tutorials.jenkov.com/java/lambda-expressions.html) di Jenkov.

## Running threads

più thread che corrono ...
```
PingPong
PingPong2
RunPingPong
```
## `sleep` (e interruzioni) e stati di un `Thread`
```
FileClock
TestFileClock
TestCalculator
```
Invitiamo a leggere le [api](https://docs.oracle.com/en/java/javase/22/docs/api/java.base/java/lang/Thread.State.html) dell'enumerazione `Yhread.State`. 

**NB** Si osservi con attenzione come il metodo `interrupt()` in `TestFileClock` setti (presenti) l'*interrupted status* ma non fermi il *thread* in modo perentorio; sempre in tale esempio utile è seguire il suggerimento che prevede il commentare dell'ultimo `try ... catch ...`.

## `join`
```
TestDataDownload
TestJoin
TestJoinAgain
SimpleThreads
```
Molto interessante l'ultimo esempio in cui `interrupt()` e `join()` operano assieme.


## gruppio di thread

```
TestSearchTask
```
## Semafori e Lock 

Semafori e *lock* per gestire una o più risorse non condivisibili.
```
TestPrintQueue
TestPrintQueueAgain
```
- ... fra cui il caso del **producers and consumers**, nell'esempio `TestProducerConsumer` vengono usati 3 semafori: 2 per accedere alla risorsa ed uno per la **sezione critica**; nel secondo esempio `TestProducerConsumer2` si usa un **lock** per gestire la sezione critica;  `
```
TestProducerConsumer
TestProducerConsumer2  
```
- Il caso del **readers and writers** realizzato con gli appositi lock; nel secondo esempio usiamo il semaforo
```
ThreadSafeArrayList<E>
ReadersAndWritersProblem
```
- Sincronizzazione avanzata fra thread
```
TestVidecongerences
TestWorker
TestCounting
TestPhaser01: phaser as a sort of CyclicBarrier
TestPhaser02
TestPhaser03: override onAdvance()
TestPhaser05: anothe example with onAdvance()
TestPhaser04: bulkRegister()
```
- Exchanger
```
TestProducerConsumer4
```
- block synchronizing
```
TestCinema
```
- methods synchronizing
vedi java tutorial
- Callable
```
TestCallable01.java
```
- executors
```
Executor01.java
Executor02.java
Executor03.java
```

## Materiali

[1] Java Lambda Expressions, J.Jenkov: [qui](http://tutorials.jenkov.com/java/lambda-expressions.html).  
[2] Concorrenza in java 9, un tutorial: [qui](http://winterbe.com/posts/2015/04/07/java8-concurrency-tutorial-thread-executor-examples/).  
[3] **critical section**: da Wikipedia(en) [qui](https://en.wikipedia.org/wiki/Critical_section).  
[3] La gestione della memoria in Java: [qui](http://tutorials.jenkov.com/java-concurrency/java-memory-model.html).
