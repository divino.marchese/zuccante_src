import java.util.concurrent.TimeUnit;

public class TestFileClock {
    public static void main(String[] args) {
        FileClock clock = new FileClock();
        Thread main = Thread.currentThread();
        Thread thread = new Thread(clock);
        thread.setName("fileClock");
        thread.start();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread.interrupt();
        System.out.printf("after interrupt %s has status: %s\n", thread.getName(), thread.getState());
        // comment try .. catch ..
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("%s has status: %s\n", main.getName(), main.getState());
        System.out.printf("%s has status: %s\n", thread.getName(), thread.getState());
    }
}
