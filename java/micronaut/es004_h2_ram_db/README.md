# es004 h2 ram db

Tratto dalla guida [qui](https://www.knowledgefactory.net/2021/10/build-rest-crud-apis-with-micronaut-hibernate.html). 
Prepariamo il progetto con [launch](https://micronaut.io/launch/)
- hibernate-jpa
- data-jpa
- validation
- yaml

## configurazione

In automatico la configurazione viene proposta, per esaminare le variabili di Hibernate 
```yaml
hbm2ddl:
  auto: update
```
aggiorna le tabelle (schema) confrontandolo col vecchio. La configurazione di Hibernate mediante *property* 
è spiegata [qui](https://docs.jboss.org/hibernate/orm/6.4/javadocs/org/hibernate/cfg/SchemaToolingSettings.html#HBM2DDL_AUTO).
Mentre
```yaml
show_sql: true
```
come spiegato [qui](https://docs.jboss.org/hibernate/orm/6.4/javadocs/org/hibernate/cfg/JdbcSettings.html#SHOW_SQL) abilita il
su console il *logging*.

## il repository

Il repository che noi andiamo a creare non fa uso di repository prefabbricati che poi, nei prossimi esempi, useremo.
Questo *bean* è un *singleton*! La documentazione di riferimento è [qui](https://micronaut-projects.github.io/micronaut-data/latest/guide/).
Per comprendere il senso delle *annotation* si va [qui](https://micronaut-projects.github.io/micronaut-data/latest/api/io/micronaut/transaction/annotation/package-summary.html). API `EntityManager` [qui](https://jakartaee.github.io/persistence/latest-nightly/api/jakarta.persistence/jakarta/persistence/EntityManager.html)
Troviamo `<T> T find(Class<T> entityClass, Object primaryKey)`, `void persist(Object entity)` ch crea
una nuova entità in modo persistente (cioè nel DB) e `void remove(Object entity)`. L'aggiornamento viene fatta con transazione
(operazione non interrompibile). Usiamo per la *query* in JPQL il cui senso è facilmente leggibile
```
SELECT u FROM User u
```
lavorando con le *entity* scriviamo `User u`. Ma a parte il caso anomalo che potremmo anche scrivere usando `SQL`
abituale (vedi commento), le *query* le scriviamo seguendo, ad esempio, quanto [qui](https://micronaut-projects.github.io/micronaut-data/latest/guide/#querying)
indicatoci usando il *method pattern*. Abbiamo anche `Query createQuery(String qlString)` che ci ritorna un oggetto `Query`
API [qui](https://jakartaee.github.io/persistence/latest-nightly/api/jakarta.persistence/jakarta/persistence/Query.html). Ecco due modi alternativi
```java
return entityManager.createQuery("SELECT c FROM User c", User.class).
    getResultList();
```
ovvero
```java
return entityManager.createNativeQuery("SELECT * FROM users", User.class).
    getResultList();
```


## controller

Leggiamo con attenzione la seguente documentazione [qui](https://docs.micronaut.io/latest/guide/#reactiveServer). Netty lavora su di un *event loop*, ora
è bene separare le azioni sincrone di interazione col DB, ad esempio tramite ORM, dagli eventi costituiti dalle richieste al server. Nel nostro caso potremmo spostare il tutto su di un *fixed thread pool* configurando come segue
```yaml
micronaut:
  executors:
    io:
      type: fixed
      nThreads: 75
```
`IO` è il nome del *pool*. Noi usiamo
```java
@ExecuteOn(TaskExecutors.BLOCKING)
```
indicando l'*executor* usato abitualmente per i *task* bloccanti (seguendo altre guide ufficiali)! Altro si può leggere [qui](https://docs.micronaut.io/latest/guide/index.html#threadPools).

LA *injection* del repository viene realizzata mediante costruttore
```java
public UserController(UserRepository userRepository) {
    this.userRepository = userRepository;
}
```

## test con `curl`

`GET`  
```
curl localhost:8080/users/
curl localhost:8080/users/1
```

`POST`  
```
curl -H 'Content-Type: application/json' -d '{ "firstName": "Tony", "lastName": "Bueghin", "email": "tony.bueghin@gmail.com" }' localhost:8080/users
curl -H 'Content-Type: application/json' -d '{ "firstName": "Mario", "lastName": "Bossa", "email": "mario.bossa@gmail.com" }' localhost:8080/users
```
`DELETE`  
```
curl -X DELETE localhost:8080/users/2
```

`PUT`  
```
curl -X PUT -H 'Content-Type: application/json' -d '{ "firstName": "Tony", "lastName": "Bueghin", "email": "tony.bueghin2@gmail.com" }' localhost:8080/users/1
```



