plugins {
    id("com.github.johnrengelman.shadow") version "8.1.1"
    id("io.micronaut.application") version "4.2.0"
    id("io.micronaut.aot") version "4.2.0"
}

version = "0.1"
group = "com.example"

repositories {
    mavenCentral()
}

dependencies {
    annotationProcessor("io.micronaut.data:micronaut-data-processor:3.9.6")
    annotationProcessor("io.micronaut:micronaut-http-validation:3.9.2")
    annotationProcessor("io.micronaut.serde:micronaut-serde-processor:1.5.2")
    annotationProcessor("io.micronaut.validation:micronaut-validation-processor")
    implementation("io.micronaut.data:micronaut-data-hibernate-jpa:3.9.6")
    implementation("io.micronaut.data:micronaut-data-tx-hibernate:3.9.6")
    implementation("io.micronaut.serde:micronaut-serde-jackson:1.5.2")
    implementation("io.micronaut.sql:micronaut-hibernate-jpa:4.8.1")
    implementation("io.micronaut.sql:micronaut-jdbc-hikari:4.8.0")
    implementation("io.micronaut.validation:micronaut-validation")
    implementation("jakarta.validation:jakarta.validation-api:3.0.2")
    compileOnly("io.micronaut:micronaut-http-client:3.8.7")
    runtimeOnly("ch.qos.logback:logback-classic:1.4.7")
    runtimeOnly("com.h2database:h2:2.1.214")
    runtimeOnly("org.yaml:snakeyaml:2.0")
    testImplementation("io.micronaut:micronaut-http-client:3.8.7")
}


application {
    mainClass.set("com.example.Application")
}
java {
    sourceCompatibility = JavaVersion.toVersion("17")
    targetCompatibility = JavaVersion.toVersion("17")
}


graalvmNative.toolchainDetection.set(false)
micronaut {
    runtime("netty")
    testRuntime("junit5")
    processing {
        incremental(true)
        annotations("com.example.*")
    }
    aot {
    // Please review carefully the optimizations enabled below
    // Check https://micronaut-projects.github.io/micronaut-aot/latest/guide/ for more details
        optimizeServiceLoading.set(false)
        convertYamlToJava.set(false)
        precomputeOperations.set(true)
        cacheEnvironment.set(true)
        optimizeClassLoading.set(true)
        deduceEnvironment.set(true)
        optimizeNetty.set(true)
    }
}



