package com.example;

import com.example.model.User;
import io.micronaut.http.annotation.*;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import java.util.List;
import java.util.Optional;

@ExecuteOn(TaskExecutors.BLOCKING)
@Controller("/users")
public class UserController {

    protected final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Get
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Get("/{id}")
    public Optional<User> getUser(Long id) {
        return userRepository.findById(id);
    }

    @Put
    public void updateUser(@Body User user) {
        userRepository.update(user);
    }

    @Put("/{id}")
    public int updateUserById(Long id, @Body User user) {
        return userRepository.updateById(id, user.getFirstName(), user.getFirstName(), user.getEmail());
    }

    @Post
    public User addUser(@Body User user) {
        return userRepository.save(user);
    }

    @Delete("/{id}")
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }


}
