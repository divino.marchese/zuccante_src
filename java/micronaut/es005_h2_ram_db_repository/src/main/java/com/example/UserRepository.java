package com.example;

import com.example.model.User;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("FROM User u WHERE u.firstName = :name")
    List<User> findAllHavingName(String name);

    @Query("UPDATE User u SET u.firstName = :firstName, u.lastName = :lastName, u.email = :email WHERE u.id = :id")
    int updateById(Long id, String firstName, String lastName, String email);
}
