# es005 h2 ram db repository

Tratto dalla guida [qui](https://www.knowledgefactory.net/2021/10/build-rest-crud-apis-with-micronaut-hibernate.html).
Prepariamo il progetto con [launch](https://micronaut.io/launch/)
- hibernate-jpa
- data-jpa
- validation
- yaml

## configurazione

In automatico la configurazione viene proposta, per esaminare le variabili di Hibernate
```yaml
hbm2ddl:
  auto: update
```
aggiorna le tabelle (schema) confrontandolo col vecchio. La configurazione di Hibernate mediante *property*
è spiegata [qui](https://docs.jboss.org/hibernate/orm/6.4/javadocs/org/hibernate/cfg/SchemaToolingSettings.html#HBM2DDL_AUTO).
Mentre
```yaml
show_sql: true
```
come spiegato [qui](https://docs.jboss.org/hibernate/orm/6.4/javadocs/org/hibernate/cfg/JdbcSettings.html#SHOW_SQL) abilita il
su console il *logging*.

## repository

Qui usiamo un *CRUD repository*, [API](https://micronaut-projects.github.io/micronaut-data/latest/api/io/micronaut/data/repository/CrudRepository.html)
già contiene una serie di metodi 

Aggiungiamo una *query* usando l'*annotation* `@Query` ed usando `JPQL` 
```java
@Query("FROM User u WHERE u.firstName = :name")
List<User> findAllHavingName(String name);

@Query("UPDATE User u SET u.firstName = :firstName, u.lastName = :lastName, u.email = :email WHERE u.id = :id")
int updateById(Long id, String firstName, String lastName, String email);
```
Purtroppo `int` è "quanto ci passa il convento", altrimenti si procede come nel precedente esempio. Una aggiornamento più articolato richiede l'uso di una *transaction*.
Non possiamo passare *named paramether* che siano oggetti!

## test con `curl`

`GET`
```
curl localhost:8080/users/
curl localhost:8080/users/1
```

`POST`
```
curl -H 'Content-Type: application/json' -d '{ "firstName": "Tony", "lastName": "Bueghin", "email": "tony.bueghin@gmail.com" }' localhost:8080/users
curl -H 'Content-Type: application/json' -d '{ "firstName": "Mario", "lastName": "Bossa", "email": "mario.bossa@gmail.com" }' localhost:8080/users
```
`DELETE`
```
curl -X DELETE localhost:8080/users/2
```

`PUT`
```
curl -X PUT -H 'Content-Type: application/json' -d '{ "firstName": "Tony", "lastName": "Bueghin", "email": "tony.bueghin2@gmail.com" }' localhost:8080/users/1
```



