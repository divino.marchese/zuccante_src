# es001 starting

Per creare il progetto usiamo il noto tool online [qui](https://micronaut.io/launch) includendo la *feature* di `yaml` in modo da poter
scrivere il file `application.yml` (un file `yaml`)
```yaml
# yml configuration
micronaut:
  application:
    name: es001_starting
  server:
    port: 3000
```
La guida che seguiamo è [questa](https://docs.micronaut.io/latest/guide/#quickStart), modificando a nostro gradimento la porta di ascolto.



## Micronaut 4.2.0 Documentation

- [User Guide](https://docs.micronaut.io/4.2.0/guide/index.html)
- [API Reference](https://docs.micronaut.io/4.2.0/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/4.2.0/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

- [Micronaut Gradle Plugin documentation](https://micronaut-projects.github.io/micronaut-gradle-plugin/latest/)
- [GraalVM Gradle Plugin documentation](https://graalvm.github.io/native-build-tools/latest/gradle-plugin.html)
- [Shadow Gradle Plugin](https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow)
## Feature serialization-jackson documentation

- [Micronaut Serialization Jackson Core documentation](https://micronaut-projects.github.io/micronaut-serialization/latest/guide/)


## Feature micronaut-aot documentation

- [Micronaut AOT documentation](https://micronaut-projects.github.io/micronaut-aot/latest/guide/)


