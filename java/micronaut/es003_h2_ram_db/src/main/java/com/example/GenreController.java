package com.example;

import com.example.models.Genre;
import com.example.repos.GenreRepository;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.persistence.PersistenceException;
import jakarta.validation.Valid;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static io.micronaut.http.HttpHeaders.LOCATION;

@ExecuteOn(TaskExecutors.BLOCKING)
@Controller("/genres")
public class GenreController {

    private final GenreRepository genreRepository;

    GenreController(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Get("/{id}")
    Optional<Genre> show(Long id) {
        return genreRepository.findById(id);
    }

    @Put
    HttpResponse<?> update(@Body @Valid GenreUpdate genreUpdate) {
        int numberOfEntitiesUpdated = genreRepository.update(genreUpdate.getId(), genreUpdate.getName());
        return HttpResponse
                .noContent()
                .header(LOCATION, location(genreUpdate.getId()).getPath());
    }

    @Get(value = "/list{?args*}")
    List<Genre> list(@Valid SortingAndOrderArguments args) {
        return genreRepository.findAll(args);
    }

    @Post
    HttpResponse<Genre> save(@Body @Valid GenreSave genreSave) {
        Genre genre = genreRepository.save(genreSave.getName());
        return HttpResponse
                .created(genre)
                .headers(headers -> headers.location(location(genre.getId())));
    }

    @Post("/ex")
    HttpResponse<Genre> saveExceptions(@Body @Valid GenreSave genreSave) {
        try {
            Genre genre = genreRepository.saveWithException(genreSave.getName());
            return HttpResponse
                    .created(genre)
                    .headers(headers -> headers.location(location(genre.getId())));
        } catch(PersistenceException e) {
            return HttpResponse.noContent();
        }
    }

    @Delete("/{id}")
    HttpResponse<?> delete(Long id) {
        genreRepository.deleteById(id);
        return HttpResponse.noContent();
    }

    private URI location(Long id) {
        return URI.create("/genres/" + id);
    }


}
