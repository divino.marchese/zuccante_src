# es003 h2 

TRatto da [qui](https://guides.micronaut.io/latest/micronaut-jpa-hibernate-gradle-java.html) con le solite nostre modifiche.
Abbiamo usato sul [launch](https://micronaut.io/launch)
- hibernate-jpa
- data-jpa
- validation
- yaml

## models

Alcuni dettagli sulle *annotation* usiamo le annotazioni *persistence* [qui](https://jakarta.ee/specifications/persistence/3.1/apidocs/)
per disegnare le tabelle (ORM)
- `@GenartedValue` che usa dui default la strategia `AUTO` vedi [qui](https://jakarta.ee/specifications/persistence/3.1/apidocs/jakarta.persistence/jakarta/persistence/generatedvalue).
- `@NotNull` è del *bean validation*
- `@Column` [qui](https://jakarta.ee/specifications/persistence/3.1/apidocs/jakarta.persistence/jakarta/persistence/column) specifica la *mapped column*
- `@JsonIgnore` non entra nella serializzazione, vedi [qui](https://www.baeldung.com/jackson-annotations).

## rerpository

I *repository* prevedono una *injection* almeno ovvero ovvero `EntityManager`, [API](https://jakarta.ee/specifications/persistence/3.1/apidocs/jakarta.persistence/jakarta/persistence/entitymanager)

## Configurazione dell'applicazione

Ecco il riferimento: [qui](https://docs.micronaut.io/latest/guide/#config). Nel nostro caso definiamo un *singleton bean* che verrà iniettato al momento opportuno,
vedi [qui](https://docs.micronaut.io/latest/api/io/micronaut/context/annotation/ConfigurationProperties.html)
Defines a singleton bean whose property values are resolved from a PropertyResolver. Infatti nel file `yaml` di configurazione abbiamo
```yaml
application:
  max: 50
```

## controller

`@ExecuteOn` API [qui](https://docs.micronaut.io/latest/api/io/micronaut/scheduling/annotation/ExecuteOn.html) indica il *pool* su cui eseguire
in questo caso usiamo `BLOCHING` come strategia, vedi [qui](https://docs.micronaut.io/latest/api/io/micronaut/scheduling/TaskExecutors.html#BLOCKING),
sempre leggendo nella documentazione osserviamo come sia opportuno separare il *task* di accesso al DB dall'*event loop*.
`HttpResponse` lo creiamo a partire dal *body* aggiungendo gli *header*:
```java
return HttpResponse
        .noContent()
        .header(LOCATION, location(genreUpdate.getId()).getPath());
```
per gli *header* le [API](https://docs.micronaut.io/latest/api/io/micronaut/http/HttpHeaders.html), qui abbiamo un reindirizzamento con
una risposta a *body* vuoto! Mentre con
```java
return HttpResponse.created(genre)
        .headers(headers -> headers.location(location(genre.getId())));
```
abbiamo una sintassi alternativa per il reindirizzamento, questa volta con *bosy* non vuoto.

## validazione

Abbiamo pensato a due classi `GenreSave` e `GenreUpdate`, entrambe validate, per passare in modo corretto il `json`
via `POST` e `PUT` (non potendo usare un oggetto `Genre` legato a `Book`).