# es002 api

Un primo test per API non gestite in modo persistente. 
Con le opportune modifiche l'esempio è tratto da [qui](https://dzone.com/articles/micronaut-tutorial-server-application). 
Nel solito tool *micronaut launch* selezioniamo
- *validation* per la validazione 
- *yaml* per la scelta della configurazione

## model

Abbiamo un'entità ed una *enumeration*. L'*annotation* `@Introspected` produce un `BeanIntrospection` grazie a cui evitiamo la *reflection* per
leggere e scrivere i dati del nostro *bean* (se non mettiamo tale annotazione il server ci dà errore), qui non è necessaria, in altre occasioni sì!
Per garantire la serializzazione e la deserializzazione in
json usiamo l'*annotation* `@Serdeable` vedi [qui](https://micronaut-projects.github.io/micronaut-serialization/latest/guide/index.html). Il model è validato
(se validiamo invece il controller utilizzeremo altro pacchetto), in particolare il `POST`, come qui sotto verdremo,
è sottoposto a tale validazione. Il costruttore non è necessario, il server lavora serializzando e deserializzando anche senza di esso.

## action

### `GET`

Qui sotto un esempio basic
```java
@Get
public List<Person> findAll() {
    return persons;
}
```
Possiamo usare le *path variable* `{var}` come `@Get("/{id:4}")`, `:4` indica che la variable è al più
di `4` caratteri. Scaricando tutto usiamo ` @Get("{?max,offset}")` indichiamo i parametri opzionali, ad esempio
`localhost:3000/persons?max=10&offset=10`. Possiamo testare il tutto con `curl`.

### `POST`

`@Body` permette di convertire (deserializzare) il json dal body in oggetto `Person` sottoponendolo
ad un *check* di validità con `@Valid`. Testiamo col solito `curl`
```
curl -H "Content-Type: application/json" -d '{ "id": 0, "firstName": "tony", "lastName": "bueghin", "age": 44, "gender": "MALE"}' localhost:3000/persons
```

### `DELETE`

Ad esempio possiamo testare
```
curl -X DELETE localhost:3000/persons/1
```

### `UPDATE`

Nel nostro caso l'aggiornamento si comporta come il `POST` ritornando un `Optional<Person>`, in tal modo 
Micronaut lavora come con `GET` (quando cerchiamo un singolo oggetto `Person` incartato con `Optional`)
```
curl -X PUT -H "Content-Type: application/json" -d '{ "id": 2, "firstName": "tony", "lastName": "bueghin", "age": 44, "gender": "MALE"}' localhost:3000/persons
```
che funziona se
- passa la validazione
- trova un `id` già presente nella nostra base di dati.

## nota

Come noto `PATCH` è consigliato per aggiornare parzialmente un oggetto, vedi [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH),
ma avendo il passaggio della validazione procediamo validando su di un intero oggetto con `PUT`.


