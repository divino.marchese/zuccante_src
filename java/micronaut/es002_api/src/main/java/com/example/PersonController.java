package com.example;

import com.example.model.Person;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.annotation.*;
import jakarta.validation.Valid;

import java.util.*;
import java.util.stream.Collectors;

@Controller("/persons")
public class PersonController {

    final List<Person> persons = new ArrayList<>();

    @Post
    public Person add(@Body @Valid Person person) {
        person.setId(persons.size() + 1);
        persons.add(person);
        return person;
    }

    @Get("/{id:4}")
    public Optional<Person> findById(Integer id) {
        return persons.stream()
                .filter(it -> it.getId().equals(id))
                .findFirst();
    }

    @Get("{?max,offset}")
    public List<Person> findAll(@Nullable Integer max, @Nullable Integer offset) {
        return persons.stream()
                .skip(offset == null ? 0 : offset)
                .limit(max == null ? 10000 : max)
                .collect(Collectors.toList());
    }

    @Delete("/{id:4}")
    public void delete(Integer id) {
        Optional<Person> deleted = persons.stream().filter(it -> it.getId().equals(id)).findFirst();
        deleted.ifPresent(persons::remove);
    }

    @Put
    public Optional<Person> update(@Body @Valid Person updated) {
        Optional<Person> old = persons.stream().filter(it -> it.getId().equals(updated.getId())).findFirst();
        if(old.isPresent()){
            persons.remove(old.get());
            persons.add(updated);
            return Optional.of(updated);
        }
        return Optional.empty();
    }
}
