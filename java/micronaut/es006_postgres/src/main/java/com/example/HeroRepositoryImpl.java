package com.example;

import com.example.model.Hero;
import io.micronaut.transaction.annotation.ReadOnly;
import io.micronaut.transaction.annotation.Transactional;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.Optional;

@Singleton
public class HeroRepositoryImpl implements HeroRepository {

    private final EntityManager entityManager;

    public HeroRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @ReadOnly
    @Override
    public Optional<Hero> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(Hero.class, id));
    }

    @ReadOnly
    @Override
    public List<Hero> findAll() {
        TypedQuery<Hero> query = entityManager.createQuery("SELECT h FROM Hero as h order by h.name", Hero.class);
        return query.getResultList();
    }

    @Transactional
    @Override
    public Hero save(Hero hero) {
        entityManager.persist(hero);
        return hero;
    }

    @Transactional
    @Override
    public Optional<Hero> update(Hero hero) {
        Hero updated =  entityManager.find(Hero.class, hero.getId());
        if(updated != null) {
            updated.setName(hero.getName());
            return Optional.of(updated);
        }
        return Optional.empty();
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        findById(id).ifPresent(entityManager::remove);
    }
}
