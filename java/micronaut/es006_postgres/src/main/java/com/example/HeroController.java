package com.example;

import com.example.model.Hero;
import io.micronaut.http.annotation.*;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import java.util.List;
import java.util.Optional;

@ExecuteOn(TaskExecutors.BLOCKING)
@Controller("/heroes")
public class HeroController {

    private final HeroRepository heroRepository;

    public HeroController(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    @Get("/{id}")
    public Optional<Hero> getHeroById(Long id) {
        return heroRepository.findById(id);
    }

    @Get
    public List<Hero> getHeroes() {
        return heroRepository.findAll();
    }

    @Post
    public Hero saveHero(@Body Hero hero) {
        return heroRepository.save(hero);
    }

    @Put
    public Optional<Hero> updateHero(@Body Hero hero) {
        return heroRepository.update(hero);
    }

    @Delete("/{id}")
    public void deleteHero(Long id) {
        heroRepository.deleteById(id);
    }

}
