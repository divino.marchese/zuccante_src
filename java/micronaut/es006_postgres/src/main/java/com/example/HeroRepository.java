package com.example;

import com.example.model.Hero;

import java.util.List;
import java.util.Optional;

public interface HeroRepository {
    Optional<Hero> findById(Long id);
    List<Hero> findAll();
    Hero save(Hero hero);
    Optional<Hero> update(Hero hero);
    void deleteById(Long id);
}
