# es006 postgres


Tratto dal tutorial [qui](https://dzone.com/articles/micronaut-cloud-jpa).
Prepariamo il progetto con [launch](https://micronaut.io/launch/)
- hibernate-jpa
- data-jpa
- validation
- yaml
- postgres

## il db

Sotto linux
```
sudo -i -u postgres
psql
```
Entrati in `psql` diamo 
```
postgres=# CREATE DATABASE micronaut006;
CREATE DATABASE
postgres=# \l
postgres=# CREATE USER micronaut WITH ENCRYPTED PASSWORD 'micronaut';
CREATE ROLE
postgres=# GRANT ALL PRIVILEGES ON DATABASE micronaut006 TO micronaut;
GRANT
postgres=# 
```
Entrati nel db col nuovo utente possiamo 
```
micronaut006=> \d
                 List of relations
 Schema |        Name        |   Type   |   Owner   
--------+--------------------+----------+-----------
 public | heroes             | table    | micronaut
 public | hibernate_sequence | sequence | micronaut
(2 rows)

micronaut006=> \d heroes
                        Table "public.heroes"
   Column   |          Type          | Collation | Nullable | Default 
------------+------------------------+-----------+----------+---------
 id         | bigint                 |           | not null | 
 first_name | character varying(255) |           | not null | 
Indexes:
    "heroes_pkey" PRIMARY KEY, btree (id)

```

## configurazione

La configurazione è quasi pronta, ecco come la sistemiamo
```yaml
default:
  url: ${JDBC_URL:`jdbc:postgresql://localhost:5432/micronaut006`}
  driver-class-name: org.postgresql.Driver
  username: ${JDBC_USER:micronaut}
  password: ${JDBC_PASSWORD:micronaut}
  db-type: postgres
  dialect: POSTGRES
```
usiamo i *placeolder*, vedi [qui](https://docs.micronaut.io/latest/guide/#propertySource).

## test 

Inseriamo due valori `POST`
```
curl -H 'Content-Type: application/json' -d '{ "name": "ratman" }' localhost:8080/heroes
curl -H 'Content-Type: application/json' -d '{ "name": "ratman" }' localhost:8080/heroes
```
possiamo testare su PostgreSQL
```
micronaut006=> select * from heroes;
 id |     name      
----+---------------
  3 | super scoassa
  4 | ratman
(2 rows)

```
quindi vediamo il risultato `GET`
```
curl localhost:8080/heroes/
curl localhost:8080/heroes/4
```
aggiorniamo 
```
curl -X PUT -H 'Content-Type: application/json' -d '{ "id": 4, "name": "sorseman" }' localhost:8080/heroes/
```
verifichiamo
```
micronaut006=> select * from heroes;
 id |     name      
----+---------------
  3 | super scoassa
  4 | sorseman
(2 rows)

```
cancelliamo
```
curl -X DELETE localhost:8080/heroes/4
```
e verifichiamo
```y
micronaut006=> select * from heroes;
 id |     name      
----+---------------
  3 | super scoassa
(1 row)

```

## Micronaut 4.2.1 Documentation

- [User Guide](https://docs.micronaut.io/4.2.1/guide/index.html)
- [API Reference](https://docs.micronaut.io/4.2.1/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/4.2.1/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

- [Micronaut Gradle Plugin documentation](https://micronaut-projects.github.io/micronaut-gradle-plugin/latest/)
- [GraalVM Gradle Plugin documentation](https://graalvm.github.io/native-build-tools/latest/gradle-plugin.html)
- [Shadow Gradle Plugin](https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow)
## Feature serialization-jackson documentation

- [Micronaut Serialization Jackson Core documentation](https://micronaut-projects.github.io/micronaut-serialization/latest/guide/)


## Feature hibernate-jpa documentation

- [Micronaut Hibernate JPA documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#hibernate)


## Feature test-resources documentation

- [Micronaut Test Resources documentation](https://micronaut-projects.github.io/micronaut-test-resources/latest/guide/)


## Feature micronaut-aot documentation

- [Micronaut AOT documentation](https://micronaut-projects.github.io/micronaut-aot/latest/guide/)


## Feature validation documentation

- [Micronaut Validation documentation](https://micronaut-projects.github.io/micronaut-validation/latest/guide/)


## Feature jdbc-hikari documentation

- [Micronaut Hikari JDBC Connection Pool documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#jdbc)


