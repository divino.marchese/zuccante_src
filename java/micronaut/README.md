# riferimenti


## guide

- Micronaut framework [qui](https://docs.micronaut.io/latest/guide/)
- injection specifications [qui](https://javax-inject.github.io/javax-inject/)
- Micronaut data [qui](https://micronaut-projects.github.io/micronaut-data/latest/guide/)
- Hibernate user guide [qui](https://docs.jboss.org/hibernate/orm/6.4/userguide/html_single/Hibernate_User_Guide.html)

## API

- `io.micronaut` [API](https://docs.micronaut.io/latest/api/)
- `io.micronaut.data` [API](https://micronaut-projects.github.io/micronaut-data/latest/api/index.html)
- `java-inject` [API](https://javax-inject.github.io/javax-inject/api/index.html)
- ` jakarta.persistence` [API](https://jakartaee.github.io/persistence/latest-nightly/api/jakarta.persistence/module-summary.html)

## JPQL

- tutorial jakarta [qui](https://jakarta.ee/learn/docs/jakartaee-tutorial/current/persist/persistence-querylanguage/persistence-querylanguage.html)
- guida Oracle [qui](https://docs.oracle.com/cd/E11035_01/kodo41/full/html/ejb3_langref.html#ejb3_langref_named_params)
- JPQL jakarta [qui](https://eclipse-ee4j.github.io/jakartaee-tutorial/#the-jakarta-persistence-query-language)
- JPQL book online [qui](Jhttps://en.wikibooks.org/wiki/Java_Persistence/JPQL)


