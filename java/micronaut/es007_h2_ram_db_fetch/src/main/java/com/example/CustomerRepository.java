package com.example;

import com.example.model.Customer;
import com.example.model.Order;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository {
    Optional<Customer> findById(Long id);
    List<Customer> findAll();
    List<Order> getCustomerOrders(Long id);
    Customer save(Customer customer);
    Optional<Customer> update(Customer customer);
    void deleteById(Long id);
    List<Order> addCustomerOrder(Long id, Order order);
    List<Order> updateCustomerOrder(Long id, Order order);
    List<Order> deleteCustomerOrder(Long idC, Long idO);
}
