package com.example;

import com.example.model.Customer;
import com.example.model.Order;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.Delete;

import java.util.List;
import java.util.Optional;

@Controller("/customers")
public class CustomerController {

    // Customer

    private final CustomerRepositoryImpl customerRepository;

    public CustomerController(CustomerRepositoryImpl customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Get
    public List<Customer> getUsers() {
        return customerRepository.findAll();
    }

    @Get("/{id}")
    public Optional<Customer> getUser(Long id) {
        return customerRepository.findById(id);
    }

    @Post
    public Customer addUser(@Body Customer customer) {
        return customerRepository.save(customer);
    }

    @Put
    public void updateUser(@Body Customer customer) {
        customerRepository.update(customer);
    }

    @Delete("/{id}")
    public void deleteUser(Long id) {
        customerRepository.deleteById(id);
    }

    // Order

    @Get("/{id}/orders")
    public List<Order> getCustomerOrders(Long id) {
        return customerRepository.getCustomerOrders(id);
    }

    @Post("/{id}/orders")
    public List<Order> addCustomerOrder(Long id, @Body Order order) {
        return customerRepository.addCustomerOrder(id, order);
    }

    @Put("/{id}/orders")
    public List<Order> updateCustomerOrder(Long id, @Body Order order) {
        return customerRepository.updateCustomerOrder(id, order);
    }

    @Delete("/{idC}/orders/{idO}")
    public List<Order> deleteCustomerOrder(Long idC, Long idO) {
        return customerRepository.deleteCustomerOrder(idC, idO);
    }

}
