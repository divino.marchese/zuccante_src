package com.example;

import com.example.model.Customer;
import com.example.model.Order;
import io.micronaut.transaction.annotation.ReadOnly;
import io.micronaut.transaction.annotation.Transactional;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.validation.constraints.NotNull;

import java.util.*;

public class CustomerRepositoryImpl implements CustomerRepository {

    private final EntityManager entityManager;

    public CustomerRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @ReadOnly
    @Override
    public Optional<Customer> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(Customer.class, id));
    }

    @ReadOnly
    @Override
    public List<Customer> findAll() {
        TypedQuery<Customer> query = entityManager.createQuery("SELECT c FROM Hero as c order by c.name", Customer.class);
        return query.getResultList();
    }

    @ReadOnly
    @Override
    public List<Order> getCustomerOrders(@NotNull Long id) {
        Customer customer = entityManager.find(Customer.class, id);
        List<Order> orders = new ArrayList<>();
        if(customer != null){
            orders.addAll(customer.getOrders());
        }
        return orders;
    }

    @Transactional
    @Override
    public Customer save(@NotNull Customer customer) {
        entityManager.persist(customer);
        return customer;
    }

    @Transactional
    @Override
    public Optional<Customer> update(@NotNull Customer customer) {
        Customer updated =  entityManager.find(Customer.class, customer.getId());
        if(updated != null) {
            updated.setName(customer.getName());
            return Optional.of(updated);
        }
        return Optional.empty();
    }

    @Transactional
    @Override
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(entityManager::remove);
    }
    @Transactional
    @Override
    public List<Order> addCustomerOrder(@NotNull Long id, @NotNull Order order) {
        Customer customer =  entityManager.find(Customer.class, id);
        List<Order> orders = new ArrayList<>();
        if(customer != null) {
            order.setCustomer(customer);
            entityManager.persist(order);
            orders.addAll(customer.getOrders());
        }
        return orders;
    }

    @Transactional
    @Override
    public List<Order> updateCustomerOrder(@NotNull Long id, @NotNull Order order) {
        Customer customer =  entityManager.find(Customer.class, id);
        Order updated = entityManager.find(Order.class, order.getId());
        ArrayList<Order> orders = new ArrayList<>();
        if(customer != null && updated != null && Objects.equals(id, updated.getCustomer().getId())) {
            updated.setName(order.getName());
            entityManager.persist(order);
            orders.addAll(customer.getOrders());
        }
        return orders;
    }

    @Transactional
    @Override
    public List<Order> deleteCustomerOrder(Long idC, Long idO) {
        Customer customer =  entityManager.find(Customer.class, idC);
        Order deleted = entityManager.find(Order.class, idO);
        ArrayList<Order> orders = new ArrayList<>();
        if(customer != null && deleted != null && Objects.equals(idC, deleted.getCustomer().getId())) {
            entityManager.remove(deleted);
            orders.addAll(customer.getOrders());
        }
        return orders;
    }
}
