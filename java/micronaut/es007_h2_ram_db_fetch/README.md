# es007 h2 ram db fetch

L'esercitazione, [qui](https://www.baeldung.com/hibernate-fetchmode), tratta il problema dell'ORM, in particolare il *fetch* di Hibernate, tratto dalla guida [qui](https://www.baeldung.com/hibernate-fetchmode).
Impostiamo con *launch* [qui0](https://micronaut.io/launch/)
- yaml
- validation
- hibernate-jpa
- data-jpa

## impostazione

Aggiunggiamo il LOG
```yaml
pa:
  default:
    properties:
      hibernate:
        hbm2ddl:
          auto: update
        show_sql: true
```

## model e associazioni

Leghiamo fra loro due entità, a tal proposito dovremo definire:
- le associazioni tramite cui le entità sono collegate fra loro
- il modo con cui vengono eseguite le *query*

### le associazioni

Qui abbiamo una associazione **bidirezionale** e **uno a molti** lato `Customer`, il termine bidirezionale sta ad indicare che 
da un lato e dall'altro dell'associazione, data un'entità possiamo recuperare l'altra: dato un `Customer` abbiamo i suoi Òrder`, e dato un Òrder`abbiamo il suo `Customer`.
Altro dettaglio importante riguarda il `fetch` ovvero il modo con ci vengono caricate le entità 
fra loro collegate:
- *eager Loading* è un *pattern* in cui l'inizializzazione dei dati avviene *on the spot* (sul momento): se inizializzo un `User`inizializzo i suoi `Ordini`, ad esempio,
- *lazy Loading* anche *proxied fetching* è un *pattern* in cui l'inizializzazione degli oggetti legati ad un entità viene rimandata, fino a che non vi sia una richiesta.
Ad esempio (vedi riferimenti)
```java
@Entity
@Table (name = "users")
public class OrderDetail {
  @Id
  @GeneratedValue
  @Column(name = "user_id")
  private Long userId;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
  private Set<OrderDetail> orderDetail = new HashSet();
    ...
```
nel momento in cui viene chiamato il *getter* `getOrderDetail()` o in caso di `JOIN` vengono 
dati i riferimenti agli oggetti `OrderDetail`, nel caso *eager* caricare in memoria uin `User`
carica subito i suoi `OrderDetail`. Di defaul abbiamo *lazy* in `@OneToMany` e `@ManyToMany` *eager* in `@ManyToOne` (nel nostro codice imponiamo *lazy*)! Nella guida di Hibernate le associazioni le
vediamo [qui](https://docs.jboss.org/hibernate/orm/6.4/introduction/html_single/Hibernate_Introduction.html#associations) 
trattate.

### le query

Come per le associazioni anche per le query si ripropone il problema del *fetch*. 
- `FetchMode.SELECT` Il `SELECT` come modo è *lazy* siamo di fronte al così detto ***N+1 select problem***: una query per la prima entità e le rimanenti per ogni singola
entità ad essa collegata! `@BatchSize` ci permette di lavorare in *batch fetch* che ottimizza
il *lazy fetch* che riduce il numero di query proporzionalmente. Interessante un esempio tratto da Stack Overflow [qui](https://stackoverflow.com/questions/25210949/understanding-batchsize-in-hibernate)
> Consider the following example: at runtime you have 25 Cat instances loaded in a Session, and each Cat has a reference to its owner, a Person. The Person class is mapped with a proxy, lazy="true". If you now iterate through all cats and call getOwner() on each, Hibernate will, by default, execute 25 SELECT statements to retrieve the proxied owners. You can tune this behavior by specifying a batch-size in the mapping of Person:
```
<class name="Person" batch-size="10">...</class>
```
> Hibernate will now execute only three queries: the pattern is 10, 10, 5.


## riferimenti
- Hibernate *association* [qui](associations)
- Hibernate "fetch" [qui](https://docs.jboss.org/hibernate/orm/6.4/introduction/html_single/Hibernate_Introduction.html#embeddable-objects)
- Baeldung "Eager/Lazy loading" [qui](https://www.baeldung.com/hibernate-lazy-eager-loading)

## Micronaut 4.2.1 Documentation

- [User Guide](https://docs.micronaut.io/4.2.1/guide/index.html)
- [API Reference](https://docs.micronaut.io/4.2.1/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/4.2.1/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

- [Micronaut Gradle Plugin documentation](https://micronaut-projects.github.io/micronaut-gradle-plugin/latest/)
- [GraalVM Gradle Plugin documentation](https://graalvm.github.io/native-build-tools/latest/gradle-plugin.html)
- [Shadow Gradle Plugin](https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow)
- 
## Feature validation documentation

- [Micronaut Validation documentation](https://micronaut-projects.github.io/micronaut-validation/latest/guide/)


## Feature serialization-jackson documentation

- [Micronaut Serialization Jackson Core documentation](https://micronaut-projects.github.io/micronaut-serialization/latest/guide/)


## Feature jdbc-hikari documentation

- [Micronaut Hikari JDBC Connection Pool documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#jdbc)


## Feature hibernate-jpa documentation

- [Micronaut Hibernate JPA documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#hibernate)


## Feature micronaut-aot documentation

- [Micronaut AOT documentation](https://micronaut-projects.github.io/micronaut-aot/latest/guide/)


