public class ProxyVideo implements Video {
    
    private RealVideo realvideo;
    private String filename;

    public ProxyVideo(String filename){
        this.filename = filename;
        // this.realvideo = RealVideo(filename);
    }

    @Override
    public void display(){
        if(realvideo == null) {
            realvideo = new RealVideo(filename);
        }
        realvideo.display();
    }
}
