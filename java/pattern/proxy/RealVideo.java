public class RealVideo implements Video {

    private String filename;

    public RealVideo(String filename){
        this.filename = filename;
    }

    @Override
    public void display(){
        System.out.println("displaying: " + filename);
    } 

    public void loadFromDisk(String filename){
        System.out.println("loading: " + filename);
    }
    
}
