# hello jgrapht

La libreria proposta per gestire i grafi è JGraphT, [qui](https://jgrapht.org/) la home page. Per importare le dipendenze Maven in Gradle (Kotlin)
dobbiamo aggiungere
```kotlin
compileOnly("org.jgrapht:jgrapht-core:1.5.2")
compileOnly("org.jgrapht:jgrapht-io:1.5.2")
```