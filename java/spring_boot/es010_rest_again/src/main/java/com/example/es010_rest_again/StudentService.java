package com.example.es010_rest_again;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    public void addNewStudent(Student student) {
        Optional<Student> optionalStudent = studentRepository.findStudentNyEmail(student.getEmail());
        if(optionalStudent.isPresent()){
            throw new IllegalStateException("student is present");
        }
        studentRepository.save(student);
    }

    public void deleteStudent(Long id) {
        if(!studentRepository.existsById(id))
            throw new IllegalStateException("student does not exist, id: " + id);
        studentRepository.deleteById(id);
    }

    @Transactional
    public void updateStudent(Long id, String name, String email) {
        Student student = studentRepository.findById(id).orElseThrow(
                ()->new IllegalStateException("student does not exist, id: " + id)
        );
        if(name != null && name.length() > 0 && !Objects.equals(student.getName(), name))
            student.setName(name);
        if(email != null && email.length() > 0 && !Objects.equals(student.getEmail(), email)){
            Optional<Student> studentOptional = studentRepository.findStudentNyEmail(email);
            if(studentOptional.isPresent())
                throw new IllegalStateException("email token");
            student.setEmail(email);
        }
        // studentRepository.save(student);
    }
}
