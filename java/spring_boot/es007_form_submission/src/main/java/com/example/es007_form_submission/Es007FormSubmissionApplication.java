package com.example.es007_form_submission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es007FormSubmissionApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es007FormSubmissionApplication.class, args);
	}

}
