package com.example.es009_upload;

import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    private final Path upload = Paths.get("uploads");

    @Override
    public void init() {
        try {
            Files.createDirectory(upload);
        } catch (IOException e) {
            throw new RuntimeException("Unable to create upload!");
        }
    }

    @Override
    public void save(MultipartFile file) {
        try {
            String originalName = file.getOriginalFilename();
            if (originalName == null ) throw new RuntimeException("null name");
            else {
                Files.copy(file.getInputStream(), upload.resolve(originalName));
            }
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = upload.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(upload.toFile());
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.upload, 1)
                    .filter(path -> !path.equals(this.upload))
                    .map(this.upload::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }
}
