# es004 jpa

**JPA** sta per **Java Persistence APi** e fa uso di un ORM 
Hibernate **[home](https://hibernate.org/orm/)**, **[guida](https://docs.jboss.org/hibernate/orm/5.6/userguide/html_single/Hibernate_User_Guide.html)**  
JPA **[guida](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference)**

## moduli usati

- `Spring Boot DevTools` per il reload.
- `Spring Web` per applicazioni RESTFUL.
- `Spring Data JPA` per l'ORM.
- `JDBC API` per connettersi al database.
- `MariaDB Driver` per i driver del database.

## classi

- `User` il *model*.
- `UserController` il *controller* REST json.
- `UserRepository` il DAO o *repository*.

## il DB

```
CREATE DATABASE es004SB;
GRANT ALL ON es004SB.* to 'admin'@localhost IDENTIFIED BY 'zuccante';
```
testiamo
```
MariaDB [es004SB]> describe user;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| email    | varchar(255) | YES  | UNI | NULL    |                |
| name     | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.003 sec)

```
per connetterci al DB impostiamo i dati in `application.yml`
```yaml
 DB

spring:
  datasource:
    url: jdbc:mariadb://localhost:3306/es004SB
    username: admin
    password: zuccante
    driver-class-name: org.mariadb.jdbc.Driver
# hibernate
  jpa:
    hibernate:
      ddl-auto: create-drop
```

## model

L'*annotation* `@Entity` viene da `java.[ersistence` **[API](https://jakarta.ee/specifications/persistence/3.0/apidocs/jakarta.persistence/module-summary.html)** e richiede che la classe, il *model* appunto, possegga almeno una chiave primaria.
Per le regole  che una classe *entuty* deve rispettare andare alla **[guida](https://docs.jboss.org/hibernate/orm/5.6/userguide/html_single/Hibernate_User_Guide.html#entity)**, un model deve possedere un **costruttore senza rgomenti**, un
*identifier* indicato da `@Id`, vedi **[guida](https://docs.jboss.org/hibernate/orm/5.6/userguide/html_single/Hibernate_User_Guide.html#identifiers)**; solo valori di tipo intero possono essere
`@GeneratedValue`

## repository

`CrudRepository<long, User>` **[API](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html)** è un interfaccia, la classe che la implementerà poi sarà costruita dal *framework*: i due *generic* fanno rispettivamente riferimento alla chiave ed al tipo dell'entita.
Tale *repository* mette a disposizione di suo tutta una serie di richieste.
- `long count()`
- `void delete(T entity)`
- `void deleteAll()`
- `void deleteAll(Iterable<? extends T> entities)`
- `void deleteById(ID id)`
- `boolean existsById(ID id)`
- `Iterable<T> findAll()`
- `Iterable<T> findAllById(Iterable<ID> ids)`
- `Optional<T> findById(ID id)`
- `<S extends T> S save(S entity)`: ritorna anche l'*entity*.
- `<S extends T> Iterable<S> saveAll(Iterable<S> entities)`.
La nostra interfaccia quindi già possiede una serie di richieste preconfezionate. 
Un *repository* lavora con i ***query method*** **[guida](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods)** JPA si preoccupa di tradurre
come indicato nelle tabelle **[guida](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#appendix.query.method.subject)** **[guida](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods)**. aD ESEMPIO
```java
List<User> findByEmailAddressAndLastname(String emailAddress, String lastname);
```
diventa
```java
select u from User u where u.emailAddress = ?1 and u.lastname = ?2
```
quest'ultima non è una *query* SQL ma una **JPA named query** **[guida](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.named-queries)**.
L'*annotation* `@Repository` **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Repository.html)** crea un *bean* che noi potremo utilizzare 
ovvero un DAO - *Data Access Object*.

Talvolta le *query* ritornano un oggetto `Optional<T>` **[API](https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html?is-external=true). Un ultimo dettaglio `save(...)`, se il valore di chiave da noi è già presente, da noi `id`
opera un aggiornamento.
```java
@PutMapping("/update/{id}")
public void updateUser(@RequestBody User user, @PathVariable Long id){
    if(repository.findById(id).isPresent()){
        User updated = new User(user.getName(), user.getEmail());
        updated.setId(id);
        repository.save(updated);
    }
}
```
Abbiamo usato gli *stream* per proporre un approccio "funzionale"
```java
@GetMapping("/findall")
public List<User> getAllUser() {
    return StreamSupport.stream(repository.findAll().spliterator(), false)
        .collect(Collectors.toList());
}
```


## demo

```
MariaDB [es004SB]> select * from user;
+----+-----------------------+-------------+----------+
| id | email                 | name        | password |
+----+-----------------------+-------------+----------+
|  1 | jack.vox@gmail.com    | Jack Vox    | 1234567  |
|  2 | sam.vmay@gmail.com    | Sam May     | 1234567  |
|  3 | tony.sbrisa@gmail.com | Tony Sbrisa | 1234567  |
|  4 | nane.onto@gmail.com   | Nane Onto   | 1234567  |
+----+-----------------------+-------------+----------+
4 rows in set (0.001 sec)

```

## test

Usiamo `curl`
```
curl -X POST -H "Content-Type: application/json" -d '{"name":"Bepi Scrauso","email":"bepi.scrauso@gmail.com"}' localhost:8080/add
curl -X PUT -H "Content-Type: application/json" -d '{"name":"Jack Vox","email":"j.vox@gmail.com"}' localhost:8080/update/1
curl -X DELETE -H "Content-Type: application/json" localhost:8080/delete/1
```

## altri materiali

[1] "Accessing Data with JPA" **[qui](https://spring.io/guides/gs/accessing-data-jpa/)**.