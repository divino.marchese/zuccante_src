package com.example.es004_jpa;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Es004JpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es004JpaApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(UserRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new User("Jack Vox", "jack.vox@gmail.com"));
			repository.save(new User("Sam May", "sam.may@gmail.com"));
			repository.save(new User("Tony Sbrisa", "tony.sbrisa@gmail.com"));
			repository.save(new User("Nane Onto", "nane.onto@gmail.com"));
		};
	}

}
