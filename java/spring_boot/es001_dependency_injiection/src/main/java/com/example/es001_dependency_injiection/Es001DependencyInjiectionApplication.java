package com.example.es001_dependency_injiection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Es001DependencyInjiectionApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Es001DependencyInjiectionApplication.class, args);
		System.out.println("*************** BEANS *****************************");
		for(String name : context.getBeanDefinitionNames())
			System.out.println(name);
		System.out.println("*************** BEANS *****************************");
	}

}
