package com.example.es001_dependency_injiection;

public interface TextWriter {
    String writeText();
}
