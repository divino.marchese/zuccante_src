package com.example.es014_authentication_oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es014AuthenticationOauth2Application {

	public static void main(String[] args) {
		SpringApplication.run(Es014AuthenticationOauth2Application.class, args);
	}

}
