# Gli esempi

- **demo** è il test fatto com **[spring initializr](https://start.spring.io/)**.
- **es000_bean** introduce il concetto di ***bean*** e di ***component*** su cui si basa la ***dependency injection***.
- **es001_dependency_injiection**.
- **es002_restful** il primo server rest `json`.
- **es003_template** uso di *template* `mustache` **[home](https://mustache.github.io/mustache.5.html)**.
- **es004_jpa** il primo test dell'ORM.
- **es005_mockito** il primo test con JUnit e Mockito.


## riferimetni

[1] Fonte ufficiale ed imprescindibile di tutorial: [qui](https://spring.io/guides#topical-guides).  
[2] "Spring Boot Reference Guide" [qui](https://docs.spring.io/spring-boot/docs/2.1.2.RELEASE/reference/htmlsingle/#boot-features-testing).  
[2] Un corso [qui](https://www.kindsonthegenius.com/spring-boot/spring-boot-entity-relationship/).
