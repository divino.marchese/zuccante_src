package com.example.es000_bean;

import org.springframework.stereotype.Component;

@Component
public class Address {

    private String location;
    private String number;
    private String ZIP;

    public Address(){
        location = "via dello SQuero Bello";
        number = "17";
        ZIP = "30172";
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }
}
