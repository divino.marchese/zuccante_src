package com.example.es005_crud_hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es005CrudHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es005CrudHibernateApplication.class, args);
	}

}
