# Inversion of Control e Dependency Injection

Partiamo dall'inizio: **inversion of control**. QUi non ci soffermiamo, rimandiamo ad articolo in bibliografia, 
prendiamo invece il **Dependency Inversion Principle** che uncle Bob (Robert Cecil Martin) identifica come
- a. High-level modules should not depend on low-level modules. Both should depend on abstractions.
- b. Abstractions should not depend on details. Details should depend on abstractions.
Perché *inversion*?
> Indeed one of the goals of these methods is to define the subprogram hierarchy that describes how the
high-level modules make calls to the low-level modules.

vediamo un esempio tratto da StackOverflow
```java
public class SMSService {
    public void sendSMS(Account account, String body) {...}
}

public class UIHandler {
    public void sendConfirmationMsg(Account account) {
        SMSService service = new SMSService();
        service.sendSMS(account, "Your order has been shipped successfully!");
    }
}
```
I limiti di un tale approccio stanno nel pfatto che se volessimo mandare una conferma ad esempio tramite email dovremmo ridisegnare l'inter classe, invece realizzando un'interfaccia ed implementandola di volta in volta
```java
inteface Service {
    void sendMessage(Account account, String body);
}
```
riscriviamo
```java
public class UIHandler {
    public void sendConfirmationMsg(string mobileNumber) {
        Service ervice = new ...
        service.senMessage(account, "Your order has been shipped successfully!");
    }
}
```
migliorando ancora
```java
public class UIHandler {

    private Service service

    public UIHandler(Service service) {
        this.service = service;
     }

    public void sendConfirmationMsg(string mobileNumber) {
        this.service.senMessage(account, "Your order has been shipped successfully!");
    }
}
```
In [2] c'èl'esempio riproposto di Fowler [3].

## Container

> Containers are the interface between a component and the low-level platform-specific functionality that supports the component. (Oracle)

## Spring e DI

In Spring la documentazione [6] parla in modo diffuso di *container* e *IoC* (Inversion of Control)
![schema](https://docs.spring.io/spring-framework/docs/current/reference/html/images/container-magic.png)

In spring preferiremo due tipi di DI quella basata su costruttore e l'altra su *setter*.  Partiamo quindi con i costruttori.
Ecco alcuni esempi tratti da [4]. Per prima cosa un *controller*
``` java
@RestController
public class HomeController {
 
    @Autowired
    GreetingService greetingService;
 
    @RequestMapping("/")
    public String home() {
        return greetingService.greet();
    }
}
```
e quindi la *business logic* cui il *controller* si appoggia
```java
public interface GreetingService {
    String greet();
}

@Service
public class EnglishGreetingService implements GreetingService {
 
    @Override
    public String greet() {
        return "Hello World!";
    }
}
```
(per le *annotation* si rimanda agli appunti dedicati). Volendo preparare una configurazione con più possibilità
```java
@Configuration
public class GreetingServiceConfig {
 
    @Bean
    @ConditionalOnProperty(name = "language.name", havingValue = "english", matchIfMissing = true)
    public GreetingService englishGreetingService() {
        return new EnglishGreetingService();
    }
 
    @Bean
    @ConditionalOnProperty(name = "language.name", havingValue = "french")
    public GreetingService frenchGreetingService() {
        return new FrenchGreetingService();
    }
}
```
e nel file di Spring possiamo impostare a nostro gradimento o modificare i valori delle impostazioni (come visto negli esempi). In alternativa con i *component* possiamo scrivere un POJO
```java
@Component
public class MeaningOfLife {
 
    final int lifeInt;
    final String lifeString;
 
    @Autowired
    public MeaningOfLife(
        @Value("#{ @environment['life.int'] ?: 0 }") int lifeInt,
        @Value("#{ @environment['life.string'] ?: '0' }") String lifeString
    ) {
        this.lifeInt = lifeInt;
        this.lifeString = lifeString;
    }
 
    public int getLifeInt() {
        return lifeInt;
    }
 
    public String getLifeString() {
        return lifeString;
    }
}
```
per poi richiamare con DI in un nostro *controller*
```java
@Autowired
MeaningOfLife meaningOfLife;
```
Qui usiamo un linguaggio particolare detto SPeL ([qui](https://www.baeldung.com/spring-expression-language) per un introduzione); a spanne possiamo dire
- `@Component` è *annotation* a livello di classe `@Bean` è a livello di *method*
- i *bean* sono elementi tipicamente di configurazione e che vengono chiamati all'avvia dell'applicazione
- una classe di terze parti apparirà come *bean*, anche un jar o qualcosa di scritto con altri linguaggi
- le nostre classi saranno *component*
Passiamo ai *setter*
```java
public interface Nameable {
    public String getName();
}

@Component
public class Dog implements Nameable {
 
    @Override
    public String getName() {
        return "Fluffy";
    }
}

@Component
public class Person implements Nameable {
 
    @Override
    public String getName() {
        return "Micah";
    }
}
```
ed ecco la DI
```java
@Component
public class NameHelper {
 
    private Nameable person;
    private Nameable dog;
 
    public String getName(String type) {
        switch (type) {
            case "person":
                return person.getName();
            case "dog":
                return dog.getName();
            default:
                return "UNKNOWN";
        }
    }
 
    @Autowired
    @Qualifier("person")
    public void setPerson(Nameable person) {
        this.person = person;
    }
 
    @Autowired
    @Qualifier("dog")
    public void setDog(Nameable dog) {
        this.dog = dog;
    }
}
```




## materiali

[1] "Dependency Injection" by Jenkov [qui](http://tutorials.jenkov.com/dependency-injection/index.html).  
[2] "Understanding Inversion of Control (IoC) Principle" di Amit Kumar [qui](https://medium.com/@amitkma/understanding-inversion-of-control-ioc-principle-163b1dc97454).  
[3] "Inversion of Control Containers and the Dependency Injection pattern" di Martin Fowler [qui](https://www.martinfowler.com/articles/injection.html).
[4] "Dependency Injection – Spring Boot Technical Concepts Series, Part 2" di Micah Silverman [qui](https://stormpath.com/blog/spring-boot-dependency-injection.
[5] "Dependency Injection Options for Java" di Shannon Friswold [qui](https://keyholesoftware.com/2014/02/17/dependency-injection-options-for-java/).   
[5] "Appunti, slide" di Luca Cabibbo (pdf) [qui](http://cabibbo.dia.uniroma3.it/asw/pdf/asw820-spring-framework.pdf).  
[6] "The IOC Container" dalla documentazione ufficiale dui Spring [qui](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans).