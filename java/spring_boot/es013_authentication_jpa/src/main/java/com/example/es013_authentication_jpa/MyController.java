package com.example.es013_authentication_jpa;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class MyController {

    @GetMapping("/")
    public RedirectView root() {
        return new RedirectView("/all");
    }

    @GetMapping("/admin")
    public Message admin() {
        return new Message("Welcome admin");
    }

    @GetMapping("/user")
    public Message user() {
        return new Message("Welcome user");
    }

    @GetMapping("/all")
    public Message all() {
        return new Message("Hello everyone");
    }
}
