package com.example.es013_authentication_jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Configuration
public class UserConfig {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Bean
    CommandLineRunner commandLineRunner(UserRepository repository) {
        return args -> {
            User admin = new User(
                    "admin",
                    passwordEncoder.encode("zuccante@2021"),
                    true,
                    "ADMIN"
            );
            User user = new User(
                    "user",
                    passwordEncoder.encode("user@2021"),
                    true,
                    "USER"
            );
            repository.saveAll(List.of(admin, user));
        };
    }
}
