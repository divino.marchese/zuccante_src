package com.example.es005_mockito;

public interface HelloRepository {

    public String get();
}
