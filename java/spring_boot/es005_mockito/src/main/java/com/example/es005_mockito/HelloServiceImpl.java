package com.example.es005_mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {

    @Autowired
    private HelloRepository repo;

    @Override
    public String get() {
        return repo.get();
    }
}
