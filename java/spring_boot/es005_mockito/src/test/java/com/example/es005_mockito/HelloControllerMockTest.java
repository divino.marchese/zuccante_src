package com.example.es005_mockito;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = Es005MockitoApplication.class)
public class HelloControllerMockTest {

    private ResponseEntity<String> responseEntity;

    @Mock
    private HelloService helloService;

    @InjectMocks
    private HelloController helloController;

    @Mock
    private TestRestTemplate restTemplate;

    @BeforeEach
    void setMockOutput() {
        when(helloService.get()).thenReturn("Hello Mockito Rest");
        responseEntity = new ResponseEntity<>(helloController.hello(), HttpStatus.OK);
        when(restTemplate.getForEntity("localhost:8080/", String.class)).thenReturn(responseEntity);
    }

    @DisplayName("Test Rest")
    @Test
    public void getHello() throws Exception {
        assertEquals("Hello Mockito Rest", responseEntity.getBody());
    }


}
