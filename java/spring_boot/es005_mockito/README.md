# es005_mockito

Tratto e modificato liberamente da **[tutorial](https://mkyong.com/spring-boot/spring-boot-junit-5-mockito/)**.

# moduli

- `Spring Boot DevTools`
- `Spring Web `

# testing con Gradle

```
dependencies {
	...
	testImplementation 'org.mockito:mockito-core:4.3.1'
	testImplementation 'org.junit.jupiter:junit-jupiter-api:5.8.2'
	testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.8.2'
}
```

## settings IDEA

Al solito: "compila progetto automaticamente" e su Gradle "esegui test con IDEA".

## JUnit 5 test

L'*annotation* `@SpringBootTest` **[API](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/test/context/SpringBootTest.html)** indica una classe pensata per i test in Spring Boot
ovvero con tutti i requisiti per poterli fare. Per il resto procediamo come in un normale Test!

## Mockito

Con Mockito le cose diventano molto più interessanti.
```java
@Mock
private HelloRepository helloRepository;
```
creiamo il *mock* usano l'apposita annotation! Un altro *mock* iniettando il precedente
```java
@InjectMocks 
private HelloService helloService = new HelloServiceImpl();
```
a questo punto *stubbing*
```java
@BeforeEach
void setMockOutput() {
    when(helloRepository.get()).thenReturn("Hello Mockito From Repository");
}
```
il test vero e proprio non merita ulteriori attenzioni!

## Test del server REST

Il primo test va a vedere solo se funziona la nostra classe `HelloController` originale, senza *mock*, la classe si chiama
`HelloControllerOriginalTest`.
```java
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
...
@LocalServerPort
private int port;
```
ci permette di lavorare con una porta *random*. Un oggetto `TestRestTemplate` **[API](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/test/web/client/TestRestTemplate.html)**. `RestTemplate` **[API}](https://docs.spring.io/spring-framework/docs/5.3.16/javadoc-api/org/springframework/web/client/RestTemplate.html?is-external=true)**
è un *client* sincrono per realizzare richieste HTTP, `TestRestTemplate` è la versione per i test. Il metodo `getForEntity()` ritorna una rappresentazione della risposta alla richiesta HTTP, a questo punto abbiamo una `ResponseEntity` **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/http/ResponseEntity.html)**
una `HttpEntity` come `RequestEntity` che ci permette di ricavare i dati della risposta.

## Test del server REST con mock service

Come nel caso precedente solo che qui creiamo un *mock* per *service* senza, per semplicità, salire al *repository*. Iniettiamo tale *service* nel controller. A differenze del test precedente qui il `TestRestTemplate`
è un *mock* quindi il test precedeuto dallo *stubbing*
```java
 @BeforeEach
void setMockOutput() {
    when(helloService.get()).thenReturn("Hello Mockito Rest");
    responseEntity = new ResponseEntity<>(helloController.hello(), HttpStatus.OK);
    when(restTemplate.getForEntity("localhost:8080/", String.class)).thenReturn(responseEntity);
}

@DisplayName("Test Rest")
@Test
public void getHello() throws Exception {
    assertEquals("Hello Mockito Rest", responseEntity.getBody());
}
```
il campo `responseEntity` la risposta del *controller*, il *server*, iniettata col *mock*, dall'altra parte quanto attende il *client*, anche questo un *mock*.

**NB** In genere si fa un test del *service* e non del server completo!
