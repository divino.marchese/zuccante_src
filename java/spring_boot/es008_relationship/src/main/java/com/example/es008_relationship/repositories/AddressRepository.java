package com.example.es008_relationship.repositories;

import com.example.es008_relationship.models.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {}
