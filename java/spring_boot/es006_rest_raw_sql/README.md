#es006_rest_raw_sql

Per questo esempio, oltre ai tutorial ufficiali ed ai precedenti esempi, siamo partiti da [qui](https://www.baeldung.com/jpa-insert), per i moduli
- **Spring Boot Dev Tools**
- **Spring Web**
- **Spring Data JPA: Hibernate e soci**
- **MariaDB Driver**
Niente template in quanto il nostro esempio è solo REST `json`!

## creazione del DB su MariaDB

Qui sotto le istruzioni per MariaDB (MySQL)
```
CREATE DATABASE es006SB;

GRANT ALL ON es006SB.* to 'es006SB'@localhost IDENTIFIED BY 'zuccante@2021';

SHOW GRANTS FOR 'es006SB'@localhost;
```
si può quindi `mysql -u es006SB -p`.

## configurazione

Modifichiamo il file `application.properrties`
```
# DB
spring.datasource.url=jdbc:mariadb://localhost:3306/es006SB
spring.datasource.username=es006SB
spring.datasource.password=zuccante@2021
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver

# hibernate
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.hibernate.use-new-id-generator-mappings=false
```

## entity

Molto semplice!
-`@GeneratedValue` ha come s*strategy* `AUTO` già usata negli altri esempi.

## il repository

Non un interfaccia ma una classe visto che scriveremo noi le *raw query*: sfruttiamo *Hibernate* direttamente: `EntityManager`, vedi anche [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#pc) e
[qui](https://docs.jboss.org/hibernate/jpa/2.2/api/javax/persistence/EntityManager.html) per le API. Spring offre gli *steretype*, [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/)
per le API. Per le *annotation* vedi [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#annotations).
- `@repository`: è un`@Component` fa riferimento ad oggetti **peristenti** ovvero ad *entity*; esso è in grado di dare `DataAccessException`.
- `@PersistenceContext` `EntityManafer`. ci permette di accedere alle *entity* in modo **persistente**, vedi [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#bootstrap-jpa-compliant-PersistenceContext-example)
API [qui](https://javaee.github.io/javaee-spec/javadocs/javax/persistence/PersistenceContext.html). Con esso individuiamo una *cache* ed una **connessione** sua propria e non condivisa. Buono anche [questo](https://www.baeldung.com/jpa-hibernate-persistence-conte) articolo.
  Grazie ad esso abbiamo una ***injection***!
  > Expresses a dependency on a container-managed EntityManager and its associated persistence context.
- `  @Transactional` [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/) per le API, si fa riferimento alla possibilità di gestione delle **transazioni**.

### native query

Il nostro esempio, appunto, usa SQL di MySQL/MariaDB: un oggetto `EntityManager` infatti ci dà la possibilità di lavorare in modo **nativo**, a tal proposito Hibernate offre una guida dettagliata [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#sql).
Per le API [qui](https://docs.jboss.org/hibernate/jpa/2.2/api/javax/persistence/EntityManager.html).
  
## controller

Abbiamo optato per un `@RestController` (non ci permette di usare la sintassi `return "redirect:...";`). Il `restController` è un `Controller` specializzato che permette di ritornare 
oggetti e liste che vengono *serializzate* in `json` senza fatica, come abbiamo visto nei precedenti esempi.

## test

```
curl -X POST -H "Content-Type: application/json" -d '{"firstName":"Bepi","lastName":"Sbrisa"}' http://localhost:8080/person/add
curl -X POST -H "Content-Type: application/json" -d '{"firstName":"Ermanno","lastName":"Rosegon"}' http://localhost:8080/person/add
```
sul client testuale di MariaDB
```
MariaDB [es006SB]> select * from person;
+----+------------+-----------+
| id | first_name | last_name |
+----+------------+-----------+
|  1 | Bepi       | Sbrisa    |
|  2 | Ermanno    | Rosegon   |
+----+------------+-----------+
2 rows in set (0.001 sec)
```
Altro test
```
curl localhost:8080/person/Antonio
```
ovvero
```
curl localhost:8080/person/antonio
```
e funziona lo stesso!