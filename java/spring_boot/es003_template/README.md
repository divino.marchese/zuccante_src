# es003 mustache

## moduli

- `Spring Boot Dev Tools`
- `Spring Web`
- `Mustache`

## le classi

- `Book` è il *model*.
- `BookController`e`il *rest controller* pensato per usare i template.
- `BockeService` è il solito *service* poi "iniettato".


## il template

Qui usiamo `ModelAndView` **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/servlet/ModelAndView.html)**
ci permette di realizzare l'MVC con i template.
> *Holder for both Model and View in the web MVC framework. Note that these are entirely distinct. This class merely holds both to make it possible for a controller to return both model and view in a single return value.*

Il costruttore [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/servlet/ModelAndView.html#ModelAndView-java.lang.String-))
```java
public ModelAndView(String viewName,
            @Nullable
            Map<String,?> model)
```
crea un `ModelView` associando un *model*, `viewName` indica il *template* da utilizzare; *model* è una mappa fra i nomi delle variabili nel template e gli oggetti ad esse associati.

Come *template* usiamo Mustache [home page](https://mustache.github.io/mustache.5.html), tratto dalla documentazione
```
Template:

{{#repo}}
  <b>{{name}}</b>
{{/repo}}

Hash:

{
  "repo": [
    { "name": "resque" },
    { "name": "hub" },
    { "name": "rip" }
  ]
}

Output:

<b>resque</b>
<b>hub</b>
<b>rip</b>

```