package com.example.es003_template;

import java.util.List;

public interface BookService {
    Book getBook(int bookId);
    List<Book> getBookRange(int start, int end);
}
