package com.example.es003_template;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class MyBookService implements BookService {

    private static final List<Book> bookList = IntStream.range(1,30)
            .mapToObj(Book::new)
            .collect(Collectors.toList());

    @Override
    public Book getBook(int bookId) {
        return bookList.get(bookId - 1);
    }

    @Override
    public List<Book> getBookRange(int start, int end) {
        return IntStream.range(start, end)
                .mapToObj(this::getBook)
                .collect(Collectors.toList());
    }
}
