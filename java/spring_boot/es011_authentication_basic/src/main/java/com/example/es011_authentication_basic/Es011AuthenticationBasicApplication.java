package com.example.es011_authentication_basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es011AuthenticationBasicApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es011AuthenticationBasicApplication.class, args);
	}

}
