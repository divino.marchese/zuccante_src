package com.example.es002_restful;

import java.util.HashSet;

public interface BookService {

    HashSet<Book> findAllBook();
    Book findBookByID(long id);
    void addBook(Book b);
    void deleteAllData();
}
