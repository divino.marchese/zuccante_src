package com.example.es002_restful;

public class Book {

    private long id;
    private String author;
    private String title;

    public Book() {}

    public Book(long id, String name, String title) {
        this.id = id;
        this.author = name;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
