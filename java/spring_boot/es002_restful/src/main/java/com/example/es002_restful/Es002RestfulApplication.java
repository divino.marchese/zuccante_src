package com.example.es002_restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es002RestfulApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es002RestfulApplication.class, args);
	}

}
