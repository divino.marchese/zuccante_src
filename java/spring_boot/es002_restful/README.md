# es002_restful

## moduli usati

- `Spring Web`
- `Spring Boot DevTools`

## le classi

- `Book` è la classe *model* un POJO cui non va aggiunto alcun costruttore!  
- `BookService` è *autowired* come *component*.
- `BookController` è il controller, vedi *annotation* `@RestController`! 

## annotation

- `@Service` **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Service.html)**: è un component legato alla *business logic*.
- `@RestController` **[API](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/bind/annotation/RestController.html)**: usa `@Controller` e `ResponseBody`,
il primo è un *component* ed indica il *controller* web nel classico pattern MVC, la seconda indica che il `return` dei metodi sono preparati per una risposta *web*.

## nota

Il server REST definito usando un `@RestController` lavora in `json` di default.

## POST

Useremo `curl` per fare i nostri esperimenti, il `POST` ed il `PUT` lo facciamo in `json`, dovremo pertanto associare l'apposito *header* alla richiesta `http`. Registriamo due studenti
```
curl -X POST -H "Content-Type: application/json" \
-d '{"id": 1, "title":"Il Nome della Rosa", "author":"U.Eco"}' \
http://localhost:8080/

curl -X POST -H "Content-Type: application/json" \
-d '{"id": 2, "title":"Delitto e Castigo", "author":"F.Dostoevsjij"}' \
http://localhost:8080/
```

## GET

Usando un browser o `curl` possiamo dare
```
curl http://127.0.0.1:8080/findall
```
o
```
curl http://127.0.0.1:8080/findbyid/2
```

## DELETE

```
curl -X DELETE http://localhost:8080/delete
```


## reboot del server

Il server viene riavviato in automatico usando `Spring Dev Tools`, vedi esempi precedenti.