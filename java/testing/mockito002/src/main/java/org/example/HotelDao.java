package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HotelDao {

    public List<String> fetchAvailableRooms() throws SQLException {
        List<String> availableRooms = new ArrayList<>();
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost/mydb", "zuccante", "zuccante@2023");
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM Rooms WHERE available = 1 ");
        while (rs.next()) {
            availableRooms.add(rs.getString("number"));
        }
        return availableRooms;
    }


}
