package org.example;

import java.sql.SQLException;
import java.util.List;

public class BookingManager {

        private final HotelDao dao;

        public BookingManager(HotelDao dao) {
            this.dao = dao;
        }

        public boolean checkRoomAvailability(String number) throws SQLException {
            List<String> roomsAvailable = dao.fetchAvailableRooms();
            return roomsAvailable.contains(number);
        }
}
