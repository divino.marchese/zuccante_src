package org.example;

public interface PasswordManager {
    String encode(String password);
}
