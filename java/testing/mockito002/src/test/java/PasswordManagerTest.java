import org.example.PasswordManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class PasswordManagerTest {

    @Mock
    PasswordManager mng, first, second;
    @Spy
    List<Integer> spyList;
    @Captor
    ArgumentCaptor<String> passwordCaptor;

    @Test
    void encryptionTest01() {
        when(mng.encode(anyString())).thenReturn("exact");

        assertEquals("exact", mng.encode("1"));
        assertEquals("exact", mng.encode("abc"));

        verify(mng).encode(or(eq("a"), endsWith("c"))); // OK
        verify(mng, timeout(500)).encode("abc");
        verify(mng, never()).encode("def");
    }

    @Test
    void encryptionTest02() {

        first.encode("pw1");
        second.encode("pw1");
        first.encode("pw2");

        InOrder inOrder = inOrder(first, second);
        inOrder.verify(first).encode("pw1");
        inOrder.verify(second).encode("pw1");
        inOrder.verify(first).encode("pw2");
    }

    @Test
    void spyTest() {
        when(spyList.get(anyInt())).thenReturn(123456);
        System.out.println(spyList.get(999));

        // stubbing using custom matcher (let's say isValid():
        ArgumentMatcher<Integer> isValid = n -> n > 2;
        when(spyList.contains(argThat(isValid))).thenReturn(true);

        // you can also verify using an argument matcher
        verify(spyList).get(anyInt());

    }

    @Test
    void mockTest06() {
        mng.encode("password1");
        mng.encode("password2");
        mng.encode("password3");
        verify(mng, times(3)).encode(passwordCaptor.capture());
        assertEquals(List.of("password1", "password2", "password3"),
                passwordCaptor.getAllValues());
    }
}
