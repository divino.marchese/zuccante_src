import org.example.BookingManager;
import org.example.HotelDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class BookingManagerTest {

    @Mock
    private HotelDao hotelDao;
    @InjectMocks
    private BookingManager bookingManager;

    @BeforeEach
    public void setup() throws SQLException {

        List<String> availableRooms = List.of("128B", "128", "12");
        when(hotelDao.fetchAvailableRooms()).thenReturn(availableRooms);

    }

    @Test
    public void bookingManagerExists() {
        assertNotNull(bookingManager);
    }

    @Test
    public void checkAvailableRoomsTrue() throws SQLException {
        assertTrue(bookingManager.checkRoomAvailability("128B"));
    }

    @Test
    public void checkAvailableRoomsFalse() throws SQLException {
        assertFalse(bookingManager.checkRoomAvailability("128A"));
    }
}
