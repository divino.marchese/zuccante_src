# mockito002

**[230520]** Questo secondo esempio, più semplice del precedente, offre un caso d'uso di un qualche interesse.

# il progetto

Come già visto in precedenza abbiamo aggiunto su `build.gradle` le seguenti righe (dopo JUnit 5)
```groovy
implementation 'org.postgresql:postgresql:42.6.0'
implementation 'org.mockito:mockito-core:5.3.1'
testImplementation 'org.mockito:mockito-junit-jupiter:5.3.1'
```
## la libreria `sql` di Java, alcuni richiami

PostgreSQL usa un suo driver open source, per i dettagli (vedi anche Gradle sopra) rimandiamo a [1].
Il resto del codice sul DAO è di facile lettura: la solita connessione ed il risultato di ricerca.
Quando opereremo col test naturalmente il DB non ci srà e nemmeno la connessione.

## il test

Molto semplice, creando un *mock* del DAO con gli *stub* non ci connettiamo a PostgreSQL. Al solito aggiungiamo la solita
*annotation* per la gestione degli *stub* (vedi `mock001`).  
In `PasswordManagerTest` cogliamo l'occasione per vedere cosa puo`fare Mockito un po' più in profondità: interessante risulta l'*argument watcher*, l'*argument captor*
e i vari connettivi logici (vedasi anche materiali).


# materiali

[1] JDBC per PostgreSQL [qui](https://jdbc.postgresql.org/documentation/), per Gradle [qui](https://mvnrepository.com/artifact/org.postgresql/postgresql/42.6.0) (attuale versione). 
[2] "Mockito ArgumentMatchers" [qui](https://www.baeldung.com/mockito-argument-matchers).  
[3] "Using Mockito ArgumentCapto" [qui](https://www.baeldung.com/mockito-argumentcaptor).  
[4] API *argument capure* [qui](https://site.mockito.org/javadoc/current/org/mockito/ArgumentCaptor.html).  
[5] Un buon tutorial su *argument captor* [qui](https://www.davidvlijmincx.com/posts/mockito_argumentcaptor/).  


