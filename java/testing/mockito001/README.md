# mockito001

**[230520]** Questo esempio è tratto da [8] e [5] con alcune leggere modifiche per i soliti scopi didattici.

## il progetto

- Junit 5 è già a posto!
- Per Mockito invece seguiamo quanto indicato [qui](https://central.sonatype.com/artifact/org.mockito/mockito-core/5.3.1).
- Nel nostro esempio faremo convivere Mockito e JUnit 5 usando un modulo, vedi [qui](https://www.javadoc.io/static/org.mockito/mockito-junit-jupiter/3.3.3/org/mockito/junit/jupiter/MockitoExtension.html)
ecco il secondo inserimento di Gradle e l'uso di `@ExtendWith(MockitoExtension.class)`.

Ecco cosa abbiamo inserito di Gradle (le prime due righe sono già presenti)
```groovy
dependencies {
    testImplementation platform('org.junit:junit-bom:5.9.1')
    testImplementation 'org.junit.jupiter:junit-jupiter'
    implementation 'org.mockito:mockito-core:5.3.1'
    testImplementation 'org.mockito:mockito-junit-jupiter:5.3.1'
}
```

## le classi

`BookService` è un servizio che beneficia di una *dependency injection* di un oggetto `BookRepository` 
che rappresenta il nostro database (repository appunto). Non possedendolo qui ci viene in aiuto Mockito.

## mock e stub

Senza sottilizzare come lavorando con gli *stub* noi siamo interessati a "cosa accade", mentre coi *mock* siamo interessati 
anche al "come accade": in quale ordine sono stati chiamati i metodi, se sono stati chiamati i metodi ecc.. I *mock* hanno dietro una programmazione articolata
come nel nostro caso per le classi che forniscono il *provider* ed il *service*. Su StackOverflow viene proposta una interessante discussione
[qui](https://stackoverflow.com/questions/3459287/whats-the-difference-between-a-mock-stub).

## i test

Usiamo
```java
@MockitoSettings(strictness = Strictness.LENIENT)
```
in modo da poter fare *unecessary stubbing* in `@BeforeEach`.

# materiali

[1] Mockito home page [qui](https://site.mockito.org/).  
[2] Mockito Reference Support [qui](https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html).  
[3] Mockito su DZone [qui](https://dzone.com/refcardz/mockito).  
[4] "Mockito and JUnit 5" [qui](https://www.baeldung.com/mockito-junit-5-extension).  
[5] Il tutorial di Vogella [qui](https://www.vogella.com/tutorials/Mockito/article.html).  
[6] "Strict and Unnecessary Stubbing" [qui](https://www.baeldung.com/mockito-unnecessary-stubbing-exception).  
[7] "Mocks aren't Stubs" di Martin Fowler [qui](https://martinfowler.com/articles/mocksArentStubs.html).  
[8] Il codice da cui abbiamo tratto ispirazione: [qui](https://github.com/dinesh-varyani/mockito/tree/master/src/test/java/com/hubberspot/mockito).