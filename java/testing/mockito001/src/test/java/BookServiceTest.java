import org.example.Book;
import org.example.BookRepository;
import org.example.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class BookServiceTest {

    @InjectMocks
    private BookService bookService;

    @Mock
    private BookRepository bookRepository;

    @BeforeEach
    void init() {
        when(bookService.getBook(12)).thenReturn(new Book(12, "Il nome della rosa", 15));
        when(bookService.getBook(13)).thenReturn(new Book(13, "Toni Bueghin, una vita allo Zuccante", 12));
        when(bookService.getBook(666)).thenThrow(new IllegalArgumentException("book does not exist"));
    }

    @Test
    public void testService() {
        assertNotNull(bookService);
    }

    @Test
    public void testBook() {
        assertEquals(12, bookService.getBook(12).getBookId());
    }

    @Test
    public void testAddBook() {
        Book book = new Book(1, "Mockito In Action", 600);
        bookService.addBook(book);
        verify(bookRepository).save(book);
    }

    @Test
    public void testUpdatePrice() {
        Book book = new Book(1234, "Mockito is the best", 50);
        when(bookRepository.findBookById(1234)).thenReturn(book);
        bookService.updatePrice(1234, 500);
        verify(bookRepository).findBookById(1234);
        verify(bookRepository).save(book);
        verifyNoMoreInteractions(bookRepository);
    }

    @Test
    public void testException() {
        RuntimeException exception = assertThrows(IllegalArgumentException.class, () -> bookService.getBook(666));
        assertEquals("book does not exist", exception.getMessage());
    }


}
