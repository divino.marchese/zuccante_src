package org.example;

import java.util.List;

public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }

    public Book getBook(int idBook) {
        return bookRepository.findBookById(idBook);
    }

    public List<Book> getAllBooks() {
        return bookRepository.getAllBooks();
    }


    public void updatePrice(int idBook, int price) {
        Book book = bookRepository.findBookById(idBook);
        book.setPrice(price);
        bookRepository.save(book);
    }
}
