package org.example;

import java.util.List;

public interface BookRepository {
    void save(Book book);
    Book findBookById(int idBook);
    List<Book> getAllBooks();
}
