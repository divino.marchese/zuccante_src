import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ControlHardwareTest {

    ControlHardware hw;

    @BeforeEach
    void init() {
        hw = new MockControlHardware();
    }

    @Test
    public void turnOnCoolerAndBlower() {
        hw.tooHot();
        assertEquals("hBChl", hw.getState());
    }

    @Test
    public void turnOnHeaterAndBlower() {
        hw.tooCold();
        assertEquals("HBchl", hw.getState());
    }

    @Test
    public void turnOnHiTemperatureAlarmThreshold() {
        hw.wayTooHot();
        assertEquals("hBCHl", hw.getState());
    }

    @Test
    public void turnOnLoTemperatureAlarmThreshold() {
        hw.wayTooCold();
        assertEquals("HBchL", hw.getState());
    }

}
