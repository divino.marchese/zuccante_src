public class MockControlHardware implements ControlHardware {

    private boolean heater = false;
    private boolean blower = false;
    private boolean cooler = false;
    private boolean hiTempAlarm = false;
    private boolean loTempAlarm = false;

    public String getState() {
        String state = "";
        state += heater ? "H" : "h";
        state += blower ? "B" : "b";
        state += cooler ? "C" : "c";
        state += hiTempAlarm ? "H" : "h";
        state += loTempAlarm ? "L"  : 'l';
        return state;
    }

    @Override
    public void tooHot() {
        blower = cooler = true;
    }

    @Override
    public void tooCold() {
        blower = heater = true;
    }

    @Override
    public void wayTooHot() {
        hiTempAlarm = blower = cooler = true;
    }

    @Override
    public void wayTooCold() {
        loTempAlarm = blower = heater = true;
    }

}
