public interface ControlHardware {
    String getState();
    void tooHot();
    void tooCold();
    void wayTooHot();
    void wayTooCold();
}
