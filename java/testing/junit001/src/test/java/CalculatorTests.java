import org.example.Calculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTests {

    Calculator calculator;

    @BeforeEach
    void setUp() {
        System.out.println("TAG: @BeforeEach");
        calculator = new Calculator();
    }


    @Test
    @DisplayName("1 + 1 = 2")
    void addsTwoNumbers() {
        assertEquals(2, calculator.add(1, 1), "1 + 1 should equal 2");
    }

    @ParameterizedTest
    @DisplayName("test add")
    @MethodSource("addFixture")
    void add(int a, int b, int result) {
        assertEquals(result, calculator.add(a, b));
    }

    private static Stream<Arguments> addFixture() {
        return Stream.of(
                Arguments.of(1, 2, 3),
                Arguments.of(4, -4, 0),
                Arguments.of(-3, -3, -6)
        );
    }

    @ParameterizedTest(name = "{index} o-> {0} + {1} =  {2}")
    @CsvSource({
            "0, 1, 1",
            "1, 2, 3",
            "49, 51, 100",
            "1, 100, 101"
    })
    void addParams(int first, int second, int expectedResult) {
        Calculator calculator = new Calculator();
        assertEquals(expectedResult, calculator.add(first, second),
                () -> first + " + " + second + " should equal " + expectedResult);
    }

    @ParameterizedTest(name = "{index} --> {0} + {1} =  {2}")
    @CsvFileSource(resources = "my-test.csv", numLinesToSkip = 1)
    void addParamsCSV(int first, int second, int expectedResult) {
        Calculator calculator = new Calculator();
        assertEquals(expectedResult, calculator.add(first, second),
                () -> first + " + " + second + " should equal " + expectedResult);
    }
}
