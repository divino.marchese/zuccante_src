import org.example.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyFirstTest {

    private final Calculator calculator = new Calculator();

    @Test
    public void addition() {
        assertEquals(3, calculator.add(1, 2));
    }
}
