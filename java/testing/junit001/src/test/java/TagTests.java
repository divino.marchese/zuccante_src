import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class TagTests {

    @Tag("zero")
    @Test
    void acceptance_test1() {
        System.out.println("ZERO");
    }

    @Tag("uno")
    @Test
    void acceptance_test2() {
        System.out.println("UNO");
    }
}
