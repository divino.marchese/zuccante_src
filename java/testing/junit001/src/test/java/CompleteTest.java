import org.example.Calculator;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompleteTest {

    Calculator calculator;

    @BeforeAll
    static void setup(){
        System.out.println("@BeforeAll executed");
    }

    @BeforeEach
    void init() {
        System.out.println("@BeforeEach executed");
        calculator = new Calculator();
    }

    @Tag("DEV")
    @Test
    void testCalcOne() {
        System.out.println("====== EST ONE EXECUTED ======");
        assertEquals( 4 , calculator.add(2, 2));
    }

    @Tag("PROD")
    @Disabled
    @Test
    void testCalcTwo() {
        System.out.println("====== TEST TWO EXECUTED =======");
        assertEquals( 6 , calculator.add(2, 4));
    }

    @AfterEach
    void tearThis() {
        System.out.println("@AfterEach executed");
    }

    @AfterAll
    static void tear(){
        System.out.println("@AfterAll executed");
    }
}
