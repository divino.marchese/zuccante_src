# junit001

QUi in questo progetto una serie di esempi.

## il progetto

Dando uno sguardo [qui](https://junit.org/junit5/docs/current/user-guide/#running-tests-build) vediamo che INTELLIK già ci
prepara tutto per il lavoro, vedi anche [3]. Come al solito creiamo un progetto `git` eliminando il repositori (cartella `.git`).

## i test con INTELLIJ

Si fanno in modo molto semplice (ricordare di fare il check della spunta nella finestra di solito in basso a sinistra).

## `MyFirstTest`

È il nostro "Hello World".

## `CalculatorTests`

Introduce ulteriori elementi fra cui i test parametrici in varie salse, andare alla documentazione per dettagli ed ulteriori esempi, qui abbiamo proposto solo alcuni casi interessanti che possono
creare qualche difficoltà, [qui](https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests).

## `RagTests`

Qui abbiamo bisogni di creare un nuovo profilo (a sinistra del triangolo di esecuzione, in alto).
Aggiungiamo un profilo JUnit in cui selezioniamo la JRE e il modulo (nel nostro progetto), scegliamo nell'apposito menù a discesa, TAG;
[qui](https://junit.org/junit5/docs/current/user-guide/#writing-tests-tagging-and-filtering) nella documentazione.

## `CompleteTest`

Offre uno schema di test completo.


# riferimenti

[1] "JUnit 5 User Guide" DALLA DOCUMENTAZIONE UFFICIALE [QUI](https://junit.org/junit5/docs/current/user-guide/).  
[2] "JUnit 5 tutorial - Learn how to write unit tests" di Lars Vogel [qui](https://www.vogella.com/tutorials/JUnit/article.html).  
[3] Junit 5 per INTELLIJ dalla documentazione ufficiale [qui](https://www.jetbrains.com/help/idea/junit.html).  