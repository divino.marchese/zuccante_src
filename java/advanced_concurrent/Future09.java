import java.util.concurrent.CompletableFuture;

public class Future09 {

    public static void main(String[] args) {

        String name = null;
        CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {
            if (name == null) {
                throw new RuntimeException("Bad computation!");
            }
            return "Hello, " + name;
        }).handle((s,t) -> s != null ? s : "Hello, Stranger!");
        try {
            System.out.println(cf.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

}
