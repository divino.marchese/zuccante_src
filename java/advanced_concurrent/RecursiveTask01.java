import java.util.concurrent.RecursiveTask;

public class RecursiveTask01 {

    public static void main(String[] args) {
        int n = 5;
        FactorialSquareCalculator f = new FactorialSquareCalculator(n);
        System.out.println(n + " factorial is " + f.compute());
    }

}

class FactorialSquareCalculator extends RecursiveTask<Integer> {

    private Integer n;

    public FactorialSquareCalculator(Integer n) {
        this.n = n;
    }

    @Override
    protected Integer compute() {
        if (n <= 1) {
            return n;
        }
        
        FactorialSquareCalculator calculator = new FactorialSquareCalculator(n - 1);
        
        calculator.fork();

        return n * n + calculator.join();
    }
}