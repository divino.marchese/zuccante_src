import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class DataProcessor implements Callable {

    static Random rnd = new Random();

    @Override
    public String call() {
        System.out.println("Processing data...");
        int delay = 1+ rnd.nextInt(5);
        try {
            TimeUnit.SECONDS.sleep(delay);   
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Data is processed";
    }
}