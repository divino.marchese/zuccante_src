import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class Future10 {

	public static void main(String[] args)  { 
		
		MyRunnable myrunnableobject1 = new MyRunnable(2000); 
		MyRunnable myrunnableobject2 = new MyRunnable(3000); 

		FutureTask<String> futureTask1 = new FutureTask<>(myrunnableobject1, "FutureTask1 is complete"); 
		FutureTask<String> futureTask2 = new FutureTask<>(myrunnableobject2, "FutureTask2 is complete"); 

		ExecutorService executor = Executors.newFixedThreadPool(2); 

		executor.submit(futureTask1);
		executor.submit(futureTask2); 

		while (true) { 
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
			if (futureTask1.isDone() && futureTask2.isDone()) { 
				System.out.println("Both FutureTask Complete"); 
                break;
			} 
			if (!futureTask1.isDone()) {
                System.out.println("futureTask1 is not complete");
            }
            if (!futureTask2.isDone()) {
                System.out.println("futureTask2 is not complete");
            } 

        } 
        
        try {
            System.out.println("futureTask1 -> " + futureTask1.get());
            System.out.println("futureTask2 -> " + futureTask2.get(1, TimeUnit.SECONDS));
        } catch (TimeoutException e) {
            System.out.println("TIMEOUT");
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("executor shut down");
        executor.shutdown();
	} 
} 


class MyRunnable implements Runnable { 

	private final long waitTime; 

	public MyRunnable(int timeInMillis) { 
		this.waitTime = timeInMillis; 
	} 

	@Override
	public void run() { 
		try { 
			Thread.sleep(waitTime); 
			System.out.println(Thread .currentThread().getName()); 
		} 
		catch (InterruptedException e) { 
            e.printStackTrace();
		} 
	} 
} 


