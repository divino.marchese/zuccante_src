import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Future05 {

    public static void main(String[] args) throws Exception {

        Future<String> fut = calculateAsync();

        while (!fut.isDone()) {
            System.out.println("Calculating...");
            Thread.sleep(500);
        }

        String result = fut.get();
        System.out.println("result:" + result);

    }

    static Future<String> calculateAsync() {
        CompletableFuture<String> completableFuture = new CompletableFuture<>();

        ExecutorService executor = Executors.newCachedThreadPool();

        executor.submit(() -> {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
            completableFuture.complete("Hello");
            return null;
        });
        executor.shutdown();
        return completableFuture;
    }

}
