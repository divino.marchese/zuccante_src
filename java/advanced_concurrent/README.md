# advanced async programming

In sostanza qui parleremo di programmazione asincrona.

## I `Future`

Un `Future` è una computazione asincrona, esistono in `Fart` e in JavaScriopt su parla di `Promise`
> A Future represents the result of an asynchronous computation

dalle API [qui](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Future.html) Java implementa l'interfaccia `Future` in varie classi. La base su cui corre un `Future` è un `Executor` (che come ben sappiamo incapsula come risorsa di calcolo i `Thread`). `Executors` ci fornisce, API [qui](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Executors.html), tutta una possibilità di *factory method* per creare *executor* che Java chiama in questo contesto `ExecutorSrevice`, API [qui](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html). Un esempio ci rende chiaro
``` java
ExecutorService executorService = Executors.newSingleThreadExecutor();

Callable<String> callable = () -> {        
    System.out.println("Entered Callable");
    Thread.sleep(2000);
    return "Hello from Callable";
};

Future<String> future = executorService.submit(callable);
```
il valore della computazione si ottiene mediante (evitiamo per pulizia il `try ... catch`)
``` java
String value = future.get();
```
Le `CompletableFuture` permetteranno di mettere in catena le computazioni asincrone in modo *functional*.

**[Future01-05]**

## `FutureTask`

Può essere creato su di un `Runnuble` dando come secondo argomento del costruttore il risultato oppure su di un `Callable`.
> A cancellable asynchronous computation. This class provides a base implementation of Future, with methods to start and cancel a computation, query to see if the computation is complete, and retrieve the result of the computation. 

**[Future10]**


## `CompletableFuture`

E' un `Future` e così possiamo usarla infatti, ma che apre a tutta una serie di possibilità che un `Future` non possiede.
> When two or more threads attempt to complete, completeExceptionally, or cancel a CompletableFuture, only one of them succeeds. 

La possibilità di creare flussi e convergenze per giungere a risultati di computazioni asincrone, in perfetto *functional style*, ci viene data dall'interfaccia
`CompletionStage` che `CompletableFuture` implementa.

Ecco come possiamo crearla
``` java
CompletableFuture.supplyAsync(() -> "Hello");
```
`Consumable` per il *lambda* 
> Represents an operation that accepts a single input argument and returns no result
In tal caso il *task* viene sottomesso sal *common pool* (viene usato se non dichiarato esplicitamente un *pool* e solo la terminazione del processo lo termina); alternativamente, come secondo argomento, passiamo un `Executor`.

### `thenApplay`

API [qui](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletableFuture.html#thenApply-java.util.function.Function-)
``` java
public <U> CompletableFuture<U> thenApply(Function<? super T,? extends U> fn)
```
Quindi un *pype*. ecco un esempio
``` java
CompletableFuture.supplyAsync(() -> "Hello").thenApply(s -> s + " World");
```
Mappa in modo **sincrono** ilrisultato su di mediante una funzione: utile per trasformare uno step finale di catena di operazioni `CompletableStage`.

### `thenCompose`

Simile al precedente, API [qui](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/CompletionStage.html#thenCompose(java.util.function.Function))
``` java
public <U> CompletableFuture<U> thenCompose(Function<? super T,? extends CompletionStage<U>> fn)
```
ed ecco un esempio esplicativo
``` java
CompletableFuture.supplyAsync(() -> "Hello")
     .thenCompose(s -> CompletableFuture.supplyAsync(() -> s + " World"));
```
questo metodo è un metodo di composizione: una `Future` ottenuta da una `Function` che riceve una `Future`
> returns a new CompletionStage that is completed with the same value as the CompletionStage returned by the given function

### `thenCombine`

Ancora una variante sul tema; le API [qui](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/CompletionStage.html#thenCombine(java.util.concurrent.CompletionStage,java.util.function.BiFunction))
``` java
public <U,V> CompletableFuture<V> thenCombine(CompletionStage<? extends U> other,
                                              BiFunction<? super T,? super U,? extends V> fn)
```
ed ecco un esempio chiarificature in cui si ha la combinazione
``` java
CompletableFuture.supplyAsync(() -> "Hello")
    .thenCombine(CompletableFuture.supplyAsync(
      () -> " World"), (s1, s2) -> s1 + s2));
```
Ad differenza del precedente, sequenziale, una sorta di *pipe* asincrono, qui abbiamo un calcolo possibilmente parallelo che converge mediante la *bifunction* ad un risultato.
> Returns a new CompletionStage that, when this and the other given stage both complete normally, is executed with the two results as arguments to the supplied function.

### `allOf`

Per seguire computazioni in parallelo possiamo usare anche
``` java
public static CompletableFuture<Void> allOf(CompletableFuture<?>... cfs)
```
ecco un esempio
``` java
CompletableFuture<String> future1  = CompletableFuture.supplyAsync(() -> "Fante");
CompletableFuture<String> future2  = CompletableFuture.supplyAsync(() -> "Cavallo");
CompletableFuture<String> future3  = CompletableFuture.supplyAsync(() -> "Re");

CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(future1, future2, future3);
```
Più semplice di `thenCombine` visto prima.

### versioni `async`

I metodi del tipo `... thenApplyAsync(...)` fanno lavorare il tutto su di un altro *worker thread* (vedi esempi).

### la gestione degli errori

`CompletableStage` ha il metodo `handle(...)`, API [qui](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/CompletionStage.html#handle(java.util.function.BiFunction)). Ecco un esempio
``` java
tring name = null;
    CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {
        if (name == null) {
            throw new RuntimeException("Bad computation!");
        }
        return "Hello, " + name;
    }).handle((s,t) -> s != null ? s : "Hello, Stranger!");
    try {
        System.out.println(cf.get());
    } catch (Exception e) {
        e.printStackTrace();
}
```
**[Future06-09]**

## `ForkJoinPool` e `ForkJoinTask`

Un `ExecutorService` pensato appunto per i `ForkJoinTask`, anche lui mette a disposizione il metodo `statoc` `commonPool()` che di solito è più che sufficiente. `ForkJoinTask` è classe astratta che si concretozza in `RecursiAvction` ed in `RecursiveTask` entrambe sono `Future`.
> > A ForkJoinTask is a lightweight form of Future

### `RecursiveTask`

Ritorna sempre un valore ed implementa, come nei processi alla C, il pattern `fork .. join`. Nella API, [qui](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html), troviamo un classico esempio
``` java
class Fibonacci extends RecursiveTask<Integer> {
   final int n;
   Fibonacci(int n) { this.n = n; }
   Integer compute() {
     if (n <= 1)
       return n;
     Fibonacci f1 = new Fibonacci(n - 1);
     f1.fork();
     Fibonacci f2 = new Fibonacci(n - 2);
     return f2.compute() + f1.join();
   }
 }
```
**[RecursiveTask01-03]**

### `RecursiveAction`

Non ritorna un risultato.

**[RecursiveAction01-02]**

