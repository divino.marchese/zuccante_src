package org.example;

import java.sql.*;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mariadb://localhost:3306/users",
                    "zuccante", "zuccante"
            );
            String query = "SELECT * FROM users;";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    String email = resultSet.getString("email");
                    System.out.printf("name: %s, email: %s\n", name, email);
                }
                connection.close();
                System.out.println("Bye Bye");
            }
        } catch (SQLException e) {
            System.out.println("mariadb drivers problem");
        }
    }
}