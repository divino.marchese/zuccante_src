# es001_mariadb

## creazione del db

Per creare il database diamo le seguenti istruzioni SQL.
```sql
grant all privileges on users.* to 'zuccante'@'%
' identified by 'zuccante';

create table users( id int auto_increment, name varchar(19) not null, email varchar(19) not null, primary key(id) );

insert into users(name, email) values ("ciccio",
"ciccio@gmail.com"), ("maria", "mary@gmail.com"), ("nane", "nane.onto@gmail.com");
```
Garantiti i privilegi l'utente viene creato! Nelle vecchie versioni di MySQL si dava anche esplicitamente i privilegi all'utente su `localhost`.


## modifica di `build.gradle`

Dobbiamo aggiungere il *driver* e il *logger*
```groovy
dependencies {
    implementation 'org.mariadb.jdbc:mariadb-java-client:3.4.1'
    implementation 'org.slf4j:slf4j-nop:2.0.13'
    testImplementation platform('org.junit:junit-bom:5.10.3')
    testImplementation 'org.junit.jupiter:junit-jupiter'
}
```
`sfl4j` non è poi necessario ma evita il *warning*.

## riferimenti

[1] Java Connector Using Gradle [qui](https://mariadb.com/kb/en/java-connector-using-gradle/).  
[1] MySQL tutorial [qui](https://www.mysqltutorial.org/).  
[3] slf4j [qui](https://slf4j.org/).