@FunctionalInterface
public interface Incrementer {
    int increment();
}
