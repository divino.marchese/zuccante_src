@FunctionalInterface
public interface Algebra {

    int operate(int a, int b);
    
}
