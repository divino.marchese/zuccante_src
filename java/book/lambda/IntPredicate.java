@FunctionalInterface
public interface IntPredicate {
    boolean check(int i);
}