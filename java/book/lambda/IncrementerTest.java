public class IncrementerTest {
    
    int val = 12;
    Incrementer inc = () -> {
        return val++;  // is this.val
    };

    public static void main(String[] args) {

        Test t = new Test();
        System.out.println("before: " + t.inc.increment());
        System.out.println("after: " + t.val);

    }
    
}
