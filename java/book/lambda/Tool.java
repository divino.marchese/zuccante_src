@FunctionalInterface
public interface Tool {

    String action();

}