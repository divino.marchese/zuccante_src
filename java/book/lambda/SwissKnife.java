public class SwissKnife {

    public static void main(String[] args) {

        SwissKnife knife = new SwissKnife();
        Tool tool1 = () -> "usa la lama";
        knife.doSomething(tool1);
        knife.doSomething(() -> "usa la forbice");

    }

    public void doSomething(Tool tool) {
        System.out.println(tool.action());
    }

}