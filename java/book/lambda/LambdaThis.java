public class LambaThis {

    String msg = "test message";
    Getter getMsg = () -> msg; // very cool

    public static void main(String[] args) {

        LambaThis test = new LambaThis();
        System.out.println(test.coolMethod(test.getMsg));
    }

    public String coolMethod(Getter g) {
        return g.get();
    }

}

@FunctionalInterface
interface Getter {
    String get();
}
