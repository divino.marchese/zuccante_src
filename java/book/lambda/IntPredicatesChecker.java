public class IntPredicatesChecker {
    
    public static boolean isPositive(int n) {
        return n > 0;
    }

    public static boolean isEven(int n) {
        return (n % 2) == 0;
    }

    private IntPredicatesChecker(){}
}
