public class LambdaFinal {

        public static void main(String[] args) {

        LambdaFinal test = new LambdaFinal();
        String myString = "bla bla bla";
        Getter getter = () -> myString;
        System.out.println(test.coolMethod(getter));
        // myString = "maramao";
    }

    public String coolMethod(Getter g) {
        return g.get();
    }

}

@FunctionalInterface
interface Getter {
    String get();
}
