import java.io.Console;

public class SumConsole {
    
    public static void main (String[] args){
        
        Console console = System.console();
        int num1 = 0;
        int num2 = 0;
        // primo addendo
        System.out.println("inserisci il primo addendo");
        try {
            num1 = Integer.parseInt(console.readLine());
        } catch (NumberFormatException e){
            System.err.println("hai inserito male il primo addendo");
            // e.printStackTrace();
            System.exit(1);
        }
        // secondo addendo
        System.out.println("inserisci il secondo addendo");
        try {
            num2 = Integer.parseInt(console.readLine());
        } catch (NumberFormatException e){
            System.err.println("hai inserito male il secondo addendo");
            System.exit(1);
        }
        int somma = num1 + num2;
        System.out.println(somma);
    }
}