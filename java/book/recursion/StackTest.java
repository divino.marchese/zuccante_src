import java.util.Stack;

public class StackTest {

    static int stackFact(int n) {
        int ret = 1; // step 0 value
        Stack<Integer> stack = new Stack<>();
        for(int k = n; k > 0; k--){
            stack.push(k);
        }
        while(!stack.empty()){
            ret = ret * stack.pop();
        }
        return ret;
    }
 
    public static void main(String[] args) {
        System.out.println("Il fattoriale di 5 è ...");
        System.out.println(stackFact(5));
        System.out.println("Il fattoriale di 0 è ...");
        System.out.println(stackFact(0));
        System.out.println("Il fattoriale di 1 è ...");
        System.out.println(stackFact(1));
    }
    
}
