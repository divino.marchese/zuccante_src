import java.util.concurrent.TimeUnit;

public class Missile {

    public void countDown(int n) throws InterruptedException {
        if(n == 0) return;
        System.out.println(n + "...");
        // wait a second
        TimeUnit.SECONDS.sleep(1);
        countDown(n-1);
    }

    public static void main(String[] args) {
        Missile atlas = new Missile();
        try {
            atlas.countDown(10);    
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}