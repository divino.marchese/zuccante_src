public class MyRadio implements Radio {

    public static final int MIN = 0;
    public static final int MAX = 300;

    public static final int RADIOUNO = 50;
    public static final int RTL = 102;
    public static final int RADIOMARIA = 55;

    private boolean isOn = false;
    private int freq = 0;
    private float volume = 0;

    public void turnOn() {
        isOn = true;
    }

    public void turnOff() {
        isOn = false;
    }

    public void popUpVolume(float up){
        volume += up;
        if (volume > 10.0f) volume = 10;
        if (volume < 0.0f) volume = 0;
    }

    public int getFreq() {
        return freq;
    }

    public void tune(int freq) {

        if (!isOn) {
            System.out.println("zz z zzzz z z");
            return;
        }

        freq += tFreq;
        if (freq < MIN) freq = MIN;
        if (freq > MAX) freq = MAX;

        switch(freq) {
            case RADIOUNO:
              System.out.println("Salve, sei su RADIO UNO, LA LALLALA LA");
              return;
            case RTL:
              System.out.println("Salve, sei su RTL TUNZ TUNZ TUNZ");
              return;
            case RADIOMARIA:
              System.out.println("Salve, sei su RADIOMARIA OO OOOOO OOO");
              return;
            default:
              System.out.println("BZ BZBZB ZBZ");
              return;
        }
    }
    
}
