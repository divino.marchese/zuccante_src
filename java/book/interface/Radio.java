public interface Radio {

    void turnOn(); // accende radio
    void turnOff(); // spegne radio
    int getFreq(); // legge la frequenza
    void popUpVolume(float popUp); // aumenta il volume
    void tune(int freq); // tuning
    
}
