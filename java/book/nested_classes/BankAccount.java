import java.util.*;
import java.text.SimpleDateFormat;

public class BankAccount {

    private static int ncc = 0;
    private int n;
    private String owner;
    private float balance = 0.0f;
    private List<Transaction> history = new ArrayList<>();

    public BankAccount(String name) {
        n = ++ncc;
        owner = name;
    }

    public float getBalance() {
        return balance;
    }

    public int getNumber() {
        return n;
    }

    public List<Transaction> getHistory() {
        return history;
    }

    // a factory method
    public Transaction makeTransaction(int type, float money) {
        return new Transaction(type, money);
    }

    public void printHistory() {
        // write on file history_123.txt
    }

    class Transaction {

        static final int WITHDRAWAL = -1;
        static final int TRANSFER = 1;

        float money;
        int type;
        Date date;

        Transaction(int type, float money) {
            this.money = money;
            this.type = type;
            this.date = new Date();
            // when we make a transaction we modify a bank account
            BankAccount.this.balance += type * money;
            BankAccount.this.history.add(this);
        }

        public String toString() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String date = sdf.format(this.date);
            String money = (type * this.money >= 0) ? "+" + this.money : "-" + this.money;
            return "date: " + date + " *** " + money + "\n";
        }
    }
}