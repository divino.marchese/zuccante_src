import java.io.FileWriter;
import java.io.IOException;

public class WriteFile {
    public static void main(String[] args) {
        try {
            FileWriter myWriter = new FileWriter("myFile.txt");
            myWriter.write("Questo è un testo di prova\n");
            myWriter.close();
            System.out.println("Successfully wrote to the file \"myFile.txt\".");
        } catch (IOException e) {
            System.out.println("An error occurred!");
            e.printStackTrace();
        }
    }
}