import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFileAgain {

    public static void main(String[] args) {
        File file = new File("myFile.txt");
        String content = "This is the text content";
        try (FileWriter fop = new FileWriter(file)) {
            if (!file.exists()) {
                file.createNewFile();
            }
            fop.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
