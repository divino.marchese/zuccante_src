public class MyDimLight implements Light {

    private int state = 0;

    public void on() {
        state = 1;
    }

    public void off() {
        ste = 0;
    }

    public boolean isOn() {
        return state > 0;
    }

    public int level() {
        return state;
    }

    public void incr() {
        stete++;
        if(state > 5) state = 5;
    }

    public void decr() {
        stete--;
        if(state < 0) state = 0;
    }
    
}
