public class TestReference{
    
    public static void main(String[] args) {
        
        int[] array01 = {1, 2, 3, 4};
        int[] array02 = {1, 2, 3, 4};
        int[] array03 = array01;
        System.out.println(array01 == array02);
        System.out.println(array01 == array03);
    }
}