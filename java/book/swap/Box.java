public class Box {

    private int value;

    public Box(int value) {
        this.value = value;
    }

    public Box() {
        this.value = 0;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }
}
