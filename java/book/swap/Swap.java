public class Swap {
    
    static void badSwap(int a, int b){
        int t = a;
        a = b;
        b = t;
    }
    
    static void badSwap(Box a, Box b){
        Box t = new Box();
        t = a;
        a = b;
        b = t;
    }
    
    static void goodSwap(Box a, box b){
        Box t = new Box();
        t.setValue(a.getValue());
        a.setValue(b.getValue());
        b.setValue(t.getValue());
       
    }
    
    public static void main(String[] args){   
        int num1, num2;
        num1 = 5;
        num2 = 6;
        System.out.println("num1 contiene " +  num1);
        System.out.println("num2 contiene " +  num2);
        System.out.println("bad swap");
        badSwap(num1, num2);
        System.out.println("num1 contiene " +  num1);
        System.out.println("num2 contiene " +  num2);
        System.out.println("*************************************");
        Box b1 = new Box(5);
        Box b2 = new Box(6);
        System.out.println("b1.getValue(): " +  b1.getValue);
        System.out.println("b2.getValue(): " +  b2.getValue);
        System.out.println("good swap:");
        goodSwap(b1, b2);
        System.out.println("b1.getValue(): " +  b1.getValue);
        System.out.println("b2.getValue(): " +  b2.getValue);
    }    
}