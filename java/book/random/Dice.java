import java.util.Random;

public class Dice {   

    private static final Random rnd = new Random();
    
    private int value;
    
    public Dice(int upperFace){
        value = upperFace;
    }
    
    public void roll() {
        value = 1 + rnd.nextInt(6);
    }

    public int getUpperFace() {
        return value;
    }
    
}	