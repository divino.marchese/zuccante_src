import java.util.Random;

public class Monster implements Fighter {

    private static Random rand = new Random();

    private final String name;
    protected final MColor color;
    protected int power = 0; 

    public Monster(String name){
        this.name = name;
        this.color = MColor.RED;
    }

    public Monster(String name, MColor color){
        this.name = name;
        this.color = color;
    }

    public static Monster getRandomMonster(String name, MColor color) {
        if(rand.nextBoolean())
            return new Pterox(name, color);
        else
            return new Tyros(name, color);
    }

    public String getName() {
        return name;
    }

    public MColor getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public int fight(Monster enemy) {
        if(power > enemy.power) {
            enemy.power--;
            power++;
            return Fighter.WIN;
        }
        if(power == enemy.power) {
            return Fighter.DRAW;
        } else {
            enemy.power++;
            power--;
            return Fighter.LOSE;
        }
    }

    public String toString() {
        return name + " is a " + getClass().getSimpleName(); 
    }
}   
