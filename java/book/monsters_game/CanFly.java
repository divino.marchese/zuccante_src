public interface CanFly {
    
    public void fly(int flightLevel);
}
