import java.awt.Color;

public class MonsterTest01 {
    
    public static void main(String[] args) {

        Monster m1 = new Monster("grgr1", MColor.BLUE);
        m1.setPower(10);
        Monster m2 = new Monster("grgr2", MColor.BLUE);
        System.out.println(m1.fight(m2) == 1 ? "the winner is m1" : "the winner is not m1");
         
    }
}
