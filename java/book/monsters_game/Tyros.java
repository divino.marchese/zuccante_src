public class Tyros extends Monster implements CanRun {

    private int velocity;

    public Tyros(String name, MColor color, int velocity) {
        super(name, color);
        this.velocity = velocity;
    }

    public Tyros(String name, MColor color) {
        this(name, color, 0);
    }

    public int getVelocity() {
        return velocity;
    }

    public void run(int velocity) {
        this.velocity = velocity; 
    }

    @Override
    public int fight(Monster enemy) {
        if(enemy.getClass() == Tyros.class) {
            if(velocity == ((Tyros)enemy).velocity) {
                super.fight(enemy);
            } else if(velocity > ((Tyros)enemy).velocity) {
                power++;
                return Fighter.WIN;
            } else {
                power--;
                return Fighter.LOSE;
            }
        } 
        if((enemy.getClass() == Pterox.class && velocity == 0) || color == MColor.RED) {
            super.fight(enemy);
        } 
        return Fighter.DRAW;        
    }    
}
