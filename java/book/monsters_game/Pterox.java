public class Pterox extends Monster implements CanFly {

    private int flightLevel = 0;

    public Pterox(String name, MColor color, int flightLevel) {
        super(name, color);
        this.flightLevel = flightLevel;
    }

    public Pterox(String name, MColor color) {
        this(name, color, 100);
    }

    public int getFlightLevel() {
        return flightLevel;
    }

    public void fly(int flightLevel) {
        this.flightLevel = flightLevel;
    }

    @Override
    public int fight(Monster enemy) {
        if(enemy.getClass() == Pterox.class) {
            if(flightLevel == ((Pterox)enemy).flightLevel) {
                super.fight(enemy);
            } else {
                return Fighter.DRAW;
            }
        } 
        if(enemy.getClass() == Tyros.class && flightLevel == 0) {
            super.fight(enemy);
        } else {
            return Fighter.DRAW;
        }
        return Fighter.DRAW;
    }
}
