public interface CanRun {
    void run(int velocity);
}
