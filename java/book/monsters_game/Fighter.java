public interface Fighter {

    int fight(Monster enemy);

    int WIN = 1;
    int LOSE = -1;
    int DRAW = 0;
    
}
