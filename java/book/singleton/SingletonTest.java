public class SingletonTest {
    
    public static void main(String[] args) {
        
        Singleton sin1 = Singleton.getInstance();
        Singleton sin2 = Singleton.getInstance();
        Singleton sin3 = Singleton.getInstance();

        System.out.println(sin1);
        System.out.println(sin2);
        System.out.println(sin3);

        System.out.println(sin1 == sin2);
        System.out.println(sin1 == sin3);
        System.out.println(sin2 == sin3);
           
    }
}
