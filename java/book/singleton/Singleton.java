public class Singleton {
    
    private static Singleton single_instance = null;

    private String msg;

    private Singleton() {
        msg = "Hello I am a string part of singleton";
    }
    
    public static Singleton getInstance() {
        if (single_instance == null) {
            single_instance = new Singleton();
        } else {
            return single_instance;
        }
    }
}
