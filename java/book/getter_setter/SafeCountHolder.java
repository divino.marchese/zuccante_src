public class SafeCountHolder {
    
    private int count = 0;
    
    public synchronized int getCount() { 
        return count; 
    }
    public synchronized void setCount(int c) { 
        count = c; 
    }

    public synchronized void addCount(int a) {
        count =+ a;
    }

}
