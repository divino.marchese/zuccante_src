public class AnimalTest {
    
    public static void main(String[] args) {
        Cat myCat = new Cat();
        Animal myAnimal = myCat;
        System.out.println("myCat is a " + myCat.getClass().getSimpleName());
        System.out.println("myAnimal is a " + myCat.getClass().getSimpleName());
        myAnimal.classMethod();
        Cat.classMethod();
        myAnimal.sing();
    }
}
