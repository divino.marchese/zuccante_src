public class FieldTest {
    
    public static void main(String[] args) {
        
        SuperField obj1 = new SubField();
        SubField obj2 = new SubField();

        System.out.println("obj1 is a " + obj1.getClass().getSimpleName());
        System.out.println("obj2 is a " + obj1.getClass().getSimpleName());

        System.out.println(obj1.field);
        System.out.println(obj2.field);
        
        System.out.println(((SubField)obj1).field);
        // System.out.println(obj1.getField());
        System.out.println(((SubField)obj1).getField());
        System.out.println(obj2.getField());

    }
}
