public class Cat extends Animal {

    public static void classMethod() {
        System.out.println("a static method in Cat");
    }

    public void sing() {
        System.out.println(" ... miew ...");
    }

}
