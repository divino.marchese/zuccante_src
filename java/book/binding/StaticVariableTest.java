public class StaticVariableTest {
    
    public static void main(String[] args) {
        
        SuperStaticVariable obj1 = new SubStaticVariable();
        SubStaticVariable obj2 = new SubStaticVariable();
        
        System.out.println("obj1 is a " + obj1.getClass().getSimpleName());
        System.out.println("obj2 is a " + obj2.getClass().getSimpleName());

        System.out.println(obj1.staticField);
        System.out.println(((SubStaticVariable)obj1).staticField);
        System.out.println(obj2.staticField);
        
    }
}
