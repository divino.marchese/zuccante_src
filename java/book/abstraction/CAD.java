public class CAD {

    public static void main(String[] args) {
        
        // Shape shape = new Shape();

        System.out.println("***********************");
        Circle circle = new Circle();
        circle.setRadius(2.0f);
        System.out.println("circle area: " + circle.getArea());
        
        System.out.println("***********************");
        Square square = new Square(2.0f);
        System.out.println("square area: " + square.getArea());
        square.move(3.0f, 2.3f);
        square.rotate(180.0f);
        System.out.println("square position = (" + square.getX() + 
           ", " + square.getY() + "), angle = " + square.getAngle());
        
        System.out.println("***********************");
        Rectangle rectangle = new Rectangle(2.0f, 1.0f);
        System.out.println("rectangle area: " + rectangle.getArea());
        
        System.out.println("***********************");
        Shape anonym = new Shape() {

            private float field = 0.0f; 

            @Override
            public void draw() {
                System.out.println("drawed anonymous class shape");
            }
            
            @Override
            public float getArea() {
                return 666f;
            }

            public void setField(float value) {
                field = value;
            }
            
            public float getField() {
                return field;
            }

        };

        System.out.println("anonym shape has area: " + anonym.getArea());
        System.out.println("the class of anonym is: " + anonym.getClass().getName());
        
    }

}