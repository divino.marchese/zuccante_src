public abstract class Shape implements Drawable {

    private float x = 0.0f;
    private float y = 0.0f;
    private float angle = 0.0f;

    @Override
    public abstract void draw();

    public abstract float getArea();

    public void move(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void rotate(float rotation) {
        this.angle += rotation;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getAngle() {
        return angle;
    }
}