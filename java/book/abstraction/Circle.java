public final class Circle extends Shape {

    private float radius = 1.0f;

    @Override
    public void draw() {
        System.out.println("drawed circle");
    }

    @Override
    public float getArea() {
        return radius*radius*Costants.PI;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }
    
}