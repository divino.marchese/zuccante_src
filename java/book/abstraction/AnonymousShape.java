public class AnonymousShape {
    
    public static void main(String[] args) {
        
        Shape shape = new Shape() {

            private float field = 0.0f; 
        
            @Override
            public void draw() {
                System.out.println("drawed anonymous class shape");
            }
                    
            @Override
            public float getArea() {
                return 666f;
            }
        
            public void setField(float value) {
                field = value;
            }
                    
            public float getField() {
                return field;
            }
        
        };
    }
}
