public class SumArgs {
    
    public static void main (String[] args){
        // controllo sul numero di argomenti
        if (args.length < 2) {
            System.err.println("non stai passando il numero corretto di argomenti");
            System.exit(1);
        }
        int num1 = 0;
        int num2 = 0;
        try {
            num1 = Integer.parseInt(args[0]);
        } catch (NumberFormatException e){
            System.err.println("hai inserito male il primo addendo");
            // e.printStackTrace();
            System.exit(1);
        }
        try {
            num2 = Integer.parseInt(args[1]);
        } catch (NumberFormatException e){
            System.err.println("hai inserito male il secondo addendo");
            System.exit(1);
        }
        int somma = num1 + num2;
        System.out.println(somma);
    }
}	