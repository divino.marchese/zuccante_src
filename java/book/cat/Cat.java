public class Cat {
    
    public static int cats = 0;
    
    private String name = "<name>";
    private String color = "<color>";
    private int age = 0;
    private boolean gender = false; // true: female
    private int catNumber;

    String getName() {
        return this.name;
    }
    
    String getColor() {
        return this.color;
    }
    
    int getAge() {
        return this.age;
    }
    
    String getGender() {
        if(this.gender)
            return "female";
        else
            return "male";
    }

    Cat(){
        catNumber = ++cats;
    }
    
    Cat(String name, String color, int age, boolean gender){
        this();
        this.name = name;
        this.color = color;
        this.age = age;
        this.gender = gender;
    }
    
    public String toString(){
        return "{" + catNumber + ": " + name + "}";
    }
    
    boolean fight(Cat cat) {
        if(this.gender == cat.gender) {
            if(this.age >= cat.age)
                return true;
            else
                return false;
        }
        if(this.gender == false)
            return true;
        else
            return false;
    }
}
