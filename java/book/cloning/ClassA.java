public class ClassA {

    private int n = 6;
    private ClassB b;

    public ClassA(){
        b = new ClassB();
    }

    public ClassA(int n, ClassB b){
        this.n = n;
        this.b = b;
    }

    public ClassA(ClassA a) {
        this.n = a.n;
        ClassB b = a.b;
        this.b = b.clone();
    }

    public ClassA clone() {
        System.out.println("ClassA cloning");
        return new ClassA(n, b.clone());
    }
}
