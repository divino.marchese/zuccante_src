public class StringReferenceTest {
    public static void main(String[] args) {
        String s1 = "ciao";
        String s2 = "ciao";
        if(s1 == s2) {
            System.out.println("s1 e s2 sono reference di uno stesso oggetto String");
        }
        String s3 = new String("ciao");
        String s4 = new String("ciao");
        if(s3 == s4) {
            System.out.println("s3 e s4 sono reference di uno stesso oggetto String");
        } else {
            System.out.println("s3 e s4 sono reference a due oggetti diversi String");
        }
        if(s3.equals(s4)) {
            System.out.println("s3 e s4 sono due String eguali");
        }
    }
}
