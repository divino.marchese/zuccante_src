public class ClassC {
    
    private int n = 4;

    public ClassC() {}

    public ClassC(int n) {
        this.n = n;
    }

    public ClassC(ClassC c) {
        this.n = c.n;
    }

    public ClassC clone(){
        System.out.println("ClassC cloning");
        return new ClassC(n);
    }
}
