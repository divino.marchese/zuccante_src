public class ClassB {
    
    private int n = 5;
    private ClassC c;

    public ClassB(){
        c = new ClassC();
    }

    public ClassB(int n, ClassC c){
        this.n = n;
        this.c = c;
    }

    public ClassB(ClassB b) {
        this.n = b.n;
        ClassC c = b.c;
        this.c = c.clone();
    }

    public ClassB clone() {
        System.out.println("ClassB cloning");
        return new ClassB(n, c.clone());
    }
}
