# es011 animation custom event

Cercheremo di generare una semplice animazione lavorando con un timer che emette eventi nella coda AWT.
Tale approccio non segue i dettami della prassi di creazione e gestione degli eventi AWT.

## Lévento del timer

Come troviamo in [JDox][https://docs.oracle.com/en/java/javase/22/docs/api/java.desktop/java/awt/AWTEvent.html]
Ogni tipologia di eventi di AWT ha un ID, se noi ne definimao uno di nuovo lavoriamo con un ID maggiore di
`RESERVER_ID_MAX`

## ricezione e gestione degli eventi AWT per un `IComponent` ed i suoi figli

Scarsa documentazione ed esempi d'altro genere portano a malfunzionamenti. Vediamo qui sotto come abbiamo risolto la cosa

### il `Timer`

La classe è motlo semplice: il dettaglio interessante sta nel costruttore: un `Timer` verrà legato ad un `JComponent`.
```java
public Timer(int interval, JComponent component) {
    this.interval = interval;
    this.component = component;
}
```
in tal modo, anche, non chiamiamo la coda degli eventi AWT.
```java
public MyPanel() {
    Timer timer = new Timer(1000, this);
    timer.start();
}
```

### il `JComponent` `MyPanel`

Qui sta la criticità: il metodo `dispatchEvent()`, vedi [JDOC](https://docs.oracle.com/en/java/javase/22/docs/api/java.desktop/java/awt/Component.html#dispatchEvent(java.awt.AWTEvent))
richiede espressamente che sia `ProcessEvent()` a gestire gli eventi AWT!