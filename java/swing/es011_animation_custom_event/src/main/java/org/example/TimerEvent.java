package org.example;

import java.awt.*;

public class TimerEvent extends AWTEvent {

    public static final int ID = AWTEvent.RESERVED_ID_MAX + 1;

    public TimerEvent(Timer t) {
        super(t, ID);
    }

}
