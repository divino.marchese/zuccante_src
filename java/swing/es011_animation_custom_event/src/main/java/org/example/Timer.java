package org.example;

import javax.swing.*;

public class Timer implements Runnable {

    private int interval;
    private JComponent component;

    public Timer(int interval, JComponent component) {
        this.interval = interval;
        this.component = component;
    }

    public void start() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(interval);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
            TimerEvent event = new TimerEvent(this);
            component.dispatchEvent(event);
            System.out.printf("fire event of type %s \n", event.getClass().getSimpleName());
        }
    }
}
