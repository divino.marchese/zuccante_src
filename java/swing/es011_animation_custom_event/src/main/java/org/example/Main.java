import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;
import org.example.MyGUI;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        FlatLaf.setup(new FlatDarkLaf());
        SwingUtilities.invokeLater(()-> new MyGUI("es011 animation"));
    }
}
