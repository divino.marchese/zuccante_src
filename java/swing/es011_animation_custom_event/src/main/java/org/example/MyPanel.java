package org.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.Random;

public class MyPanel extends JPanel {

    private int y = 0;
    private List<Ellipse2D> circles = new ArrayList<>();
    private static final Random GENERATOR = new Random();
    private static final int SIZE = 9;

    public MyPanel() {
        Timer timer = new Timer(1000, this);
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.translate(0, y);
        for (Ellipse2D circle : circles) {
            g2.setColor(Color.WHITE);
            g2.draw(circle);
        }
    }

    @Override
    public void processEvent(AWTEvent e) {
        System.out.println("action event");
        if(e.getClass() == TimerEvent.class) {
            int x = GENERATOR.nextInt(getWidth());
            Ellipse2D circle = new Ellipse2D.Double(x, -y, SIZE, SIZE);
            circles.add(circle);
            y++;
            repaint();
        }
    }

}
