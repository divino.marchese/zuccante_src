package org.example;

import javax.swing.*;
import java.awt.*;

public class MyGUI extends JFrame {

    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 400;

    public MyGUI(String title) {
        super(title);
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        MyPanel panel = new MyPanel();
        // getContentPane().removeAll();
        getContentPane().add(panel);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
