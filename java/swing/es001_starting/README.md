# es001 starting

**FlatLaf** è una libreria open source per rinfrescare il tema di `swing` in chiave moderna.
In [1] si può osservare come personalizzare i componenti di `swing`. 

Come indicato in [1] il tema va applicato prima di creare la GUI.

`JFrame` [api](https://docs.oracle.com/javase/8/docs/api/javax/swing/JFrame.html), come il `Frame` in `awt`
contiene, oltre al *content pane* (contenuto) un titolo ed un bordo. Un `Frame` possiede un *layout* di *default*
`BorderLayout` possiede inoltre un `RootPane` che di solito non si tocca e quindi un `ContentPane`, 
i vari metodi come `add` tipici di un `Container`vengono sovrascritti - `Frame` e `JFrame` sono `Container` - 
in modo da agire sul *content pane*, pertaanto, se vogliamo aggiungere componenti al `JFrame`
non dobbiamo avere in mano il *content pane* mediante `getContentPane()`. 

`BorderLayout()`
Con questo *layout* `label.setPreferredSize(new Dimension(100, 50));` influenzerà soltanto l'altezza.

Per chiudere la finestra useremo in genere
```java
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
```
Nel nostro caso, volendo gestire in modo personalizzato la cosa facciamo ricorso ad `awt`e a `Frame`.

[1] `FlatLaf` [home](https://www.formdev.com/flatlaf/).  
[2] `FlatLaf` [GitHub](https://github.com/JFormDesigner/FlatLaf).  
[3] Java Doc per la versione 3.5 [qui](https://javadoc.io/doc/com.formdev/flatlaf/3.5/index.html). 
[4] "Using Top Level Containers" [qui](https://docs.oracle.com/javase/tutorial/uiswing/components/toplevel.html).  