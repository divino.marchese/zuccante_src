package org.example;

import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MyGUI extends JFrame {
    public MyGUI(String title) {
        super(title);
        setSize(200, 100);
        JLabel label = new JLabel("Hello World", JLabel.CENTER);
        label.setPreferredSize(new Dimension(100, 50));
        add(label, BorderLayout.NORTH);
        JButton button = new JButton("click");
        button.addActionListener((ActionEvent e) -> {
            label.setText("Button Clicked.");
            System.out.println("click");
        });
        add(button, BorderLayout.CENTER);
        setVisible(true);
        // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // awt
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.out.println(
                        getTitle() +
                                " says Bye-Bye!  " +
                                new java.util.Date());
                dispose();
            }
        });
    }

}
