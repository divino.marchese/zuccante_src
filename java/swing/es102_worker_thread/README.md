# Worker Thread

In `sqing` abbiamo
- `main`
- `awt` *event dispatch thread* (interazioni con la GUI e brevi task)
- `WorkerThread` eventuali per lunghi lavori in background!
Nell'animazione non abbiamo usato un `WorkerThread` qui ne creaimo uno che va a modificare l'asperro dell UI. Nelle [api](https://docs.oracle.com/javase/8/docs/api/javax/swing/SwingWorker.html) - ne consigliamo vivament la lettura - vediamo 
```
SwingWorker<T,V>
```
- `T` - il ruisultato finale
- `V` - il prodotto intermedio

## `doInBackground()`

Eseguito una sola volta (vedi api) in un *thread* in *background*.

## `done()`

Viene eseguito nell'*event dispatch thread* terminato `doInBackground()`.

## `get()`

Attende di ottenere il risultato offerto dalla computazione di `doInBackgroudn()`, se questo è terminato chiaramente non dovrebbe dare ritardo!

[1] "Concurrency in swing" [qui](https://docs.oracle.com/javase/tutorial/uiswing/concurrency/index.html).  
[2] "Multithreading in swing" di J.Purcell [qui](https://www.javacodegeeks.com/2012/12/multi-threading-in-java-swing-with-swingworker.html).  