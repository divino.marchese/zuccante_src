import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

public class WTTest extends JFrame {

    private JLabel label; 
    private SwingWorker<String,Void> sw = new SwingWorker<>() {
        protected String doInBackground() throws Exception {
            for(int i=0; i<=10; i++) {
                Thread.sleep(1000);
                System.out.println("task: " + i);
                label.setText("task: " + i);
            }
            return "finish";
        }
    
        protected void done(){
            try {
                String s = get();
                label.setText(s);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    };

    public WTTest(String title) {
        super(title);
        setSize(200, 60); 
        setLayout(new GridLayout(2, 1)); 
        label = new JLabel("Not Completed", JLabel.CENTER);
        add(label);
        JButton btn = new JButton("Start counter"); 
        btn.setPreferredSize(new Dimension(5, 5)); 
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                System.out.println( 
                    "Button clicked, thread started"); 
                    sw.execute();
            }
        });
        add(btn); 
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(()->new WTTest("es102 worker thread"));
    }
    
}