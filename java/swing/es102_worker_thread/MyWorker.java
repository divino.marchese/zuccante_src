import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

public class MyWorker extends SwingWorker<String, Void>{

    protected String doInBackground() throws Exception {
        // a long work
        for(int i=0; i<=10; i++) {
            Thread.sleep(1000);
        }
        return "finish";
    }

    protected void done(){
        try {
            String s = get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    
}