package org.example;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        FlatLaf.setup(new FlatDarkLaf());
        SwingUtilities.invokeLater(()-> new MyGUI("title"));
    }
}