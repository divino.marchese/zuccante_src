package org.example;

import javax.swing.*;
import java.awt.*;

public class MyGUI extends JFrame {
    public MyGUI(String title) {
        super(title);
        setSize(300, 300);
        JPanel panel = new JPanel();
        panel.setBounds(40,80,200,200);
        panel.setBackground(new Color(227, 197, 76));
        JButton b1 = new JButton("Button 1");
        b1.setBounds(50,100,80,30);
        b1.setBackground(new Color(32, 119, 131));
        JButton b2 = new JButton("Button 2");
        b2.setBounds(100,100,80,30);
        b2.setBackground(new Color(124, 66, 31));
        panel.add(b1); panel.add(b2);
        add(panel);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}
