# es006 JPanel

[api](https://docs.oracle.com/javase/8/docs/api/javax/swing/JPanel.html) di `JPanel`.
In questo esempio, usando
```java
setLayout(null);
```
abbiamo scelto per il posizionamento assoluto (no `BorderLayout`) effettuato con, ad esempio per il `JPanel`
```java
panel.setBounds(40, 80, 200, 200);
```