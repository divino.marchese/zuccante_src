package org.example;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;

import javax.swing.*;
import java.awt.*;

public class MyGUI extends JFrame {
    public MyGUI(String title) {
        super(title);
        setSize(200, 120);
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbcnt = new GridBagConstraints();
        setLayout(gbl);
        gbcnt.fill = GridBagConstraints.HORIZONTAL;

        gbcnt.gridx = 0;
        gbcnt.gridy = 0;
        add(new JButton("Linux"), gbcnt);

        gbcnt.gridx = 1;
        gbcnt.gridy = 0;
        add(new JButton("Windows"), gbcnt);

        gbcnt.ipady = 20;
        gbcnt.gridx = 0;
        gbcnt.gridy = 1;
        add(new JButton("UNIX"), gbcnt);

        gbcnt.gridx = 1;
        gbcnt.gridy = 1;
        add(new JButton("DOS"), gbcnt);

        gbcnt.gridx = 0;
        gbcnt.gridy = 2;
        gbcnt.gridwidth = 2;
        add(new JButton("Operation System"), gbcnt);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
