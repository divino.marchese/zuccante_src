package org.example;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;

import javax.swing.*;
import java.awt.*;

public class MyGUI extends JFrame {
    public MyGUI(String title) {
        super(title);
        setSize(180, 70);
        setLayout(new FlowLayout());
        JButton btn = new JButton("one");
        add(btn);
        btn = new JButton("two");
        add(btn);
        btn = new JButton("three");
        add(btn);
        btn = new JButton("four");
        add(btn);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
