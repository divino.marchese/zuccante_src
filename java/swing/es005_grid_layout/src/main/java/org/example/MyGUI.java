package org.example;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;

import javax.swing.*;
import java.awt.*;

public class MyGUI extends JFrame {
    public MyGUI(String title) {
        super(title);
        setSize(200, 90);
        setLayout(new GridLayout(3, 2));
        add(new JButton("Button A"));
        add(new JButton("Button B"));
        add(new JButton("Button C"));
        add(new JButton("Button D"));
        add(new JButton("Button E"));
        add(new JButton("Button F"));
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
