package org.example;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLaf;

import javax.swing.*;
import java.awt.*;

public class MyGUI extends JFrame {

    public MyGUI(String title) {
        super(title);

        setSize(300, 140);
        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints constraints = new GridBagConstraints();
        // constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);

        constraints.gridx = 0;
        constraints.gridy = 0;
        JLabel labelUsername = new JLabel("username: ");
        panel.add(labelUsername, constraints);

        constraints.gridx = 1;
        // constraints.gridy = 0;
        JTextField textUsername = new JTextField();
        int height = textUsername.getPreferredSize().height;
        textUsername.setMinimumSize(new Dimension(150, height));
        // the next does not work
        // textUsername.setPreferredSize(new Dimension(150, height));
        panel.add(textUsername, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        JLabel labelPassword = new JLabel("password: ");
        panel.add(labelPassword, constraints);

        constraints.gridx = 1;
        JPasswordField fieldPassword = new JPasswordField();
        fieldPassword.setPreferredSize(new Dimension(300, 0));
        fieldPassword.setMinimumSize(new Dimension(150, height));
        panel.add(fieldPassword, constraints);

        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        JButton buttonLogin = new JButton("Login");
        panel.add(buttonLogin, constraints);

        // set border for the panel
        panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "login panel"));


        // pack();
        setLocationRelativeTo(null);
        setContentPane(panel);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
