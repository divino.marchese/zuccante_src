# es003 box layout

Qui dobbiamo fare attenzione:
```java
setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
```
e non `this` che è la finestra e non il suo implicito *content pane*. Sulle dimensioni dei componenti - leggere [1] - 
`BoxLayout` e `SpringLayou` fanno attenzione a `setMaximumSize(...)`, per gli altri `setMinimumSize(...)` e
`setPreferredSize(...)` funzionano!

## riferimenti

[1] "Using Layout Managers" [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/using.html)