package org.example;

import javax.swing.*;
import java.awt.*;

public class MyGUI extends JFrame {
    public MyGUI(String title) {
        super(title);
        setSize(100, 90);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        JButton button = new JButton("one");
        button.setMaximumSize(new Dimension(100, 30));
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(button);
        button = new JButton("two");
        button.setMaximumSize(new Dimension(100, 30));
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(button);
        button = new JButton("three");
        button.setMaximumSize(new Dimension(100, 30));
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(button);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
