package org.example;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.awt.*;

public class Timer extends JComponent {

    private int interval;
    private EventQueue queue;

    public Timer(int interval) {
        queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
        listenerList = new EventListenerList();
        this.interval = interval;
    }

    public void start() {
        new Thread(() -> go()).start();
    }

    public void go() {
        while (true) {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TimerEvent event = new TimerEvent(this);
            queue.postEvent(event);
            System.out.println("fire a TimerEvent");
        }
    }

    @Override
    public void processEvent(AWTEvent evt) {
        if (evt instanceof TimerEvent) {
            TimerListener[] listeners = listenerList.getListeners(TimerListener.class);
            for(TimerListener listener : listeners) {
                listener.timeElapsed((TimerEvent) evt);
            }
        } else
            super.processEvent(evt);
    }


    public void addTimerListener(TimerListener listener) {
        listenerList.add(TimerListener.class, listener);
    }

}
