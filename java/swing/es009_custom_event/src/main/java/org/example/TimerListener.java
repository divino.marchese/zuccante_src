package org.example;

import java.util.EventListener;

public interface TimerListener extends EventListener {
    public void timeElapsed(TimerEvent event);
}
