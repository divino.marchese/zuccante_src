package org.example;

import javax.swing.*;
import java.awt.*;

public class MyJPanel extends JPanel implements TimerListener {


    private int n = 0;

    public MyJPanel() {
        Timer timer = new Timer(1000);
        timer.addTimerListener(this);
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.draw(new Rectangle((getWidth() - n)/2 , (getHeight() - n)/2, n, n));
    }

    @Override
    public void timeElapsed(TimerEvent event) {
        System.out.println("TimerEvent captured");
        n = (n + 10) % 250;
        repaint();
    }

}
