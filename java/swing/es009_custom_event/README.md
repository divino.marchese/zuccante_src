# es009 custom component

In questo esempio vogliamo semolicemente mostrare come scrivere `AWTEvent` *custom*.

## La classe `TimerEvent`

È il nostro evento personalizzato. Come richiesto ogni evento *custom* deve avere un `id` superiore a
`AWTEvent.RESERVED_ID_MAX`.

## Gestione degli eventi e classe `Timer`

Premesso che un evento `AWT` *custom* viene abilitato automaticamente in presenza di *listener* in un `JComponent`, vedi [JDOC](https://docs.oracle.com/en/java/javase/22/docs/api/java.desktop/java/awt/Component.html#enableEvents(long)),
prepariamo `Timer` come `JComponent`. Il contratto da seguire per gestire tali eventi lo troviamo nella documentazione della classe
`EventListenerList` [JDOC](https://docs.oracle.com/en/java/javase/22/docs/api/java.desktop/javax/swing/event/EventListenerList.html).

Il metodo `processEvent` di `Container` processa gli eventi ascoltati in questo `Container`, vedi [JDOC](https://docs.oracle.com/en/java/javase/22/docs/api/java.desktop/java/awt/Container.html#processEvent(java.awt.AWTEvent)).
