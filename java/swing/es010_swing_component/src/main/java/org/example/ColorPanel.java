package org.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;


public class ColorPanel extends JComponent {

    private static Random gen = new Random();
    private int oldValue = 5;
    private Color color;

    public ColorPanel(int width, int height) {
        this.color = Color.BLACK;
        oldValue = 0;
        setPreferredSize(new Dimension(width, height));
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                color = getRandomColor();
                repaint();
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(color);
        g.fillRect(0, 0, getWidth(), getHeight()); // fill the area with the color
    }

    private Color getRandomColor() {
        int newValue = Math.abs(gen.nextInt()) % 6;
        while(newValue == oldValue) {
            newValue = (newValue + 1) % 6;
        }
        System.out.println(newValue);
        switch (newValue) {
            case 0:
                return Color.CYAN;
            case 1:
                return Color.GREEN;
            case 2:
                return Color.YELLOW;
            case 3:
                return Color.GRAY;
            case 4:
                return Color.MAGENTA;
            case 5:
                return Color.ORANGE;
        }
        oldValue = newValue;
        return Color.WHITE;
    }
}
