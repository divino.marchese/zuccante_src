package com.example.es003_rest_mariadb;

import io.vertx.core.Future;
import io.vertx.core.VerticleBase;
import io.vertx.core.impl.future.SucceededFuture;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MainVerticle extends VerticleBase {

  private static final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

  ProductRepository repository;

  @Override
  public Future<?> start() {

    // *** init repository ****

    repository = ProductRepository.getInstance(vertx);

    // *** routers ***

    Router router = Router.router(vertx);
    router.route("/products/*").handler(BodyHandler.create());


    router.get("/")
      .respond(
        ctx -> Future.succeededFuture(new JsonObject().put("msg", " hello world")));

    router.get("/products")
      .respond(
        ctx -> getAllProducts());

    router.get("/count")
      .respond(
        ctx -> countProducts());

    router.get("/products/:id")
      .respond(
        ctx -> getProductById(ctx));

    router.post("/products").respond(ctx -> addProduct(ctx));

    router.put("/products").respond(ctx -> updateProduct(ctx));

    router.delete("/products/:id").respond(ctx -> deleteProduct(ctx));

    return vertx.createHttpServer().requestHandler(router).listen(8080);
  }

  // *** handlers ***

  private Future<List<Product>> getAllProducts() {
    return repository.getAllProducts(12);
  }

  private Future<Product> getProductById(RoutingContext ctx) {
    int id = Integer.parseInt(ctx.pathParam("id"));
    return repository.getProductById(id);
  }

  private Future<JsonObject> countProducts() {
    return repository.countProducts();
  }

  private Future<JsonObject> addProduct(RoutingContext ctx) {
    JsonObject jsonObj = ctx.body().asJsonObject();
    Product product = jsonObj.mapTo(Product.class);
    repository.addProduct(product);
    return new SucceededFuture<>(new JsonObject().put("msg", "product added"));
  }

  private Future<JsonObject> updateProduct(RoutingContext ctx) {
    JsonObject jsonObj = ctx.body().asJsonObject();
    Product product = jsonObj.mapTo(Product.class);
    repository.updateProduct(product);
    return new SucceededFuture<>(new JsonObject().put("msg", "product updated: " +
      product.getId()));
  }

  private Future<JsonObject> deleteProduct(RoutingContext ctx) {
    int id = Integer.parseInt(ctx.pathParam("id"));
    JsonObject ret = new JsonObject();
    repository.deleteProduct(id);
    return new SucceededFuture<>(new JsonObject().put("msg", "product updated: " +
      id));
  }

}
