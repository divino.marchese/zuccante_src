package com.example.es003_rest_mariadb;

public class Product {

  private int id;

  private String name;

  private float price;

  public Product(String name, float price) {
    this.name = name;
    this.price = price;
  }

  public Product() {}

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public float getPrice() {
    return this.price;
  }

  public void setId(int id) { this.id = id; }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(float price) {
    this.price = price;
  }
}
