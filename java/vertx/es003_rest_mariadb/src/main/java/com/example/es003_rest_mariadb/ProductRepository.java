package com.example.es003_rest_mariadb;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.jdbcclient.JDBCConnectOptions;
import io.vertx.jdbcclient.JDBCPool;
import io.vertx.sqlclient.Pool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.templates.RowMapper;
import io.vertx.sqlclient.templates.SqlTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.StreamSupport;

public class ProductRepository {
  private static final Logger logger = LoggerFactory.getLogger(ProductRepository.class);

  private Pool pool;

  // main row mapper
  private static final RowMapper<Product> ROW_PRODUCT_MAPPER = row -> {
    Product product = new Product();
    product.setId(row.getInteger("id"));
    product.setName(row.getString("name"));
    product.setPrice(row.getFloat("price"));
    logger.info("MAP");
    return product;
  };

  // hide constructor
  private ProductRepository() {
  }

  // pool initializer
  public static ProductRepository getInstance(Vertx vertx) {
    ProductRepository repository = new ProductRepository();
    repository.pool = JDBCPool.pool(
      vertx,
      new JDBCConnectOptions()
        .setJdbcUrl("jdbc:mariadb://localhost:3306/vertx")
        .setUser("root")
        .setPassword("zuccante"),
      new PoolOptions().
        setMaxSize(8).
        setName("pool")
    );
    return repository;
  }

  public Future<List<Product>> getAllProducts(int limit) {
    return SqlTemplate.forQuery(pool, "SELECT * FROM products LIMIT #{limit}").
      mapTo(ROW_PRODUCT_MAPPER).
      execute(Map.of("limit", limit)).
      map(set -> {
        List<Product> products = new ArrayList<>();
        StreamSupport.stream(set.spliterator(), false).forEach(products::add);
        logger.info("size {}", products.size());
        return products;
      }).
      onFailure(error -> {
        logger.info("FAILURE: get {} products", limit);
      }).
      onSuccess(products -> {
        logger.info("get {} products", limit);
      });
  }

  public Future<Product> getProductById(int id) {
    return SqlTemplate.forQuery(pool, "SELECT * FROM products WHERE id = #{id}").
      mapTo(ROW_PRODUCT_MAPPER).
      execute(Collections.singletonMap("id", id)).
      map(set -> {
        List<Product> products = new ArrayList<>();
        StreamSupport.stream(set.spliterator(), false).forEach(products::add);
        return products.getFirst();
      }).
      onFailure(error -> {
        logger.info("FAILURE get product {}", id);
      }).
      onSuccess(products -> {
        logger.info("get product {}", id);
      });
  }

  public Future<JsonObject> countProducts() {
    RowMapper<Integer> ROW_INTEGER_MAPPER = row -> row.getInteger("total");
    return SqlTemplate.forQuery(pool, "SELECT COUNT(*) AS total FROM products")
      .mapTo(ROW_INTEGER_MAPPER)
      .execute(Collections.emptyMap())
      .map(set -> {
        List<Integer> counts = new ArrayList<>();
        StreamSupport.stream(set.spliterator(), false).forEach(counts::add);
        return new JsonObject().put("count", counts.getFirst());
      }).
      onFailure(error -> {
        logger.info("FAILURE count products");
      }).
      onSuccess(products -> {
        logger.info("count products");
      });
  }

  public void addProduct(Product product) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("name", product.getName());
    parameters.put("price", product.getPrice());
    SqlTemplate.forUpdate(pool,
        "INSERT INTO products(name, price) VALUES (#{name}, #{price})")
      .execute(parameters).
      onFailure(error -> {
        logger.info("FAILURE add product");
      }).
      onSuccess(products -> {
        logger.info("add products");
      });
  }

  public void updateProduct(Product product) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("id", product.getId());
    parameters.put("name", product.getName());
    parameters.put("price", product.getPrice());
    SqlTemplate.forUpdate(pool,
        "UPDATE products SET name = #{name}, price = #{price} WHERE id = #{id}").
      execute(parameters).
      onFailure(error -> {
        logger.info("FAILURE update products");
      }).
      onSuccess(products -> {
        logger.info("update products");
      });
  }

  public void deleteProduct(int id) {
    SqlTemplate.forUpdate(pool,
        "DELETE FROM products WHERE id = #{id}").
      execute(Collections.singletonMap("id", id)).
      onFailure(error -> {
        logger.info("FAILURE elete product {}", id);
      }).
      onSuccess(products -> {
        logger.info("delete product {}", id);
      });

  }

}
