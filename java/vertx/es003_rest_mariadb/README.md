# es003 rest mariadb

Un primo esempio limitato che usa i *template* per le *query* superando
la necessità di un *ORM*. Moduli e driver (vedi configurazione Gradle)
- `vertx-jdbc-client`
- `vertx-web`
- `vertx-jdbc-client`
- `vertx-sql-client-templates` una *utility* per *query*
- `mariadb-java-client:3.5.1`
- `com.fasterxml.jackson.core:jackson-databind` Jackson *binding* (vedi **es001**)
- `ch.qos.logback:logback-classic:1.5.12` log4j [5]

## requisiti

Si rimanda ad `es002_mariadb` per i dettagli sul DB, qui riproponiamo un REST server minimale con accesso al DB MariaDB con *driver* JSBC.
Avviata il container `mariadb` entriamo con
```
docker exec -it mariadb bash
```
Prepariamo il db `vertx`
```
create table products ( id int not null auto_increment, name varchar(20) not null, price float not null, primary key(id));
```

## logger

In [5] vediamo una possibile inizializzazione del *logger*
```java
private static final Logger logger = LoggerFactory.getLogger();

public void truncateTable(String tableName) throws IOException {
  logger.info("truncating table `{}`", tableName);
  db.truncate(tableName);
}
```

## router

Come anticipato il controllo degli errori nel server lo evitiamo per ora. Il
`Router` viene inizializzato nelle modo seguente
```java
Router router = Router.router(vertx);
router.route("/products/*").handler(BodyHandler.create());
```
Nella seconda riga esponiamo il *body* solo alle richieste che rispetano il *pattern* indicato
(altrimenti il *body* risulterà `null` indipendentemente dalle richieste del *client*).

## repository e connessione al db

`ProductRepository` contiene:
- connessione e
- interfacciamento mediante la suddetta connessione col db.

Per le considerazioni sul *pool* di connessioni si rimanda ad **es002**.

Usando i *template* [2] una *query* presenta i vari stati:
- creazione del *template* con i parametri `#{...}`,
- *mapping*, `mapTo(..)` delle `Row` in `Product` tramite un `RowMapper<Product>` o un altro come nella *query* che conta i prodotti,
- esecuzione della *query*

Eseguita la *query*, nel caso di un template del tipo
```java
return SqlTemplate.forQuery(pool, ...).
  mapTo(ROW_PRODUCT_MAPPER).
  execute(Map.of(...)).
  map(set -> {
    List<Product> products = new ArrayList<>();
    StreamSupport.stream(set.spliterator(), false).forEach(products::add);
    logger.info("size {}", products.size());
        return products;
    }).
```
Nel quale recuperiamo dati lavoriamo in modo **asincrono** coi `Future`, la lista di `Product`
diviene un `Future` grazie al `map(...)`. Per altre *query* di aggiornamento lavoriamo come segue in modo **sincrono**
```java
SqlTemplate.forUpdate(pool,"...")
  .execute(parameters);
```
`forUpdate(..)` ritorna dopo `execute(..)` un `Future<SqlResult<Void>>`.

Tornando alle *query* `SqlTemplate.forQuery ... `, per il `map()`
finale possiamo procedere nel modo tradizionale.
```java
List<Product> products = new ArrayList<>();
repository.getAllProducts().
  onFailure(error -> { ... }).
  onSuccess(set -> {
    for (Product product : set) {
      products.add(product);
    }
  });
```
Anche usando la più compatta
```java
set.iterator().forEachRemaining(products::add);
```
Per il metodo `forEachRemaining(...)` rimandiamo a [JDOC](https://docs.oracle.com/en/java/javase/22/docs/api/java.base/java/util/Iterator.html#forEachRemaining(java.util.function.Consumer)).
Probabilmente è più efficiente (per un gran numero di dati)
```java
StreamSupport.stream(set.spliterator(), false).forEach(products::add);
```
Con questa soluzione [JDOC](https://docs.oracle.com/en/java/javase/22/docs/api/java.base/java/util/stream/StreamSupport.html#longStream(java.util.Spliterator.OfLong,boolean))
lavoriamo su dino `Stream` squenziale (vedi `false`).

## handler

Di solito realizzati in una classe a parte gestiscono le richieste del *client* usando
un *repository* ed i suoi metodi.

## test con `curl`

Andiamo sul *container*, il *client* è `mariadb`, e aggiungiamo al db vuoto due prodotti.
```
insert into products(name, price) values ("bagigi", 0.5);
insert into products(name, price) values ("patatine", 1.5);
```
Ora proviamo i metodi.

### GET

Recupero dati.
```
curl localhost:8080/products
curl localhost:8080/products/2
curl localhost:8080/count
```

### POST

Tentiamo un inserimento.
```
curl -X POST -H 'Content-Type: application/json' -d '{"id":0, "name":"pop corn", "price": 3.5}' localhost:8080/products
```
Vediamo sul *container*
```
MariaDB [vertx]> select * from products limit 10;
+----+----------+-------+
| id | name     | price |
+----+----------+-------+
|  1 | bagigi   |   0.5 |
|  2 | patatine |   1.5 |
|  3 | pop corn |   3.5 |
+----+----------+-------+
3 rows in set (0.001 sec)
```

### PUT

Tentiamo un aggiornamento.
```
curl -X PUT -H 'Content-Type: application/json' -d '{"id":3, "name":"pop corn", "price": 4.0}' localhost:8080/products
```
Vediamo sul *container*
```
MariaDB [vertx]> select * from products limit 10;
+----+----------+-------+
| id | name     | price |
+----+----------+-------+
|  1 | bagigi   |   0.5 |
|  2 | patatine |   1.5 |
|  3 | pop corn |   3.6 |
+----+----------+-------+
3 rows in set (0.000 sec)
```

### DELETE

Per terminare, la cancellazione.
```
curl -X DELETE localhost:8080/products/3
```
Vediamo sul *container*
```
MariaDB [vertx]> select * from products limit 10;
+----+----------+-------+
| id | name     | price |
+----+----------+-------+
|  1 | bagigi   |   0.5 |
|  2 | patatine |   1.5 |
+----+----------+-------+
2 rows in set (0.001 sec)
```

## materiali

[1] API [qui](https://vertx.io/docs/apidocs/).
[2] "SQL Client Templates" dalla documentazione ufficiale [qui](https://vertx.io/docs/vertx-sql-client-templates/java/).
[3] "How to build a reactive RESTful Web Service" su Medium [qui](https://limadelrey.medium.com/vert-x-4-how-to-build-a-reactive-restful-web-service-dd845e3d0b66).
[4] "Building RESTful APIs with Eclipse Vertx" su Medium [qui](https://itnext.io/building-restful-apis-with-eclipse-vertx-4ce89d8eeb74).
[5] "LogBack" manuale [qui](https://logback.qos.ch/setup.html#ide).
[6] "A Guide to Log4j and the" su Baldung [qui](https://www.baeldung.com/java-log4j-properties-guide).
[7] Un esempio di server REST su GitHub [qui](https://github.com/limadelrey/vertx-4-reactive-rest-api?tab=readme-ov-file).
