# es000 hello world

Abbiamo deciso di usare **Vert.X** 5.0  per la pulizia con cui scrive il codice. Per creare l'applicazione ed aggiungere eventuali moduli
[qui](https://start.vertx.io/) (scaricare il file `zip`, decomprimerlo ed aprirlo con IDEA).

## `VerticleBase`

- [JDOC](https://javadoc.io/doc/io.vertx/vertx-core/latest/io.vertx.core/io/vertx/core/VerticleBase.html)

La classe astratta principale; implementa l'interfaccia `Deployable` [JDOC](https://javadoc.io/doc/io.vertx/vertx-core/latest/io.vertx.core/io/vertx/core/Deployable.html).
La classe mette a disposizione il `protected vertx` per accedere alle *core API* di Vert.X. Il resto è facilmente leggibile.

## `HttpServerRequest`

- [JDOC](https://javadoc.io/doc/io.vertx/vertx-core/latest/io.vertx.core/io/vertx/core/http/HttpServerRequest.html)

Rappresenta la richiesta al *server* per come essa viene gestita dal *server medesimo*, viene gestita tramite un *handler*.
Il metodo `response()` propone la risposta del server. Si dia, inoltre, uno sguardo al metodo `listen()`

## `HttpServerResponse`

- [JDOC](https://javadoc.io/doc/io.vertx/vertx-core/latest/io.vertx.core/io/vertx/core/http/HttpServerResponse.html)

Il metodo `end(String chunk)` termina la risposta HTTP! Per mamdare una risposta di testo semplice
```java
return vertx.createHttpServer().requestHandler(req -> {
  req.response().putHeader("content-type", "text/plain").end("Hello World");
}).listen(...);
```
Nel nostro caso possiamo ritornare oggetti *json* nel modo seguente
```java
@Override
public Future<?> start() {
  return vertx.createHttpServer().requestHandler(req -> {
    req.response()
      .putHeader("content-type", "application/json")
      .end("{\"msg\": \"hello world\"}");
  }).listen(8888).onSuccess(http -> {
    System.out.println("HTTP server started on port 8888");
  });
}
```
Vert.X ci dà a disposizone `Json Object` [JDOC](https://javadoc.io/doc/io.vertx/vertx-core/latest/io.vertx.core/io/vertx/core/json/JsonObject.html).
Ricordiamoci di convertirlo a `String` con il suo `toString()`.

## `Future<T>`

- [JDOC](https://javadoc.io/doc/io.vertx/vertx-core/latest/io.vertx.core/io/vertx/core/Future.html)

Un'iterfaccia
> Represents the result of an action that may, or may not, have occurred yet.

## esecuzione del server usando Gradle

Su *gradle* (pannello laterale in IDEA): `application -> task - run`.

