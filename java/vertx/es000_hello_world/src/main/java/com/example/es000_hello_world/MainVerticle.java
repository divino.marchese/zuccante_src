package com.example.es000_hello_world;

import io.vertx.core.Future;
import io.vertx.core.VerticleBase;
import io.vertx.core.json.JsonObject;

public class MainVerticle extends VerticleBase {

  @Override
  public Future<?> start() {
    return vertx.createHttpServer().requestHandler(req -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("msg", "hello world");
      req.response()
        .putHeader("content-type", "application/json")
        .end(jsonObject.toString());
    }).listen(8888).onSuccess(http -> {
      System.out.println("HTTP server running");
    });
  }
}
