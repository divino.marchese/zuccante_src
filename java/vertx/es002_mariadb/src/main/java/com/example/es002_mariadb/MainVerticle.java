package com.example.es002_mariadb;

import io.vertx.core.Future;
import io.vertx.core.VerticleBase;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.jdbcclient.JDBCConnectOptions;
import io.vertx.jdbcclient.JDBCPool;
import io.vertx.sqlclient.Pool;
import io.vertx.sqlclient.PoolOptions;

public class MainVerticle extends VerticleBase {

  JsonArray jsonArray = new JsonArray();

  Router router = Router.router(vertx);

  @Override
  public Future<?> start() {
    // init connection pool
    Pool pool = JDBCPool.pool(
      vertx,
      new JDBCConnectOptions()
        .setJdbcUrl("jdbc:mariadb://localhost:3306/vertx")
        .setUser("root")
        .setPassword("zuccante"),
      new PoolOptions().
        setMaxSize(8).
        setName("pool")
    );

    router.get("/products")
      .handler(ctx -> {
        ctx.response()
          .setChunked(true)
          .setStatusCode(200)
          .end(jsonArray.toBuffer());
      });

    // get connection and query test
    pool
      .query("SELECT * FROM products")
      .execute()
      .onFailure(e -> {
        jsonArray.add(new JsonObject().put("msg", "error"));
      })
      .onSuccess(rows -> {
        rows.forEach(row -> {
          JsonObject json = new JsonObject();
          json.put("id", row.getInteger("id"));
          json.put("name", row.getString("name"));
          json.put("price", row.getFloat("price"));
          jsonArray.add(json);
        });
      });
    // the server
    return vertx.createHttpServer().requestHandler(router).listen(8080);
  }
}
