# es002 mariadb

Moduli
- `jdbc client`
- `vertx web`
Gradle
- `implementation("org.mariadb.jdbc:mariadb-java-client:3.5.1")`

Nel seguente progetto ci connetteremo e tenteremo una *query* a un db su di un *container* Docker.

## docker

Usiamo il file `compose.yaml`
```declarative
name: 'mariadb'
services:
  mariadb:
    container_name: mariadb
    image: mariadb:latest
    ports:
      - "3306:3306"
    environment:
      MARIADB_ROOT_PASSWORD: zuccante
```
Il file è incluso nel progetto, diamo `docker compose up -d` per lanciare l'immagine nel
*container* e `docker compose down` per spegnere il *container*. Diamo `docker exec -it mariadb bash` e
quindi `mariadb -u root -p` inserendo quindi la password specificata in `compose.yaml`. A questo punto possiamo lavorare.
```
create database vertx
```
entriamo con `use vertx` e creiamo la tabella
```
MariaDB [vertx]> create table products(
    -> id int not null auto_increment,
    -> name varchar(20) not null,
    -> price float not null,
    -> primary key(id));
```
quindi inseriamo un valore
```
MariaDB [vertx]> insert into products(name, price) values ("bagigi", 0.5);
MariaDB [vertx]> insert into products(name, price) values ("patatine", 1.5);
```
e testiamo l'inserimento avvenuto.
```
MariaDB [vertx]> select * from products;
+----+----------+-------+
| id | name     | price |
+----+----------+-------+
|  1 | bagigi   |   0.5 |
|  2 | patatine |   1.5 |
+----+----------+-------+

```

## la connessione

Usando JDBC facciamo uso dell'interfaccia n`JDBCPool` [jDOC](https://vertx.io/docs/apidocs/io/vertx/jdbcclient/JDBCPool.html)
in particolare del motodo `static` `pool(...)`. **NB** La documentazione porta un errore in quanto
```
static Pool pool(
  Vertx vertx,
  JDBCConnectOptions connectOptions,
  PoolOptions poolOptions)
```
Il tipo di ritorno è `Pool` [JDOC](https://vertx.io/docs/apidocs/io/vertx/reactivex/sqlclient/Pool.html). Il
*pool* ci permette di avere un certo numero di connessioni attive che, alla bisogna, possono essere riutilizzate.
`JDBCConnectOptions` tiene i dati di accesso alla risorsa [JDOC](https://vertx.io/docs/apidocs/io/vertx/jdbcclient/JDBCConnectOptions.html)
`PoolOptions` [JDOC](https://vertx.io/docs/apidocs/io/vertx/core/http/PoolOptions.html) tiene i dati di configurazione del *pool* di connessioni:
il massimo numero di connessioni è `4` come valore di *default* qui lo portiamo a `8`.

## le query e la risposta

Il metodo `query(String sql)` di `Pool`ritorna un oggetto di tipo `Query` [JDOC](https://vertx.io/docs/apidocs/io/vertx/sqlclient/Query.html)
> ovvero una connessione del *pool* vien presa per poi essere riconsegnata al *pool* una volta eseguita la *query*

Prepariamo quindi la risposta `json` in `jsonArray` come di seguito indicato
```java
.onFailure(e -> {
  jsonArray.add(new JsonObject().put("msg", "error"));
}).onSuccess(rows -> {
  rows.forEach(row -> {
    JsonObject json = new JsonObject();
    json.put("id", row.getInteger("id"));
    json.put("name", row.getString("name"));
    json.put("price", row.getFloat("price"));
    jsonArray.add(json);
  });
});
```

## test con `curl`

```
url localhost:8080/products
[{"id":1,"name":"bagigi","price":0.5},{"id":2,"name":"patatine","price":1.5}]
```

## riferimenti

[1] "Vert.x JDBC client" [qui](https://vertx.io/docs/vertx-jdbc-client/java/).
[2] "Using the asynchronous SQL client" [qui](https://vertx.io/blog/using-the-asynchronous-sql-client/).
[3] "Pool Datasource Implementation" dalla documentazione dui MariaDB [qui](https://mariadb.com/kb/en/pool-datasource-implementation/).
[4] "MySQL tutorial" [qui](https://www.mysqltutorial.org/).
[5] "vert-x3/vertx-jdbc-client" [qui](https://github.com/vert-x3/vertx-jdbc-client/blob/master/src/main/java/examples/JDBCSqlClientExamples.java).
