package com.example.es001_rest;

import java.util.concurrent.atomic.AtomicInteger;

public class Product {

  private static final AtomicInteger COUNTER
    = new AtomicInteger();

  private final int id;

  private String name;

  private float price;

  public Product(String name, float price) {
    this.id = COUNTER.getAndIncrement();
    this.name = name;
    this.price = price;
  }

  public Product() {
    this.id = COUNTER.getAndIncrement();
  }

  public int getId() {
      return id;
  }

  public String getName() {
    return name;
  }

  public float getPrice() {
    return price;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(float price) {
    this.price = price;
  }
}
