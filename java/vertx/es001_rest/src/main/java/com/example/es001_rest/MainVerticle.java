package com.example.es001_rest;

import io.vertx.core.Future;
import io.vertx.core.VerticleBase;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class MainVerticle extends VerticleBase {

  @Override
  public Future<?> start() {

    Repository.initProducts(); // init products

    // routers

    Router router = Router.router(vertx);
    router.route().handler(BodyHandler.create());
    // router.route("products/*").handler(BodyHandler.create());
    
    router.get("/")
      .respond(
        ctx -> Future.succeededFuture(new JsonObject().put("msg", " hello world")));

    router.get("/products")
      .respond(
        ctx -> Future.succeededFuture(getAllProducts()));

    router.get("/products/:id")
      .respond(
        ctx -> Future.succeededFuture(getProduct(ctx)));

    router.post("/products").respond(ctx -> Future.succeededFuture(
      addProduct(ctx)));

    router.put("/products/:id").respond(ctx -> Future.succeededFuture(
      updateProduct(ctx)));

    router.delete("/products/:id").respond(ctx -> Future.succeededFuture(
      deleteProduct(ctx)));

    return vertx.createHttpServer().requestHandler(router).listen(8080);
  }

  // handlers

  private Product[] getAllProducts() {
    return Repository.getAllProducts();
  }

  private Product getProduct(RoutingContext ctx) {
    int id = Integer.parseInt(ctx.pathParam("id"));
    return Repository.getProduct(id);
  }

  private Product addProduct(RoutingContext ctx) {
    JsonObject jsonObj = ctx.body().asJsonObject();
    Product product = jsonObj.mapTo(Product.class);
    return Repository.addProduct(product);
  }

  private Product updateProduct(RoutingContext ctx) {
    int id = Integer.parseInt(ctx.pathParam("id"));
    JsonObject jsonObj = ctx.body().asJsonObject();
    Product product = jsonObj.mapTo(Product.class);
    return Repository.updateProduct(id, product);
  }

  private Product deleteProduct(RoutingContext ctx) {
    int id = Integer.parseInt(ctx.pathParam("id"));
    Product product = Repository.getProduct(id);
    return Repository.deleteProduct(id);
  }

}
