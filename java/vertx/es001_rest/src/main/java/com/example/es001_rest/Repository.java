package com.example.es001_rest;

import java.util.ArrayList;
import java.util.List;

public class Repository {

  private static final List<Product> products = new ArrayList<>();

  public static void initProducts() {
    products.add(new Product("bagigi", 0.5f));
    products.add(new Product("patatine", 0.3f));
    products.add(new Product("fighi", 1.0f));
    products.add(new Product("seme col sal", 0.4f));
  }

  public static Product[] getAllProducts() {
    return products.toArray(new Product[0]);
  }

  public static Product getProduct(int id) {
    return products.stream().filter(p -> p.getId() == id).findFirst().get();
  }

  public static Product addProduct(Product product) {
    Product ret = new Product();
    ret.setName(product.getName());
    ret.setPrice(product.getPrice());
    products.add(ret);
    return ret;
  }

  public static Product updateProduct(int id, Product product) {
    /*
    for(Product item: products) {
      if(item.getId() == id) {
        item.setName(product.getName());
        item.setPrice(product.getPrice());
        return;
      }
    }
    */
    Product ret = getProduct(id);
    ret.setName(product.getName());
    ret.setPrice(product.getPrice());
    return ret;
  }

  public static Product deleteProduct(int id) {
    Product ret = getProduct(id);
    products.remove(ret);
    return ret;
  }
}
