# es001 rest

Moduli importati:
- `vertx-web`
Modifiche a gradle
- `implementation("com.fasterxml.jackson.core:jackson-databind:2.13.4.2")`

## routing

Dalla documentazione
```java
HttpServer server = vertx.createHttpServer();

Router router = Router.router(vertx);

router.route().handler(ctx -> {

  // This handler will be called for every request
  HttpServerResponse response = ctx.response();
  response.putHeader("content-type", "text/plain");

  // Write to the response and end it
  response.end("Hello World from Vert.x-Web!");
});

server.requestHandler(router).listen(8080);
```
Il modulo `Vert.x-Web` fra ele varie cose, permette agevolmente il *routing* gestendolo anche a grana molto fine come si può
notare leggendo al documentazione.
```java
Router router = Router.router(vertx);
router.route().handler(BodyHandler.create());
```
anche e meglio
```java
Router router = Router.router(vertx);
router.route("products/*").handler(BodyHandler.create());
```
`Router` è interfaccia [JDOC](https://vertx.io/docs/apidocs/io/vertx/ext/web/Router.html). L'oggetto `router` istanziato, il metodo `route()` ci permette di ottenere un oggetto `Route`
[JDOC](https://vertx.io/docs/apidocs/io/vertx/ext/web/Route.html) grazie al quale possiamo applicate un *handler* a ntutte le richieste
```
handler(Handler<RoutingContext> requestHandler)
```
I metodi `get()`, `post()`, `put()`, `delete*()`, `patch()`, `head()` sono anloghi a `rpute()`
ma per specifiche richieste. Vendiamo quindi  a `respond()` di `Route` [JDOC](https://vertx.io/docs/apidocs/io/vertx/ext/web/Route.html#respond-java.util.function.Function-)
```
respond(java.util.function.Function<RoutingContext,Future<T>> function)
```
`Function<T,R>` [JDOC](https://docs.oracle.com/en/java/javase/22/docs/api/java.base/java/util/function/Function.html)
è un interfaccia per una *lamnda expression*, gli argomenti di tale espressioni sono il `RoutingContext`
[JDOC](https://vertx.io/docs/apidocs/io/vertx/ext/web/RoutingContext.html), un'istanza di tale oggetto viene creata
ogni qual volta viene fatta una richiesta al server, permette, inoltre, l'accesso a `HttpServerRequest` e
`HttpServerResponse`. Sempre tale espressione ritorna un `Future` ovvero una computazione asincrona, nel nostro esempio usiamo
`Future.succeededFuture(...)`

## repository e POJO

La classe `Repository` ad imitazione di Spring, rappresenta la nostra base di dati, la classe POJO `Product`
è pronta ad essere gestita dalla libreria `Jackson` inglobata in Vert.X che comunque
va importata in `build.gradle`

## test con `curl`

Testiamo il nostro semplice server REST con `curl`.

### GET

`curl localhost:8080/products/` con esito
`[{"id":0,"name":"bagigi","price":0.5},{"id":1,"name":"patatine","price":0.3},{"id":2,"name":"fighi","price":1.0},{"id":3,"name":"seme col sal","price":0.4}]`

`curl localhost:8080/products/1` con esito
`{"id":1,"name":"patatine","price":0.3}`

### POST

`curl -X POST -H 'Content-Type: application/json' -d '{"id":0, "name":"pop corn", "price": 3.5}' localhost:8080/products` con esito
`{"id":5,"name":"pop corn","price":3.5}genji@P330:~/zuccante/materiali/zuccante_src/java/vertx$`

### PUT

`curl -X PUT -d '{"id":0, "name":"pop corn", "price": 3.6}' localhost:8080/products/5` dando come esito
`{"id":5,"name":"pop corn","price":3.6}`

## DELETE

`curl -X DELETE localhost:8080/products/5`  con esito
`{"id":5,"name":"pop corn","price":3.6}`

## riferimenti

[1] "apidocs" [qui](https://vertx.io/docs/apidocs/).
[2] "Vert.X Web" [qui](https://vertx.io/docs/vertx-web/java/).
[3] "Jackson Docs" [qui](https://github.com/FasterXML/jackson-docs).
[4] "POJO su Baeldung" [qui](https://www.baeldung.com/java-pojo-class).
[5] "Some Rest with Vert.X" dal Blog (versione un po' vecchia ma utile) [qui](https://vertx.io/blog/some-rest-with-vert-x/)
