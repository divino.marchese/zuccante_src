package org.example;

import java.util.EventObject;

public class MyEvent {
    private final String msg;
    public MyEvent(String msg) {
        this.msg = msg;
    }
    public String getMessage() {
        return msg;
    }
}
