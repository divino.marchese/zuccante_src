package org.example;

import java.util.ArrayList;
import java.util.List;

public class MyEventEmitter {
    private final List<MyEventListener> listeners = new ArrayList<>();

    public void addMyEventListener(MyEventListener listener) {
        listeners.add(listener);
    }
    public void removeMyEventListener(MyEventListener listener) {
        listeners.remove(listener);
    }
    void fireMyEvent(MyEvent event) {
        for (MyEventListener listener : listeners) {
            listener.onMyEvent(event);
        }
    }
}
