package org.example;

public class Main {
    public static void main(String[] args) {
        // prepare emitter (observable)
        MyEventEmitter emitter = new MyEventEmitter();
        MyEvent event = new MyEvent("my event message");
        // prepare two listeners (observers)
        emitter.addMyEventListener((e) -> System.out.println("01: an event is occurred with msg: "
                + e.getMessage()));
        emitter.addMyEventListener((e) -> System.out.println("02: an event is occurred with msg: "
                + e.getMessage()));
        // event emitted
        emitter.fireMyEvent(event);
    }
}