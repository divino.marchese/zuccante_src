package org.example;

import java.util.EventListener;

@FunctionalInterface
public interface MyEventListener extends EventListener {
    void onMyEvent(MyEvent event);
}
