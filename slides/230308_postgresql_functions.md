---
theme: default
paginate: false
marp: true
---

![logo](./logo_small.png)

# function in PostgreSQL

AS 2022-2023   
Andrea Morettin

-----

# 1 - preparazione del DB

DB e user sono entrambi `test`, password a gusto personale! 
```
test=> create table products (
test(> id serial,
test(> product text,
test(> description text,
test(> price numeric(6, 2),
test(> discount real,
test(> primary key(id));
CREATE TABLE
test=> \d
```
Il file `products.csv`
```
bagigi,arachidi sfiziosi,12.4,0.1
patatine,sfoglie salate e gustose,11.2,0.1
segoe,magnitee ti,1.2,0.01
```

-----

# 2 - preparazione del DB

```
\copy products(product, description, price, discount) 
from '/home/genji/zuccante/materiali/postgresql/products.csv' with delimiter ',' csv;
```
L'istruzione `SQL` `COPY` è più potente ma meno versatile qui richiede un *rule* che andrebbe concesso all'utente. 
Utile consultare le API [qui](https://www.postgresql.org/docs/current/sql-copy.html).

-----

# 3 - function

- API [qui](https://www.postgresql.org/docs/current/sql-createfunction.html), 
- guida [qui](https://www.postgresql.org/docs/14/xfunc-sql.html).

Il comando
```
\df
```
ci mostra le funzioni create.

-----

# 4 - function

Lo schema di una *function* è
```
CREATE FUNCTION function_name (<args>) RETURNS <type> AS
<sep>
<body function>
<sep>
LANGUAGE <language>
```
Il separatore `<sep>` è `'` o `$label$`, anche `$$` che si presta meglio dell'apice in quanto questo lo si usa per il testo!

-----

# 5 - function

Un po' di esempio (come utente `test`)
```
test=> create function one() returns integer as
$$
select 1;
$$
language sql;
CREATE FUNCTION
```
per cancellare la funzione appena creata
```
drop function one;
```

-----

# 6 - function

```
test=> create function incr(i int) returns int as
test-> $$
test$> select i + 1;
test$> $$ language sql;
CREATE FUNCTION
test=> select incr(2);
 incr 
------
    3
(1 row)
```

-----

# 7 - function

Possiamo accedere al DB (utile è la guida).
```
test=> create or replace function get_price(p text) returns text as
$$
select price from products where product = p;
$$
language sql;
CREATE FUNCTION
test=> select get_name('bagigi');
```

-----

# 8 - function

```
test=> create or replace function get_discount(name text) returns real as 
$$
select price * discount from products where product = name;
$$
language sql;
CREATE FUNCTION
test=> select get_discount("bagigi");
ERROR:  column "bagigi" does not exist
LINE 1: select get_discount("bagigi");
                            ^
test=> select get_discount('bagigi');
 get_discount 
--------------
         1.24
(1 row)

test=> select get_discount('patatine');
 get_discount 
--------------
         1.12
(1 row)
```

-----

# 9 - gli indici ...

Un richiamo alla funzione degli indici: le chiavi primarie sono indici
```
\d products
```

-----

# 10 - functions

```
test=> drop function get_discount;
DROP FUNCTION
test=> create or replace function get_discount(i integer) returns real as 
$$
select price * discount from products where id = i;
$$
language sql;
CREATE FUNCTION
```

-----

# 11 - function language

Altri linguaggi per programmare le *function* sono disponibile (installarli ad esempio con `apt` sotto Linux).
Come utente `postgres` (amministratore), una volta connessi al DB `test`
```
\c test
```
può essere creata l'estensione (qui per Python 3)
```
create extension plpython3u;
```
Dare `\dx` (sempre `psql`) per conferma!

----

# 12 - trusted language

Dando
```
test=> SELECT * FROM pg_language;
```
nel campo `lumptrusted` si vedonoi linguaggi *trusted* per cui all'utente non amministratore è permesso di creare *function*.
```
UPDATE pg_language SET lanpltrusted = true WHERE lanname = 'plpython3u';
```
rende Python *trusted*! Vedi [qui](https://www.postgresql.org/docs/current/catalog-pg-language.html).

> `lanpltrusted` True if this is a trusted language, which means that it is believed not to grant access to anything outside the normal SQL execution environment. Only superusers can create functions in untrusted languages.

-----

# 12 - python

Ecco un esempio di *function con Python 3
```
test=> create or replace function pymax(a integer, b integer) returns integer as
$$
if a > b:
  return a
return b
$$
language plpython3u;
CREATE FUNCTION
test=> select pymax(2, 3);
 pymax 
-------
     3
(1 row)

test=> 
```

-----

# 13 - python

Anche con Python abbiamo accesso al DB, vedi [qui](https://www.postgresql.org/docs/14/plpython-database.html).







