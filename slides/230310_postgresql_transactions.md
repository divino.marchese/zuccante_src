---
marp: true
---

![logo](./logo_small.png)

# transaction in PostgreSQL

AS 2022-2023  
Andrea Morettin

-----

# 1 - preparazione del DB

Il DB è sempre `test` con utente amministratore `test`. Creiamo una tabewlla con una chiave generata come già vito in precedenza, per un ripasso [qui](https://www.sqltutorial.org/sql-identity/).
```
test=> create table accounts (
test(> id int generated by default as identity,
test(> name varchar(40) not null,
test(> balance numeric(12, 2) not null,
test(> primary key(id));
CREATE TABLE
test=>
``` 
-----

# 2 - transaction

Possiamo iniziare e terminare in vari modi, qui useremo:
- `BEGIN TRANSACTION` per iniziare e
- `COMMIT` per terminare
Una ***transaction*** è una sequenza **atomica** - *one shot* cioè non interrompibile - di step. Inserito un record vediqamo il risultato.
```
test=> begin transaction;
test=*> select * from accounts;
 id | name  | balance 
----+-------+---------
  1 | beppi |  250.00
(1 row)

test=*> 
```

-----

# 3 - transaction

Come altro utente, nel nostro caso per comodità `postgres`:
```
postgres=# \c test
You are now connected to database "test" as user "postgres".
test=# select * from accounts;
 id | name | balance 
----+------+---------
(0 rows)
```
come utente `test`
```
test=*> commit transaction;
COMMIT
```


-----

# 4 - transaction

e quindi:
```
test=# select * from accounts;
 id | name  | balance 
----+-------+---------
  1 | beppi |  250.00
(1 row)
```


-----

# 5 - transaction

Vediamo un altro esempio: una transqazione bancaria.
```
test=> begin transaction;
BEGIN
test=*> update accounts set balance = balance - 10 where id = 1;
UPDATE 1
test=*> update accounts set balance = balance + 100 where id = 1;
UPDATE 1

test=# select * from accounts;
 id | name  | balance 
----+-------+---------
  1 | beppi |  250.00
(1 row)

test=*> commit;
COMMIT
```

-----

# 6 - transaction

Come utente `postgres`
```
test=# select * from accounts;
 id | name  | balance 
----+-------+---------
  1 | beppi |  340.00
(1 row)
```

-----

# 7 - transaction

Ora vediamo un prelievo da un conto ed il corrispettivo deposito su di un altro. Per prima cosa creiamo un nuovo correntista con un saldo positivo sul suo conto.
```
test=> insert into accounts(name, balance) values ('nane', 200);
INSERT 0 1
test=> select * from accounts;
 id | name  | balance 
----+-------+---------
  1 | beppi |  340.00
  2 | nane  |  200.00
(2 rows)
```

-----

# 8 - transaction

```
test=> begin transaction;
BEGIN
test=*> update accounts set balance = balance + 20 where id = 2;
UPDATE 1
test=*> update accounts set balance = balance - 20 where id = 1;
UPDATE 1
test=*> rollback;
ROLLBACK
test=> select * from accounts;
 id | name  | balance 
----+-------+---------
  1 | beppi |  340.00
  2 | nane  |  200.00
(2 rows)
```

-----

# 9 - transaction

`ROLLBACK` ci permette di tornare indietro, ad esempio nel caso in cui sia il pimo correntista a desistere dall'operazine di trasferimento!
Senza il `ROLLBACK`creeremmo (o distruggeremmo) denaro. Non che in banca non venga creata **moneta**, ma non certo in questo modo!


-----

# 10 - transaction

Dalla documentazione ufficiale [qui](https://www.postgresql.org/docs/current/tutorial-transactions.html) vediamo qualcosa di più articolato.
```
BEGIN;
UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';
SAVEPOINT my_savepoint;
UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Bob';
-- oops ... forget that and use Wally's account
ROLLBACK TO my_savepoint;
UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Wally';
COMMIT;
```

-----

# 11 - transaction

La precedente transazione regola i passaggi con delle *label* sulle quali è possibile ritornare coi `ROLLABACK`. Tali *label* sono detti `SAVEPOINT`.
Tali punti di salvataggio sono essenziali anche per la gestione di eventuali *crash* del sistema per mantenere la **coerenza** del DB.


-----

# 12 - materiali

[1] "Dal Tutorial di Postgres" [qui](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-transaction/).  
[2] "PostgreSQL Transactions and How To Use Them" by  F. Schildorfer [qui](https://arctype.com/blog/posgres-transaction/).  



