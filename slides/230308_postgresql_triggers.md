---
marp: true
---

![logo](./logo_small.png)

# trigger in PostgreSQL

AS 2022-2023  
Andrea Morettin

-----

# 1 - SQL Procedural Language

Praticamente un estensione di SQL che lo porta a comportarsi con cicli, condizioni ecc.. Per vedere i linguaggi a disposizione
```
\dx
```
Per la documentazione [qui](https://www.postgresql.org/docs/14/plpgsql.html).

-----

# 1 - SQL Procedural Language

Per dichiarare una funzione abbiamo una sintassi del tipo
```
CREATE FUNCTION <name>(<paramethers>) RETURNS <type> AS 
$$
DECLARE
  <declarations>
BEGIN
  <body>
END;
$$ 
LANGUAGE plpgsql;
```
La documentazione (slide precedente) offre uan dettagliata sintesi della sintassi.

-----

# 2 - DB di test

In questo definiremo due tabelle usando le *generated column* alternativa con più possibilità rispetto a `serial` (vedi `AUTO INCREMENT` in MySQL).
Rimandiamo a [qui](https://www.postgresql.org/docs/current/ddl-generated-columns.html) per la documentazione ufficiale e ad [q].
```
test=> create table employees (
test(> id int generated always as identity,
test(> first_name varchar(40) not null,
test(> last_name varchar(40) not null,
test(> primary key(id)
test(> );
CREATE TABLE
```

-----

# 5 - DB di test

Una tabella per i log
```
test=> create table employee_audits (
test(> id int generated always as identity,
test(> employee_id int not null,
test(> last_name varchar(40) not null,
test(> changed_on timestamp(6) not null,
test(> primary key(id)
test(> );
CREATE TABLE
```

-----

# 6 - trigger

Un **trigger** agisce in concomitanza di **eventi** CRUD, tipicamente `INSERT`, `DELETE` e `UPDATE` [qui](https://www.postgresql.org/docs/current/sql-createtrigger.html) per un'introduzione. Un trigger agisce
- `FOR EACH ROW` se viene chiamato su ogni riga su cui ha effetto l'operazione di CRUD.
- `FOR EACH STATEMENT` se il focus è sull'azione del trigger, *one showt*, indipendentemente dalle righe coinvolte.
- `BEFORE`: l'azione del trigger accade prima,
- `AFTER`: prima ... .
- `INSTEAD OF`: sostituisce l'operazione CRUD richoesta.


-----

# 7 - trigger function

Un trigger di fatto è una coppia evento-azione, l'azione viene descritta tramite una *function*, utile uno sguardo [qui](https://www.postgresql.org/docs/current/plpgsql-trigger.html). Tale funzione ha alcune particolarità.
- `NEW` e `OLD` (in `SQL` o `Pl/pgSQL`) indicano la riga prima o dopo l'evento CRUD.
- Ritorna `TRIGGER` come tipo ed esso può essere `NEW` nel caso di `FOR EACH ROW`, più  usuale, e `NULL` per `FOR EACH STATEMENT` che inibisce il *trigger manager* dal proseguire con le operazioni altrimenti.

-----

# 8 - function in plpgsql

Creeremo una apposita funzione
```
test=> create or replace function log_last_name_change() returns trigger as 
test-> $$
test$> begin
test$>   if new.last_name <> old.last_name then
test$>     insert into employee_audits(employee_id, last_name, changed_on)
test$>     values(old.id, old.last_name, now());
test$>   end if;
test$>   return new;
test$> end;
test$> $$
test-> language plpgsql;
CREATE FUNCTION
```
`\df` per conferma! Essa ritorna come tipo speciale `trigger`.

-----

# 9 - trigger

Creiamo un trigger di facile lettura!
```
test=> create trigger last_name_changes
test-> before update
test-> on employees
test-> for each row
test-> execute function log_last_name_change();
```
`\dft` per verificarne la creazioni! Un *trigger* in sostanza è costruito su di una funzione!

-----

# 10 - trigger

Per dichiarare una variabile procediamo come indicato [qui](https://www.postgresql.org/docs/14/plpgsql-declarations.html), ad esempio
```
age integer := 30;
```
(si può usare anche `=`). Utile anche, per non lavorare con gli argomenti col `$`
```
subtotal ALIAS FOR $1;
```
Per assegnare usiamo
```
sum := x + y;
```

-----

# 11 - trigger

`IF ... THEN` *statement* [qui](https://www.postgresql.org/docs/14/plpgsql-control-structures.html).
```
IF <expr> THEN
    <body>
END IF;
```
`LOOP`
```
LOOP
    <body>
    IF count > 0 THEN
        EXIT;  -- exit loop
    END IF;
END LOOP;
```

-----

# 12 - trigger

Per `FOR` oltre alla documentazione rimandiamo a [3]
```
$$
declare
    f record;
begin
    for f in select title, length 
	       from film 
	       order by length desc, title
	       limit 10 
    loop 
	raise notice '%(% mins)', f.title, f.length;
    end loop;
end;
$$ 
```

-----

# 13 - trigger

Testiamo!
```
test=> insert into employees(first_name, last_name) values ('Bepi', 'Venesian');
INSERT 0 1
test=> select * from employees;
 id | first_name | last_name 
----+------------+-----------
  1 | Bepi       | Venesian
(1 ro
-----
```
------

# 14 - trigger

Aggiorniamo:
```
update employees set last_name = 'Venexian' where first_name = 'Bepi';

test=> select * from employees;
 id | first_name | last_name 
----+------------+-----------
  1 | Bepi       | Venexian
(1 row)

test=> select * from employee_audits;
 id | employee_id | last_name |         changed_on
----+-------------+-----------+----------------------------
  1 |           1 | Venesian  | 2023-03-08 20:13:27.031946
(1 row)
``` 

------

# 15 - trigger in python

Prepariamo la *function* (senza tipo di ritorno, se no non funzia), vedi [qui](https://www.postgresql.org/docs/14/plpython-trigger.html).
```
est=> create or replace function update_price() returns trigger as
test-> $$
test$> price = TD["new"]["price"]
test$> if price < 0:
test$>   plpy.execute("UPDATE products SET price = -prive WHERE price < 0")
test$> $$
test-> language plpython3u;
CREATE FUNCTION
```

------

# 16 - trigger in python

```
test=> create trigger set_price
test-> after insert on products
test-> for each row
test-> execute function update_price();
CREATE TRIGGER
```

------

# 17 - trigger in python

```
test=> insert into products(product, description, price, discount) values ( 'bigoi', 'pasta veneta', -11.0, 0.1);
INSERT 0 1
test=> select * from products;
 id | product  |       description        | price | discount 
----+----------+--------------------------+-------+----------
  4 | bagigi   | arachidi sfiziosi        | 12.40 |      0.1
  5 | patatine | sfoglie salate e gustose | 11.20 |      0.1
  6 | segoe    | magnitee ti              |  1.20 |     0.01
 15 | bigoi    | pasta veneta             | 11.00 |      0.1
(4 rows)
```

------

# 18 - for each statement trigger

Tratto da *Stack Overflow*, partiamo dalla *function*
```
CREATE OR REPLACE FUNCTION trg_notify_after() RETURNS trigger AS
$$
BEGIN
   PERFORM pg_notify(TG_TABLE_NAME, TG_OP);
   RETURN NULL;
END;
LANGUAGE plpgsql
$$
```

------

# 19 - for each statement trigger

Il trigger
```
CREATE TRIGGER notify_after
AFTER INSERT OR UPDATE OR DELETE ON my_tbl
FOR EACH STATEMENT
EXECUTE FUNCTION trg_notify_after();
```

-----

# 20 - materiali

[1] "identity column" [qui](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-identity-column/
).  
[2] Il tutorial da cui siamo partiti [qui](https://www.postgresqltutorial.com/postgresql-triggers/creating-first-trigger-postgresql/
).  
[3] "For loop" dal Tutorial di PostgreSQL [qui](https://www.postgresqltutorial.com/postgresql-plpgsql/plpgsql-for-loop/).  
[4] "PL/Python – Pythonic Trigger Functions for Postgres" [qui](https://anitagraser.com/2010/12/10/plpython-pythonic-trigger-functions-for-postgres/).


