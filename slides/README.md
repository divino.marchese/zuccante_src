# zuccante slides

Slides di vario genere come materiale aggiuntivo per le mie lezioni.

## marp

Slide in `md` [qui](https://marp.app/) home page. Utile estensione anche in VSC (si può esportare anche in `pdf`).
