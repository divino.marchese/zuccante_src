---
theme: default
paginate: false
marp: true
---

![logo](./logo_small.png)

# Shagger and Open API Specification

AS 2023-2024   
Andrea Morettin
aggiornato il 23/03/2024

-----

# riferimenti

- Yaml [qui](https://yaml.org/spec/1.2.2/)  
- OpenAPI Specification [qui](https://spec.openapis.org/oas/v3.0.3#openapi-specification)  
- Nuovo editor Swagger con auto completamento [qui](https://editor-next.swagger.io/)  
- Swagger API [qui](https://swagger.io/docs/specification/about/)  
- Il file completo [qui](./230922_openapi.yaml)
- La versione aggiornata [qui](./240323_openapi.yaml)

**NB** Swagger supporta OAS (Open APi Specification)!

-----

# STEP I metadata - 1

Per prima cosa definiamo l'oggetto - *maps* - OpenAPI [API](https://spec.openapis.org/oas/v3.0.3#openapi-object) compostp pbbligatioriamente da
- `pwenapi` la versione
- `info`
- `path` i *path* e le operazioni

-----

# STEP I metadata - 2

```
openapi: 3.0.3
info:
  title: My API - OpenAPI 3.0
  description: |
    multiline
    description
  version: "0.1"
servers:
  - url: https://mioserver.edu
    description: una descrizione opzionale del server
```

-----

# STEP II path - 1

Ovvero *path* relativi agli ***endpoint*** [API](https://spec.openapis.org/oas/v3.0.3#paths-object).

```
paths:
  /users:
    get:
      summary: returns a list of users.
      description: optional extended description in CommonMark or HTML.
      responses:
        '200':        # status code
          description: A JSON array of user names
          content:
            application/json: # media type
              schema: # type
```

-----

# STEP II path - 2

Un *path* singolo può contenere una o più azioni (`GET, `POST`, `HEADER` ecc. ) vedi *operation object* [API](https://spec.openapis.org/oas/v3.0.3#operation-object).

-----

# STEP III schema - 1

Per i **tipi primitivi** [API](https://spec.openapis.org/oas/v3.0.3#data-types), lo **schema** è descritto
[API](https://spec.openapis.org/oas/v3.0.3#schema-object), utile su Swagger la documentazione [qui](https://swagger.io/specification/#schema-object).

```
type : integer
format: int64
minimum: 1
```

-----

# STEP III schema - 2

Per gli **oggetti**
```
type: object
  required:
    - name
    - surname
  properties:
    name:
      type: string
    surname:
      type: string
```

-----

# STEP III schema - 3

Per le **liste**

```
type: array
  items: 
    type: string
```

-----

# STEP IV post - 1

Andiamo "come i gamberi" e scriviamo la risposta (obbligatoria)

```
post: 
  summary: create an user
  responses: 
    '201':
      description: object created
      content:
        application/json: 
        schema:
          type: string
```

-----

# STEP IV post - 2


```
post: 
  summary: create an user
  requestBody: 
    required: true
    content: 
      application/json: 
        schema: 
          type: object
          required:
            - id
            - name
            - surname
          properties:
            id: 
              type: integer
              format: int64
            name:
              type: string
              surname:
              type: string
```

-----

# STEP V delete

```
delete: 
  summary: delete an user by ID
  parameters:
    - name: userId
        in: path
        required: true
        schema:
          type : integer
          format: int64
          minimum: 1
  responses:
    '200':
      description: deleted
```

-----

# STEP VI example

Può risultare efficace fare un esempio dell'oggetto mostrato (nel nostro file `yaml` non lo facciamo)
```
type: object
properties:
  id:
    type: integer
    format: int64
  name:
    type: string
required:
  - name
example:
  name: Puma
  id: 1
```

-----

# STEP VII components - 1

Nel file `yaml` aggiornato nel nostro esempio, abbiamo `$ref`
```
/store/order:
  post:
    tags:
      - store
    summary: Place an order for a pet
    description: Place a new order in the store
    operationId: placeOrder
    requestBody:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Order'
```

-----

# STEP VII components - 2

```
components:
  schemas:
    User:
      type: object
      required:
        - id
        - name
        - surname
      properties:
        id: 
          type: integer
          format: int64
          example: 1
        name:
          type: string
          example: Gigio
        surname:
          type: string
          example: Bagigio
```
