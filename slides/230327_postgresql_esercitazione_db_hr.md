---
marp: true
---

![logo](./logo_small.png)

# esercitazione col DB hr

AS 2022-2023  
Andrea Morettin

-----

# 1 - preparazione del DB

Il DB `hr` è un noto DB di test, di seguito forniremo le fonti. Ecco come prepararlo
Come  `postgres`
```
create database hr;
create user hr with ecrypted password 'zuccante@2023';
grant all on database hr to hr;
```
uscire da `psql` eseguire quindi i due file `.sql`
```
psql -d hr -U hr -W < /<path>/hr_schema.sql
psql -d hr -U hr -W < /<path>/hr_data.sql
```

-----

# 2 - diagramma

![ER](https://www.sqltutorial.org/wp-content/uploads/2016/04/SQL-Sample-Database-Schema.png)

-----

# 3 - riferimenti

[1] I due file `hr_schema.sql` e `hr_data.sql` sono disponibili fra i materiali del docente.  
[2] Il sito originale [qui](https://www.sqltutorial.org/sql-sample-database/) da cui è tratto il DB e la figura.  
[3] Altre esercitazioni [qui](https://w3resource.com/postgresql-exercises/) sempre sullo stesso DB!