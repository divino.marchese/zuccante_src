#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
    pid_t childB, childC;

    // Process A forks Process B
    childB = fork();

    if (childB == 0) {
        // Process B
        printf("Process B: PID = %d\n", getpid());
        printf("Process B: Parent PID = %d\n", getppid());

        // Process B forks Process C
        childC = fork();

        if (childC == 0) {
            // Process C
            printf("Process C: PID = %d\n", getpid());
            printf("Process C: Parent PID = %d\n", getppid());
            printf("Process C: Exiting\n");
            exit(0);
        } else if (childC > 0) {
            // Process B
            printf("Process B: Forked Process C with PID = %d\n", childC);

            // Wait for Process C to finish
            int status;
            waitpid(childC, &status, 0);

            printf("Process B: Exiting\n");
            exit(0);
        } else {
            // Error occurred during fork() for Process C
            fprintf(stderr, "Process B: Fork failed for Process C\n");
            exit(1);
        }
    } else if (childB > 0) {
        // Process A
        printf("Process A: Forked Process B with PID = %d\n", childB);

        // Wait for Process B to finish
        int status;
        waitpid(childB, &status, 0);

        printf("Process A: Exiting\n");
    } else {
        // Error occurred during fork() for Process B
        fprintf(stderr, "Process A: Fork failed for Process B\n");
        return 1;
    }

    return 0;
}