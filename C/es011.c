#include <stdio.h>

int main() {
    FILE *fp;
    fp = fopen("cat.txt", "r");
    // fteel: the current file position
    printf("The location of the current pointer is %ld bytes from the start of the file\n", ftell(fp));
    // 6 bytes from SEEK_SET (relative to the start of the file) 
    fseek(fp, 6, SEEK_SET);
    printf("The location of the current pointer is %ld bytes from the start of the file\n", ftell(fp));
    fclose(fp);
    return 0;
}