# C

Per compilare e estare useremo prendendo il primo sorgente
```
gcc -o exe es001.c
```
Il `man` Linux contiene ampia e diffusa documentazione che invitiamo a consultare!   I primi 6 esercizi trattano della gestione dei processi, vedi anche [1]
```
#define SIGHUP  1   /* Hangup the process */ 
#define SIGINT  2   /* Interrupt the process */ 
#define SIGQUIT 3   /* Quit the process */ 
#define SIGILL  4   /* Illegal instruction. */ 
#define SIGTRAP 5   /* Trace trap. */ 
#define SIGABRT 6   /* Abort. */
```

[1] "Exploring Process Creation and Execution in Linux: A Hands-On Guide" di Isuru Harischandra [qui](https://towardsdev.com/exploring-process-creation-and-execution-in-linux-a-hands-on-guide-a59b171a0a3).

## pipe

Un tipo di *file* che permette la comunicazione fra processi, dal `man`
```
int pipe2(int pipefd[2], int flags);
```
`pipefd[0]` è la parte di *read* mentre `pipefd[1]` è il *write*. In `es005.c` ne diamo un esempio.

## file descriptor

I *file descriptor* permettono la gestione a basso livello - Kerne - dei file. Ogni processo eredita i seguenti descrittori
- `0` -> *standard input* `STDIN_FILENO`
- `1` -> *standard output* `STDOUT_FILENO`
- `2` -> *standard error* `STDERR_FILENO`
Nel PCB (Process Control Block) i file cui il rocesso accede sono rappresentati, appunto, da *file descriptor*, una tabella per ogni processo, la tabella dei file aperti (con i contatori rappresentanti processi che accedono ad un singolo file) e dgli *inode* condivisi da tutti i processi. Per ulteriori dettagli rimandiamo a [2].

## stream

Forniscono, a deifferenza dei *file descriptor* un'interfaccia ad alto livello. Il tipo `FILE *`possiede anche informazioni sulla "posizione" nel file e sulla gestione della lettura e scrittura in modo bufferizzato. Per ulteriori dettagli rimandiamo a [4]. Fra le funzioni che ci permettono di scorrere il file abbiamo anche `fseek(...)` di cui presentiamo un semplice esempio.

## signal

In Bash abbiamo il comando `kill`, ad esempio
```
kill -9 20134
```
termina il processo il cui PID è `20134`. Rimandiamo a per i dettagli ai manuali o a [5].

## thread

Per completare vediamo la sinossi della funzione `pthread(..)`
```
#include <pthread.h>

int pthread_create(pthread_t *restrict thread,
    const pthread_attr_t *restrict attr,
    void *(*start_routine)(void *),
    void *restrict arg);
```
noi presentiamo due casi esemplificativi.

[2] "Handling a File by its Descriptor in C" di M.Combeau [qui](https://www.codequoi.com/en/handling-a-file-by-its-descriptor-in-c/).  
[3] "Formatted Output" gcc [qui](https://www.gnu.org/software/libc/manual/html_node/Formatted-Output.html).      
[4] "Input/Output on Streams" [qui](https://www.gnu.org/software/libc/manual/html_node/I_002fO-on-Streams.html).  
[5] "Signals in C language" [qui](https://www.geeksforgeeks.org/signals-c-language/).  