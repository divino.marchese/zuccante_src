#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>     
#include <sys/wait.h>   
#define SIZE 16

int main() {
    char* msg1 = "hello, child #1";
    char* msg2 = "hello, child #2";
    char* msg3 = "hello, child #3";
    char inbuf[SIZE];
    int p[2];
    pid_t pid;

    if (pipe(p) == -1) {
        perror("pipe failed");
        exit(1);
    }

    pid = fork();

    if (pid > 0) { 
        close(p[0]); // close read file descriptor
        write(p[1], msg1, SIZE);
        write(p[1], msg2, SIZE);
        write(p[1], msg3, SIZE);
        close(p[1]); // close write
        wait(NULL);  // Wait for child to finish
    } else if (pid == 0) { 
        close(p[1]); // close unused write 
        while (read(p[0], inbuf, SIZE) > 0) {
            printf("Child received: %s\n", inbuf);
        }
        close(p[0]); // close read end after reading
        printf("Child finished reading\n");
        exit(0);
    } else {
        perror("fork failed");
        exit(1);
    }
    return 0;
}