#include <stdio.h>
#include <unistd.h>
#include <stdlib.h> 
#define SIZE 16

int main() {
    char* msg1 = "hello, world #1"; // 15 byte + EOS
    char* msg2 = "hello, world #2";
    char* msg3 = "hello, world #3";
    char inbuf[SIZE]; 
    int p[2];            

    if (pipe(p) == -1) {
        perror("pipe failed");
        exit(1);
    }

    // write messages to the pipe
    write(p[1], msg1, SIZE);
    write(p[1], msg2, SIZE);
    write(p[1], msg3, SIZE);

    // read messages from the pipe
    for (int i = 0; i < 3; i++) {
        read(p[0], inbuf, SIZE);
        printf("%s\n", inbuf);
    }
    return 0;
}