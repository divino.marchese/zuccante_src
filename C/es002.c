#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
    pid_t pid;
    pid = fork();
    if (pid == 0) {
        printf("Child process: PID = %d\n", getpid());
        printf("Child process: Parent PID = %d\n", getppid());
        // simulating some work in the child process
        for (int i = 0; i < 5; i++) {
            printf("Child process: Iteration %d\n", i);
            sleep(1);
        }
        printf("Child process: Exiting\n");
        exit(0);
    } else if (pid > 0) {
        // Parent process
        printf("Parent process: PID = %d\n", getpid());
        printf("Parent process: Child PID = %d\n", pid);
        // waiting for the child process to finish
        int status;
        // 0 -> waiting for any child process
        waitpid(pid, &status, 0);
        if (WIFEXITED(status)) {
            printf("Parent process: Child exited with status %d\n", WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            printf("Parent process: Child terminated by signal %d\n", WTERMSIG(status));
        } 
        printf("Parent process: Exiting\n");
    } else {
        // Error occurred during fork()
        fprintf(stderr, "Fork failed\n");
        return 1; 
    }
}