#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int	main(void) {
	int	fd;
    // O_TRUNC not O_APPEND
	fd = open("test.txt", O_WRONLY | O_TRUNC | O_CREAT, 0640);
    // error
	if (fd == -1)
		return (1);
	printf("fd = %d\n", fd);
    // not insert EOS
	write(fd, "Hello World!\n", 13);
	close(fd);
	return(0);
}