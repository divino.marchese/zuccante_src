# es03 post

Testiamo prima di tutto `json-server`
```
curl -X POST -H "Content-Type: application/json" -d '{"id": 5, "title": "post", "views": 0}' 127.0.0.1:3000/posts
```
riceviamo su terminale, se tutto va bene
```
{
  "id": 5,
  "title": "post",
  "views": 0
}
```
Possiamo quindi procedere col `POST` mediante `fetch(...)`, andando a verificare, mediante ulteriore richiesta `GET` la presenza del nuovo post inserito.

**NOTA** chiusi i server, `json-server` ed il serve di VSC dell'estensione *live server*, cancellare a mano sul file di test il post aggiunto!




