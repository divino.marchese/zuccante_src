fetch("http://localhost:3000/posts", {
  method: "post",
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    id: 5,
    title: "post",
    views: 0
  })
}).then(
   () => fetch("http://localhost:3000/posts/5")
).then(
    response => response.json()
).then((data) => {
    let post = document.getElementById("post");
    post.innerHTML = `post n.${data.id}: title: ${data.title}, views: ${data.views}`;            
});



