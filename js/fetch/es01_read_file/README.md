# es001 read file

L'esempio deve girare su di un revre, noi usiamo LiveServer di VSC, per non incappare nel CORS! ALcuni riferimnti da leggere sonop:
- il metodo `fetch(...)` su MDN [API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API).
- `Response` su MDN [API](https://developer.mozilla.org/en-US/docs/Web/API/Response) di cui usiamo la funzione `read` per leggere in formato testo il *body* della richiesta `GET`.