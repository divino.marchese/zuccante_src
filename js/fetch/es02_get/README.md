# es02 get

Qui usiamo *LiveServer* e *json-server*, vedi [qui](https://www.npmjs.com/package/json-server), tutto fatto in casa quindi!
- `Response.json()` da MDN [API](https://developer.mozilla.org/en-US/docs/Web/API/Response/json) è molto potente in quanto ritorna o un singolo oggetto o un array diu oggetti!

## altri materiali

[1] L'esempio proposto in MDN da provare [qui](https://github.com/mdn/dom-examples/tree/main/fetch/fetch-json).