// hoisting of a global variable

if (typeof x == 'undefined') {
    console.log("x is undefined");
  
// comment
var x;
// uncomment in Node
// let x
