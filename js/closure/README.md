# Closure

Cpn la **closure** terminiamo le nostre considerazioni sul **execution context**. Partiamo da un articolo: [qui](https://medium.com/dailyjs/i-never-understood-javascript-closures-9663703368e8). Teniamo, una volta letto l'articolo, a mente la seguente frase: 

> Here is how it works. Whenever you declare a new function and assign it to a variable, you store the function definition, as well as a closure. The closure contains all the variables that are in scope at the time of creation of the function. It is analogous to a backpack. A function definition comes with a little backpack. And in its pack it stores all the variables that were in scope at the time that the function definition was created.

**The closure is a collection of all the variables in scope at the time of creation of the function.**

Per un ulteriore ripasso su **execution context**: [qui](http://davidshariff.com/blog/what-is-the-execution-context-in-javascript/). 

L'ultimo dei tre esempi di codice ci riporta al concetto di **namespace**, prendendo in prestito un termine da altri linguaggio, vedi C++, che qui in JS chiamiamo **modulo**.

Terminando: risulta utile anche la lettura su MDN.