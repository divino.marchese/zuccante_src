let note = document.getElementById('yellow');

function highlight(elem, callback) {
    const bgColor = '#ffff55';
    elem.style.backgroundColor = bgColor;
    if (callback && typeof callback === 'function') {
        callback(elem);
    }
}

function addBorder(elem) {
    elem.style.border = "solid 2px #000";
}

highlight(note, addBorder);