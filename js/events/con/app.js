let note = document.getElementById('yellow');

function highlight(elem, callback) {
    const bgColor = '#ffff55';
    elem.style.backgroundColor = bgColor;
    if (callback && typeof callback === 'function') {
        callback(elem);
    }

    // create a custom event
    const myEvent = new CustomEvent('highlight', {
        detail: {
            backgroundColor: bgColor
        }
    });

    // l'evento viene emesso
    elem.dispatchEvent(myEvent);

}

function addBorder(elem) {
    elem.style.border = "solid 2px #000";
}

// registrazione dell'observer
note.addEventListener('highlight', function (e) {
    addBorder(this);
    console.log(e.detail);
});







// highlight div element
highlight(note);