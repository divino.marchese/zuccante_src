# custom event

Il *pattern* dell'*observer* ci permette di comprendere la programmazione ad eventi o **asincrona**.
Nella gestione degli eventi, seguendo il *pattern* procediamo come segue
- definire l'evento,
- preparare l'*observer* e
- generare - emettere - l'evento!
In W3C per il DOM troviamo: [qui](https://www.w3.org/TR/uievents/).

<img src="https://www.w3.org/TR/uievents/images/eventflow.svg" alt="img" style="width:300px;"/>

lavoriamo seguendo 3 fasi:
- ***capture*** ci permette di individuare il percorso per arrivare al bersaglio lungo l'albero del DOM partendo da `Window` percorrendo gli antenati del *target*
- ***target*** giunti al target, qui possiamo avere indicazioni se procedere o meno nella fase successiva (propagazione dell'ebento agli altri *listener*)
- ***bubble*** dove tipicante gestiamo l'evento per i *listener* fra gli antenati del *target* che lo intercettano (gli *observer* che si sono registrati), levento risale come una "bolla d'aria" il DOM. 

## la classe `Event`

[MDN](https://developer.mozilla.org/en-US/docs/Web/API/Event), troviamo che un oggetto `Event` possiede varie *property* fra cui `bobble` (vedi sopra). Fra le sue sottoclassi troviamo `MouseEvent` [MDN](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent).

Ogni evento ha un `type` identificato da una stringa, [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Event/type).


Vediamo un esempio in codice (tratto da [MDN](https://developer.mozilla.org/en-US/docs/Web/Events/Creating_and_triggering_events))
```js
const event = new Event("build");

// Listen for the event.
elem.addEventListener(
  "build",
  (e) => {
    /* … */
  },
  false,
);

// Dispatch the event.
elem.dispatchEvent(event);

```

## la sua sottoclasse `CustomEvent`

[MDN](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent) è un evento cui possiamo aggiungere dei "dettagli' che si porta dietro!

## *fire*

Per lanciare un ecvento abbiamo bisogno di emmetterlo, ed ecco il metodo - **sincrono** - `dispatchEvent()`, [MDN](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/dispatchEvent). `EventTarget` è un possibile bersaglio dell'evento [MDN](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget).

## ascolto dell'evento

Si tratta della registrazione per l'elemento del DOM
```js
addEventListener()
```
utile è leggere le APU in [MDN](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener).
