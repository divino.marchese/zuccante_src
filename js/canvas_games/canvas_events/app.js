let canvas = document.getElementById('canvas');
canvas.width  = 400;
canvas.height = 200;
let context = canvas.getContext('2d');
context.fillStyle = 'rgb(128,128,200)';
context.fillRect(0, 0, 400, 200);

function writeMessage(canvas, message) {
    context.font = '14pt Ubuntu';
    context.textAlign = 'center';
    context.fillStyle = 'rgb(255,255,255)';
    context.fillText(message, canvas.width/2, canvas.height/2);
}

canvas.addEventListener('mousemove', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    var message = '(' + mousePos.x.toFixed(2) + ',' + mousePos.y.toFixed(2) + ')';
    context.fillStyle = 'rgb(128,128,200)';
    context.fillRect(10, 10, 400, 200);
    writeMessage(canvas, message);
});

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

