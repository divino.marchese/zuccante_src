# es01

Quest primo esempio sull"uso dei *module* in Javascript già possiede spunti interessannti.
- Abbiamo usato il *web server* `http-server`, [qui](https://www.npmjs.com/package/http-server), un server Node utile a fare esperimenti senza dover installare un vero e proprio *web server* come Apache o bgubx [qui](https://nginx.org/en/). ALtrimentui avremo `Cross-Origin Request Blocked` con click solo sul file `html`.
- Sulla sintassi fino ad ora non c'è molto da dire, attenzione a `type="module"` nel tag `<script>`