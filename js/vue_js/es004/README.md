# es004

**[231231]** Un po' di esempi a completare gli *input* e sulle *computed properties*, queste a differenza dei metodi vengono calcolate in modo *recative* (ad eventi) aò cambiamento delle *property* da cui dipensono.
- "Form Input Bindings" [qui](https://vuejs.org/guide/essentials/forms.html).
- "Computed Properties" [qui](https://vuejs.org/guide/essentials/computed.html).
- "Watchers" [qui](https://vuejs.org/guide/essentials/watchers.html) (nei prossimi esempi).

## metodi vs computed property

Le *computed property* vengono ricalcolate in modo *reactive*, ad esempio
```js
const publishedBooksMessage = computed(() => {
  return author.books.length > 0 ? 'Yes' : 'No'
});
```
viene ricalcolata se `author.books.length` cambia, supponendo che `author`
sia un *reactive proxy* di un oggetto, mentre
```js
const now = computed(() => Date.now())
```
**non funziiona**, per questa situazione abbiamo bisogno di un metodo. I metodi invece vengono, quando invocati, sempre calcolati
```js
function calculateBooksMessage() {
  return author.books.length > 0 ? 'Yes' : 'No'
}
```
Osserviamo inoltre quanto segue:
- le *computed property* non devono cambiare altri elementi di stato (esse sono attributi derivati dallo stato),
- a mio giudizio è bene usarle per lavori leggeri, altrimenti ci si affidi ai metodi.

La sequenza in cui valutare cosa usare è la seguente  
```
method -> computed property -> watcher
``` 
Rimandiamo a [1], dei *watchers* parleremo poi ad esempio usando `Axios`.

## materiali 

[1] "Watchers vs Computed Properties" di Flavio Copes [qui](https://flaviocopes.com/vue-methods-watchers-computed-properties/).
