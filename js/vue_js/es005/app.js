const Es005 = {
    data() {
        return {
            value: '',
            result: 0,
            oldValue: ''
        };
    },
    methods: {},
    watch: {
        value(newVal, oldVal) { 
            this.result = 2 * newVal; 
            this.oldValue = oldVal;
        },
     }
}

Vue.createApp(Es005).mount('#es005')