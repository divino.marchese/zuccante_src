# es005

**[240102]** Nel momento in cui desideriamo un approccio *reactive* grazie a cui variabili di stato in  `data` influenzino altre variabili di stato scegliamo i `watch`, insomma, quello che ceerchiamo, vedi documentazione [qui](https://vuejs.org/guide/essentials/watchers.html), è un *side effect*!
Per dettagli ulteriori ecco le le [API](https://vuejs.org/api/options-state.html#watch). Seguiremo la sintassi
```js
watch: {
  propertyToWatch(newValue, oldValue) {
    // non-returning logic
  }
}
```