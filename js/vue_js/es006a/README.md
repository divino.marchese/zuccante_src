# es006a

**[230103]** Per una introduzione ai *component* [qui](https://vuejs.org/guide/essentials/component-basics.html). Questo esercizio è il primo di una serie, un esempio in più parti. Il componente viene per prima cosa **registrato**, vedi [qui](https://vuejs.org/guide/components/registration.html):
```js
app.component(
  // the registered name
  'MyComponent',
  // the implementation
  {
    /* ... */
  }
)
```
Un *component* prevede delle *props*, analogo ai parametri di una funzione, vedi [qui](https://vuejs.org/guide/components/props.html).
