# es006b

**[240103]** Continuando l'esempio precedente mettiamo all'opera la cancellazione: in questa soluzione lavoriamo in modo sincrono accedendo a proprietà paterne mediante `$parent`, [API](https://vuejs.org/api/component-instance.html#parent); ecco il metodo di cancellazione
```js
deleteMemo(index) {
    this.$parent.memos.splice(index, 1);
}
```
La variabile dVuwe `$parent` ci permette di accedereallo **stato centralizzato** (condiviso dall'intera app e da tutti i suoicomponenti).
