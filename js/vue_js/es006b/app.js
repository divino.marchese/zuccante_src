const Es006 = {
    data() {
        return {
            memos: [{text: "primo"}, {text: "secondo"}]
        }
    },
    methods: {
        addMemo(text) {
            if (text.length == 0) return;
            this.memos.push({ text: text })
        },
        deleteAll() {
            this.memos.splice(0, this.memos.length);
        }
    }
}

const app = Vue.createApp(Es006);

app.component('memo', {
    props: ['index', 'text'],
    methods: {
        deleteMemo(index) {
            this.$parent.memos.splice(index, 1);
        }
    },
    template:
        `<button @click="deleteMemo(index)">delete</button> : {{ text }}`
});

app.mount('#es006');
