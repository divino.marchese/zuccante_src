# es002

**[231226** Il nostro secondo esempio si articola: una lista di memo. Qui sotto alcuni riferimenti alla guida introduttiva.

## la direttiva `v-for`

Per questa **direttiva** - le direttive fanno parte come per i *proxy* del cuore della *reactivity* di Vue * - Il riferimento per la documentazione sono le API [qui](https://vuejs.org/api/built-in-directives.html#v-for). Il modfo più semplice di usarla è
```html
<div v-for="item in items">
  {{ item.text }}
</div>
```
Noi usiamo la sintassi
```html
<div v-for="(item, index) in items"></div>
</div>
```
con `index` recuperiamo anche la posizione. Interessante la possibilità di dare un ordine agli elementi
```html
<div v-for="item in items" :key="item.id">
  {{ item.text }}
</div>
``` 

## form imput e `v-model`

Usato nei *form* ecco le [API](https://vuejs.org/api/built-in-directives.html#v-model), per il tutorial possiamo guardare [qui](https://vuejs.org/guide/essentials/forms.html).


## `v-if` *conditional rendering* 

Nella guida [qui](https://vuejs.org/guide/essentials/conditional.html) possiamo leggere del suo utilizzo;
- `v-if` [API](https://vuejs.org/api/built-in-directives.html#v-if),
- `v-else` [API](https://vuejs.org/api/built-in-directives.html#v-else),
- `v-else-if` per più casi [API](https://vuejs.org/api/built-in-directives.html#v-else-if).

## `v-on` gli eventi

Già presentato nel primo evento per questa direttiva le [API](https://vuejs.org/api/built-in-directives.html#v-on) che di seguito useremo in forma abbreviate, vedi sintassi con `@`.

## altri riferimenti

[1] i metodi di `Array` in MDN [qui](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array); noi abbiamo usato `splice(pos, n)`.