const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  pwa: {
    themeColor: '#6CB9C8',
    msTileColor: '#484F60'
}
})
