# Vue js: un'introduzione

[[_TOC_]]

Vue js permette di creare applicazioni *reactive* *single page* Vue usa il pattern **MVVM**
- ***Model***: oggetti in *plain javascript*.
- ***View Model***: l'oggetto di mezzo coi *DOM listener* e le *directive*
- ***View***: è HTML arricchito con elementi custom 
> i) Gli elementi HTML come oggetti e nuovi elementi custom  
ii) le proprietà di tali elementi   
iii) i metodi per accedere a tali elementi  
iv) eventi anche *custom*  !

## il laboratorio online

Per lavorare con Vue js è sufficiente un browser, molto spesso ho usato [codepen](https://codepen.io), in particolare [https://codepen.io/pen/](https://codepen.io/pen/). Ad esempio

`html`  
```html
<div id="app">
  <button @click="count++">{{ count }}</button>
</div>
```

`js`  
```javascript
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

const app = createApp({
  data() {
    return {
      count: 0;
    }
  }
});

app.mount('#app');
```

## gli esempi

- **es001** esempio di partenza `data`, `mounted` e `methods`.
- **es002** CRUD su lista dei memo.
- **es003** eventi in Vue.
- **es004** *computed properties*.
- **es005** *watchers* per una semplice introducione.
- **es006** (**a**, **b**, **c** e **d**) ci mostrano i *component* e i *custom event*.
- **es007 axios** integriamo a Vue Axios, nota libreria per richieste web che usa, nel nostro utilizzo, AJAX.



## materiali generali

Rimandiamo ai singoli esempi per ulteriori materiali di dettaglio.

[1] "Lifecycle Diagram" [qui](https://vuejs.org/guide/essentials/lifecycle.html).  
