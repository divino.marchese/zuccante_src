# es007  axios

Axios, [qui](https://axios-http.com/), è una libreria che lavora sia in Node che in JS. L'esempio è tratto da un più ampio tutorial [qui](https://www.digitalocean.com/community/tutorials/vuejs-rest-api-axios).

## la chiamata di axios

Ecco come lavora Axios, avendo a disposizione l'oggetto `axios`
```js
axios.get('/users')
  .then(res => {
    console.log(res.data);
  });
```
nel nostro caso le API di test ci danno come richiesta `GET`
```
curl http://jsonplaceholder.typicode.com/posts
```
ecco il risultato
```
[
  {
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  },
  {
    "userId": 1,
    "id": 2,
    "title": "qui est esse",
    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
  },
  ...
]
```
ecco la richiesta sincrona con `axios`, vedi la documentazione [qui](https://axios-http.com/docs/api_intro)
```js
axios.get(`http://jsonplaceholder.typicode.com/posts`)
    .then(response => {
        // JSON responses are automatically parsed.
        this.posts = response.data
    })
    .catch(e => {
        this.errors.push(e)
    })
```
che si spiega da sola!

## created

Il primo step dell'applicazione, vedi [API](https://vuejs.org/api/options-lifecycle.html#created), una volta impostato lo stato dell'applicazione con `data`, [API](https://vuejs.org/api/options-state.html#data),

