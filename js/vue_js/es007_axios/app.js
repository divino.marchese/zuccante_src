const Es007 = {
    data() {
        return {
            posts: [],
            errors: []
        }
    },
    created() {
        axios.get(`http://jsonplaceholder.typicode.com/posts`)
            .then(response => {
                // JSON responses are automatically parsed.
                this.posts = response.data
            })
            .catch(e => {
                this.errors.push(e)
            })
    }
}

Vue.createApp(Es007).mount('#es007');