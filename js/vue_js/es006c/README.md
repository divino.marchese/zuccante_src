# es006c

**[240103]** Dopo la soluzione **sincrona** presentata nella fase precedente, ora presentiamo un approccio **asincrono** andando a definire degli **eventi**, vedi [qui](https://vuejs.org/guide/components/events.html.
La logica è la seguente:
```
emissione evento --> chiamata callback con eventuali parametri
```
A rtal proposito Vue ci mette a disposizione il metodo `$emit` [API](https://vuejs.org/api/component-instance.html#emit).
Qui solo una piccola modifica rispetto a quanto qui proposto è la seguente:
- **registramio** (nel *component*) nella proprietà `emits: ['deleteMemo']` gli eventi emessi e 
- portiamo in un metodo l'emissione
```js
emitEvent(index){
    this.$emit('deleteMemo', index);
    }
```
in modo da poter scrivere
```js
template:
    `<button @click="emitEvent(index)">delete</button> : {{ text }}`
```