const Es03 = {
    data() {
        return {
            url: "https://vuejs.org/guide/essentials/event-handling.html",
            msg: "try",
            data: 0,
            clickX: 0,
            clickY: 0,
            moveX: 0,
            moveY: 0,
            propagation: 'click here ... ',
            text: `PAGE DOWN`
        }
    },
    methods: {
        mouseOver() {
            this.msg = "mouse over";
            console.log("mouse over");
        },
        mouseLeave() {
            this.msg = "mouse leave";
            console.log("mouse leave");
        },
        click(event, data) {
            this.msg = "click";
            this.data = data;
            this.clickX = event.offsetX;
            this.clickY = event.offsetY;
            console.log(event);
        },
        doubleClick(event) {
            this.msg = "double click";
            console.log(event);
        },
        move(event) {
            this.moveX = event.offsetX;
            this.moveY = event.offsetY;
            this.msg = "move";
        },
        click1() {
            this.propagation += ' click1,'
        },
        click2() {
            this.propagation += ' click2,'
        },
        click3() {
            this.propagation += ' click3,'
        },
        onPageDown() {
            this.text = "OPLA"
        }
        
    }
}
  
Vue.createApp(Es03).mount('#es003')
  