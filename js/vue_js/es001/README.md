# es001

**** Qui facciamo il nostro primo esempio, metodi e direttive qui di seguito.

## metodi

- `data()` in **Vue.js 3** è una funzione (non più un oggetto) che ritorna l'oggetto *data*, lo **stato** della nostra app [API](https://vuejs.org/api/options-state.html#data)
```js
// direct instance creation
const data = { a: 1 }

// The object is added to a component instance
const vm = createApp({
  data() {
    return data
  }
}).mount('#app')

console.log(vm.a) // proxy => 1
console.log(vm.$data.a) // => 1
```
abbiamo a disposizione dei *proxy*, vvero dei riferimenti, alle variabili che `data` ci offre:
l'oggetto `$data` lega a se le proprità cui fanno riferimento i *proxy*
```
vm.$data.a => vm.a
```
Per il *design pattern Proxy* si può vedere [qui](https://en.wikipedia.org/wiki/Proxy_pattern). I *proxy* sono elementi essenziali nell'approccio *reactive* di Vuie.

- `methods`: di tipo `{ [key: string]: Function }`, per le API [qui](https://vuejs.org/api/options-state.html#methods).
```js
const app = createApp({
  data() {
    return { a: 1 }
  },
  methods: {
    plus() {
      // proxy di ap.$data.a
      this.a++
    }
  }
})

const vm = app.mount('#app')

vm.plus()
console.log(vm.a) // => 2
```
- `mounted()`: chiamato quando *instance* è `mounted` [API](https://vuejs.org/api/application.html#createapp).
- `Vue.createApp()`: un *factory method* che ritorna un *instance*, per le API [qui](https://v3.vuejs.org/api/global-api.html#createapp).
- `mount(...)`: per la QPI [qui](https://v3.vuejs.org/api/application-api.html#mount).

## direttive

Il ***virtual DOM*** prevede direttive non presenti come attributi nei normali file HTML.
- `v-bind` [API](https://vuejs.org/api/built-in-directives.html#v-bind) permette di collegarsi dinamicamente, *bind* appunto, ad un attributo. Nel nostro caso un esempio di `title` [qui](https://www.w3schools.com/tags/att_global_title.asp).
- `v-on` [qui](https://v3.vuejs.org/api/directives.html#v-on) permette di attaccare un *event listener*.

## altri riferimenti

[1] "Reactivity in Depth" [qui](https://vuejs.org/guide/extras/reactivity-in-depth.html).  