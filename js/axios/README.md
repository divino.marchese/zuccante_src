# una esercitazione per il PON Node (febbraio 2023)

I seguenti appunti sono stati proposti per gli studenti del modulo PON di JS e Node del 2023.

## requisiti

Per la nostra esercitazione Node va preparato ed installato nella macchina. Fra le estensioni di VSC suggeriamo l'installazione di "Live Server" per testare un server vero e `markdownlint` per vedere i file `.md`, vedi [5]!

## strumenti di Firefox

Firefox possiede strumenti per lo sviluppo, dare `CTRL` + `SHIFT` = `i`.

## per provare Node e JS senza installare nulla

Ad esempio c'è RunKit [qui](https://runkit.com): richiede registrazione. Node possiede naturalmente la possibilità di essere usato in modo interattivo su terminale! Basta digitare `node` (se installato).

## HTTP e CRUD

HTTP è protocollo testuale che permette la comunicazione fra client e server. Interagendo col nostro server faremo alcune operazioni sul database (vedi poi `json-server`) che riassumiamo con l'acronimo CRUS, vedi [7]. Legeremo i metodi HTP a tali azioni nel modo seguente
- `POST` -> **Create**.
- `GERT` -> **Read**.
- `PUT` o `PATCH` -> **Update**.
- `DELETE` -> **Delete**.

## json

Il server Web non mostra solo pagine HTML ma può lavorare fornendo informazioni testuali. JSON che imita gli oggetti JS (vedi poi)
permette di codificare l'informazione in modo efficace, [qui](https://www.w3schools.com/js/js_json_intro.asp) per un'introduzione su W3SChool, vedasi anche [1].

## json-server

Un pacchetto di Node, vedi [w], che va installato globalmente
```
npm install -g json-server
```
Se Node è stato configurato in modo corretto l'eseguibile `json-server` è già disponibile nel `PATH`. Utile dare
```
json-server --help
```
Per lanciare il server con il suo database (un file `json`)
```
json-server -w db.json
```

## curl

I primi esperimenti per interagire col server possono essere fatti, coltre che con Firefox anche con `curl`, vedi [3]. Vediamo come inserire un nuovo post usando il database `db.json`. A terminale (`>` sta ad indicarci che l'istruzione non è ancora terminata, non lo scriviamo noi ma ci appare al terminale)
```
curl -H "Content-Type: application/json" \
> -X POST \
> -d '{"id": 6, "post": "sesto post", "user_id": 4}' \
> http://localhost:3000/posts
```

## gli oggetti di Javascript

Per una introduzione provare il tutorial di W3School [qui](https://www.w3schools.com/js/js_objects.asp).
JS ci offre anche delle sintassi molto comode, ad esempio
```js
const name = "Bepi";
const surname = "Sbrisa";
const user = { name: name, user: user }
```
è equivalente a
```js
const name = "Bepi";
const surname = "Sbrisa";
const user = { name, user }
```
È bene familiarizzarsi con sintassi del tipo
```js
user.name
```

## axios

Funziona sia su Node (lato server) che su Js (lato client), usa la stessa sintassi solo che
- Node lavora sul server in modo asincrono,
- `XMLHttpRequests` lavora lato client con una sintassi molto simile a `fetch`.
Non resta che da leggere con attenzione, selezionando gli argomenti, la documentazione [qui](https://axios-http.com/docs/intro).

## il form negli esempi

Molto spesso troveremo una cosa del tipo
```js
form.addEventListener('submit', (event) => {
    event.preventDefault();
    ...
});
```
`event.preventDefault();` previene l'utilizzo (un tempo) canonico di chiamare il Server per eseguire uno script, qui lavoreremo lato client!

## manca qualcosa

Manca il `DELETE` che può essere fatto per esercizio.

## materiali per capire

[1] "Json" da Wikipedia (en) [qui](https://en.wikipedia.org/wiki/JSON).  
[2] "Json Server" [qui](https://www.npmjs.com/package/json-server).  
[3] "CURL tutorial" [qui](https://curl.se/docs/manual.html).  
[4] "How to use Axios POST requests" [qui](https://blog.logrocket.com/how-to-use-axios-post-requests/).  
[5] `markdown` da Wikipedia (en) [qui](https://en.wikipedia.org/wiki/Markdown).  
[6] Axios (home page) [qui](https://axios-http.com/).  
[7] CRUD su Wikipedia (en) [qui](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete).