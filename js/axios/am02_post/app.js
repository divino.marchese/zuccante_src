const form = document.getElementById('myForm');

form.addEventListener('submit', (event) => {
    event.preventDefault();
    let id = 0;
    const name = form.elements['name'].value;
    axios.get("http://localhost:3000/users/").then((response) => {
        id = response.data.length + 1;
        console.log(id);
    }).then(() => {
        console.log({ id, name });
        axios.post('http://localhost:3000/users', { id, name })
    }).then(function (response) {
        console.log(response);
    }).catch((error) => { console.log(error)});
});