const form = document.getElementById('myForm');

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const name = form.name.value;
    const id = 1;
    const user = { id, name };
    axios.put('http://localhost:3000/users/1', user).then((response) => {
        console.log(response);
        console.log(user);
    }).catch((error) => { console.log(error) });
});

axios.get("http://localhost:3000/users/1").then((response) => {
    let user = response.data;
    console.log(user);
    form.name.value = user.name;
});