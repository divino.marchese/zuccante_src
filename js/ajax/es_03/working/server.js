/*
curl 127.0.0.1:3000
curl -X POST -d '{"foo" : "bar"}' 127.0.0.1:3000
*/

const http = require('http')

const port = 3000
const host = '127.0.0.1'

const server = http.createServer((req, res) => {

  if (req.method == 'POST') {
    console.log('oooooooooo MAIN REQUEST oooooooooooooooooooooooo');
    console.log(`METHOD: ${req.method}`);
    console.log(`Content-Type: ${req.headers['content-type']}`);
    console.log(`Origin: ${req.headers['origin']}`);
    console.log('oooooooooooooooooooooooooooooooooooooooooooooooo');
    let body = '';
    req.on('data', (chunk) => {
      body += chunk;
      console.log("************* JSON BODY ************************");
      console.log('body: ' + body);
      console.log("************************************************");
    });
    req.on('end', () => {
      data = JSON.parse(body);
      console.log("************* FORM OBJECT **********************");
      console.log("name: " + data.name);
      console.log("kind: " + data.kind);
      console.log("check: " + data.check);
      console.log("***********************************************");
    });
  } 
  else if (req.method == 'GET') {
    console.log('GET')
  } else if (req.method == 'OPTIONS') {
    console.log('oooooooooo PREFLISGHT REQUEST oooooooooooooooooo');
    console.log(`METHOD: ${req.method}`);
    console.log(`Origin: ${req.headers['origin']}`);
    console.log(`Access-Control-Request-Method: ${req.headers['access-control-request-method']}`);
    console.log(`Access-Control-Request-Headers: ${req.headers['access-control-request-headers']}`);
    console.log('oooooooooooooooooooooooooooooooooooooooooooooooo');
    res.writeHead(200, {
      'Access-Control-Allow-Origin': 'http://127.0.0.1:5500',
      'Access-Control-Allow-Method': 'GET, POST, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Content-Type': 'application/json'
    });
    res.end();
  }
})

server.listen(port, host, () => {
  console.log(`Server running at http://${host}:${port}/`);
});
