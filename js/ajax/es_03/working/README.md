# Le richieste AJAX ed il CORS

In questo esempio tentiamo una richiesta AJAX. Abbiamo usato due server, quindi il nostro è un approccio *cross domain*; il client gira col server dell'estensione *live server* di Visual Studio Code
```
res.writeHead(200, {
  'Access-Control-Allow-Origin': 'http://127.0.0.1:5500',
  'Access-Control-Allow-Method': 'POST, GET',
  'Access-Control-Allow-Headers': 'Content-type', 
   'Content-Type': 'application/json'
})
```
L'oggetto `XMLHttpRequest` nei Browser moderni dialoga con server che gestiscono il CORS.

## Cross-Origin Resource Sharing (CORS)

Come scritto sopra AJAX oltre ad essere uno strumento utile nel realizzare l'**usabilità** ma anche per la **sicurezza**, ed è qui che arriva [CORS](https://developer.mozilla.org/en/docs/Web/HTTP/CORS).
