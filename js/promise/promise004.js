// chaining

let p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
    resolve(333);
  }, 3 * 1000);
});

let p2 = new Promise((resolve, reject) => {
    setTimeout(() => {
    resolve(777);
  }, 4 * 1000);
});

p1.then((value) => {
    console.log(value);
    return p2
}).then((value) => {
  console.log(value);
}).then(() => console.log("the end"));
