Promise.resolve(123)
    .then(x => {
        console.log(`x === 123: ${x === 123}`);
    });

const myError = new Error('My error!');
Promise.reject(myError)
    .catch(err => {
        console.log(`err === myError: ${err === myError}`);
    });
