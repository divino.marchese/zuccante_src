async function getDelayedNumber(n) {
    let promise = new Promise((resolve, reject) => {
      setTimeout(() => resolve("done!"), 1000)
    });
    // blocking
    let result = await promise; 
  
    console.log(`result: ${result}`); 
  }
  
getDelayedNumber(666);
