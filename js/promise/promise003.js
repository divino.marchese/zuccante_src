// chaining

let keepsHisWord = true;
// let keepsHisWord = false;

let promise = new Promise((resolve, reject) => {
  if (keepsHisWord) {
    resolve("xe ben");
  } else {
    reject("no xe ben");
  }
});

let msg = promise.catch(msg => console.log(msg)).then(msg => console.log(msg));
