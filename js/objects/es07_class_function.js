// class syntax
class MyClass1 {
    constructor(prop1, prop2) {
        this.prop1 = prop1;
        this.prop2 = prop2;
    }
}

let obj1 = new MyClass1(2, 2);
console.log(`obj1 name properties: ${Object.getOwnPropertyNames(obj1)}`)

// function syntax
function MyClass2(prop1, prop2) {
    this.prop1 = prop1;
    this.prop2 = prop2;
}

let obj2 = new MyClass2(2, 2);
console.log(`obj2 name properties: ${Object.getOwnPropertyNames(obj2)}`)