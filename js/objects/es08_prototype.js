const a = {};
const b = {__proto__: a};
const c = {__proto__: b};
// check prototype chain
console.log(a.isPrototypeOf(b));
console.log(a.isPrototypeOf(c));
console.log(b.isPrototypeOf(c));
console.log(b.isPrototypeOf(a));
console.log(c.isPrototypeOf(a));
console.log(c.isPrototypeOf(b));