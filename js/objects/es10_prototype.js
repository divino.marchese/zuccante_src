let obj1 = {
  isProto: false,
  name: "awa awa",
  task: function(){
    console.log("execute action");
  }
};

let obj2 = Object.create(obj1);
console.log(obj2.name);
console.log(Object.getPrototypeOf(obj2));
console.log(obj2.__proto__);
obj2.task();
let obj3 = Object.create(null);
console.log(Object.getPrototypeOf(obj3));