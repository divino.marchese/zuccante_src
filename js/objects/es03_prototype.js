let obj = {prop: 'prop'};
let proto = {prop_proto: 'prop_proto'};
Object.setPrototypeOf(obj, proto);
console.log(proto === obj.__proto__); // setter
console.log(obj.__proto__ === Object.getPrototypeOf(obj)); // getter
console.log(`own property: ${obj.prop}`);
console.log(`prototype property: ${obj.prop_proto}`);
console.log(Object.getOwnPropertyNames(obj));
console.log(Object.keys(obj)); // modern
// shadowing
obj.prop_proto = 3;
console.log(obj.__proto__.prop_proto);
console.log(obj.prop_proto);



