let obj1 = {};
Object.defineProperties(obj1, {
    one: {enumerable: true, value: 1},
    two: {enumerable: false, value: 2},
});
console.log('for obj1')
console.log(Object.keys(obj1)); 
console.log(Object.getOwnPropertyNames(obj1)); 

let obj2 = {};
obj2.one = 1;
obj2.two = 2;
console.log('for obj2')
console.log(Object.keys(obj2)); 
console.log(Object.getOwnPropertyNames(obj2)); 
console.log('... othe examples')
let array = ['a', 'b', 'c', 'd'];
console.log(Object.keys(array));  
console.log(Object.getOwnPropertyNames(array));  

