const cat1 = {
    eat() {},
    sleep() {},
    talk() {}
};

class Cat {
    eat() {}
    sleep() {}
    talk() {}
}

const cat2 = new Cat();

console.log(Object.getOwnPropertyNames(cat1));
console.log(Object.getOwnPropertyNames(cat2));
console.log(Object.getOwnPropertyNames(Object.getPrototypeOf(cat1))); 
console.log(Object.getOwnPropertyNames(Object.getPrototypeOf(cat2))); 
console.log('***************************************');
console.log(Object.keys(cat1)); 
console.log(Object.keys(cat2)); 
console.log(Object.keys(Object.getPrototypeOf(cat1)));
console.log(Object.keys(Object.getPrototypeOf(cat2)));

