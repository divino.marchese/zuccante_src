tclass MyClass {
    constructor(){
        console.log("object MyClass created");
        this.prop = "MyClassProp";
    }
}

function MyFunction(){
    console.log("object MyClass created");
    this.prop = "MyClassProp";
}

let obj1 = new MyClass();
let obj2 = new MyFunction();

console.log('********* typeof **********');

console.log(typeof obj1);
console.log(typeof obj2);

console.log('********* construct.name **********');

console.log(Object.getPrototypeOf(obj1).constructor.name);
console.log(Object.getPrototypeOf(obj2).constructor.name);

console.log('********* __proto__ **********');

console.log(Object.getPrototypeOf(obj1) === MyClass.prototype);
console.log(Object.getPrototypeOf(obj2) === MyFunction.prototype);

console.log('********* Object.getOwnPropertyNames(...) **********');

console.log(Object.getOwnPropertyNames(obj1));
console.log(Object.getOwnPropertyNames(obj2));

console.log('********* mascheramento **********');

MyClass.prototype.prop = "protoPropC";
MyFunction.prototype.prop = "protoPropF";

console.log(Object.getPrototypeOf(obj1));
console.log(Object.getPrototypeOf(obj2));

console.log(obj1.prop);
console.log(obj2.prop);





