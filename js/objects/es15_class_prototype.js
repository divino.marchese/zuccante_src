class MyClass{
    constructor(){
        console.log('a MyClass object');
        this.prop = "obj of MyClass";
    }
}

let obj1 = new MyClass();
console.log(MyClass.prototype === Object.getPrototypeOf(obj1));


function MyObject(){
    console.log('a MyObject object');
    this.prop = "obj of MyObject";
}

let obj2 = new MyObject();
console.log(MyObject.prototype === Object.getPrototypeOf(obj2));

console.log(`properties of obj1: ${Object.getOwnPropertyNames(obj1)}`);
console.log(`properties of obj1.__proto__: ${Object.getOwnPropertyNames(obj1.__proto__)}`);
console.log(`enumerable properties of obj1.__proto__: ${Object.keys(obj1.__proto__)}`);
console.log(`properties of obj2: ${Object.getOwnPropertyNames(obj2)}`);

