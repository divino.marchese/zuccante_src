// this and object without a class definition

let rabbit = {};
rabbit.color = "white";
rabbit.speak = function(line) {
console.log(`The ${this.color} rabbit says '${line}'`);
};
rabbit.speak("I'm alive.");