lass Example {
    constructor() {
        this.prop = '[prop]';
        const proto = Object.getPrototypeOf(this);
        // not inherited from its prototype (read only)
        console.log(`prototype own properties: ${Object.getOwnPropertyNames(proto)}`);
        console.log(`prototype keys: ${Object.keys(proto)}`);
        console.log(`object own properties: ${Object.getOwnPropertyNames(this)}`);
        console.log(`object keys: ${Object.keys(this)}`);
    }
    first() { }
    second() { }
    static third() { }

}

new Example(); 
