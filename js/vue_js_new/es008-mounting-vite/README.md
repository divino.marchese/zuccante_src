# es008-mounting-vite

Qui riproponiamo il nostro secondo esempio! Vediamo di ripercorrere i passi.

## `main.js`

È il file principale e viene semplificato rispetto alla App autocreata!
```js
import { createApp } from 'vue'

import App from './App.vue'
createApp(App).mount('#app')
```
- `createApp()` [api](https://vuejs.org/api/application.html#createapp): prende un file `.vue` per creare una *app* invece di separare i tre file (`,js`, `.html` e `.css` eventuale).

## `App.vue`

Uniamo i 3 component. Nella parte `<\style>` importiamo il `css` di Bulma (non lo installiamo direttamente nella nostra Ap) per ragioni didattiche). Nel `<template>` pe4r alleggerire ci limitiamo al `<div>`. Ovviamente, lo rimarchiamo, `<script>` segue la `Composition API` (ecco il `setup` che usanda l'aktra API eviteremo).