const { createApp, ref } = Vue;

const app = createApp({
    setup() {
        const memos = ref([
            {text: "memo1"},
            {text: "memo2"}
        ]);
        const deleteMemo = (index) => {
            memos.value.splice(index,1);
        };
        const addMemo = (text) => {
            if(text.length == 0) return;
            memos.value.push({text: text})
        };
        const deleteAll = () => {
            let n = memos.value.length;
            memos.value.splice(0, n);
        }
        return {
            memos, deleteMemo, addMemo, deleteAll
        };
    }
});
  
app.mount('#root');


  