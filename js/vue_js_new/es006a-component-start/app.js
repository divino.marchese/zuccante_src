const { createApp, ref, watch } = Vue;

const app = createApp({
    setup() {
        const memos = ref([{text: "primo"}, {text: "secondo"}]);
        const addMemo = (text) => {
            if (text.length == 0) return;
            memos.value.push({ text: text })
        };
        const deleteAll = () => {
            memos.value.splice(0, memos.value.length);
        };
        return {
            memos, addMemo, deleteAll
        };
    }
});
  

app.component('memo', {
    props: ['text'],
    setup() {},
    template:
        `<span class="tag is-warning">
        {{ text }}
        <button class="delete is-small"></button>
        </span>`
});

app.mount('#root');
