const { createApp, ref, onMounted } = Vue;

const app = createApp({
    setup() {
        const msg = ref("try");
        const data = ref(0);
        const clickX = ref(0);
        const clickY = ref(0);
        const moveX = ref(0);
        const moveY = ref(0);
        const propagation = ref('click here ... ');
        const text = ref('PAGE DOWN');
        const message = ref(`'You loaded this page on ${new Date().toLocaleString()}`);
        const mouseOver = () => {
            msg.value = "mouse over";
            console.log("mouse over");
        };
        const mouseLeave = () => {
            msg.value = "mouse leave";
            console.log("mouse leave");
        };
        const click = (e, d) => {
            msg.value = "click";
            data.value = d;
            clickX.value = e.offsetX;
            clickY.value = e.offsetY;
            console.log(e);
        };
        const doubleClick = (e) => {
            msg.value = "double click";
            console.log(e);
        };
        const move = (e) => {
            moveX.value = e.offsetX;
            moveY.value = e.offsetY;
            msg.value = "move";
            console.log(e);
        };
        const click1 = () => {
            propagation.value += ' click1,'
        };
        const click2 = () => {
            propagation.value += ' click2,'
        };
        const click3 = () => {
            propagation.value += ' click3,'
        };
        const onPageDown = () => {
            text.value = "OPLA"
        };
        return {
            msg, data, 
            clickX, clickY,
            moveX, moveY,
            propagation, text, message,
            mouseOver, mouseLeave, 
            click, doubleClick,
            move,
            click1, click2, click3,
            onPageDown 
        };
    }
});
  
app.mount('#root');  