# es003

**[240404]** Il seguente esempio è tratto da [qui](https://www.youtube.com/watch?v=CYPZBK8zUik&list=PL4cUxeGkcC9hYYGbV60Vq3IXYNfDk8At1&index=6).
Sul tutorial ufficiale "Event Handling" [qui](https://vuejs.org/guide/essentials/event-handling.html).  Qui leggiamo come Vue ci metta a disposizione la variabile interna
```javascript
$event
```
per accedere all'evento del DOM originale.

Per accedere alla console con Firefox `More Tools -> Dev Tools`.  
**NB** Qui useremop la scorciatoia `v-on:mouseover` sostituito da `@mouseover` così come `v-bind:href="..."` con `:href="..."`.

## v-bind

Come già segnalato nell'esempio perecedente ecco le [API](https://vuejs.org/api/built-in-directives.html#v-bind).

## propagazione

Provare a rimuovere `stop` su
```js
@click.stop
```
la propagazione degli eventi (dall'interno all'esterno) nel DOM viene interrotta! Vue offre dei *modifiers* per i quali rimandiamo al tutorial succitato!

## altri materiali

[1] "Mouse events' [qui](https://javascript.info/mouse-events-basics) su MDN.  
[2] "JavaScript Mouse Events" da Javascript Tutorial [qui](https://www.javascripttutorial.net/javascript-dom/javascript-mouse-events/).