# es006b

**[240405]** Qui introduziona un importantissimi concetto: "provide adn inject".
Il riferimento nella guida è [qui](https://vuejs.org/guide/components/provide-inject.html). Con *provide* intendiamo il fornire da parte di un componente  (ai duoi discendenti) una qualche *property*, *inject* è l'operazione grazie a cui un discendente recupera tale *property*. Nel nostro esempio recuperiamo nel componento la lista dei memo.
- `provide()` [api](https://vuejs.org/api/composition-api-dependency-injection.html#provide).
- `inject()` [api](https://vuejs.org/api/composition-api-dependency-injection.html#inject).

## la variabile `$parent`

Tale variabile, presente nella Option API, non è piu`necessaria: tale approccio risulta molto più flessibile ed elegante.