const { createApp, ref, provide, inject } = Vue;

const app = createApp({
    setup() {
        const memos = ref([{text: "primo"}, {text: "secondo"}]);
        const addMemo = (text) => {
            if (text.length == 0) return;
            memos.value.push({ text: text })
        };
        const deleteAll = () => {
            memos.value.splice(0, memos.value.length);
        };
        provide('memos', memos);
        return {
            memos, addMemo, deleteAll
        };
    }
});
  

app.component('memo', {
    props: ['text', 'index'],
    setup() {
        const memos = inject("memos");
        const deleteMemo = (index) => {
            memos.value.splice(index, 1);
        };
        return { deleteMemo };
    },
    template:
        `<span class="tag is-warning">
        {{ text }}
        <button class="delete is-small" v-on:click="deleteMemo(index)"></button>
        </span>`
});

app.mount('#root');