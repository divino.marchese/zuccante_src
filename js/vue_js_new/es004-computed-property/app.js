const { createApp, ref, computed } = Vue;

const app = createApp({
    setup() {
        const name = ref("");
        const surname = ref("");
        const picked = ref("");
        const price = ref(0.0);
        const vat = ref(0.21);
        const discount = ref(false);
        const fullName = computed(() => {
            return `full name: ${picked.value} ${name.value} ${surname.value}`;
        });
        const fullPrice = computed(() => {
            let d = discount.value ? 0.2 : 0.0;
            return price.value * (1 - d) * (1 + vat.value);
        });
        return {
            name, surname, picked, price, vat, discount,
            fullName, fullPrice
        };
    }
});
  
app.mount('#root');