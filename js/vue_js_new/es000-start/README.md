# es001 start

**[240403]** Prima aplicazione con VueJS. Il riferimento per le API è **Composition API** [api](https://vuejs.org/api/), in particolare abbiamo usato: 
- `ref()` [api](https://vuejs.org/api/reactivity-core.html#ref) il *reactive object*.
- `setup()` [api](https://vuejs.org/api/composition-api-setup.html#basic-usage) il metodo principale. Esso espone - vedi `return` - *function* e *recative object* in modo che questi posano essere chiamati nella app.

## nota

In ES `const` corrisponde al `final` in altri linguaggi di programmazione quando si lavora con oggetti.