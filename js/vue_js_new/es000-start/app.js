const { createApp, ref } = Vue;

const app = Vue.createApp({
  setup() {
    const counter = ref(0);
    const increaseCounter = () => { 
      counter.value++;
    };
    const resetCounter = () => { 
      counter.value = 0;
    };
    return { 
      counter, 
      increaseCounter,
      resetCounter
    }; 
  }
});

app.mount('#root');