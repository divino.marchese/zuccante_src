# es009-memo-vite

Introduziamo il primo componente; usiamo come al solito il *Single File Component* (vedi api). 
- `defineProps()` [api](https://vuejs.org/api/sfc-script-setup.html#defineprops-defineemits)
Procediamo scrivendo (come già fatto in un precedente esempio)
```js
defineProps(['text']);
```
o più elegantemente
```js
const props = defineProps({
  text: String
})
```
con il **controllo sui tipi**. Notiamo che `index` non serve in quanto viene recepito *deafult* (nelle api non è scritto, ma funziona!).