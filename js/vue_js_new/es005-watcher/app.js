const { createApp, ref, watch } = Vue;

const app = createApp({
    setup() {
        const num = ref(0);
        const double = ref(0);
        const oldNum = ref(0);
        watch(num, async (newN, oldN) => {
            double.value = 2 * newN; 
            oldNum.value = oldN;
        });
        return {
            num, oldNum, double
        };
    }
});
  
app.mount('#root');
