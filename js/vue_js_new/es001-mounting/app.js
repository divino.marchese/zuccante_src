
const { createApp, ref, onMounted } = Vue;

const app = createApp({
    setup() {
        const message = ref(`'You loaded this page on ${new Date().toLocaleString()}`);
        const counter = ref(0);
        const resetCounter = () => {
            counter.value = 0;
        };
        onMounted(() => {
            setInterval(() => {
                if (counter.value++ == 9) {
                    counter.value = 0;
                }
            }, 1000)
        });
        return {
            message, counter, resetCounter
        };
    }
});
  
app.mount('#root');
  