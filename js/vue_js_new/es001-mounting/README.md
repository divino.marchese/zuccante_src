# es001 mounting

Ogni ap VueJs ha un **ciclo di vita** con degli *hooks*, vedi [qui](https://vuejs.org/guide/essentials/lifecycle.html#registering-lifecycle-hooks) la guida! Abbiamo integrato l'esempio precedente con
- `onMounted()` [api](https://vuejs.org/api/composition-api-lifecycle.html#onmounted) che chiamiamo non appena l'appe viene montata.

## direttive

Il ***virtual DOM*** prevede direttive non presenti come attributi nei normali file HTML.
- `v-bind` [API](https://vuejs.org/api/built-in-directives.html#v-bind) permette di collegarsi dinamicamente, *bind* appunto, ad un attributo. Nel nostro caso un esempio di `title` [qui](https://www.w3schools.com/tags/att_global_title.asp).
- `v-on` [qui](https://v3.vuejs.org/api/directives.html#v-on) permette di attaccare un *event listener*.

## altri riferimenti

[1] "Reactivity in Depth" [qui](https://vuejs.org/guide/extras/reactivity-in-depth.html).  