# es010-bitcoin-pwa

## vite pwa

Come riposrtato nei riferimenti procediamo nell'installazione come **modulo di sviluppo** (opzione `-D`)
del *plugin*
```
npm install -D vite-plugin-pwa
```
Una *PWA* la si sviluppa come una normalissima web app. Va ritoccato il file di configurazione di cui noi abbiamo scelto una configurazione minimale [qui](https://vite-pwa-org.netlify.app/guide/pwa-minimal-requirements.html).
```js
VitePWA({
    includeAssets: ['favicon.ico', 'apple-touch-icon.png', 'mask-icon.svg'],
    manifest: {
        name: 'My Bitcoin App',
        short_name: 'Bitcoin',
        description: 'A bitcoin value app',
        theme_color: '#000000',
        icons: [
          {
            src: 'pwa-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
    }
})
```

## deployment

Dato
```
npm run build
```
appare la cartella `dist` con la *PWA*, ricordrarsi di aggiungere le icone! Per testarla basta dare ora
```
npm run vite preview
```
Riucordiamo che
```
npm run dev
```
mostra soltanto l'app Vue e non la *PWA*. La cartella `dist` - **solo lei** - è pronta per il *deployment*; noi abbiamo usato Netifly [qui](https://app.netlify.com).
La nostra appa è scaricabile [qui](https://661e87f981320723273bebb3--inquisitive-rugelach-43dd1a.netlify.app/).

## installare e disinstallare pwa

In Chrome la si installa al solito modo, per disinstallarla andare su
```
chrome://apps/
```

## riferimenti

[1] "Vite PWA' [qui](https://vite-pwa-org.netlify.app/).  
[2] "PWA in VUE VITE" [qui](https://dev.to/adefam/pwa-in-vue-vite-53a3).
