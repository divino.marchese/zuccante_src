# es006c

**[240103]** Dopo la ***dependency injection*** vediamo i *custom event* affrontiamo la questione degli eventi nei *component*, vedi [qui](https://vuejs.org/guide/components/events.html).

## i detagli di `setip(...)`

Dalle [api](https://vuejs.org/api/composition-api-setup.html#accessing-props) notiamo che `setUp` presenta all'abbisogna 2 argomenti
```js
setUp(props, context)
```
mediante `props` possiamo accedere all'aggetto omonimo definito nel componente come sua *property*; `context` invece è
```is
{ attrs, slots, emit, expose }
```
in particolare possiamo usare `emit` che sostituisce il vecchio `$emit` e ci permette di emettere eventi. Come il  etodo `$emit`
```js
emit('my-event`, arfs)
```
equivalente a
```js
emit('myEvent`, arfs)
```
(la sintassi del cammello diviene la sintastti kebab) Usando il componente come template (in entrambi i casi) abbiamo scritto
```
<span class="m-1" v-for="(item, index) in memos">
<memo :text="item.text" :index="index" @delete-memo="deleteMemo"></memo>
</span>
```
quindi
```
emissione evento --> chiamata callback con eventuali parametri
```
La sintassi, poco documentata, in realtà dovrebbe risultare chiara!
```js
setup(props, {emit}) {
    const index = props['index'];
    const throwEvent = () => {
        console.log(`deleted ${index} via custom event`);
        emit('delete-memo', index);
    }
    return { throwEvent };
},
```
Notiamo come `throwEvent` vada "esposto" in quanto verrà usato nella successiva *property* del *template*.

## dichiarazione degli eventi emessi

A completare il tutto possimao aggiungere, oltre alla *property* `props` e `template`, la dichiarazione esplicita, non obbligatoria ma utile non solo nello stile, nella *property* `emitts` gli eventi emessi dal componente. 
