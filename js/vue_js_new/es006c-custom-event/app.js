const { createApp, ref, emit } = Vue;

const app = createApp({
    setup() {
        const memos = ref([{ text: "primo" }, { text: "secondo" }]);
        const addMemo = (text) => {
            if (text.length == 0) return;
            memos.value.push({ text: text })
        };
        const deleteMemo = (data) => {
            memos.value.splice(data, 1);
        };
        const deleteAll = () => {
            memos.value.splice(0, memos.value.length);
        };
        return {
            memos, addMemo, deleteMemo, deleteAll
        };
    }
});


app.component('memo', {
    emits: ['delete-memo'],
    props: ['text', 'index'],
    setup(props, {emit}) {
        const index = props['index'];
        const throwEvent = () => {
            console.log(`deleted ${index} via custom event`);
            emit('delete-memo', index);
        }
        return { throwEvent };
    },
    template:
        `<span class="tag is-warning">
        {{ text }}
        <button class="delete is-small" @click="throwEvent"></button>
        </span>`
});

app.mount('#root');


