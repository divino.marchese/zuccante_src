var http = require('http');
var dt = require('./mymod');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write(dt.dateMsg());
  res.end();
}).listen(8080); 