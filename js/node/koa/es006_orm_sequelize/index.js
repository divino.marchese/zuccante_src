const Koa = require('koa');
const koaBody = require('koa-body');
const Router = require('koa-router');
const Sequelize = require('sequelize');

const app = new Koa();
const router = new Router();

app
  .use(koaBody())  
  .use(router.routes());

// ***** DB *****

// db connection
let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: { max: 5, min: 0, idle: 2000 },
});

// sync models
const Memo = sequelize.import(__dirname + '/memo');
Memo.sync();

// ***** END DB *****

// **** REST ******

// GET all
router.get('/', async (ctx) => {
    let memos = await Memo.findAll();
    ctx.body = JSON.stringify(memos);
});

// GET only one using params
router.get('/:title', async (ctx) => {
    let title = ctx.params.title;
    let memo = await Memo.findOne({ where: {title: title}});
    ctx.body = JSON.stringify(memo); 
});

// POST: insert
router.post('/form', async (ctx) => {
    let memo = ctx.request.body; 
    console.log(memo.title);
    await Memo.create(memo);
    ctx.body = `created: ${JSON.stringify(memo)}`; 
});

// PUT: update
router.put('/', async (ctx) => {
    let memo = ctx.request.body; 
    await Memo.update({
        description: memo.description
    },
    {
    where: {
        title: memo.title
    }});
    ctx.body = `updated: ${JSON.stringify(memo)}`;
});

// DELETE: delete
router.del('/:title', async (ctx) => {
    let title = ctx.params.title;
    await Memo.destroy({
    where: {
            title: title
    }
    })
    ctx.body = `delete memo with title: ${title}`;  
});

// *** END REST ****

  
app.listen(3000);