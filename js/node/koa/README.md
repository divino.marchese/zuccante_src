# Koa

E' un web framework fatto da chi ha creato *Express*, [qui](https://koajs.com/#introduction) l'home page. Abbandonadno il *callback* usa i `Promise` avendo un codice più pulito, efficace e leggibile.

**materiali**

- examples from GitHub [qui](https://github.com/koajs/examples)
- Wiki, *modules for Koa* [qui](https://github.com/koajs/koa/wiki#body-parsing).
- "Koa.js basics in 7-step tutorial" [qui](https://ralabs.org/blog/koa-basics-in-7-step-tutorial/).
