const Koa = require('koa');
const Router = require('koa-router');
const koaBody = require('koa-body');

const app = new Koa();
const router = new Router();
const views = require('koa-views');

let fs = require('fs'); 

app.use(koaBody({
    formidable: {
        uploadDir: __dirname+'/uploads',
        keepExtensions: true 
    },    
    multipart: true,
  }))
  .use(views(__dirname + '/views', {extension: 'ejs'}))
  .use(router.routes());

router.get('/', async (ctx) => {
    let files = [];
    fs.readdir(__dirname+'/uploads/', (err, items) => {
        if ( err ) console.log('ERROR: ' + err);
        items.forEach((item) => files.push(item));
        console.log(items);
    });
    await ctx.render('index.ejs',{files: files})
});

router.post('/upload', async (ctx) => {
    const fileName = ctx.request.files.fileToUpload.name;
    const path = ctx.request.files.fileToUpload.path;
    console.log("upload:", fileName);
    // rename file just uploaded
    fs.rename(path, __dirname+'/uploads/' + fileName, (err) => {
        if ( err ) console.log('ERROR: ' + err);
    }); 
    // read
    let files = [];
    fs.readdir(__dirname+'/uploads/', (err, items) => {
        if ( err ) console.log('ERROR: ' + err);
        items.forEach((item) => files.push(item));
    });
    await ctx.render('index.ejs',{files: files})
});

  
app.listen(3000);