const Koa = require('koa');
const koaBody = require('koa-body');
const Router = require('koa-router');

const app = new Koa();
const router = new Router();

app
  .use(koaBody())  
  .use(router.routes());
  //.use(router.allowedMethods());


router.get('/form', (ctx) => {
  ctx.body = 'form';
});

router.post('/form', (ctx) => {
  ctx.body = `Request Body: ${JSON.stringify(ctx.request.body)}`;  
});
  
app.listen(3000);