# stream

Vi sono ...

## Readable

Vediamo qui un `ReadableStream`, due evnti risultano cruciali:
- `readable`: evento generato quando i dati sono disponibili, una sorta di *start* [API](https://nodejs.org/docs/latest/api/stream.html#event-readable).
- `data`: evento emesso quando un *chunk* di dati è disponibile [API](https://nodejs.org/docs/latest/api/stream.html#event-data), l'evento `readable` è un `data` (ecco l'apparente stranezza del risultato).
- `end`: emesso alla fine della lettura dei dati [API](https://nodejs.org/docs/latest/api/stream.html#event-end).

Per il metodo `read([size])` rimandiamo alle [API](https://nodejs.org/docs/latest/api/stream.html#readablereadsize).

## 

## Transform

Gli *stream* di trasformazione gli creiamo usando, ad esempio, l'approccio semplificato
```js
const { Transform } = require('node:stream');

const myTransform = new Transform({
  transform(chunk, encoding, callback) {
    // ...
  },
}); 
```


## riferimenti

[4] `Transform` nella guida ufficiale [qui](https://nodejs.org/docs/latest/api/stream.html#stream_implementing_a_transform_stream).
