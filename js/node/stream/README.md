# stream

Node definisce uno `Stream` [API](https://nodejs.org/docs/latest/api/stream.html) come istanza del noto `EventEmitter`, abbiamo vari tipi di *stream*
- `Writable`: su cui possiamo aggiungere eventi (es: `fs.createWriteStream()`).
- `Readable`: da cui ascoltiamo eventi (es: `fs.createReadStream()`).
- `Duplex`: che permettono sia l'ascolto che l'aggiunta di eventi (ad esempio `net.Socket`).
- `Transform`: *stream duplex* per trasformare altri *stream* (es: `zlib.createDeflate()`).
Utile uno sguardo attento alle API dopo aver studiato gli eventi!

Gli `Stream` `Writable` e `Readable` entrambi posseggono un *buffer* interno.

In genere gli *stream* lavorano con strincge ed affini anche se per loro è previsto l*object mode* )vedi API) a differenza di quanto accade per altri linguaggi di programmazione, uno fra tutti `Dart`.

## `Readable`

Per leggere i dati deve essere chiamato il metodo `read(...)` (vedasi nelle API i dettagli sul *buffer* interno). Essi lavorano in modo **sincrono** ed **asincrono** che sarebbe quello più "naturale".
- Nel *flowing mode*, i dati vengono letti all'evento `'data'` come `EventEmitter` usando `on('data'`
- In *paused mode* il metodo `read()` deve essere chiama di volta in volta per consumare i dati (modalità sincrona o bloccante).
Per passare da una all'altra modalità abbiamo i metodi `pause()` e `resume()` o aggiungendo l'*handler* per l'evento `'data'`. ALtro importante evento è `'end'` che segna la fine dei dati forniti dallo *stream*. Raggiunta la "fine" dello *stream* `read()` ritorna `null`. Di seguto un segmento di codice tratto dalle API.
```js
const readable = getReadableStreamSomehow();
readable.on('data', (chunk) => {
  console.log(`Received ${chunk.length} bytes of data.`);
});
readable.on('end', () => {
  console.log('There will be no more data.');
}); 
```
L'evento `'readable'` viene emesso quando vis ono dati nello *stream* o quando la "fine" delo *stream* è stata raggiunta.
```js
const readable = getReadableStreamSomehow();
readable.on('readable', function() {
  // There is some data to read now.
  let data;

  while ((data = this.read()) !== null) {
    console.log(data);
  }
}); 
```

## `Writable`

Lo si usa in modo **sincrono** col metodo `write(...)`, esiste anche il metodo `end()` per terminare i dati immessi nell *strem* (vedi sopra), chiamato tale metodo non si può più chiamare `write()`.

## `Duplex`

Implementano sia `Readable` che `Writable`. Esempi lo sono i *socket* TCP.

## `Transform`

`Transform` sono `Duplex`, `zlib` e `crypto` sono due esempi utili.
Gli *stream* di trasformazione gli creiamo usando, ad esempio, l'approccio semplificato
```js
const { Transform } = require('node:stream');

const myTransform = new Transform({
  transform(chunk, encoding, callback) {
    // ...
  },
}); 
```
Nelle API c'è poco o nulla, leggere [4] per integrare informazioni!

## `pipeline`

Interessante metodo, più completo rispetto al `pipe()`. Dalle API un esempio di codice.
```js
const fs = require('node:fs');
const http = require('node:http');
const { pipeline } = require('node:stream');

const server = http.createServer((req, res) => {
  const fileStream = fs.createReadStream('./fileNotExist.txt');
  pipeline(fileStream, res, (err) => {
    if (err) {
      console.log(err); // No such file
      // this message can't be sent once `pipeline` already destroyed the socket
      return res.end('error!!!');
    }
  });
}); 
```


## riferimenti

[1] `node:stream` [API](https://nodejs.org/api/stream.html#duplex-and-transform-streams).  
[2] "Introduction to Node Js" [qui](https://nodejs.org/en/learn/getting-started/introduction-to-nodejs), ufficiale dal sito Node.   
[4] "Implementing Transform Stream" nella guida ufficiale [qui](https://nodejs.org/docs/latest/api/stream.html#stream_implementing_a_transform_stream).




