// const EventEmitter = require('events');
import { EventEmitter } from 'node:events';

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event1', () => {
  console.log('event1 occurred!');
});
myEmitter.on('event2', () => {
  console.log('event2 occurred!');
});

myEmitter.emit('event1');
myEmitter.emit('event2');


myEmitter.on('event3', function(a, b) {
  console.log('event3', a, b, this, this === myEmitter);
});

myEmitter.emit('event3', 'arg a', 'arg b');

