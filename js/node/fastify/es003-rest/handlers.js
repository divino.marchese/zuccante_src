// db
const posts = [
    {
        id: 1,
        title: 'This is an experiment'
    },
    {
        id: 2,
        title: 'Fastify is pretty cool'
    },
    {
        id: 3,
        title: 'Just another post, yea!'
    },
    {
        id: 4,
        title: 'The 4th post!'
    }
]

// Handlers
export const getAllPosts = (req, reply) => {
    reply.send(posts);
}

export const getPost = (req, reply) => {
    const id = parseInt(req.params.id) 
    const post = posts.find(post => post.id === id)
    reply.send(posts);
}

export const addPost = (req, reply) => {
    const id = posts.length + 1 
    const newPost = {
        id,
        title: req.body.title
    }
    posts.push(newPost)
    return newPost
}

export const updatePost = (req, reply) => {
    const id = parseInt(req.params.id);
    posts = posts.map(post => {
        if (post.id === id) {
            return {
                id,
                title: req.body.title
            }
        } else {
            return {id: post.id, title: post.title}
        }
    })
    return {
        id,
        title: req.body.title
    }
}

export const deletePost = (req, reply) => {
    const id = parseInt(req.params.id)
    posts = posts.filter(post => post.id !== id)
    // return { msg: `Post with ID ${id} is deleted` }
}