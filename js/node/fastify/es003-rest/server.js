import Fastify from 'fastify'
import { routes } from './routes.js'

const fastify = Fastify({
  logger: true
})

fastify.get('/', (request, reply) => {
  return { es003: 'rest' }
})

routes.forEach((route, index) => {
    fastify.route(route)
})

try {
  await fastify.listen({ port: 3000 });
  console.log('serever is running');
} catch (err) {
  fastify.log.error(err)
  process.exit(1)
}