# es003 -rest

**[240422]** Un semplice esempio di REST! Questo esempio separa in modo ordinato
le varie parti in classico approccio MVC!
- `fastify.route(...)` [api](https://fastify.dev/docs/latest/Reference/Routes/#full-declaration).
- `forEach(...)` per un ripasso da MDN [qui](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach).

## testing

# esperimenti

Proviamo con `curl` a testare il tutto dopo aver dato
```
node index.js
```
e vediamo
```
curl http://localhost:3000/posts
curl http://localhost:3000/posts/1
```
inseriamo
```
curl -H "Content-Type: application/json" -d '{"id": 5, "title": "5th blog"}' http://localhost:3000/posts
```
aggiorniamo
```
curl -X PUT -H "Content-Type: application/json" -d '{"id": 5, "title": "5th post updated"}' http://localhost:3000/posts/5
```
cancelliamo
```
curl -X DELETE http://localhost:3000/posts/5
```
a scanso di equivoci
```
curl -d '{"id": 5, "title": "5th blog"}' http://localhost:3000/posts
{"statusCode":415,"code":"FST_ERR_CTP_INVALID_MEDIA_TYPE","error":"Unsupported Media Type","message":"Unsupported Media Type: application/x-www-form-urlencoded"
```