import {
    getAllPosts,
    getPost,
    addPost,
    updatePost,
    deletePost
} from './handlers.js'

export const routes = [{
    method: 'GET',
    url: '/posts',
    handler: getAllPosts
},
{
    method: 'GET',
    url: '/posts/:id',
    handler: getPost
},
{
    method: 'POST',
    url: '/posts',
    handler: addPost
},
{
    method: 'PUT',
    url: '/posts/:id',
    handler: updatePost
},
{
    method: 'DELETE',
    url: '/posts/:id',
    handler: deletePost
}
]