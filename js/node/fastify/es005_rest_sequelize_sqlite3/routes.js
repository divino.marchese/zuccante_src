const {
    getAllProducts,
    getProduct,
    addProduct,
    updateProduct,
    deleteProduct
} = require('./db/repository.js');

const getProductsOpts = {
    // schema: ... ,
    handler: getAllProducts,
};

const getProductOpts = {
    // schema: ...,
    handler: getProduct,
};

const addProductOpts = {
    // schema: ...,
    handler: addProduct,
};

const updateProductOpts = {
    // schema: ..,
    handler: updateProduct,
};

const deleteProductOpts = {
    // schema: ...,
    handler: deleteProduct,
};


// main plugin code
const productRoutes = (fastify, opts, done) => {
    fastify.get('/api/products/', getProductsOpts);
    fastify.get('/api/products/:id', getProductOpts);
    fastify.post('/api/products/', addProductOpts);
    fastify.put('/api/products/:id', updateProductOpts);
    fastify.delete('/api/products/:id', deleteProductOpts);
    done();
};

module.exports = productRoutes;