// obtain fastify app
const app = require('fastify')({
    logger: true
});
// sequelize and db connection
const connection = require('./db/connection.js');
const initialize = require('./db/initialize.js');

app.register(require('./routes.js'));

app.listen(3000, async (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    app.log.info(`server listening on ${address}`);
    // sync database connection, the table will be recreated
    await connection.sync({ force: true });
    // initalize db
    await initialize();
});


