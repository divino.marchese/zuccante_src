# es005

Questo è un REST con Sequelize, un ORM oramai maturo. Non serve conoscere `SQL`.

## preparazione del progetto

Installiamo i pacchetti `npm`
```
npm install fastify sequelize sqlite3

+ fastify@3.29.0
+ sequelize@6.19.0
+ sqlite3@5.0.8
```

## orm sequelize

Un ORM maturo e ben documentato! Curare durante gli inserimenti il dettaglio nelle opzioni ndei campi coinvolti. Invitiamo a dare uno sguardo alla documentazione.
- [API](https://sequelize.org/api/v6/identifiers)
- [guida](https://sequelize.org/docs/v6/)

## test

Per i `GET` tentiamo
```
curl localhost:3000/api/products/
curl localhost:3000/api/products/1
```
Per `DELETE`
```
curl -X DELETE localhost:3000/api/products/1
```
Per `POST`
```
curl -H "Content-Type: application/json" -d '{"name": "spalmella", "price": 3.33, "description": "spalmella"}' http://localhost:3000/api/products/
```
Ed infine `PUT`
```
curl -X PUT -H "Content-Type: application/json" -d '{"name": "spalmella", "price": 3.33, "description": "spalmella"}' http://localhost:3000/api/products/1
```
`PUT` e `DELETE` ritonano un messaggio, `PUT` va messo in sicurezza, qui sono richiesti nel *body* i 3 campi (non dimenticarne nessuno).


## esetnsioni VSC

**SQLiteVeiwer**