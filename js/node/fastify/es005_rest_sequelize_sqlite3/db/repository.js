const Product = require('./model.js');
const { Op } = require('sequelize');

const getAllProducts = async (req, reply) => {
    let products = await Product.findAll();
    return products;
}

const getProduct = async (req, reply) => {
    const id = parseInt(req.params.id);
    let products = await Product.findAll({
        where: {
            id: {
                [Op.eq]: id
            }
        }
    });
    return products[0];
}

const addProduct = async (req, reply) => {
    const name = req.body.name;
    const price = req.body.price; // parse is not usefull
    const description = req.body.description;
    const product = await Product.create({ name, price, description }, { fields: ['name', 'price', 'description']});
    return product;
}

const updateProduct = async (req, reply) => {
    const id = parseInt(req.params.id);
    const name = req.body.name;
    const price = parseFloat(req.body.price); // do not use parseFloat
    const description = req.body.description;
    await Product.update({ name, price, description }, {
        where: { id }
    });
    return { msg: `Post with ID ${id} is updated` };
}

const deleteProduct = async (req, reply) => {
    const id = parseInt(req.params.id);
    await Product.destroy({
        where: { id }
    });
    return { msg: `Post with ID ${id} is deleted` }
}

module.exports = {
    getAllProducts,
    getProduct,
    addProduct,
    updateProduct,
    deleteProduct
}