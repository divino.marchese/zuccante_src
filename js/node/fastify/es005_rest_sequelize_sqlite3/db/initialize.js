const Product = require('./model.js');

const initialize = () => Product.bulkCreate(
    [
        { name: `prodotto1`, price: 12.01, description: `descrizione prodotto 1`},
        { name: `prodotto2`, price: 22.02, description: `descrizione prodotto 2`},
        { name: `prodotto3`, price: 32.03, description: `descrizione prodotto 3`},
        { name: `prodotto4`, price: 42.04, description: `descrizione prodotto 4`},
        { name: `prodotto5`, price: 52.05, description: `descrizione prodotto 5`},
        { name: `prodotto6`, price: 62.06, description: `descrizione prodotto 6`},
        { name: `prodotto7`, price: 72.07, description: `descrizione prodotto 7`},
        { name: `prodotto8`, price: 82.08, description: `descrizione prodotto 8`},       
    ],
    {fields: ['name', 'price', 'description']}
).then(() => console.log("Products data have been saved"));

module.exports = initialize;