const fastify = require('fastify')({
    logger: true
})

fastify.register(require('@fastify/jwt'), {
    secret: 'zuccante@2022'
})

const loginAdminAuth = require('./auth/loginAdminAuth')

fastify.decorate('loginAdminAuth', loginAdminAuth);

fastify
    .register(require('@fastify/auth'))
    .after(() => {
        fastify.route({
            method: 'POST',
            url: '/api/login',
            preHandler: fastify.auth([
                fastify.loginAdminAuth
            ]),
            handler: (req, reply) => {
                req.log.info('Auth route');
                let payload = { username: req.body.username };
                const token = fastify.jwt.sign(payload);
                reply.send({ token });
            }
        })
    })

fastify.route({
    method: 'POST',
    url: '/api/secret',
    handler: (req, reply) => {
        req.log.info('Secret access');
        const userToken = req.headers.auth;
        fastify.jwt.verify(userToken, (err, decoded) => {
            if (err) reply.send({ msg: "forbidden" });
            reply.send({msg: `you are ${decoded.username}`});
        })
    }
})

fastify.route({
    method: 'GET',
    url: '/api/hello',
    handler: (req, reply) => {
        reply.send({msg: `hello world`})
    }
})

fastify.listen(3000, async (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    app.log.info(`server listening on ${address}`);
});