let admins = require('../cloud/admins');

module.exports = function (req, reply, done) {

    const { username, password } = req.body;

    const admin = admins.find((admin) => {
        return admin.username === username;
    });

    if (!admin) {
        reply.send({ msg: "This admin doesn't exist" });
    }

    if (password !== admin.password) {
        reply.send({ msg: 'Invalid credentials' });
    }

    done();

}