# es006

Preparazione del progetto
```
npm init
npm i fastify
npm i @fastify/auth
npm i @fastify/jwt
```
Questo esempio è un remake in stile un po' più moderno del precedente ed usa per `jwt` il plugin omonimo di fastify.

## auth

Il pacchetto non fornisec un meccanismo di autenticazione, questo è lasciato al programmatore che usera o un *decorator* o un *plugin*. Gli esempi di codice - dal primo noi siamo proceduti - sono interessabti ed autoesplicativi. Il nostro esempio è un pretesto per introdurre il suddetto *plugin*.

## jwt

Il plugin è ben illustrato [qui](https://github.com/fastify/fastify-jwt) con dovizia di esempi di codice.

## test

```
curl -X POST -H 'Content-Type: application/json' -d '{"username":"toni", "password":"123"}' localhost:3000/api/login

{"msg":"Invalid credentials"}

curl -X POST -H 'Content-Type: application/json' -d '{"username":"tony", "password":"123"}' localhost:3000/api/login

{"msg":"This admin doesn't exist"}
```
e
```
curl -X POST -H 'Content-Type: application/json' -d '{"username":"toni", "password":"1234"}' localhost:3000/api/login

{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRvbmkiLCJpYXQiOjE2NTMzMjIzMzF9.YSnThxZz_xRgp_z-QwAMag5oxayu7BM4qFSmZH-UbSQ"}
```
infine, copn lo stesso token
```
curl -X POST -H 'Content-Type: application/json' -d '{"username":"toni", "password":"1234"}' localhost:3000/api/login

{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRvbmkiLCJpYXQiOjE2NTMzMjIzMzF9.YSnThxZz_xRgp_z-QwAMag5oxayu7BM4qFSmZH-UbSQ"}
```
ora tentiamo di fregarlo
```
curl -X POST -H 'Content-Type: application/json' -H 'auth: aaa": 0}' localhost:3000/api/secret

{"statusCode":400,"code":"FST_ERR_CTP_EMPTY_JSON_BODY","error":"Bad Request","message":"Body cannot be empty when content-type is set to 'application/json'"}
```
Chiaramente
```
curl localhost:3000/api/hello
```
ci dà l'atteso risultato.

## letture proposte

[1] "Add Authentication to Our Fastify App with fastify-auth" by John Au-Yeung [qui](https://thewebdev.info/2020/09/04/add-authentication-to-our-fastify-app-with-fastify-auth/0).
