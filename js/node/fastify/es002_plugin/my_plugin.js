module.exports = function (fastify, options, done) {
    fastify.route({
        method: 'GET',
        url: '/',
        handler: (req, reply) => {
            reply.send({ hello: 'plugin' })
        }
    })
    done()
}