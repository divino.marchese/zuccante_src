# es002

Qui proviamo il nostro primo *plugin* fatto da noi, rimandiamo alla documentazioni per ulteriori dettagli [qui](https://www.fastify.io/docs/latest/Guides/Plugins-Guide/). Un *plugin* realizza un **incapsulamento** e si crea come segue
```js
module.exports = function (fastify, options, done) { ... }
```

## documentazione

- `module.exports` [qui](https://nodejs.org/api/modules.html#exports-shortcut).
- i plugin in Fastify [qui](https://www.fastify.io/docs/latest/Reference/Plugins/).