const fastify = require('fastify')({
    logger: true
})

fastify.register(require('./my_plugin'))

// Run the server!
fastify.listen(3000, function (err, address) {
    if (err) {
        fastify.log.error(err)
        process.exit(1)
    }
})