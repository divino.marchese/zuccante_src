# es007

Preparazione del progetto
```
npm init
npm i fastify
npm i sequelize
npm i pg pg-hstore
npm i fastify-plugin
```
`pg` ci è noto e `pg-hstore` serve a `sequelize` per serializzare. 

## PostgreSQL

Prepariamo il database 
```
psql
CREATE DATABASE fastify_008;
```
Usiamo il nostro utente `fastify`
```
GRANT ALL PRIVILEGES ON DATABASE fastify_008 TO fastify;
GRANT
```
usciamo con `\q` e tentiamo l'accesso.
```
psql -d fastify_008 -U fastify -W
```
Usando l'ORM non dovremo creare tabelle.

## la connessione

Nella connessione definiamo gli elementi che ci permettono di agganciarci al nostro db PostgreSQL, è un plugin.
Per motivare l'uso di `fastify-plugin` rimandiamo sia alla documentazione [qui](https://www.fastify.io/docs/latest/Reference/Decorators/) che a *plugin* analoghi forniti dalla comunità (che abbiamo preferito non usare).

## primo server test di connessione al DB

Dando uno sguardo alla documentazione di `sequelize`, anche al `pool` [qui](https://sequelize.org/docs/v6/other-topics/connection-pool/) costruiamo il primo server
```js
onst fastify = require('fastify')({
    logger: true
});

fastify.register(require('./db/connection')).ready().then(()=>{
    fastify.connection.authenticate();
});

fastify.listen(3000, async (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    fastify.log.info(`server listening on ${address}`);
});
```

## model 

Il *model* lo definiamo nell'omonimo file. Rimandiamo ancora al versante Node al modulo `crypto` vedi [qui](https://nodejs.org/dist/latest-v16.x/docs/api/crypto.html#cryptorandomuuidoptions). Con
``` js
await User.sync({ force: true });
```
sincronizziamo il model e forziamo il cancellamento della tabella e la sua ricreazione al lancio del server.



## test

Inseriamo 2 oggetti di prova
```
curl localhost:3000/init
```
possiamo provare subito
```
curl localhost:3000/
```
aggiungiamo
```
curl -X POST -H 'Content-Type: application/json' -d '{"firstName": "Nane", "lastName": "Brusà" }' localhost:3000/
{"id":"b5e5ce36-f9b4-422b-990e-39ce9f47f529","firstName":"Nane","lastName":"Brusà","updatedAt":"2022-05-31T11:58:11.542Z","createdAt":"2022-05-31T11:58:11.542Z"}
```
aggiorniamo 
```
curl -X PATCH -H 'Content-Type: application/json' -d '{"lastName": "Bruson"}' localhost:3000/d33ad2a6-0f75-4854-a062-592072ea7e2a
{"id":"d33ad2a6-0f75-4854-a062-592072ea7e2a","firstName":"Nane","lastName":"Bruson","createdAt":"2022-05-31T15:08:01.354Z","updatedAt":"2022-05-31T15:14:19.913Z"}
```
ed infine cancelliamo
```
curl -X DELETE localhost:3000/d33ad2a6-0f75-4854-a062-592072ea7e2a
{"msg":"user d33ad2a6-0f75-4854-a062-592072ea7e2a deleted"}
```
naturalmente gli `uuid` si ripresenteranno diversamente su di un test sul campo.



## letture proposte

[1] 