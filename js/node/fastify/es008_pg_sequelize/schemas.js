const msg = {
    response: {
        200: {
            type: 'object',
            required: ['msg'],
            properties: {
                msg: { type: 'string' }
            }
        }
    }
}

const addUser = {
    body: {
        type: 'object',
        required: ['firstName', 'lastName'],
        properties: {
            firstName: { type: 'string' },
            lastName: { type: 'string' },
        }
    }
}

const getUser = {
    params: {
        type: 'object',
        properties: {
            id: { type: 'string', format: 'uuid' }
        }
    }
}

const updateUser = {
    body: {
        type: 'object',
        properties: {
            firstName: { type: 'string', },
            lastName: { type: 'string', },
        }
    },
    params: {
        type: 'object',
        properties: {
            id: { type: 'string', format: 'uuid' }
        }
    }
}

const deleteUser = {
    params: {
        type: 'object',
        properties: {
            name: { type: 'string' }
        }
    }
}

module.exports = { getUser, addUser, updateUser, deleteUser }