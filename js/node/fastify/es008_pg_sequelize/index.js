const fastify = require('fastify')({
    logger: true
});

fastify.register(require('./db')).ready().then(()=>{
    fastify.connection.authenticate();  
}).then(()=>{
    fastify.User.sync({ force: true });
});

fastify.register(require('./routes.js'));

fastify.listen(3000, async (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    fastify.log.info(`server listening on ${address}`);
});