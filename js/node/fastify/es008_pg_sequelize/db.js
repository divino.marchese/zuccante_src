const fp = require('fastify-plugin');
const { DataTypes, Sequelize } = require('sequelize');

module.exports = fp(function (fastify, options, done) {

  const sequelize = new Sequelize('fastify_008', 'fastify', 'zuccante@2022', {
    host: 'localhost',
    dialect: 'postgres',
    pool: {
      max: 9,
      min: 0,
      idle: 10000
    }
  });

  const User = sequelize.define("user", {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    firstName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.STRING,
    },
  });

  fastify.decorate('connection', sequelize);
  fastify.decorate('User', User);

  done();

});