const { getUser, addUser, updateUser, deleteUser } = require('./schemas.js')

module.exports = (fastify, options, done) => {

    const User = fastify.User;

    fastify.get('/', async (request, reply) => {
        const users = await User.findAll();
        reply.send(users);
    });

    fastify.get('/:id', { schema: getUser }, async (request, reply) => {
        const id = request.params.id;
        const user = await User.findByPk(id);
        if(user != null){
            reply.send(user);
        }
        reply.send({msg: `user ${id} not found`});
    });
    
    fastify.post('/', { schema: addUser }, async (request, reply) => {
        const { firstName, lastName } = request.body;
        const user = await User.create({firstName, lastName});
        reply.code(201);
        reply.send(user);
    })
    
    fastify.patch('/:id', { schema: updateUser }, async (request, reply) => {
        const id = request.params.id;
        const { firstName, lastName } = request.body
        const user = await User.findByPk(id);
        if(user != null){
            user.firstName = firstName || user.firstName;
            user.lastName = lastName || user.lastName;
            user.save();
            reply.send(user);
        }
        reply.send({msg: "user not found"});
    })

    fastify.delete('/:id', { schema: deleteUser }, async (request, reply) => {
        const id = request.params.id;
        const user = await User.findByPk(id);
        if(user != null){
            await user.destroy();
            reply.send({msg: `user ${id} deleted`});
        }
        reply.send({msg: `user ${id} not found`});
    })
    

    fastify.get('/init', async (request, reply) => {
        const users = await User.bulkCreate([
            { firstName: 'Toni', lastName: 'Bueghin' },
            { firstName: 'Nane', lastName: 'Sfijo' },
            { firstName: 'Bepi', lastName: 'Sbrisa' },
            { firstName: 'Maria', lastName: 'Descusia' },
          ]);
          reply.send({ msg: `insert ${users.length} products` });
    })

    done();
}