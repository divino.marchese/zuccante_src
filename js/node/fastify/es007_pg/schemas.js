const msg = {
    response: {
        200: {
            type: 'object',
            required: ['msg'],
            properties: {
                msg: { type: 'string' }
            }
        }
    }
}

const allTodos = {
    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                required: ['id', 'name', 'created_at', 'important', 'due_date', 'done'],
                properties: {
                    id: { type: 'string', format: 'uuid' },
                    name: { type: 'string' },
                    created_at: { type: 'string', format: "date-time" },
                    important: { type: 'boolean' },
                    due_date: { type: 'string', format: "date-time" },
                    done: { type: 'boolean' },
                }
            }
        }
    }
}

const addTodo = {
    body: {
        type: 'object',
        required: ['name'],
        properties: {
            name: { type: 'string', },
            due_date: { type: 'string', format: 'date-time', nullable: true, default: null },
            important: { type: 'boolean', default: false },
        }
    },
    response: {
        201: {
            type: 'object',
            properties: {
                msg: { type: 'string' }
            }
        }
    }
}

const updateTodo = {
    body: {
        type: 'object',
        properties: {
            due_date: { type: 'string', format: 'date-time' },
            important: { type: 'boolean' },
            done: { type: 'boolean' }
        }
    },
    params: {
        type: 'object',
        properties: {
            id: { type: 'string', format: 'uuid' }
        }
    }
}

const deleteTodo = {
    params: {
        type: 'object',
        properties: {
            name: { type: 'string' }
        }
    }
}

module.exports = { msg, allTodos, addTodo, updateTodo, deleteTodo }