const fastify = require('fastify')({
    logger: true
})

fastify.register(require('@fastify/postgres'), {
    connectionString: 'postgres://fastify:zuccante%402022@localhost:5432/fastify_007'
})

fastify.register(require('./routes.js'));


fastify.listen(3000, async (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    app.log.info(`server listening on ${address}`);
});