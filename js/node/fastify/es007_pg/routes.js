const { randomUUID } = require('crypto')
const { msg, allTodos, addTodo, updateTodo, deleteTodo } = require('./schemas.js')

module.exports = (fastify, options, done) => {

    fastify.get('/', { schema: allTodos }, async (request, reply) => {
        const client = await fastify.pg.connect();
        try {
            const { rows }  = await client.query('SELECT * FROM todos')
            reply.send(rows);
        } catch (err) {
            throw new Error(err)
        }
    });

    fastify.post('/', { schema: addTodo }, async (request, reply) => {
        const client = await fastify.pg.connect();
        const { name, important, due_date } = request.body
        const id = randomUUID();
        const done = false
        created_at = new Date().toISOString()
        const query = {
            text: `INSERT INTO todos (id, name, created_at, important, due_date, done)
                    VALUES($1, $2, $3, $4, $5, $6 ) RETURNING *`,
            values: [id, name, created_at, important, due_date, done],
        }
        try {
            const { rows } = await client.query(query);
            reply.code(201);
            // reply.code(200); ... should be better
            reply.send({ msg: "object creasted" });
        } catch (err) {
            throw new Error(err)
        }

    })

    fastify.patch('/:id', { schema: updateTodo }, async (request, reply) => {
        const client = await fastify.pg.connect();
        const id = request.params.id
        const { important, dueDate, done } = request.body
        const query = {
            text: `UPDATE todos SET 
                    important = COALESCE($1, important), 
                    due_date = COALESCE($2, due_date), 
                    done = COALESCE($3, done) 
                    WHERE id = $4 RETURNING *`,
            values: [important, dueDate, done, id]
        }
        try {
            await client.query(query);
            reply.code(204);
        } catch (err) {
            throw new Error(err)
        }
    })

    fastify.delete('/:name', { schema: deleteTodo }, async (request, reply) => {
        const client = await fastify.pg.connect();
        const name = request.params.name;
        try {
            await client.query('DELETE FROM todos WHERE name = $1 RETURNING *', [name]);
            reply.code(204);
        } catch (err) {
            throw new Error(err)
        }
    })


    fastify.get('/init', { schema: msg }, async (request, reply) => {
        const client = await fastify.pg.connect();
        try {
            // first todos
            let uuid1 = randomUUID();
            let name = "bagigi";
            let createdAt = "2021-05-21 04:05:06";
            let important = false;
            let dueDate = "2021-05-30 04:05:06";
            let done = true;
            const query1 = {
                text: `INSERT INTO todos (id, name, created_at, important, due_date, done)
                        VALUES($1, $2, $3, $4, $5, $6 ) RETURNING *`,
                values: [uuid1, name, createdAt, important, dueDate, done],
            }
            await client.query(query1);
            // second todos
            let uuid2 = randomUUID();
            name = "bigoi";
            createdAt = "2021-06-21 04:05:06";
            important = true;
            dueDate = "2021-07-30 04:05:06";
            done = true;
            const query2 = {
                text: `INSERT INTO todos (id, name, created_at, important, due_date, done)
                        VALUES($1, $2, $3, $4, $5, $6 ) RETURNING *`,
                values: [uuid2, name, createdAt, important, dueDate, done],
            }
            await client.query(query2);
            reply.send({ msg: "insert tow products" });
        } catch (err) {
            throw new Error(err)
        }
    })

    done();
}