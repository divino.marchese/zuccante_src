# es007

Preparazione del progetto
```
npm init
npm i fastify
npm i pg @fastify/postgres

```
Si è preso spunto dal seguente tutorial [qui](https://dev.to/kenwanjohi/fastify-and-postgresql-rest-api-2f9l). 

## PostgreSQL

Prepariamo il database 
```
psql
CREATE DATABASE fastify_007;
```
diamo quindi `\l`; ci possimao connettere al db creato nuovamente con
```
\c fastify_007
```
A questo punto però è opportuno preparare un utente, non `SUPERUSER` per gestire
il db.
```
CREATE USER fastify WITH ENCRYPTED PASSWORD 'zuccante@2022';
CREATE ROLE
```
diamo quindi `\du`, quindi diamo i privilegi
```
GRANT ALL PRIVILEGES ON DATABASE fastify_007 TO fastify;
GRANT
```
per avere garanzia dei privilegi dati al nostri utente possiamo dare di nuovo `\l`. Un `USER` è `ROLE`
dato ad un singolo `WITH LOGIN`. A questo punto possiamo entrare col nostro utente
```
psql -d fastify_007 -U fastify -W
```
creiamo la tabella
```
CREATE TABLE todos ( 
    id UUID PRIMARY KEY, 
    name VARCHAR(255) NOT NULL, 
    created_at TIMESTAMP NOT NULL, 
    important BOOLEAN NOT NULL, 
    due_date TIMESTAMP, 
    done BOOLEAN NOT NULL 
);
```
avremmo potuto usare anche
```
id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
```
per saperne di più su **uuid** si può leggere (Wikipedia en) [qui](https://en.wikipedia.org/wiki/Universally_unique_identifier). Con `\d todos` abbiamo la descrizione della tabella.
I nomi dei campi sono per convenzione in minuscolo e si usa `_` come separatore, alternativamente proteggere con `"` (come nel tutorial originale).

## primo server test di connessione al DB

QUi di seguito l`esempio 
```js
const fastify = require('fastify')({
    logger: true
})

fastify.register(require('@fastify/postgres'), {
    connectionString: 'postgres://fastify:zuccante%402022@localhost:5432/fastify_007'
})

fastify.get('/api/connection', (req, reply) => {
    fastify.pg.connect(onConnect)
  
    function onConnect (err, client, release) {
      if (err) return reply.send(err)
      reply.send({msg: "connected"});
  
    }
  })


fastify.listen(3000, async (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    app.log.info(`server listening on ${address}`);
});
```
la password contiene un `@` sostituito da `%40`, vedi [qui](https://en.wikipedia.org/wiki/Percent-encoding). Abbiamo dato inoltre
```
sudo netstat -plunt |grep postgres
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      12027/postgres      
```
per recuperare la porta nella `connectionString`.

## il modulo 'crypto' di Node

Node mette a disposizione il modulo `crypto` [qui](https://nodejs.org/dist/latest-v16.x/docs/api/crypto.html).
In passato esisteva un modulo esterno (pacchetto) che ora viene sostituito.

## il modulo pg e la preparazione dellq uery

Un classico per la connessione a PostgreSQL, è il modulo cui si appoggia il *plugin* da noi usato, [qui](https://node-postgres.com/) per la documentazione. Inizializziamo.
Per le query rimandiamo alla documentazione [qui](https://node-postgres.com/features/queries): le quesry possono essere gestite in modo semplice (semplice stringa) o parametriche. 
- `result`, risultato di una *query*, è oggetto che contiene varie *property* fra cui `query` un *array* di oggetti con campi corrispondenti agli omonimi campi della tabella corrispondente. In documentazione si rimanda ad altre possibilità.

Ora vediamo le singole query.
- la prima è un `GET` che non merita ulteriori commenti. Dare uno sguardo alla documentazione di `pg` in caso. Per esercizio è possibile fare il `GET` di un singolo oggetto `memos`.
- Nel `POST` vi è un'interessante costruzione della *query* parametrica.
- per aggiornare usiamo `PATCH`

## test

Inseriamo 2 oggetti di prova
```
curl localhost:3000/init
```
possiamo provare subito
```
curl localhost:3000/
```
aggiungiamo
```
curl -X POST -H 'Content-Type: application/json' -d '{"name": "sarde", "important": true }' localhost:3000/
```
trovando conferma con
```
curl localhost:3000/
```
aggiorniamo (nienet verrà tornato)
```
curl -X PATCH -H 'Content-Type: application/json' -d '{"important": true}' localhost:3000/ae35d2e4-4df9-4d34-b4c4-438e3e0c31f9
```
conferma con
```
curl localhost:3000/
```
ed infine cancelliamo
```
curl -X DELETE localhost:3000/bigoi
```



## letture proposte

[1] `PATCH` su MDN [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH).  
[2] "HTTP response status codes" su MDN [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status).  
[3] "Understanding JSON Schema" [qui](https://json-schema.org/understanding-json-schema/index.html).