# es001

Il metodo `listen(...)` come da **[API](https://www.fastify.io/docs/latest/Reference/Server/#listen)** lo si sua con *callback* o ritorna un `Promise` quindi, in questo caso, si usa il *pattern* `async ... await`. Ad esempio, qui col `then ... catch`
```js
fastify.listen(3000)
  .then((address) => console.log(`server listening on ${address}`))
  .catch(err => {
    console.log('Error starting server:', err)
    process.exit(1)
  })
```
Per un semplice test provare `curl`.