import Fastify from 'fastify'
const fastify = Fastify({
    logger: true
})

fastify.get('/', (request, reply) => {
    return { hello: 'world' }
})

fastify.get('/request', (request, reply) => {
    reply.send(`request arrive from ${request.hostname}\n`)
})

try {
    await fastify.listen({ port: 3000 });
    console.log('server is running');
} catch (err) {
    fastify.log.error(err)
    process.exit(1)
}
