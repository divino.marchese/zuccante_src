const {
    getPostsSchema,
    getPostSchema,
    addPostSchema,
    updatePostSchema,
    deletePostSchema,
  } = require('./controllers/schemas.js');
  
  const {
    getAllPosts,
    getPost,
    addPost,
    updatePost,
    deletePost
  } = require('./controllers/handlers.js');
  
  const getPostsOpts = {
    schema: getPostsSchema,
    handler: getAllPosts,
  };
  
  const getPostOpts = {
    schema: getPostSchema,
    handler: getPost,
  };
  
  const addPostOpts = {
    schema: addPostSchema,
    handler: addPost,
  };
  
  const updatePostOpts = {
    schema: updatePostSchema,
    handler: updatePost,
  };
  
  const deletePostOpts = {
    schema: deletePostSchema,
    handler: deletePost,
  };


  // main plugin code
  const postRoutes = (fastify, opts, done) => {
    fastify.get('/api/posts/', getPostsOpts);
    fastify.get('/api/posts/:id', getPostOpts);
    fastify.post('/api/posts/', addPostOpts);
    fastify.put('/api/posts/:id', updatePostOpts);
    fastify.delete('/api/posts/:id', deletePostOpts);
    done();
  };
  
  module.exports = postRoutes;