const app = require('fastify')({
    logger: true
})

app.register(require('./routes.js'));

app.listen(3000, (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    app.log.info(`server listening on ${address}`)
})