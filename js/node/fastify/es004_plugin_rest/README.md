# es002

Riproponiamo il precedente esempio abbandonando lo stile un po' classico usando `es002` sui *plugin*. APpiamo preso spunto dall'ottimo tutorial [qui](https://dev.to/elijahtrillionz/build-a-crud-api-with-fastify-688), sorgenti sono [qui](https://github.com/Elijah-trillionz/fastify-api). La nostra modifica è lieve in quanto permane la struttura (organizzazione di file e cartelle) del progetto.

## il fake cloud

La nostra sorgente di dati simla la realtà: il modulo `posts.js` in `cloud`. Per semplicità un post è composto, come nel precedente esempio, da
- `id` di tipo `number`
- `title` di tipo `string`

I tipi di dato serviranno qui di seguito per la parte più interessante.

## json schema

Tradizionalmente gli amanti di `XML` hanno sempre detto che `json` dà la leggerezza e perde la sicurezza! Ora non più, esiste [json schema](https://json-schema.org/). Leggere la documentazione ufficiale [qui](https://www.fastify.io/docs/latest/Reference/Validation-and-Serialization/).

## test

Usando `curl` proviamo `GET`
```
localhost:3000/api/posts
localhost:3000/api/posts/1
```
quindi `POST`
```
curl -H "Content-Type: application/json" -d '{"id": 5, "title": "5th blpost"}' http://localhost:3000/api/posts/
```
invece
```
curl -H "Content-Type: application/json" -d '{"id": "ciao"}' http://localhost:3000/api/posts/
{"statusCode":400,"error":"Bad Request","message":"body.id should be integer"}
```
quindi la validazione funzione!. Ancora
```
curl -H "Content-Type: application/json" -d '{"id": 2}' http://localhost:3000/api/posts/
{"statusCode":400,"error":"Bad Request","message":"body should have required property 'title'"}
```
ed invece, il solito *casting*
```
curl -H "Content-Type: application/json" -d '{"title": 6666}' http://localhost:3000/api/posts/
{"id":6,"title":"6666"}
```
Aggiorniamo con `PUT`
```
curl -X PUT -H "Content-Type: application/json" -d '{"title": "1th updated"}' http://localhost:3000/api/posts/1
```
ed infine `DELETE`
```
url -X DELETE http://localhost:3000/api/posts/1
```

## altri materiali

[1] "Understanding Json Schema" [qui](https://json-schema.org/understanding-json-schema/).  
[2] "AJV" [home page [qui](https://ajv.js.org/) pacchetto npm [qui](https://www.npmjs.com/package/ajv/v/6.12.6) 
