const postSchema = {
  type: 'object',
  properties: {
    id: {
      description: "post should be unique", 
      type: 'integer' },
    title: { type: 'string' }
  },
};

const msgSchema = {
  type: 'object',
  properties: {
    msg: { type: 'string' },
  },
};

const getPostsSchema = {
  response: {
    200: {
      type: 'array',
      items: postSchema,
    },
  },
};

const getPostSchema = {
  params: {
    id: { type: 'integer' },
  },
  response: {
    200: postSchema,
  },
};

const addPostSchema = {
  body: {
    type: 'object',
    required: ['title'],
    properties: {
      id: { type: 'integer' },
      title: { type: 'string' },
    },
  },
  response: {
    200: postSchema, 
  },
};

const updatePostSchema = {
  body: {
    type: 'object',
    required: ['title'],
    properties: {
      id: { type: 'integer' },
      title: { type: 'string' },
    },
  },
  params: {
    id: { type: 'integer' },
  },
  response: {
    200: postSchema
  },
};

const deletePostSchema = {
  params: {
    id: { type: 'integer' },
  },
  response: {
    200: msgSchema,
  },
};

module.exports = {
  getPostsSchema,
  getPostSchema,
  addPostSchema,
  updatePostSchema,
  deletePostSchema,
};