let posts = require('../cloud/posts.js');

const getAllPosts = (req, reply) => {
    return posts
}

const getPost = (req, reply) => {
    const id = parseInt(req.params.id) 
    const post = posts.find(post => post.id === id)
    return post
}

const addPost = (req, reply) => {
    const id = posts.length + 1 // generate new ID
    const newPost = {
        id,
        title: req.body.title
    }
    posts.push(newPost)
    return newPost
}

const updatePost = (req, reply) => {
    const id = parseInt(req.params.id);
    posts = posts.map(post => {
        if (post.id === id) {
            return {
                id,
                title: req.body.title
            }
        } else {
            return {id: post.id, title: post.title}
        }
    })
    return {
        id,
        title: req.body.title
    }
}

const deletePost = (req, reply) => {
    const id = parseInt(req.params.id)
    posts = posts.filter(post => post.id !== id)
    return { msg: `Post with ID ${id} is deleted` }
}

module.exports = {
    getAllPosts,
    getPost,
    addPost,
    updatePost,
    deletePost
}