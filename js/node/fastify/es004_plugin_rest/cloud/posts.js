let posts = [
    {
        id: 1,
        title: 'This is an experiment'
    },
    {
        id: 2,
        title: 'Fastify is pretty cool'
    },
    {
        id: 3,
        title: 'Just another post, yea!'
    },
    {
        id: 4,
        title: 'The 4th post!'
    }
]

module.exports = posts;