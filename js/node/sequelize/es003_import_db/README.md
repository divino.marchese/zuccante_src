# es003 import db

**Pagila** è il remake di un noto database di nome **Sakila** pensato per MySQL [qui](https://github.com/devrimgunduz/pagila). Installiamolo. 

## lo schema delle tabelle

![schema relazionale](https://www.jooq.org/img/sakila.png)

## creazione del database

Vediamo quindi i passi per installare. Per prima cosa creiamo il db ed il suo utente
```
sudo -i -u postgres
psql
REATE DATABASE pagila;
CREATE USER zuccante WITH ENCRYPTED PASSWORD 'zuccante@2022';
GRANT ALL PRIVILEGES ON DATABASE pagila TO zuccante;
\q
```
creiamo lo schema (posizionandoci prima opportunamente)
```
psql -d pagila -U zuccante -f pagila-schema.sql -a -W
```
connettiamoci al db al solito modo e verifichiamo con `\dt` la presenza delle tabelle e testiamo ad esempio `\dt actor`. Usciamo `\q` e inseriamo i dati (appaiono degli errori ma tutto va abbastanza).
```
psql -d pagila -U zuccante -f pagila-data.sql -a
```
entriamo 
```
psql -d pagila -U zuccante
```

## test query del database

```
SELECT * FROM actor LIMIT 10;
```
qualcosa di più complesso
```
SELECT first_name, last_name, COUNT(film_id) AS n_film 
FROM actor INNER JOIN film_actor USING (actor_id) 
GROUP BY actor_id 
LIMIT 10;
```

## preparazione del progetto

Lavorando con PostgreeSQL installiamo
```
npm i sequelize
npm install i pg pg-hstore
npm i sequelize-auto
```
l'ultimo ci permetterà il *rverse engineering* del database. Gerneriamo i *models*
```
./sequelize-auto -o './models' -d pagila -u zuccante -h localhost -p 5432 -x 'zuccante@2022' --dialect postgres
```
(attenzione a non tralasciare `-h localhost`). Con `npm uninstall sequelize-auto` possiamo eliminare poi il pacchetto.

## raw query

Questa è la soluzione ideale per query complesse o in cui si voglia far apparire SQL. Il riferimento per la documentazione è [qui](https://sequelize.org/v5/manual/raw-queries.html), ed il lavoro risulta pure agile! Qui sotto il primo esempio
``` javascript
sequelize.query('SELECT customerName, phone FROM customers LIMIT 10').then(([results, metadata]) => {
        results.forEach((result) => console.log(JSON.stringify(result))); 
    });
```
l'oggetto `results`, è una query `SELECT`, contiene i risultati, cioè le righe. Lo stesso risultato si può ottenere mappando sul model `customers` (corrispondente alla tabella `customers`).
``` javascript
sequelize.query('SELECT * FROM customers LIMIT 10', {
        model: Customer
    }).then(customers => {
        customers.forEach((customer) => {
            console.log(`name: ${customer.customerName}`);
            console.log(`phone: ${customer.phone}`);
        }); 
    })
```
la soluzione ideale per quando si lavora su di un singolo model. Se invece volessimo realizzare col nostro ORM un join
``` javascript
Customer.findAll({
        attributes: ['customerName', 'phone', 'city'],
        limit: 10,
        include: [{
            model: Order,
            attributes: ['orderNumber', 'orderDate', 'status'],
            where: { status: 'Shipped'}
        }]
    }).then(customers => {
        customers.forEach((customer) => {
            ...
            customer.ordersforEach((order) => {
               ...
            });
        }); 
    });
```
dobbiamo fare attenzione 
- selezionare i campi e non usare `findAll()` in quanto contiene anche i *time stamps*.
- impostare correttamente le chiavi esterne (qui facciamo uso di un DB preesistente): `Customer.hasMany(Order, {foreignKey: 'customerNumber'});`.
