const { sequelize, User, Project } = require('./db');

const init = async () => {
    Project.belongsToMany(User, { through: 'userProject' });
    User.belongsToMany(Project, { through: 'userProject' });
    await sequelize.sync({ force: true });
}

const test1 = async () => {
    const user1 = await User.create({ name: 'bepi baengo', email: 'bepi.baengo@gmail.com' });
    const user2 = await User.create({ name: 'nane boassa', email: 'nane.boassa@gmail.com' });
    await user1.createProject({ name: 'drissa banane'});
    const project2 = await user2.createProject({ name: 'magna confeti'});
    await user1.addProject(project2);
    const projects2user1 = await user1.getProjects();
    console.log(`${user1.name} has projects: \n ${JSON.stringify(projects2user1, null, 4)}`);
}

const test2 = async () => {
    const user1 = await User.create({ name: 'bepi baengo', email: 'bepi.baengo@gmail.com' });
    const user2 = await User.create({ name: 'nane boassa', email: 'nane.boassa@gmail.com' });
    await user1.createProject({ name: 'drissa banane'});
    const project2 = await user2.createProject({ name: 'magna confeti'});
    await user1.addProject(project2);
    const users2project2 = await project2.getUsers();
    console.log('****************************************************************');
    console.log(`${project2.name} belongs to: \n ${JSON.stringify(users2project2, null, 4)}`);
}

init().then(test2);