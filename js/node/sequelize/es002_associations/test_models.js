const { sequelize, User, Project } = require('./db');

// User.hasOne(Project);
// User.hasMany(Project);
// Project.belongsTo(User);
Project.belongsToMany(User, {through: 'userProject'});
User.belongsToMany(Project, {through: 'userProject'});


sequelize.sync({force: true});

