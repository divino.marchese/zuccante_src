const { sequelize, User, Project } = require('./db');

const init = async () => {
    User.hasMany(Project);
    Project.belongsTo(User);
    await sequelize.sync({force: true});
}

const test = async () => {
    const user = await User.create({ name: 'bepi baengo', email: 'bepi.baengo@gmail.com' });
    await user.createProject({ name: 'drissa banane'}); 
    await user.createProject({ name: 'magna confeti'}); 
    const projects = await user.getProjects();
    const nProjects = await user.countProjects();
    console.log(`${user.name} has ${nProjects} projects: \n ${JSON.stringify(projects, null, 4)}`);
}

init().then(test);