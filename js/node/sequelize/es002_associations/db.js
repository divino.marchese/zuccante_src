const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './db.sqlite',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});

const User = sequelize.define('user', {
    name: DataTypes.STRING,
    email: DataTypes.STRING
  }
);

const Project = sequelize.define('project', {
    name: DataTypes.STRING
  }
);

module.exports = {sequelize, User, Project}