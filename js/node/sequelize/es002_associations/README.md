# es002 associations

L'ORM `squeleize` permette di gestire le associazioni anche in modo molto raffinato; il documento di riferimento per partire è [qui](https://sequelize.org/docs/v6/core-concepts/assocs/).  
**NB** Nel modello ER esistono le associazionio (*relationship* en), l'ORM le realizzain modo pressoché corrispondente: l'ORM è un'astrazione che si trova a metà strada fra modello relazionale e modello ER.

## Preparazione del progetto

Per VSC usiamo l'esptensione `SQLite`.
```
npm i sequelize
npm i sqlite3
```

## sqlite3: client testuale

Alcuni comandi utili ( ci si connette al db dando `sqlite3 miodbfile.sqlite`)
- `.tables` mostra le rabelle di un DB
- `.schema <tbl>` descrive la tabella

## Dal model alle tabelle

Il file `test_models.js` ci permette di esaminare, tentiamo i nostri esempi.


### ER One2One (parziale) `hasOne` 

Useremo
```js
User.hasOne(Project);
``` 
dà il seguente esito usando la shell di `sqlite3`
```
sqlite> .tables
projects  users   
sqlite> .schema users
CREATE TABLE `users` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `email` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
sqlite> .schema projects
CREATE TABLE `projects` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `userId` INTEGER REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE);
sqlite> 
```
a livello di relazioni (tabelle) si può osservare che a questo punto possiamo legare i `project` agli `user` mediante chiave esterna. L'ORM invece vede l'associazione ER lungo il verso che va da `User` a `Project` e potrà quindi interrogare i dati persistenti seguendo il suddetto verso (il loro legame).

### ER One2One (parziale) `belongsTo`

```js
Project.belongsTo(User);
```
e in `sqlite3`
```
sqlite> .schema users
CREATE TABLE `users` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `email` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
sqlite> .schema projects
CREATE TABLE `projects` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `userId` INTEGER REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE);
sqlite> 
```
a livello di relazioni non cambia nulla rispetto al caso esaminato, cambia il verso di osservazione dell'ORM (e quindi il modo in cui potrà interrogare i legami fra gli oggetti persistenti).

### ER One2One `hasOne` e `belongsTo`

L'associazione ER al completo permette di vedere dai due versi
```js
User.hasOne(Project);
Project.belongsTo(User);
```
Nulla cambia sulle tavbelle SQlite quindi `sequelize` **non usa due chiavi esterne** una lato `User` e una lato `Project`! Sarà però impossibile legare più `project` ad un'unica chiave esterna? Bisogna testare!

### ER One2Many `hasMany` e `belongsTo`
- 
Solo il primo verso della relazione cambia rispetto a **one2one**
```js
User.hasMany(Project);
Project.belongsTo(User);
```
otteniamo ancora una volta
```
sqlite> .schema users
CREATE TABLE `users` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `email` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
sqlite> .schema projects
CREATE TABLE `projects` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `userId` INTEGER REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE);
sqlite> 
```
quindi a livello di tabelle nulla cambia rispetto al caso precedente: verrà rilassato il vincolo di aggiungere più `Project` per un singolo `User`? Bisogna testare!

### ER Many2Many (parziale) `belongsToMany`

Anche qui partiamo in una sola direzione 
```js
Project.belongsToMany(User, {through: 'userProject'});
```
ed analizzaiamo il db mediante `sqlite3`
```
sqlite> .tables
projects     userProject  users      
sqlite> .schema users
CREATE TABLE `users` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `email` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
sqlite> .schema projects
CREATE TABLE `projects` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
sqlite> .schema userProject
CREATE TABLE `userProject` (`createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `projectId` INTEGER NOT NULL REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, `userId` INTEGER NOT NULL REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY (`projectId`, `userId`));
sqlite> 
```
osservasi il `CASCADE` sia su `ON DELETE` che in `ON UPDATE` (vedi partecipazione totale e parziale).

### ER Many2Many `belongsToMany`

Qui trattiamo entrambe le direzioni
```js
Project.belongsToMany(User, {through: 'userProject'});
User.belongsToMany(Project, {through: 'userProject'});
```
e usando il client testuale `sqlite3`
```
sqlite> .schema users
CREATE TABLE `users` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `email` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
sqlite> .schema projects
CREATE TABLE `projects` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
sqlite> .schema userProject
CREATE TABLE `userProject` (`createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `projectId` INTEGER NOT NULL REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, `userId` INTEGER NOT NULL REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY (`projectId`, `userId`));
sqlite>
```

## ulteriori opzioni

Come si può osservare abbiamo possibilità di nodifica sul comportamento di default dell'ORM andando ad agire direttamente sul *model*, siano `Foo` e `Bar` vedi [qui](https://sequelize.org/docs/v6/core-concepts/assocs/#options)
```js
Foo.hasOne(Bar, {
  onDelete: 'RESTRICT',
  onUpdate: 'RESTRICT'
});
Bar.belongsTo(Foo);
```

## i test

I test gli abbiamo divisi per categorie (corrispondenti ai vari file)
```
one2one
one2many
many2many
```
partiamo coi primi test. In `one2one` facciamo alcuni test facendo notare come l'ssociazione ER 1:1 in realtà in `sequelize` non dà il vincolo, toglie solo possibilità col suo linguaggio di *query*.
- `one2one` dare uno sguardo per iniziare [qui](https://sequelize.org/docs/v6/core-concepts/assocs/#foohasonebar) e [qui](https://sequelize.org/docs/v6/core-concepts/assocs/#foobelongstobar).
- `one2many` dare uno sguardo [qui](https://sequelize.org/docs/v6/core-concepts/assocs/#foohasmanybar).

## perchè le associazioni ER si raddoppiano in Sequelize

La cosa è ben spiegata [qui](https://sequelize.org/docs/v6/core-concepts/assocs/#why-associations-are-defined-in-pairs). In realtà la cosa è intuibile anche partendo dal modello ER: tipicamente per definire una associazione si guarda prima partendo da un entità per andare all'altra e quindi simmetricamente. L'ORM ripropone le due visuali e quindi il modo con cui interrogare da i due diversi punti di vista rappresentati in un'associazione ER.