const { sequelize, User, Project } = require('./db');

const init = async () => {
    User.hasOne(Project);
    Project.belongsTo(User);
    await sequelize.sync({force: true});
}

const test1 = async () => {
    const user = await User.create({ name: 'bepi baengo', email: 'bepi.baengo@gmail.com' });
    await user.createProject({ name: 'drissa banane'}); 
    console.log(await user.getProject()); 
}

const test2 = async () => {
    const user = await User.create({ name: 'bepi baengo', email: 'bepi.baengo@gmail.com' });
    await user.createProject({ name: 'drissa banane'}); 
    await user.createProject({ name: 'magna confeti'}); 
    const projects = await Project.findAll();
    console.log(`All users: \n ${JSON.stringify(projects, null, 4)}`);
}

const test3 = async () => {
    const user = await User.create({ name: 'bepi baengo', email: 'bepi.baengo@gmail.com' });
    const project = await user.createProject({ name: 'drissa banane'}); 
    console.log(await project.getUser()); 
}

// change test number
init().then(test3);