/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('employees', {
    employeeNumber: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    lastName: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    firstName: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    extension: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    officeCode: {
      type: DataTypes.STRING(10),
      allowNull: false,
      references: {
        model: 'offices',
        key: 'officeCode'
      }
    },
    reportsTo: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'employees',
        key: 'employeeNumber'
      }
    },
    jobTitle: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'employees'
  });
};
