/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('orders', {
    orderNumber: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    orderDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    requiredDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    shippedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(15),
      allowNull: false
    },
    comments: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    customerNumber: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'customers',
        key: 'customerNumber'
      }
    }
  }, {
    tableName: 'orders'
  });
};
