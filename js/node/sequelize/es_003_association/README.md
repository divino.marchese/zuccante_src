# association

In questo esempio prendiamo consapevolezza, al di la della gestione del DB col DBMS, nel nostro caso MariaDB, delle poteznialità dell'ORM!
``` javascript
Post.hasMany(Comment, {onDelete: 'CASCADE'}); 
Comment.belongsTo(Post);
```
In MariaDB, il DBMS da noi utilizzato, entrambe le istruzioni creerebbero la stessa chiave esterna ma frazie alla prima possiamo scrivere
``` javascript
Post.create({
        title: 'My First Post',
        text: 'text for my first post',
        comments: [
            { author: 'Toni Bueghin', text: 'Scrivo tante cose inutili e dannose ....'},
            { author: 'Bepi Sbrisa', text: 'Scrvo, mah, boh ....'},
        ]
    }, {
        include: [ Comment ]
    })
```
e grazie alla seconda
``` javascript
 Comment.create({
        author: 'Nane Ombra',
        text: 'Bla bla bla',
        post: {
            title: 'In Galera!',
        }
    }, {
        include: [ Post ]
    });
```
Per creare con le *association* [qui](https://sequelize.org/v5/manual/associations.html#creating-with-associations), con attenzione all'uso dell'opzione `include`. Dando uno sguardo a [qui](https://sequelize.org/v5/manual/associations.html#associating-objects) si vedono altre interessanti potenzialità ... che noi non approfondiremo in questa sede! 



