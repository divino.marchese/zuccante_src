const Sequelize = require('sequelize');

var sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, 
      min: 0, 
      idle: 10000 
    },
});

// defining models

let Post = sequelize.define('post', {
  title: {type: Sequelize.STRING(30), unique: true}, 
  text: Sequelize.TEXT
});

let Comment = sequelize.define('comment', {
  author: Sequelize.STRING(20),
  text: Sequelize.TEXT,
});

// left to right and right to left
Post.hasMany(Comment, {onDelete: 'CASCADE'}); 
Comment.belongsTo(Post);

sequelize.sync({force: false}).then(() => { 
    Post.findAll({ include: [ Comment ] }).then(posts => {
        console.log(JSON.stringify(posts))
    })
});