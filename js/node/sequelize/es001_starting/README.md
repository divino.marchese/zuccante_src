# First step with sequelize

Per le **api** [qui](https://sequelize.org/api/v6/identifiers.html).


## progetto ed estensioni

Per VSC usiamo l'estensione SQlite
```
npm init
npm i sequelize
npm i sqlite
```

## connection pool

Vedi [qui](https://sequelize.org/docs/v6/other-topics/connection-pool/). Interessante strumento per gestire le connessioni.

## model

Con un *model* definiamo l'oggetto che verrà salvato in modo persistente. Nella tabella corispondente, creata in automatico, verrà aggiunta `id` chiave primaria e i *timestamps*
``` sql
CREATE TABLE IF NOT EXISTS `users` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT, 
  `firstName` VARCHAR(255), 
  `lastName` VARCHAR(255), 
  `createdAt` DATETIME NOT NULL, 
  `updatedAt` DATETIME NOT NULL
);
Executing (default): PRAGMA INDEX_LIST(`users`)
```
Gestiamo quindi il DB.
Usiamo una scorciatoia
- `create` = `build` (crea oggetto) + `save` salva permanentemente oggetto sul db.    

