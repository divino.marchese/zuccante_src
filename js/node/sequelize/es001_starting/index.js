const { Sequelize, DataTypes } = require('sequelize');

let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
  dialect: 'sqlite',
  storage: './db.sqlite',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});

let User = sequelize.define('user', {
  firstName: {
    type: DataTypes.STRING,
    // field: 'first_name' 
  },
  lastName: {
    type: Sequelize.STRING
  }
},
  // { freezeTableName: true }
);

const init = async () => {
  // destroy and recreate table
  await User.sync({ force: true });
}

init().then(
  async () => {
    const john = await User.create({
      firstName: 'John',
      lastName: 'Hancock'
    });
    const users = await User.findAll();
    console.log(`All users: \n ${JSON.stringify(users, null, 4)}`);
  }
)





// using pool not use sequelize.close(); 