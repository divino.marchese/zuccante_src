# es_003_ejs

In questo esempio scegliamo un template per creare pagine dinamiche con Express. Partiamo da [questo](https://medium.com/@dannibla/node-js-with-express-and-ejs-59643c123009) su Medium: useremo **ejs** come sistema di template. Dobbiamo prima di tutto installare `ejs`
```
npm install ejs
```
Il sito ufficiale di ejs è [qui](https://ejs.co/), qui invitiamo a leggere la [documentazione](https://ejs.co/#docs).
