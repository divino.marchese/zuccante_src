# es_001_starting

Express, [qui](https://expressjs.com/en/) la sua home, non lavora ancora con i `Promise` ma usa i **call-nack** alla vecchia manier!. Dalle api, [qui](https://expressjs.com/en/4x/api.html#app), traiamo quanto segue.


## middleware

Nelle [guida](https://expressjs.com/en/guide/using-middleware.html) do Express sui **middleware** troviamo
> Middleware functions are functions that have access to the **request** object (req), the **response** object (res), and the **next** middleware function in the application’s request-response cycle. The next middleware function is commonly denoted by a variable named next.

nel nostro esempio presentiamo un *application level*
``` javascript
app.use((req, res, next) => {                 
  console.log("Request IP: " + req.url);
  console.log("Request date: " + new Date());
  next();
}); 
```
commentando l'ultima riga si avrà l'effetto indesiderato di bloccare il flusso delle operazioni del nostro framework.

**NB** I middleware permettono di costruire un *piplenie* di operazioni - eventualmente un filtro sulla *request*  - da eseguire prima di fornire la *response*


## app.get(path, callback [, callback ...])

Invitiamo alla lettura della **guida al routing** [qui](http://expressjs.com/en/guide/routing.html). Abbiamo a disposizione oltre a `get(...)`, `post(...)`, `put(...)` e `delete(...)` che useremo poi per la nostra REST Web app.


## query string

La *query string* si ottiene da `req.query`, vedi [qui](https://expressjs.com/en/4x/api.html#req.query). Proponiamo per il test
```
curl localhost:3000/query?ciao=miao
curl localhost:3000/query?test=miao
```
Utile nel REST per passare richieste da passare poi al DB tramite il DBMS.

## params

Sempre in chiaro si possono usare parametrizzazioni del path, vedi [qui](https://expressjs.com/en/4x/api.html#req.params). Test:
```
curl localhost:3000/uppercase/ciao
```

## nodemon

Installato *nodemon* [qui](https://www.npmjs.com/package/nodemon) è possivile evitare di stoppare e riavviare il server ogno volta dopo gli aggiornamenti, un po' come fa Angular.
Per l'installazione dei moduli a livello globale e per le impostazioni rimandiamo al `README.md` di node.

