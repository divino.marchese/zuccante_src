# es_0004_,ultipart

In questa esercitazione vedremo di trasferire file al server. Un buon articolo su Medium per iniziare potrebbe essere [questo](https://medium.com/@romeroricky/file-uploads-with-express-js-4-0-30ff3a60f23f). Invitiamo comunque a (ri)leggere la documentazione su MDN [qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST). Per partire diamo
```
npm init
```
inizializza il progetto, scegliamo come file principale server.js,
```
npm install express
npm install multer
```
`multer` è un **middleware** di `express`: [qui](https://www.npmjs.com/package/multer) e su GitHub il [repository](https://github.com/expressjs/multer).

## impostazioni

Per poter gestire in modo *static* i file (in modo che non vengano interpretati dal framework) usiamo il middleware
```
app.use(express.static('/uploads'))
```
è utile a tal proposito la [questa](https://flaviocopes.com/express-static-assets/)