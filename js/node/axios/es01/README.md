# es001

Tratto da un tutorial [qui](https://zetcode.com/javascript/jsonserver/) con le solite aggiunte. Creato il progetto con
```
npm init
```
Usiamo i moduli
- `json-server` [qui](https://www.npmjs.com/package/json-server) preinstallato globalmente.
- `axios` [qui](https://www.npmjs.com/package/axios) per fare richieste HTTP in modo asincrono.

Il server verrà mandato in esecuzione come qui sotto indicato
```
json-server --watch users.json
```
per axios (`--save` è impliito)
```
npm install axios
```

DI seguito alcuni esempi d'uso

## GET

```
curl localhost:3000/users/3/
```
axios lavora così
```javascript
const axios = require('axios');

axios.get('http://localhost:3000/users')
    .then(resp => {
        data = resp.data;
        data.forEach(e => {
            console.log(`${e.first_name}, ${e.last_name}, ${e.email}`);
        });
    })
    .catch(error => {
        console.log(error);
    });
```

## POST

```
curl -X POST -H "Content-Type: application/json" -d '{"id": 6, "first_name": "Fred", "last_name": "Blair", "email": "freddyb34@gmail.com"}' http://localhost:3000/users
```
il codicer axios
```javascript
const axios = require('axios');

axios.post('http://localhost:3000/users', {
    id: 6,
    first_name: 'Fred',
    last_name: 'Blair',
    email: 'freddyb34@gmail.com'
}).then(resp => {
    console.log(resp.data);
}).catch(error => {
    console.log(error);
});
```

## PUT

Usato per **aggiornare**, qui proponiamo solo il lavoro di axios
```javascript
const axios = require('axios');

axios.put('http://localhost:3000/users/6/', {
    first_name: 'Fred',
    last_name: 'Blair',
    email: 'freddyb34@yahoo.com'
}).then(resp => {

    console.log(resp.data);
}).catch(error => {

    console.log(error);
});
```

## DELETE

```
curl -X DELETE  http://localhost:3000/users/6
```

## GET sorted

```javascript
const axios = require('axios');

axios.get('http://localhost:3000/users?_sort=last_name&_order=asc')
    .then(resp => {
        data = resp.data;
        data.forEach(e => {
            console.log(`${e.first_name}, ${e.last_name}, ${e.email}`)
        });
    }).catch(error => {
        console.log(error);
    });

```

## GET gte

```javascript
const axios = require('axios');

axios.get('http://localhost:3000/users?id_gte=4')
    .then(resp => {
        console.log(resp.data)
    }).catch(error => {
        console.log(error);
    });
```

## GET filtering text

```javascript
const axios = require('axios');

axios.get('http://localhost:3000/users?q=yahoo')
    .then(resp => {
        console.log(resp.data)
    }).catch(error => {
        console.log(error);
    });
```

