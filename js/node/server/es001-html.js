const http = require('http');

// Create a local server to receive data from
const server = http.createServer();

// Listen to the request event
server.on('request', (req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/html' });
  res.end(`
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <title>My Page</title>
    </head>
    <body>
      <h1>Hello</h1>
      <p>hello world</p>
    </body>
    </html>`);
});

server.listen(3000, () => console.log('server running'));