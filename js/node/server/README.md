# il server Web in Node

Rimandando agli esempi che i quadrano come Node lavora in modo **asincrono** qui presentiamo alcune classi, riferendoci alle API ufficiali.

## `http.Server`

[API](https://nodejs.org/api/http.html#class-httpserver) è un `EventEmitter` [API](https://nodejs.org/api/events.html#class-eventemitter) e, fra gli altri, è in grado di gestire l'evento `request` con un *callback* con due parametri `req`, un `IncomingMessage` che estende uno `stream.Readable` e `res` di tipo `http.ServerResponse`.

## `EventEmitter`

[API](https://nodejs.org/api/events.html#class-eventemitter) è il gestore degli eventi, i suoi metodi sono
- `emit(...)`: ogni evento viene identificato da una stringa.
- `on(...)`: permette di aggiungere un *listener*, una funzione, per un evento identificato da una stringa.

Rimandiamo agli esempi proposti.

## `Stream`

Ogni *stream* è un `EventEmitter`; oltre ad eserci vari tipi di *stream* - `Writabe`, `Readable`, `Duplex` e `Transform`. In Node gli *stream* sono oggetti che posseggono una loro ricchezza e non rappresentano solo i gestori di sequenze di eventi, sono, ad esempio, bufferizzati ecc..

### `Writable`

Ecco un esempio di codice tratto dalle API per comprendere cosa possiamo fare
```js
const myStream = getWritableStreamSomehow();
myStream.write('some data');
myStream.write('some more data');
myStream.end('done writing data'); 
```
il metodo `end(...)` è legato all'evento `'finish'`.

### `Readable`

Tali stream iniziano a lavorare in *paused mode*
- va gestito l'evento `'data'` con un *handler*.
- `stream.resume()` riprende l'ascolto di uno *stream* in pausa.
- `stream.pipe()` fa il *pipe*.

## `http.IncomingMessage`

Sempre dalle API vediamo proprietà e metodi
- `message.mtehod` ovvero le stringhe `'GET'`, `'POST'`, ...
- `message.url` una stringa, ad esempio `'/status?name=ryan'` per gestirlo meglo lo si trasformi in un `Url` (oggetto) con
`new URL(`http://${process.env.HOST ?? 'localhost'}${request.url}`);`

## `url.URL`

[API](https://nodejs.org/api/url.html#class-url) fra l'altro espone
- `search` mostra in stringa quanto segue `?`
- `searchParams` moto più utile del precedente restituisce un oggetto `URLSearchParams` [API](https://nodejs.org/api/url.html#class-urlsearchparams)

## `http.ServerResponse`

[API](https://nodejs.org/api/http.html#class-httpserverresponse) un `http.OutgoingMessage` che estende uno `Stream` per la precisione un `Writable` quindi con i metodi
- `pipe(...)`
- `write(...)`
- `end(...)`
Rimandiamo al dettaglio della [API](https://nodejs.org/api/stream.html#stream) di `Stream`.

# TCP su Node

## `net.Server`

[API](https://nodejs.org/api/net.html#class-netserver) anche lui un `EventEmitter` quindi ha il solito metodo
- `net.createServer(...)` reagisce automaticamente all'*event* `'connection'` [API](https://nodejs.org/api/net.html#event-connection), l'*handler* prevede l'argomento `Socket`.

## `net.Socket`

[API](https://nodejs.org/api/net.html#class-netsocket) uno *stream duplex*.
- `connect((port, connection, ....))` [API](https://nodejs.org/api/net.html#socketconnectport-host-connectlistener).
- `end()` mada il pacchetto `FIN`: "ho finito di mandare ma ascolto ancora da te, server".