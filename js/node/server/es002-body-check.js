const http = require('http');

const server = http.createServer();

server.on("request", (req, res) => {
  let body = '';
  req.setEncoding('utf8');
  req.on('data', (chunk) => {
    body += chunk;
  });

  req.on('end', () => {
    try {
      const data = JSON.parse(body);
      res.write(typeof data);
      res.end();
    } catch (er) {
      res.statusCode = 400;
      return res.end(`error: ${er.message}`);
    }
  });
});

server.listen(3000, () => console.log("listening"));

/******************* 

$ curl localhost:3000 -d "{}"
object
$ curl localhost:3000 -d "\"foo\""
string
$ curl localhost:3000 -d "not json"
error: Unexpected token o in JSON at position 1

*/