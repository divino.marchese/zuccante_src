var net = require('net');

// listen using nc 127.0.0.1 3000

const server = net.createServer((socket) => {
    socket.on('error', (err) => {
        console.error('SOCKET ERROR:', err);
    });
    socket.write('Echo server\r\n');
    socket.pipe(socket);
});

server.listen(3000, () => {
    console.log('opened server on', server.address());
});
