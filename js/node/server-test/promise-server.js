const fs = require('node:fs/promises');
const http = require('node:http');

const server = http.createServer(async (req, res) => {
    const data = await fs.readFile('./file.txt');

    res.write(data);
    res.end();
});

server.listen(3000, () => {
    console.log('promise-server is listening');
});