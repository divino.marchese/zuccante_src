# server-test

Ora vediamo il vantaggio nell'uso degli *stream* rispetto al semplice *promise*  Possiamo rpovare il tutto con: `curl localhost:3000`. Il file piccolo, rinominato poi come `file.txt` è il solito *lorem ipsum*. `Per creare il file grosso scriviamo `fallocate -l 50MB file.txt`

## `promise-server` file piccolo

`wrk -t10 -c100 -d30s http://localhost:3000`
Il risultato det test è qui sotto
```
Running 30s test @ http://localhost:3000
  10 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     4.07ms    2.18ms 128.96ms   99.62%
    Req/Sec     2.51k   158.99     3.40k    87.53%
  748353 requests in 30.01s, 419.65MB read
Requests/sec:  24935.56
Transfer/sec:     13.98MB
```

## `stream-server` file piccolo

`wrk -t10 -c100 -d30s http://localhost:3000`
Il risultato det test è qui sotto
`wrk -t10 -c100 -d30s http://localhost:3000`
```
Running 30s test @ http://localhost:3000
  10 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     4.80ms    4.95ms 203.47ms   99.64%
    Req/Sec     2.20k   179.06     4.43k    93.30%
  657677 requests in 30.10s, 368.80MB read
Requests/sec:  21850.54
Transfer/sec:     12.25MB
```

## `peomise server` file da 50MB

```
Running 30s test @ http://localhost:3000
  10 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.35s   116.19ms   1.77s    91.72%
    Req/Sec    22.02     18.80    80.00     63.64%
  2175 requests in 30.04s, 101.31GB read
Requests/sec:     72.41
Transfer/sec:      3.37GB
```

## `stream server` file da 50MB

```
Running 30s test @ http://localhost:3000
  10 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     0.00us    0.00us   0.00us    -nan%
    Req/Sec    20.74     28.16    90.00     78.40%
  1300 requests in 30.01s, 62.12GB read
  Socket errors: connect 0, read 0, write 0, timeout 1300
Requests/sec:     43.32
Transfer/sec:      2.07GB
```

## `promise-server` file da 200MB


```
Running 30s test @ http://localhost:3000
  10 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     0.00us    0.00us   0.00us    -nan%
    Req/Sec     0.17      0.39     1.00     83.33%
  12 requests in 30.04s, 3.07GB read
  Socket errors: connect 0, read 1, write 0, timeout 12
Requests/sec:      0.40
Transfer/sec:    104.58MB
```

## `stream-server` file da 200MB

```
Running 30s test @ http://localhost:3000
  10 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     0.00us    0.00us   0.00us    -nan%
    Req/Sec    24.45     33.81    90.00     75.47%
  300 requests in 30.01s, 64.02GB read
  Socket errors: connect 0, read 0, write 0, timeout 300
Requests/sec:     10.00
Transfer/sec:      2.13GB
```


