const fs = require('node:fs');
const http = require('node:http');

const server = http.createServer(async (req, res) => {
    const stream = fs.createReadStream('./file.txt');

    stream.pipe(res);
    stream.on('end', () => {
        res.end();
    })
});

server.listen(3000, () => {
    console.log('stream-server is listening.')
});