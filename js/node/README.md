**Node**

[[_TOC_]]

**[240325]** Con **node** possiamo scrivere server web, [qui](https://nodejs.org/it/) la sua home page, esso si basa su **V8**, [qui](https://v8.dev/) l'home page, a sua volta scritto in *C++*.

# installazione

Sotto Linux Node lo si scarica, lo si scompatta e si espone il suo *path*! Alternativa meno pratica è quella di installare i pacchetti
della propria distribuzione.

# npm settings

L'installazione **global** dei moduli avviene nella directory apposita (vedasi la [documentazione](https://docs.npmjs.com/files/folders)).  Nei sistemi Linux vengono impostate di default in `/usr`. L'utente però necessità di una directory con i suoi permessi (altrimenti nel progetto viene creata la directory di installazione globale che chiaramente non lo è più di fatto). Nel mio caso, dopo aver settato il prefix (come utente senza `sudo`, creata la cartella dando il possesso all'utente ed al suo gruppo)
```
npm config set prefix '/home/genji/development/npm_global'
```
(gli `'` non sono necessari in realtà), vedi anche [qui](https://docs.npmjs.com/cli/config). Per i folder usati da `npm` si può andare [qui](https://docs.npmjs.com/files/folders). Resta da esporre il *path*, ovvero la cartella `/home/genji/development/npm_global/bin`. Temporaneamente possiamo dare
```
export PATH="/home/genji/development/npm_global/bin:$PATH"
```
oppure nel fikle `.bashrc` aggiungere la precedente riga. Tale file viene eseguito ogni qual volta viene aperto un terminale, oppure nel file `.profile` eseguito ad ogni *login* (meglio la prima opzione)
```
source ~/.profile
```
Dando
```
npm config get prefix
```
ottengo
```
/home/genji/development/npm_global
```
Tale info l'avremmo potuta ottenere osservando il contenuto del file `.npmrc` 
```
genji@P330:~$ cat .npmrc 
prefix=/home/genji/development/npm_global
```
Vediamo alcuni usi di `npm config`
```
genji@P330:~$ npm config -h
Manage the npm configuration files

Usage:
npm config set <key>=<value> [<key>=<value> ...]
npm config get [<key> [<key> ...]]
npm config delete <key> [<key> ...]
npm config list [--json]
npm config edit
npm config fix

Options:
[--json] [-g|--global] [--editor <editor>] [-L|--location <global|user|project>]
[-l|--long]

alias: c

Run "npm help config" for more info
```
# conoscere i moduli globalmente installati

Ecco come fare
```
genji@P330:~$ npm list -g --depth 0
/home/genji/development/npm_global/lib
├── @vue/cli@5.0.8
├── http-server@14.1.1
├── json-server@1.0.0-alpha.23
├── nodemon@3.0.3
├── npm@10.5.0
├── to@0.2.9
└── update@0.7.4
```

# disinstallare un pacchetto installato

Ecco come fare
```
npm uninstall -g <package_name>
```

# aggiornare i pacchetti

per aggiornare
```
npm update -g
```
Talvolta ci verrà chiesto di reinstallare ǹpm`.

# projects

Ogni progetto di `Node` possiede un file di configurazione che viene creato automaticamente con
```
npm init
```
creiamo all'interno dell'applicazione il file `package.json` impostando
```
entry point: (app.js)
```
Ora diamo (senza opzione `--save` non più necessaria da npm 5)
```
npm install fastify
```
che salva il modulo in locale e modifica il file `package.json`; basterà poi usare
```
npm install 
```
per installare i moduli specificati in `package.json` (ad esempio dopo averli caricati con git).

# git

Se si vuole evitare la fatica di crearsi il proprio `.gitignore` si può procedere cercando in rete [qui](https://github.com/github/gitignore/blob/master/Node.gitignore) un esempio.
