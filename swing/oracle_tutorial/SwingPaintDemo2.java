import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class SwingPaintDemo2 {
   
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> createAndShowGUI());
        System.out.println("Another example");
    }

    private static void createAndShowGUI() {
        System.out.println("Created GUI on EDT thread? "+
        SwingUtilities.isEventDispatchThread());
        JFrame frame = new JFrame("Es02: insert Jéane");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new MyPanel());
        frame.pack(); 
        frame.setVisible(true);
    }
}

class MyPanel extends JPanel {

    public MyPanel() {
        // setBorder(BorderFactory.createLineBorder(Color.red));
        setBorder(BorderFactory.createLineBorder(Color.black));
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(250,200);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        g.drawString("This is my custom Panel!",20,20);
    }  
}