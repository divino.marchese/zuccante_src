import java.awt.EventQueue;

public class EventeQueueTest {

    public static void main(String[] args) {
        Runnable doHelloWorld = new Runnable() {
            public void run() {
                System.out.println("Hello World on " + Thread.currentThread());
            }
        };
       
        EventQueue.invokeLater(doHelloWorld);
        System.out.println("Before in thread: " + Thread.currentThread());
    }
    
}
