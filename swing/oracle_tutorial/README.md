# EventQueue e SwingUtilitiesTes

Esempio tratto dalle API, fa comprendere come viene gestito il *thread* di `awt`. Il metodo `invokeLater(Runnable doRun)`, viene chiamato dal **EDT** (Event Dispatching Thread di awt) dopo aver eseguito tutti i *task* pendenti su tale thread e dopo quelli consegnati a `main` (dove eventualmente si procede ad inizializzazione) consegna il *task* al *thread* di `awt`. Nella applicazioni Java è tipico dedicare ad un *thread* grafico la gestione degli eventi della GUI.

# SwingPaintDemo1

`JFrame` supporta i componenti leggeri *swing* estende infatti `java.awt.Frame` è la *top level window* per `awt` vedi anche [qui](https://docs.oracle.com/javase/tutorial/uiswing/components/frame.html). Un tempo, per ottenere un contenitor - classe `abstract` - cui aggiungere i componenti `awt` bisognava chiamar
```java
public Container getContentPane()
```
ottenuto il contenitore si poteva aggiungere il componente con il metodo
```java
public Component add(Component comp)
```
che ora si può chiamare direttamente!

# SwingPaintDemo2

Qui invece aggiustiamo le dimensioni del `JFrame` in funzione del contenuto - metodo `pack()` - un `JPanel` opportunamente modificato. `JPanel` è un generico contenitore per *lightweight component*.
``` java
@Override
public Dimension getPreferredSize() { ... }

@Override
public void paintComponent(Graphics g) {
    super.paintComponent(g);       
    ...    
}  
```
il primo dà le dimensioni ed il secondo permette di disegnare, leggere [qui](https://docs.oracle.com/javase/tutorial/uiswing/painting/closer.html) in particolare. `g` è un oggetto di tipo `Graphics` una classe astratta - [qui](https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics.html) per le API ed un'interessante sintesi di come lavora il disegno dell'UI in `awt` - nel caso noi lavoriamo con un oggetto del tipo `Graphics2D` - [qui](https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics2D.html) per ulteriori detagli.

# SwingPaintDemo3

Qui gestiamo gli eventi del mouse. Per convenienza e semplicità usiamo `MouseAdapter` in modo da intercettare gli eventi del mouse in modo semplice!
Nulla di nuovo in questa versione se non la creazione della classe `RedSquare` e la sua gestione, il resto è come nell'esempio precedente! Interessante il lavoro di `repaint()` vedi [qui](https://docs.oracle.com/javase/7/docs/api/java/awt/Component.html#repaint()).