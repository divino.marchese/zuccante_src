import javax.swing.SwingUtilities;

public class SwingUtilitiesTest {

    public static void main(String[] args) {
        Runnable doHelloWorld = new Runnable() {
            public void run() {
                System.out.println("Hello World on " + Thread.currentThread());
            }
        };
       
        SwingUtilities.invokeLater(doHelloWorld);
        System.out.println("Before in thread: " + Thread.currentThread());
    }

    
}