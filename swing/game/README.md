# game

I materiali sono tratti dai riferimenti citati sul `README.md` della cartella principale `swing`. I primi due esempi usano diverse strategie per produrre un'animazione, i dettagli nei `README.md`.

## animation_timer

Usa un `Timer`.

## animation_thread

Usa un `Thread`.

## move_ball

Completa il terzetto degli esempi!


