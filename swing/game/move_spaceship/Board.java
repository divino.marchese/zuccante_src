import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
// import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JPanel 
    implements ActionListener {

    private final int B_WIDTH = 600;
    private final int B_HEIGHT = 400;

    private Timer timer;
    private SpaceShip spaceShip;
    private final int DELAY = 10;

    public Board() {
        initBoard();
    }

    private void initBoard() {

        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
        setFocusable(true);

        spaceShip = new SpaceShip();
        
        addKeyListener(new KeyAdapter(){
            
            @Override
            public void keyReleased(KeyEvent e) {
                spaceShip.keyReleased(e);
            }
            
            @Override
            public void keyPressed(KeyEvent e) {
                spaceShip.keyPressed(e);
            }
            
        });

        timer = new Timer(DELAY, this);
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
        Toolkit.getDefaultToolkit().sync();
    }
    
    private void doDrawing(Graphics g) {
        // Graphics2D g2d = (Graphics2D) g;
        g.drawImage(spaceShip.getImage(), spaceShip.getX(), 
            spaceShip.getY(), this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {  
        step();
    }
    
    private void step() {    
        spaceShip.move();
        repaint();     
    }    
}
