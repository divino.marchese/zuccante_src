import javax.swing.SwingUtilities;
import javax.swing.JFrame;

public class MoveSpaceShip {
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> createAndShowGUI());
    }

    private static void createAndShowGUI() {
        System.out.println("Game started");
        JFrame frame = new JFrame("Swing Paint Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        frame.add(new Board());
        frame.pack();
        frame.setVisible(true);
    } 

}