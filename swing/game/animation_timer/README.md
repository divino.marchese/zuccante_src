# Alcuni dettagli sul codice

`Board` è il motore del gioco, di fatto la finestra.

- `ActonListener`: interfaccia pensata per porsi in ascolto - un *obserber* - di `AcionEvent`, sono gli eventi di `awt` ad alto livello, un esempio è il *click* di un bottone o di un tasto del mouse o della tastiera.
- `JPane` in parte lo abbiamo conosciuto 
> The JPanel class provides general-purpose containers for lightweight components.

Il metodo `initBoard()` è un metodo di inizializzazione, al nostro `JPanel` qui `Board`, leghiamo un *timer* - un `javax.Swing.Timer` - che scandisce l'animazione ridisegnando i *frame*
``` java
timer = new Timer(DELAY, this); 
        timer.start();
```

Il metodo
``` java
@Override
public void paintComponent(Graphics g) {
    super.paintComponent(g);
    drawBall(g);
}
```
è metodo di `JComponent`, la classe base dei componenti `swing`. La sua chiamata avviene certamente dopo aver chiamato `repaint()` di `Component`; l'argomento è il contesto grafico - `Graphics` è classe astratta` su cui disegneremo.

In `private void drawBall(Graphics g)` chiamiamo ` Toolkit.getDefaultToolkit().sync()` che ci assicura la sincronizzazione, vedi [qui](https://docs.oracle.com/en/java/javase/17/docs/api/java.desktop/java/awt/Toolkit.html#sync()).

`repaint()` ci opermette di mettere in coda la zona che andrà ridisegnata (tutto in questo caso): il tutto viene messo alla fine nella coda degli evenyi di `awt`.