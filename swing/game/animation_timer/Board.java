import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingUtilities;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JPanel
        implements ActionListener {

    private final int WIDTH = 400;
    private final int HEIGHT = 400;

    private final int INITIAL_X = 120;
    private final int INITIAL_Y = 80;

    private final int DELAY = 25;

    private final int VEL = 1;

    private final int IMG_W = 100;
    private final int IMG_H = 100;

    private Image ball;
    private Timer timer;
    private int x, y;
    private int vel_x, vel_y;

    public Board() {
        initBoard();
    }

    private void loadImage() {
        ImageIcon icon = new ImageIcon("basket.png");
        ball = icon.getImage();
    }

    private void initBoard() {
        // basic settings
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setBackground(new Color(0, 0, 0));
        // the object
        loadImage();
        // first position of star 
        x = INITIAL_X;
        y = INITIAL_Y;
        // and initial velocity
        vel_x = 1*VEL;
        vel_y = 1*VEL;
        // an event ticker on an action listener i.e. this
        timer = new Timer(DELAY, this); 
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawBall(g);
    }

    private void drawBall(Graphics g) {
        g.drawImage(ball, x, y, this);
        // synchronizes the painting on systems that buffer graphics events (usefull in Linux)
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        x += vel_x;
        y += vel_y;
        if (y + IMG_H > HEIGHT || y < 0 ) {
            vel_y = -vel_y;
        } 
        if (x + IMG_W  > WIDTH || x  < 0 ) {
            vel_x = -vel_x;
        } 
        repaint(); // then paintComponent() is called
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> createAndShowGUI());
    }

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("animation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new Board());
        frame.pack(); 
        frame.setVisible(true);
    }
}

