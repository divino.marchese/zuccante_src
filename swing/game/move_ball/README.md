# commenti al codice

Dopo aver visto la versione coi due timer qui finalmente vediamo un `Thread`, il metodo `run()` si spiega da solo. Interessante è
``` java
@Override
public void addNotify() {
    super.addNotify();

    animator = new Thread(this);
    animator.start();
}
```
esso viene chiamato automaticamente quando il `JPane` viene messo nel `JFrame`: questo è il momento in cui far partire il `Thread`. Esso inoltre è responsabile della raccolta di tutti gli eventi, vedi il nome, dell UI costruita a partire dal `Component` in cui viene chiamato il metodo. In genere tale metodo non viene ritoccato!

```java
timeDiff = System.currentTimeMillis() - beforeTime;
```
Ci permette di gestire l'attesa in modo da recuperare il tempo di disegno, ovvero quello impiegato dai metodi
```java
cycle();
repaint();
```

Usiamo ad un certo punto la classe astratta `MouseAdapter` per evitarci di implementare tutti i metodi (vedi parte commentata)

# materiali

[1] Stack Overflow, discussione [qui](https://stackoverflow.com/questions/23690937/what-does-addnotify-do).
