import javax.swing.SwingUtilities;
import javax.swing.JFrame;

public class MoveBall {
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> createAndShowGUI());
    }

    private static void createAndShowGUI() {
        System.out.println("Game started");
        JFrame f = new JFrame("Move Ball");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        f.add(new Board());
        f.pack();
        f.setVisible(true);
    } 

}
