public class Missile extends Sprite {

    private final int RANGE = 390;
    private final int MISSILE_SPEED = 2;

    public Missile() {
        super();
        initMissile();
    }
    
    private void initMissile() {
        loadImage("img/missile.png");  
        getImageDimensions();
    }

    public void move() {
        x += MISSILE_SPEED;
        if (x > RANGE) {
            visible = false;
        }
    }

    
}

