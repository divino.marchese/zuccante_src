import java.awt.EventQueue;
import javax.swing.border.EmptyBorder;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class GUITest {

    private static void createAndShowGUI() {

        JFrame frame = new JFrame("gui test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        JLabel label = new JLabel("Hello World Swing");
        label.setBorder(new EmptyBorder(20,20,20,20));
        frame.add(label);
 
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(()->createAndShowGUI());
    }
}