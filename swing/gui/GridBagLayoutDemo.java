import java.awt.EventQueue;
import java.awt.Button;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JFrame;
import javax.swing.JButton;

public class GridBagLayoutDemo {

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Demo for GridBag Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // if uncommented remove pack!
        // frame.setSize(180, 180);
        frame.setLocation(400, 200);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbcnt = new GridBagConstraints();
        frame.setLayout(gbl);

        gbcnt.fill = GridBagConstraints.HORIZONTAL;
        
        gbcnt.gridx = 0;
        gbcnt.gridy = 0;
        frame.add(new JButton("Linux"), gbcnt);

        gbcnt.gridx = 1;
        gbcnt.gridy = 0;
        frame.add(new Button("Windows"), gbcnt);
        
        gbcnt.ipady = 20;
        gbcnt.gridx = 0;
        gbcnt.gridy = 1;
        frame.add(new Button("UNIX"), gbcnt);
        
        gbcnt.gridx = 1;
        gbcnt.gridy = 1;
        frame.add(new Button("DOS"), gbcnt);
        
        
        gbcnt.gridx = 0;
        gbcnt.gridy = 2;
        gbcnt.gridwidth = 2;
        frame.add(new Button("Operation System"), gbcnt);
        
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> createAndShowGUI());
    }
    
}
