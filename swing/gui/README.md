# `GUITest`

Un primo semplice esempio. Facciamo conoscenza del *thread* `AWT` - *Abstract Window Toolkit* - e mettiamo in coda il *task*, un oggetto `Runnable`, il *thread* `AWT`, realizzabile anche con più *thread* della CPU, gestisce gli aventi `AWT` e `Swing`.

# `GUIREgistration`

Tratto dalla rete e modificato un po' modernamente. Qui non c'è layout!
``` java
public void setBounds(int x,
             int y,
             int width,
             int height)
```
da `Window` [qui](https://docs.oracle.com/en/java/javase/17/docs/api/java.desktop/java/awt/Window.html#setBounds(int,int,int,int)). Posiziona il componente e ne dà le dimensioni.

```java
public Container getContentPane()
```
di `JFrame` ritorna il *container* della finestra, obsoleto, vedi API di `JFrame`!
```java
resadd.setBackground(getBackground());
```
solo nell'ultimo `JTextArea` salva capra e cavoli.

# `BorderLayoutDemo`

Per le API [qui](https://docs.oracle.com/en/java/javase/17/docs/api/java.desktop/java/awt/BorderLayout.html): i *widget* 
vengono organizzati in 5 regioni `NORTH`, `SOUTH`, `EAST`, `WEST` e `CENTER`. Offre inltre altri posizionamenti più fini per i quali si rimanda alla documentazione.

# `FlowLayoutDemo`

Per la API [qui](https://docs.oracle.com/en/java/javase/17/docs/api/java.desktop/java/awt/FlowLayout.html). E` un po' come nei CSS. 

# `GridLayoutDemo`

Per le API [qui](https://docs.oracle.com/en/java/javase/17/docs/api/java.desktop/java/awt/GridLayout.html). Nel nstro esempio non abbiamo specificato il posizionamento per il quale si rimanda alla documentazioe.
```java
setLayout(new GridLayout(3,2);
```
Col metodo `add()` potremmo pozionare, come nell'esercizio precedente, potremmo aggiungere, a partire dalla prima riga ed a seguire, i *widget*. Noi, invece, usiamo `GridBagConstraints` [qui](https://docs.oracle.com/en/java/javase/17/docs/api/java.desktop/java/awt/GridBagConstraints.html) per un posizionamento più fine, si rimada per i dettagli alla documentazione.


# `GridBagLayoutDemo`

Questo è il layout più complesso e che offre maggiori personalizzazione. Per una guida ufficiale [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/gridbag.html).
```java
GridBagLayout gbl = new GridBagLayout();
GridBagConstraints gbcnt = new GridBagConstraints();
frame.setLayout(gbl);
```
Prepariamo il *layout* ed i *constraint* (vincoli) per gestire gli elementi che inseriremo nella GUI.
```java
gbcnt.gridx = 0;
gbcnt.gridy = 0;
```
prepara l'ogetto perché sia posizionato nella prima posizione (in alto a sinistra).
```java
gbcnt.fill = GridBagConstraints.HORIZONTAL;
```
allarga in orizzontale i componenti in modo che occupino una larghezza superiore, tale da adattarsi al resto dell GUI (provare a togliere per vedere cosa succede), `GridBagConstraints.HORIZONTAL` è il valore di *default* da richiamare esplicitamente in modo da annullare i precedenti `fill` nel caso.
```java
gbcnt.ipady = 20;
```
definisce il *padding* (spazio intorno al contenuto delle'elemento posizionato nella GUI).
```java
gbcnt.gridwidth = 2;
```
L'elemento occuperà due righe.

# `RBMTestApplication`

Interessante esempio da studiare tratto da "Stack Overflow" abbastanza aggiornato come stile!

# `SwingListenerDemo`

Un esempio interessante con gestione degli eventi moderno mediante un oggetto *lambda*: nel bottono usiamo
```java
 okButton.addActionListener(
    (ActionEvent e) -> {
    ...            
    });
```
Altri *widget*hanno metodi analoghi e ci permettono di legare l'ascoltatore al singono *widget* piuttosto che far sì che l'intera classe implementi `ActionLIstener`.

# approfondimenti

[1] "Lesson: Using Swing Components" di Oracle [qui](https://docs.oracle.com/javase/tutorial/uiswing/components/index.html).  
[2] "Lesson: Laying Out Components Within a Container" di Oracle [qui](https://docs.oracle.com/javase/tutorial/uiswing/layout/index.html).  
[3] "A Visual Guide to Swing Components" [qui](https://web.mit.edu/6.005/www/sp14/psets/ps4/java-6-tutorial/components.html). 

# Nota

Mentre `awt` più pesante, dipende dalla piattaforma in cui viene utilizzato, `swing` più leggero no edimpone una sua estetica!
