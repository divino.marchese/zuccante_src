import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SwingListenerDemo {

    private JFrame mainFrame;
    private JPanel panel;
    private JLabel statusLabel;
    

    public SwingListenerDemo() {
        prepareGUI();
    }

    private void prepareGUI() {
        mainFrame = new JFrame("SwingListenerDemo");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(400, 400);
        mainFrame.setLayout(new GridLayout(3, 1));

        JLabel headerLabel = new JLabel("Listener in action: ActionListener", JLabel.CENTER);
        statusLabel = new JLabel("...", JLabel.CENTER);

        JPanel controlPanel = new JPanel();
        panel = new JPanel();
        panel.setBackground(Color.ORANGE);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(
            (ActionEvent e) -> {
                statusLabel.setText("Ok Button Clicked.");
                panel.setBackground(Color.GREEN);
                // try to remove
                mainFrame.setVisible(true);
                System.out.println("click");
            });
        
        panel.add(okButton);
        controlPanel.add(panel);
        
        mainFrame.add(headerLabel);
        mainFrame.add(controlPanel);
        mainFrame.add(statusLabel);

        mainFrame.setVisible(true);
    }

    private static void createAndShowGUI() {
        SwingListenerDemo swingListenerDemo = new SwingListenerDemo();
        swingListenerDemo.prepareGUI();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> createAndShowGUI());
    }
}