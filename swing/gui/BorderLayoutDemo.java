import java.awt.EventQueue;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;

public class BorderLayoutDemo {

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Demonstration of Border Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JButton jbtn1 = new JButton("ip");
        JButton jbtn2 = new JButton("down");
        JButton jbtn3 = new JButton("left");
        JButton jbtn4 = new JButton("right");
        JButton jbtn5 = new JButton("middle");
        
        frame.setLayout(new BorderLayout());
        frame.add(jbtn1, BorderLayout.NORTH);
        frame.add(jbtn2, BorderLayout.SOUTH);
        frame.add(jbtn3, BorderLayout.WEST);
        frame.add(jbtn4, BorderLayout.EAST);
        frame.add(jbtn5, BorderLayout.CENTER);
        
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> createAndShowGUI());
    }

}