import java.awt.EventQueue;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.FlowLayout;

public class FlowLayoutDemo {

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Demonstration of Flow Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(200, 200);
        
        JButton jbtn1 = new JButton("one");
        JButton jbtn2 = new JButton("two");
        JButton jbtn3 = new JButton("three");
        JButton jbtn4 = new JButton("four");
        
        frame.setLayout(new FlowLayout());
        frame.add(jbtn1);
        frame.add(jbtn2);
        frame.add(jbtn3);
        frame.add(jbtn4);
        
        // NO: frame.pack();
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> createAndShowGUI());
    }
}
