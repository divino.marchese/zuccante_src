import java.awt.EventQueue;
import java.awt.Button;
import java.awt.GridLayout;
import javax.swing.JFrame;

public class GridLayoutDemo {

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Demonstration of Grid Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(200, 200);
        frame.setLayout(new GridLayout(3, 2));

        frame.add(new Button("Button A"));
        frame.add(new Button("Button B"));
        frame.add(new Button("Button C"));
        frame.add(new Button("Button D"));
        frame.add(new Button("Button E"));
        frame.add(new Button("Button F"));

        // NO: frame.pack();
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> createAndShowGUI());
    }
}
