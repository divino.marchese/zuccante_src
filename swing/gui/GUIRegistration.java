import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUIRegistration extends JFrame implements ActionListener {

	private JLabel name;
	private JTextField tname;
	private JLabel mno;
	private JTextField tmno;
	private JLabel gender;
	private JRadioButton male;
	private JRadioButton female;
	private ButtonGroup gengp;
	private JLabel dob;
	private JComboBox date;
	private JComboBox month;
	private JComboBox year;
	private JLabel add;
	private JTextArea tadd;
	private JCheckBox term;
	private JButton sub;
	private JButton reset;
	private JTextArea tout;
	private JLabel res;
	private JTextArea resadd;

	private String dates[]
		= { "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "10",
			"11", "12", "13", "14", "15",
			"16", "17", "18", "19", "20",
			"21", "22", "23", "24", "25",
			"26", "27", "28", "29", "30",
			"31" };
	private String months[]
		= { "Jan", "feb", "Mar", "Apr",
			"May", "Jun", "July", "Aug",
			"Sup", "Oct", "Nov", "Dec" };
	private String years[]
		= {"2007", "2008", "2009", "2010",
        "2011", "2012", "2013", "2014",
        "2015", "2016", "2017", "2018",
        "2019", "2020", "2021", "2022" };

	
	public GUIRegistration() {
		setTitle("Registration Form");
		setBounds(300, 90, 700, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// setBackground(Color.RED);
		setResizable(false);

		name = new JLabel("Name");
		name.setSize(50, 20);
		name.setLocation(50, 50);
		add(name);

		tname = new JTextField();
		tname.setSize(100, 20);
		tname.setLocation(100, 50);
		add(tname);


		mno = new JLabel("Mobile");
		mno.setSize(50, 20);
		mno.setLocation(50, 100);
		add(mno);

		tmno = new JTextField();
		tmno.setSize(100, 20);
		tmno.setLocation(100, 100);
		add(tmno);


		gender = new JLabel("Gender");
		gender.setSize(70, 20);
		gender.setLocation(50, 150);
		add(gender);

		male = new JRadioButton("Male");
		male.setSelected(true);
		male.setSize(75, 20);
		male.setLocation(110, 150);
		add(male);

		female = new JRadioButton("Female");
		female.setSelected(false);
		female.setSize(80, 20);
		female.setLocation(180, 150);
		add(female);

		gengp = new ButtonGroup();
		gengp.add(male);
		gengp.add(female);


		dob = new JLabel("DOB");
		dob.setSize(70, 20);
		dob.setLocation(50, 200);
		add(dob);

		date = new JComboBox(dates);
		date.setSize(50, 20);
		date.setLocation(100, 200);
		add(date);

		month = new JComboBox(months);
		month.setSize(60, 20);
		month.setLocation(150, 200);
		add(month);

		year = new JComboBox(years);
		year.setSize(60, 20);
		year.setLocation(210, 200);
		add(year);


		add = new JLabel("Address");
		add.setSize(100, 20);
		add.setLocation(50, 250);
		add(add);

		tadd = new JTextArea();
		tadd.setSize(200, 75);
		tadd.setLocation(50, 280);
		tadd.setLineWrap(true);
		add(tadd);

		term = new JCheckBox("Accept Terms And Conditions.");
		term.setSize(250, 20);
		term.setLocation(50, 400);
		add(term);


		sub = new JButton("Submit");
		sub.setSize(100, 20);
		sub.setLocation(50, 450);
		sub.addActionListener(this);
		add(sub);


		reset = new JButton("Reset");
		reset.setFont(new Font("Ubuntu", Font.PLAIN, 15));
		reset.setSize(100, 20);
		reset.setLocation(150, 450);
		reset.addActionListener(this);
		add(reset);


		tout = new JTextArea();
		tout.setSize(300, 400);
		tout.setLocation(350, 50);
		tout.setLineWrap(true);
		tout.setEditable(false);
		add(tout);

		res = new JLabel("");
		res.setFont(new Font("Ubuntu", Font.PLAIN, 20));
		res.setSize(500, 25);
		res.setLocation(100, 500);
		add(res);

		resadd = new JTextArea();
        resadd.setBackground(getBackground());
		resadd.setSize(200, 75);
		resadd.setLocation(580, 175);
		resadd.setLineWrap(true);
		add(resadd);
        
        // pack(); NO
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
        // submit
		if (e.getSource() == sub) {
			if (term.isSelected()) {
				String data1;
				String data
					= "Name : "
					+ tname.getText() + "\n"
					+ "Mobile : "
					+ tmno.getText() + "\n";
				if (male.isSelected())
					data1 = "Gender : Male"
							+ "\n";
				else
					data1 = "Gender : Female"
							+ "\n";
				String data2
					= "DOB : "
					+ (String)date.getSelectedItem()
					+ "/" + (String)month.getSelectedItem()
					+ "/" + (String)year.getSelectedItem()
					+ "\n";

				String data3 = "Address : " + tadd.getText();
				tout.setText(data + data1 + data2 + data3);
				tout.setEditable(false);
				res.setText("Registration Successfully..");
			}
			else {
				tout.setText("");
				resadd.setText("");
				res.setText("Please accept the"
							+ " terms & conditions..");
			}
		}
        // reset
		else if (e.getSource() == reset) {
			String def = "";
			tname.setText(def);
			tadd.setText(def);
			tmno.setText(def);
			res.setText(def);
			tout.setText(def);
			term.setSelected(false);
			date.setSelectedIndex(0);
			month.setSelectedIndex(0);
			year.setSelectedIndex(0);
			resadd.setText(def);
		}
	}

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new GUIRegistration());
    }
}