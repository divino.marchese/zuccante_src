# awt e swing tutorial

`swing` è la componentistica leggera che si appoggia su `awt` creata a suo tempo dalla Sun, comunque tutta roba *vintage*. Rispetto a `JavaFX` che Oracle ha abbandonato, pur essendo ancora portata avanti come progetto *open*, ci troviamo di fronte a qualcosa su cui pesano indubbiamente gli anni, ma sicuramente più leggero e valido dal punto di vista didattico.

## game

"Java 2D games" da ZenCode [qui](http://zetcode.com/javagames/) è stato il punto di partenza su cui abbiamo lavorato.

## oracle tutorial

Aggiustando il materiale ufficiale di Oracle [qui](https://docs.oracle.com/javase/tutorial/uiswing/).

## gui

Esempi di GUI e layout per iniziare da integrare con [2].

# riferimenti

[1] "Oracle Tutorials" da Oracle [qui](https://docs.oracle.com/javase/tutorial/uiswing/index.html).  
[2] "Widget Examples" da Oracle [qui](https://docs.oracle.com/javase/tutorial/uiswing/examples/components/index.html).  
