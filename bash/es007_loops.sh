#!/bin/bash

echo "- loop01"
i=0
while (( i < 5 ))
do
  echo "num: $i"
  (( i++ ))                    # anche $(( i++ ))
  if (( i == 2 )); then
    break
  fi
done

echo "- loop02"
for i in {1..3}; do          # Ref.3.5.1 brace expansion
  for j in {1..3}; do
    if (( j == 2 )); then
      break
    fi
    echo "j: $j"
  done
  echo "i: $i"
done

echo "- loop03"
i=0
while (( i < 5 )); do
  (( i++ ))
  if (( i == 2 )); then
    continue
  fi
  echo "number: $i"
done

echo "- loop04"
for i in $(ls *.sh)
do
  echo "\""$i\"" is a bash script"
done

echo "- loop 05"
for txt in $(ls *.txt)
do
  echo "txt: $txt"
done
