#!/bin/bash

if [ $# -lt 2 ]
then
  echo "mancano argomenti"
  # exit shell script
  # exit 1
  return 1 
fi
sum=$(( $1+$2 ))
echo "la somma di $1 e $2 è $sum"
