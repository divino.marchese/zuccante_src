# programmazione

Qui di seguito elenchiamo i riferimenti in "Linux Fun" di P.Cobbaut. Quando si trova `Ref`
si fa riferimento allla "Bash Reference"

- `23.1 - 23.2` introduzione
- `23.3` *sha-bang* `#!/bin/bash`
- `23.4 - 23.7`
- `24.1` `Ref.4.1` `Ref.3.5.2` istruzione `test` ed altri test `[...]`, `[[...]]` e `((...))`
- `24.2` `Ref.3.5.2` istruzione `if`
- `24.3` istruzione `case`
- `24.4` `Ref.3.2.5.1` istruzione `for`
- `Ref. 6.7` aarray
- `24.5` e `24.6`, `Ref.3.2.5.1`: `while` e `until` (vedi `do ... while`)
- `Ref.3.5.1` *brace expansion*   
- `Ref.3.5.4` *command substitution* `$(...)` (meglio della vecchia versione)    
- `Ref.3.5.5` `Ref.6.5` espressioni aritmetiche (ripasso) `$((...))`
- `Ref. 3.5.7` La variabile `IFS`
