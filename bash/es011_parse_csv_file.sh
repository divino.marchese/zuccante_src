#!/bin/bash

ILE=$1

i=1
#IFS Internal Field Separator
while IFS=',' read -r name zip rule
do
  echo "$i -> name: $name, zip: $zip, rule: $rule"
  ((i++))
done < $FILE
