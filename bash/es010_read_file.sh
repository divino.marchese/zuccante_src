#!/bin/bash

FILE=$1

i=1
while read -r line
do
  echo "$i: $line"
  ((i++))
done < $FILE
