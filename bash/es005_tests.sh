#!/bin/bash

# ref. 3.5.2, 6.4, 6.5
# man test

# il test è un'istruzione e ritorna un esito, vedi $?

[ 1 -lt 2 -a 2 -lt 3 ]; echo $?  # -a per and
[[ 1 -lt 2 && 2 -lt 3 ]]; echo $? # possiamo lavorare come in C con && e ||
[[ ( 1 -lt 2 ) && ( 2 -lt 3 ) ]]; echo $? # attenzione alle tonde 
(( 1 < 2 && 2 < 3 )); echo $? # confronto matematico vedi $((...)),
(( $# == 0 )); echo $? 
[ $# -eq 0 ]; echo $?
[ $# == "0" ]; echo $? # il confronto è fra stringhe
(( (1 < 2) && (2 < 3) )); echo $? # nessun problema con gli spazi fra le parentesi
