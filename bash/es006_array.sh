#!/bin/bash

echo "- array01"

nums=(2 3 5 7 9 11)
echo $nums
echo $nums[0]    # errore
echo ${nums[0]}
echo "${nums[9]} non mi mostra nulla"  # ?????
echo ${nums[*]}  # molto utile per
for i in ${nums[*]}
do 
  echo "echo $i"
done

echo "- array02"

array=()
array+=(12)
array+=(17)
echo ${array[*]}
