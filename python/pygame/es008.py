import pygame   
pygame.init()  

window = pygame.display.set_mode((600, 600)) 
pygame.display.set_caption("es007") 

x = 200
y = 200
width, height = 20, 20
vel = 5
red =  (255, 0, 0)

run = True
while run: 
    # space = 10*vel
    pygame.time.delay(10) 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            run = False
    # get_pressed() -> bools
    keys = pygame.key.get_pressed() 
    if keys[pygame.K_LEFT] and x > 0: 
        x -= vel 
    if keys[pygame.K_RIGHT] and x < 600 - width: 
        x += vel 
    if keys[pygame.K_UP] and y > 0: 
        y -= vel 
    if keys[pygame.K_DOWN] and y < 600 - height: 
        y += vel 
    window.fill((0, 0, 0)) 
    pygame.draw.rect(window, red, (x, y, width, height)) 
    pygame.display.update()  
pygame.quit() 
