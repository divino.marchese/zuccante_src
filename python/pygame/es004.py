import pygame 
pygame.init() 

screen = pygame.display.set_mode((500,500)) 
pygame.display.set_caption("es004")
white = (255,255,255) 

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    rect = pygame.Rect(350, 250, 60, 90)
    pygame.draw.ellipse(screen, white , rect, 4)
    # pygame.draw.ellipse(screen, white , (350, 250, 60, 90), 4)
    pygame.display.update()