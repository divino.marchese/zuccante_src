import random
import pygame   

pygame.init()  

vec = pygame.math.Vector2

WIDTH, HEIGHT = 600, 600

screen = pygame.display.set_mode((WIDTH, HEIGHT)) 
pygame.display.set_caption("es012") 

#velocity
SIZE = 20
# colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
# tick data
clock = pygame.time.Clock()
FPS = 60

# ================ class ===============================

class Segment(pygame.sprite.Sprite):
    def __init__(self, pos):
        super().__init__()
        self.pos = pos 
        self.score = 0

    def draw(self):
        self.rect = pygame.draw.rect(screen, WHITE, pygame.Rect(self.pos.x-SIZE//2, self.pos.y-SIZE//2, SIZE, SIZE), width = 2, border_radius = 4)

    def move(self, dx, dy):
        self.pos.x += dx
        self.pos.y += dy

    def key_detect(self):
        keys = pygame.key.get_pressed() 
        if keys[pygame.K_LEFT] and self.pos.x > SIZE//2 + 10: 
            self.move(-SIZE,0) 
        if keys[pygame.K_RIGHT] and self.pos.x < WIDTH - SIZE//2 - 10: 
            self.move(SIZE,0) 
        if keys[pygame.K_UP] and self.pos.y > SIZE//2 + 10: 
            self.move(0,-SIZE)
        if keys[pygame.K_DOWN] and self.pos.y < HEIGHT - SIZE//2 - 10: 
            self.move(0,SIZE)

    def hit(self):
        hits = pygame.sprite.spritecollide(self, fruits, False)
        if hits:
            self.score += 1
            print(self.score)
            for fruit in hits:
                fruit.kill()
            fruit = Fruit.random_fruit()
            sprites.add(fruit)
            fruits.add(fruit)

class Fruit(pygame.sprite.Sprite):
    def __init__(self, pos):
        super().__init__()
        self.pos = pos # initial position in front of ship

    def draw(self):
        self.rect = pygame.draw.rect(screen, WHITE, pygame.Rect(self.pos.x-SIZE//2, self.pos.y-SIZE//2, SIZE, SIZE), border_radius = 4)

    def random_fruit():
        return Fruit(vec(10 + SIZE//2 + random.randint(0, WIDTH//SIZE -2)*SIZE,10 + SIZE//2 + random.randint(0, HEIGHT//SIZE - 2)*SIZE))
 
            

# =============================================================
    
sprites = pygame.sprite.Group()
head = Segment(vec(WIDTH//2, HEIGHT//2))
sprites.add(head)

fruits = pygame.sprite.Group()
fruit = Fruit(vec(100, 100))
fruits.add(fruit)
sprites.add(fruit)


run = True
while run: 
    for event in pygame.event.get(): 
        head.key_detect()
        if event.type == pygame.QUIT: 
            run = False

    screen.fill(BLACK)    
    for entity in sprites:
        entity.draw()

    head.hit()

    pygame.display.update()  
    clock.tick(FPS)
