import pygame 
from pygame import display, draw, QUIT
pygame.init() 

screen = display.set_mode((500,500)) 
display.set_caption("es005")

purple = (102, 0, 102) 

running = True
while running:
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
    points = (146, 0), (291, 106),(236, 277), (56, 277), (0, 106)
    draw.polygon(screen, purple, points)
    display.update() 
    