import pygame 
pygame.init() 

screen = pygame.display.set_mode((500,500)) 
pygame.display.set_caption("es003")

green = (0,255,0)
 

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    line_start = 40, 300
    line_stop = 140, 300
    line_width = 6
    pygame.draw.line(screen, green, line_start, line_stop, line_width)
    pygame.display.update()