import random
import pygame
 
vec = pygame.math.Vector2  # 2 for two dimensional

pygame.init()

SIZE = WIDTH, HEIGHT = 400, 450

YELLOW = (255,204,0)
GREEN = (0,255,0)
RED = (255,0,0)
BLACK = (0,0,0)

ACC = 0.5
FRIC = -0.12

clock = pygame.time.Clock()
FPS = 60
 
displaysurface = pygame.display.set_mode(SIZE)
pygame.display.set_caption("es008")

# ==== class =======================

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__() 
        self.surf = pygame.Surface((30, 30))
        self.surf.fill(YELLOW) 
        self.rect = self.surf.get_rect()
        self.pos = vec(10, 20)    # start position
        self.vel = vec(0,0)       # velocity
        self.acc = vec(0,0)       # accelleration
        self.jumping = False      # jumping status 

    def move(self):
        self.acc = vec(0,0.5) # gravity
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_LEFT]:
            self.acc.x = -ACC
        if pressed_keys[pygame.K_RIGHT]:
            self.acc.x = ACC
        # object dynamic evolution
        self.acc.x += self.vel.x * FRIC
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc
        # at the edges
        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH
        self.rect.midbottom = self.pos

    def update(self):
        hits = pygame.sprite.spritecollide(p1 , platforms, False)
        if hits:
            self.pos.y = hits[0].rect.top + 1
            self.vel.y = 0

    def jump(self):
        hits = pygame.sprite.spritecollide(self, platforms, False)
        if hits:
            self.jumping = True
            self.vel.y = -15

    def cancel_jump(self):
        if self.jumping:
            if self.vel.y < -3:
                self.vel.y = -3
 
class Platform(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((random.randint(50,100), 12))
        self.surf.fill(GREEN)
        self.rect = self.surf.get_rect(center = (random.randint(0,WIDTH-10),
                                                 random.randint(0, HEIGHT-30)))

class Ground(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((WIDTH, 20))
        self.surf.fill(RED)
        self.rect = self.surf.get_rect(center = (WIDTH/2, HEIGHT - 10))

def check(platform, groupies):
    if pygame.sprite.spritecollideany(platform,groupies):
        return True
    else:
        for entity in groupies:
            if entity == platform:
                continue
            if (abs(platform.rect.top - entity.rect.bottom) < 40) and (abs(platform.rect.bottom - entity.rect.top) < 40):
                return True
        C = False

def plat_gen():
    while len(platforms) < 6:
        width = random.randrange(50,100)
        p  = Platform()      
        C = True
        while C:
             p = Platform()
             p.rect.center = (random.randrange(0, WIDTH - width), random.randrange(-50, 0))
             C = check(p, platforms)
        platforms.add(p)
        all_sprites.add(p)
 
# ===== create and add sprites ======

all_sprites = pygame.sprite.Group()
p1 = Player()
all_sprites.add(p1)
platforms = pygame.sprite.Group()
for x in range(random.randint(4,5)):
    C = True
    pl = Platform()
    while C:
        pl = Platform()
        C = check(pl, platforms)
    platforms.add(pl)
    all_sprites.add(pl)
pt1 = Ground()
platforms.add(pt1)
all_sprites.add(pt1)

# ==== add levels 

def plat_gen():
    while len(platforms) < 7 :
        width = random.randrange(50,100)
        p  = Platform()             
        p.rect.center = (random.randrange(0, WIDTH - width),
                             random.randrange(-50, 0))
        platforms.add(p)
        all_sprites.add(p)


run = True
while run: 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT:   
            run = False
        if event.type == pygame.KEYDOWN: 
            if event.key == pygame.K_SPACE:
                p1.jump()
        if event.type == pygame.KEYUP:    
            if event.key == pygame.K_SPACE:
                p1.cancel_jump()

    displaysurface.fill(BLACK)
 
    for entity in all_sprites:
        displaysurface.blit(entity.surf, entity.rect)

    p1.move()
    p1.update()
    plat_gen()
    # move levels
    if p1.rect.top <= HEIGHT / 3:
        p1.pos.y += abs(p1.vel.y)
        for plat in platforms:
            plat.rect.y += abs(p1.vel.y)
            if plat.rect.top >= HEIGHT:
                plat.kill()

    pygame.display.update()
    clock.tick(FPS)
