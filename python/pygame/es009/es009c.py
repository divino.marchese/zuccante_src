import pygame
 
vec = pygame.math.Vector2  # 2 for two dimensional

pygame.init()

SIZE = WIDTH, HEIGHT = 400, 450

YELLOW = (255,204,0)
RED = (255,0,0)
BLACK = (0,0,0)
## dynamic
ACC = 0.5
FRIC = -0.12

clock = pygame.time.Clock()
FPS = 60
 
displaysurface = pygame.display.set_mode(SIZE)
pygame.display.set_caption("es008")

# ==== class =======================

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__() 
        self.surf = pygame.Surface((30, 30))
        self.surf.fill(YELLOW) 
        self.rect = self.surf.get_rect()
        self.pos = vec(10, 385)   # start position
        self.vel = vec(0,0)       # velocity
        self.acc = vec(0,0)       # accelleration

    def move(self):
        self.acc = vec(0,0)
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_LEFT]:
            self.acc.x = -ACC
        if pressed_keys[pygame.K_RIGHT]:
            self.acc.x = ACC
        # object dynamic evolution
        self.acc.x += self.vel.x * FRIC
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc
        # at the edges
        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH
        self.rect.midbottom = self.pos
 
class Platform(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((WIDTH, 20))
        self.surf.fill(RED) 
        self.rect = self.surf.get_rect(center = (WIDTH/2, HEIGHT - 10))
 
# ===== create and add sprites ======

all_sprites = pygame.sprite.Group()
pt1 = Platform()
all_sprites.add(pt1)
p1 = Player()
all_sprites.add(p1)

run = True
while run: 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT:   
            run = False

    displaysurface.fill(BLACK)
 
    for entity in all_sprites:
        displaysurface.blit(entity.surf, entity.rect)
    # move
    p1.move()
    pygame.display.update()
    clock.tick(FPS)
