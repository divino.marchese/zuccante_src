# un primo gioco completo

[[_TOC_]]

Reaiamo il nostro esempio, con nostre modifiche, da [qui](https://coderslegacy.com/python/pygame-platformer-game-development/).

## es009a

Impostiamo il gioco: disegniamo la finestra ed impostiamo l'evento di uscita. Il ciclo conterrà tutto l'aggiornamento del
gioco che andremo poi a sviluppare.

## es009b

- `pygame.Surface` è una classe per le immagini occupa una porzione di display rettangolare
- `get_rect()` per ottenere il  `Rect` 
- `pygame.math` è un modulo [qui](https://www.pygame.org/docs/ref/math.html), in partcolare `Vector2` come classe ci offre la possibilità di gestire vettori 2D.
- `pygame.display.update()` aggiorna l'intera area o sue parti se le passiamo una serie di rettangoli come argomenti e per questo risulta ottimizzata rispetto a `pygame.display.flip()`.
- `displaysurface.blit(entity.surf, entity.rect)` posiziona sul `rect`, zona del display, la superficie `surf` (l'oggetto grafico): ritorna un `Rect`. `Rect` infatti identifica una zona rettangolare nello spazio posizionata (con le sue coordinate).

Le classi rappresentano degli oggettin (il giocatore ed una piattaforma nel nostro esempi) un oggetto occupa unza zona: un `Rect` posizionato in riferimento al suo centro, `surf` è l'oggetto nella sua forma non posizionato.

Nel gioco abbiamo definito il metodo `move()` in cui definiamo la **fisica** del nostro giocatore

## es009c

Articoliamo la classe per il *player* definendo
```python
self.pos = vec(10, 385)   # start position
self.vel = vec(0,0)       # velocity
self.acc = vec(0,0)       # accelleration
```
la classe `vec` è `pygame.math.Vector2` che ci permette di esprimere con agilità l'ascissa e l'ordinata di un vettore. In `move()` vediamo la dinamica dell'oggetto!

## es009d

Qui introduciamo la **gravità** - la **forza peso** - e la **collisione**, due fattori importanti per il nostro gioco.
- `pygame.sprite.spritecollide()` cerca gli sprite in un gruppo che collidono con un dato sprite: nel nostro caso questo è `p1` ed il gruppo sono i `platforms` (per ora contenente un solo elemento); l'ultimo argomento è `doKill == False` che sta ad indicare che i due sprite che collidono non vengono eliminati dal loro gruppo, il `True` diviene utile quando ci si trova nella situazione in cui un proiettile colpisce un bersaglio.
- `self.pos.y = hits[0].rect.top + 1` evita l'iseterisi delle collisioni (matematicamente). Ricordiamo inoltre che la posizione del giocatore è situata al centro in basso!

## es009e

Inseriamo il salto. **NOTA**: poiché il salto viene rilevato una sola volta, non si bada se il tasto viene mantenuto, lo mettiamo fra gli eventi rileti in testa, analogamente accadrebbe per spari e cose simili! Vediamo il metodo
```python
def update(self):
    hits = pygame.sprite.spritecollide(p1 , platforms, False)
    if hits:
        self.pos.y = hits[0].rect.top + 1
        self.vel.y = 0
```
Una volta verificatosi il contatto viene fatto in modo che il *player* si sovrapponga di `1px` in tal modo non abbiamo continue collisione fra *player* e piattaforma
```python
self.pos.y = hits[0].rect.top + 1
```
provare a mettere un `-5` per vedere il *bouncing*, inoltre viene fermato il *player*
```python
self.vel.y = 0
```

## es009f

Il salto può essere effettuato solo quando vi è contatto con una piattaforma, l'unica piattaforma per ora, non in volo!
```python
def jump(self):
    hits = pygame.sprite.spritecollide(self, platforms, False)
    if hits:
        self.vel.y = -15
```

## es009g

Aggiungiamo più piattaforme in modo random

## es009h

Muoviamo i livelli.

## es009i

Interrompiamo il salto alzando `K_SPACE`.

## es009j

Sistemiamo la distanza fra le piattaforme evitandone la collisione. Vedi la funzione `check()`.

## es009k

Il *game over* ed il contatore di salti.

## es009l

Le piattaforme si muovono


