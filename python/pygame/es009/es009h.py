import random
import pygame
 
vec = pygame.math.Vector2  # 2 for two dimensional

pygame.init()

SIZE = WIDTH, HEIGHT = 400, 450

YELLOW = (255,204,0)
GREEN = (0,255,0)
RED = (255,0,0)
BLACK = (0,0,0)

ACC = 0.5
FRIC = -0.12

clock = pygame.time.Clock()
FPS = 60
 
displaysurface = pygame.display.set_mode(SIZE)
pygame.display.set_caption("es008")

# ==== class =======================

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__() 
        self.surf = pygame.Surface((30, 30))
        self.surf.fill(YELLOW) 
        self.rect = self.surf.get_rect()
        self.pos = vec(10, 20) # start position
        self.vel = vec(0,0)       # velocity
        self.acc = vec(0,0)       # accelleration

    def move(self):
        self.acc = vec(0,0.5) # gravity
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_LEFT]:
            self.acc.x = -ACC
        if pressed_keys[pygame.K_RIGHT]:
            self.acc.x = ACC
        # object dynamic evolution
        self.acc.x += self.vel.x * FRIC
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc
        # at the edges
        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH
        self.rect.midbottom = self.pos

    def update(self):
        hits = pygame.sprite.spritecollide(p1 , platforms, False)
        if hits:
            self.pos.y = hits[0].rect.top + 1
            self.vel.y = 0

    def jump(self):
        hits = pygame.sprite.spritecollide(self, platforms, False)
        if hits:
            self.vel.y = -15
 
class Platform(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((random.randint(50,100), 12))
        self.surf.fill(GREEN)
        self.rect = self.surf.get_rect(center = (random.randint(0,WIDTH-10),
                                                 random.randint(0, HEIGHT-30)))

class Ground(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.Surface((WIDTH, 20))
        self.surf.fill(RED)
        self.rect = self.surf.get_rect(center = (WIDTH/2, HEIGHT - 10))
 
# ===== create and add sprites ======

all_sprites = pygame.sprite.Group()
p1 = Player()
all_sprites.add(p1)
platforms = pygame.sprite.Group()
for x in range(random.randint(5, 6)):
    pl = Platform()
    platforms.add(pl)
    all_sprites.add(pl)
pt1 = Ground()
platforms.add(pt1)
all_sprites.add(pt1)

# ==== add levels 

def plat_gen():
    while len(platforms) < 7 :
        width = random.randrange(50,100)
        p  = Platform()             
        p.rect.center = (random.randrange(0, WIDTH - width),
                             random.randrange(-50, 0))
        platforms.add(p)
        all_sprites.add(p)


run = True
while run: 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT:   
            run = False
        if event.type == pygame.KEYDOWN:    
            if event.key == pygame.K_SPACE:
                p1.jump()

    displaysurface.fill(BLACK)
 
    for entity in all_sprites:
        displaysurface.blit(entity.surf, entity.rect)

    p1.move()
    p1.update()
    plat_gen()
    # move levels
    if p1.rect.top <= HEIGHT / 3:
        p1.pos.y += abs(p1.vel.y)
        for plat in platforms:
            plat.rect.y += abs(p1.vel.y)
            if plat.rect.top >= HEIGHT:
                plat.kill()

    pygame.display.update()
    clock.tick(FPS)
