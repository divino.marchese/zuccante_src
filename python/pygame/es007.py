import pygame   
pygame.init()  

window = pygame.display.set_mode((600, 600)) 
pygame.display.set_caption("es007") 

x = 200
y = 200
width, height = 20, 20
step = 20
red =  (255, 0, 0)


run = True
while run: 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            run = False
        if event.type == pygame.KEYDOWN: 
            if event.key == pygame.K_LEFT and x > 0:
                x -= step
            if event.key == pygame.K_RIGHT and x < 600 - width:
                x += step
            if event.key == pygame.K_UP and y > 0:
                y -= step
            if event.key == pygame.K_DOWN and y < 600 - height:
                y += step
    window.fill((0, 0, 0)) 
    pygame.draw.rect(window, red, (x, y, width, height)) 
    pygame.display.update()  
pygame.quit() 