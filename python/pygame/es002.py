import pygame
pygame.init()

white = (255, 255, 255)
red = (200, 0, 0)

screen = pygame.display.set_mode((600, 500))
pygame.display.set_caption("es002")

running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill(white)
    center = 250, 250
    radius = 80
    pygame.draw.circle(screen, red, center, radius)
    pygame.display.update()

pygame.quit()