import pygame 
pygame.init() 

screen = pygame.display.set_mode((500,500)) 
pygame.display.set_caption("es006")

yellow = (255, 255, 100) 

font = pygame.font.SysFont("Arial", 80)  
text = font.render("es006 ", True, yellow)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    # blit -> draw an image onto another
    position = 320 - text.get_width()//2, 240 - text.get_height() // 2
    screen.blit(text, position) 
    pygame.display.update() 
    