# es010

Un gioco "sparea spara" vintage!

# es010a

Costruiamo la nave e associamo ad essa lo sprite. 
- `aalines(surface, color, closed, points) -> Rect` il parametro `losed` permette di aggiungere un segmento per chiudere il percorso in una poligonale chiusa. 

# es010b

Aggiungiamo i missili.

# es010c

Aggiungiamo lo sciame di nemici

# es010d

Aggiungiamo le collisioni e lo *score*
