import random
import pygame   

pygame.init()  

vec = pygame.math.Vector2

SIZE = WIDTH, HEIGHT = 600, 600

screen = pygame.display.set_mode(SIZE) 
pygame.display.set_caption("es009") 

#velocity
VEL = 5
# colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED =  (255, 0, 0)
# tick data
clock = pygame.time.Clock()
FPS = 60
# enemy generator
dice = 6

# ================ class ===============================

class SpaceShip(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.pos = vec(WIDTH/2, HEIGHT/2) # initial position in front of ship

    def draw(self):
        self.rect = pygame.draw.aalines(screen, WHITE, True,
                    ((-40 + self.pos.x, self.pos.y), (self.pos.x, self.pos.y),(-40 + self.pos.x, -20 + self.pos.y)))

    def move(self, x, y):
        self.pos.x += x
        self.pos.y += y

    def key_detect(self):
        keys = pygame.key.get_pressed() 
        if keys[pygame.K_LEFT] and self.pos.x > 40 + 10: 
            self.move(-VEL,0) 
        if keys[pygame.K_RIGHT] and self.pos.x < WIDTH -10: 
            self.move(VEL,0) 
        if keys[pygame.K_UP] and self.pos.y > 20 + 10: 
            self.move(0,-VEL)
        if keys[pygame.K_DOWN] and self.pos.y < HEIGHT - 10: 
            self.move(0,VEL)

    def fire(self):
        missile = Missile(self)
        sprites.add(missile)
        missiles.add(missile)


class Missile(pygame.sprite.Sprite):
    def __init__(self, ship):
        super().__init__()
        self.pos = vec(ship.pos.x + 4, ship.pos.y) 
        self.active = True

    def move(self):
        if self.active:
            self.pos.x += VEL*1.5
            if self.pos.x > WIDTH - 10:
                self.active = False
                self.kill()             # remove sprite

    def draw(self):
        self.rect = pygame.draw.aaline(screen, WHITE,
                    (self.pos.x, self.pos.y), (self.pos.x + 10, self.pos.y))


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.pos = vec(WIDTH - 10, 10 + random.randint(0, HEIGHT - 20))
        self.active = True

    def move(self):
        if self.active:
            self.pos.x -= VEL*0.5
            if self.pos.x < 10:
                self.active = False
                self.kill()             # remove sprite

    def draw(self):
        self.rect = pygame.draw.rect(screen, WHITE, [self.pos.x, self.pos.y, 20, 20], width=1)


# =============================================================
    
sprites = pygame.sprite.Group()
missiles = pygame.sprite.Group()
enemies = pygame.sprite.Group()

player = SpaceShip()
sprites.add(player)


run = True
while run: 
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            run = False
        if event.type == pygame.KEYDOWN: 
            if event.key == pygame.K_SPACE:
                player.fire()

    

    player.key_detect()

    # enemy generation
    if dice == 6 and len(enemies) < 10:
        enemy = Enemy()
        sprites.add(enemy)
        enemies.add(enemy)
    dice = random.randint(1, 6)
    
    for missile in missiles:
        missile.move()

    for enemy in enemies:
        enemy.move()

    screen.fill(BLACK)

    for entity in sprites:
        entity.draw()

    
    pygame.display.update()  
    clock.tick(FPS)
