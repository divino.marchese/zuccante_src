import arcade

# window size and title
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = "template"

# create window and set backgroind color
arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
arcade.set_background_color(arcade.color.WHITE)

# START rendering
arcade.start_render()

# here put your code

#STOP renbdering
arcade.finish_render()

# run
arcade.run()
