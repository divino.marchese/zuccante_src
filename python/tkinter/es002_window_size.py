import tkinter as tk

root = tk.Tk()
root.title("es002")
width = 400
height = 400
# root.geometry('400x400+600+400')
# position = (600, 400)
# root.geometry(f'{width}x{height}+{position[0]}+{position[1]}')
position = {'x': 600, 'y': 400}
root.geometry(f'{width}x{height}+{position["x"]}+{position["y"]}')
root.resizable(False, False)


message = tk.Label(root, text="Hello World!")
message.pack()

# put everything on display
root.mainloop()