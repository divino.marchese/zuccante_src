import tkinter as tk

root = tk.Tk()
root.title("es004")
width = 200
height = 100
position_x, position_y =  600, 400
root.geometry(f'{width}x{height}+{position_x}+{position_y}')
root.resizable(False, False)

def print_one():
    print("one")

def print_two():
    print("two")

def print_three():
    print("theww")

btn1 = tk.Button(root, text = 'one', bd = '1', command = print_one)
btn2 = tk.Button(root, text = 'two', bd = '1', command = print_two)
btn3 = tk.Button(root, text = 'three', bd = '1', command = print_three)


btn1.pack(side = tk.LEFT, expand = True, fill = tk.BOTH)
btn2.pack(side = tk.LEFT, expand = True, fill = tk.BOTH)
btn3.pack(side = tk.LEFT, expand = True, fill = tk.BOTH)

root.mainloop()