import tkinter as tk

root = tk.Tk()
root.title("es007")
width = 240
height = 110
position_x, position_y =  600, 400
root.geometry(f'{width}x{height}+{position_x}+{position_y}')
root.resizable(False, False)

root.columnconfigure(0, weight = 1)
root.columnconfigure(1, weight = 3)

name_var = tk.StringVar()
passw_var = tk.StringVar()
 
def submit():
    name = name_var.get()
    password = passw_var.get()
    print("The name is : " + name)
    print("The password is : " + password)
    name_var.set("")
    passw_var.set("")
     
name_label = tk.Label(root, text = 'username', font = ('ubuntu', 10, 'bold'))
name_entry = tk.Entry(root, textvariable = name_var, font = ('monospace',10,'normal'))
passw_label = tk.Label(root, text = 'password', font = ('ubuntu', 10,'bold'))
passw_entry = tk.Entry(root, textvariable = passw_var, font = ('monospace', 10,' normal'), show = '*')
sub_btn = tk.Button(root,text = 'Submit', command = submit)
  
name_label.grid(row = 0, column = 0, sticky = tk.W, padx = 5, pady = 5)
name_entry.grid(row = 0, column = 1, sticky = tk.E, padx = 5, pady = 5)
passw_label.grid(row = 1, column = 0, sticky = tk.W, padx = 5, pady = 5)
passw_entry.grid(row = 1, column = 1, sticky = tk.E, padx = 5, pady = 5)
sub_btn.grid(column =1, row = 2, sticky = tk.E, padx = 5, pady = 5)

root.mainloop()