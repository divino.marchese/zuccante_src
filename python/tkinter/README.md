# tkinter

Un tutorial per principianti per creare interfacce grafiche - GUI - in IK.

## es001

Un classico `Hell WOrld`.

## es002

Posizionamento e ridimensionamento della finestra. `pack()` presenta varie opzioni, vedi [qui](https://docs.python.org/3/library/tkinter.html#packer-options).

## es003, es004

Esempi coi bottoni.

## es005

Un esempio complesso che qui commentiamo nel dettaglio. Prepariamo la finestra come al solito.
```python
type_var = tk.StringVar()
show_text = ""
```
`StringVar` è un serbatoio per depositare `str` (la sua utilità sarà chiata coi porssimi due esempi), esiste anche `tk.IntVar()`.
```python
show_label = tk.Label(root, text = "", font = ('ubuntu', 10,'bold'))
```
questa etichetta vedrà poi modificare il suo testo al premere di un tasto della tastiera
```python
show_label.config(text = type_var.get())
```
Ora possiamo associare alla nostra applicazione gli eventi da tastiera
```python
def key_handler(event):
    ...

root.bind("<Key>", key_handler)
```
il resto del codice sistema i *widget* e quindi disegna.

## es006 - es007

Nel primo dei due esercizi mostriamo qual è lo svantaggio nel non usare `StringVar`: possiamo sia settare il valore che ottenere il suo contenuto in termini di stringa. Un'ottimo tutorial su `StringVar` è [qui](https://www.pythontutorial.net/tkinter/tkinter-stringvar/) (vedi riferimenti). I *widget* sono posizionati su di un *grid*, [qui](https://www.pythontutorial.net/tkinter/tkinter-grid/) il *tutorial* per l'uso della griglia.
```python
root.columnconfigure(0, weight = 1)
root.columnconfigure(1, weight = 3)
```
Configurano le due istruzioni la prima e la seconda colonna dicendoci quanto spazio occupa la prima rispetto alla seconda. Configuriamo i nostri due portatori di valore.
```python
name_var = tk.StringVar()
passw_var = tk.StringVar()
```
Quindi lázione del bottone
```python
def submit():
    name = name_var.get()
    password = passw_var.get()
    print("The name is : " + name)
    print("The password is : " + password)
    name_var.set("")
    passw_var.set("")
```
Nel momento in cui posizioniamo i *widget nella griglia, ad esempio
```python
name_label.grid(row = 0, column = 0, sticky = tk.W, padx = 5, pady = 5)
```
noi diamo
- `row`: riga di appartenenza,
- `column`: colonna di appartenenza
- `sticky`: posizionamento all'interno della cella della griglia, se non specificato il posizionamento è al centro.
- `padx` spazio in orizzontale fra bordo esterno del *widget* e bordo interno della cella: sia a sinistra che a destra e
- `pady` spazio in verticale fra bordo esterno del *widget* e bordo interno della cella: sia in alto che in basso.


## riferimenti

[1] `tkinter` [qui](https://docs.python.org/3/library/tkinter.html#).  
[2] Un tutorial [qui](https://www.pythontutorial.net/tkinter/).  
[3] "Create a GUI Using Tkinter and Python' un tutorial [qui](https://www.pythonguis.com/tutorials/create-gui-tkinter/).  
[3] `Tk` un manuale [qui](https://www.tcl.tk/man/tcl8.6/TkCmd/contents.html).  