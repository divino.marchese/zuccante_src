import tkinter as tk

root = tk.Tk()
root.title("es005")
width = 240
height = 80
position_x, position_y =  600, 400
root.geometry(f'{width}x{height}+{position_x}+{position_y}')
root.resizable(False, False)

type_var = tk.StringVar()
show_text = ""

show_label = tk.Label(root, text = "", font = ('ubuntu', 10,'bold'))
type_label = tk.Label(root, text = 'type here', font = ('ubuntu', 10, 'bold'))
type_entry = tk.Entry(root, textvariable = type_var, font = ('monospace',10,'normal'))

def key_handler(event):
    global show_text
    text_event = event.char, event.keysym, event.keycode
    print(f"event: {text_event}, typed string: {type_var.get()}")
    show_label.config(text = type_var.get())

root.bind("<Key>", key_handler)

type_label.pack(side = tk.TOP, expand = True, fill = tk.BOTH)
type_entry.pack(side = tk.TOP, expand = True, fill = tk.BOTH)  
show_label.pack(side = tk.TOP, expand = True, fill = tk.BOTH)
  
root.mainloop()