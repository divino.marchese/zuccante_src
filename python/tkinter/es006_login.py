import tkinter as tk

root = tk.Tk()
root.title("es006")
width = 240
height = 80
position_x, position_y =  600, 400
root.geometry(f'{width}x{height}+{position_x}+{position_y}')
root.resizable(False, False)
  
def submit(): 
    name = name_entry.get()
    password = passw_entry.get()
    print("The name is : " + name)
    print("The password is : " + password)
    # these does not work, set does not exists
    # name_entry.set("")
    # passw_entry.set("")

name_label = tk.Label(root, text = 'Username', font=('ubuntu', 10, 'bold'))
name_entry = tk.Entry(root, font = ('monospace',10,'normal')) 
passw_label = tk.Label(root, text = 'Password', font = ('ubuntu', 10,'bold'))
passw_entry = tk.Entry(root, font = ('monospace', 10,' normal'), show = '*')
sub_btn = tk.Button(root,text = 'Submit', command = submit)

name_label.grid(row = 0, column = 0)
name_entry.grid(row = 0, column = 1)
passw_label.grid(row = 1, column = 0)
passw_entry.grid(row = 1, column = 1)
sub_btn.grid(row = 2, columnspan = 2, sticky = 'ew')

root.mainloop()