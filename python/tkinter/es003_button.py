import tkinter as tk

root = tk.Tk()
root.title("es003")
width = 200
height = 100
position_x, position_y =  600, 400
root.geometry(f'{width}x{height}+{position_x}+{position_y}')
root.resizable(False, False)

def print_hello():
    print("hello world!")

message = tk.Label(root, text="Strucca botton!")
btn1 = tk.Button(root, text = 'hello !', bd = '1', command = print_hello)
btn2 = tk.Button(root, text = 'exit !', bd = '1', command = root.destroy)

message.pack(side = tk.TOP, expand = True, fill = tk.BOTH)
btn1.pack(side = tk.TOP, expand = True, fill = tk.BOTH)  
btn2.pack(side = tk.TOP, expand = True, fill = tk.BOTH)

root.mainloop()