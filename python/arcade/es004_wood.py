import arcade as arc
import random as rnd

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

# sun
def draw_sun():
    arc.draw_circle_filled(
        SCREEN_WIDTH * 2 / 3, 
        SCREEN_HEIGHT * 4 / 5,
        20,
        arc.color.LIGHT_YELLOW)

# grass
def draw_grass():
    arc.draw_lrtb_rectangle_filled(
        0, SCREEN_WIDTH, 
        SCREEN_HEIGHT / 3, 0,
        arc.color.DARK_SPRING_GREEN)

# pine tree
def draw_pine_tree(x, y):
    # 80 x 100 striangle
    arc.draw_triangle_filled(x + 40, y,
                            x, y - 100,
                            x + 80, y - 100,
                            arc.color.DARK_GREEN)
    # draw the trunk 20 x 40
    arc.draw_lrtb_rectangle_filled(x + 30, x + 50, y - 100, y - 140,
                                      arc.color.DARK_BROWN)

# game window
class Wood(arc.Window):
    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "es004 wood")
        arc.set_background_color(arc.color.SKY_BLUE)
        arc.start_render()
        draw_sun()
        draw_grass()
        for i in range(1, 40):
            x = rnd.randrange(0,11) * 60
            y = rnd.randrange(1,21) * SCREEN_HEIGHT / 60 + 100
            draw_pine_tree(x, y)
        arc.finish_render()


    def on_draw(self):
        pass
        

if __name__ == "__main__":
    app = Wood()
    arc.run()




