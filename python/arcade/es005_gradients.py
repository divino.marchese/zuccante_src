import arcade as arc

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

class MyGame(arc.Window):

    def __init__(self, width, height):

        super().__init__(width, height, "es005 gradients")
        arc.set_background_color(arc.color.BLACK)
        self.shapes = arc.ShapeElementList()
        
        # first gradient top bottom
        color1 = (215, 214, 165)
        color2 = (219, 166, 123)
        colors = (color1, color1, color2, color2)
        points = (0, 0), (SCREEN_WIDTH, 0), (SCREEN_WIDTH, SCREEN_HEIGHT), (0, SCREEN_HEIGHT)
        rect = arc.create_rectangle_filled_with_colors(points, colors)
        self.shapes.append(rect)

        # gradient left to rught
        color1 = (165, 92, 85, 255)
        color2 = (165, 92, 85, 0)
        colors = (color2, color1, color1, color2)
        points = (100, 100), (SCREEN_WIDTH - 100, 100), (SCREEN_WIDTH - 100, 300), (100, 300)
        rect = arc.create_rectangle_filled_with_colors(points, colors)
        self.shapes.append(rect)

        # two lines
        color1 = (7, 67, 88)
        color2 = (69, 137, 133)
        colors = (color2, color1, color2, color1)
        points = (100, 400), (SCREEN_WIDTH - 100, 400), (SCREEN_WIDTH - 100, 500), (100, 500)
        shape = arc.create_lines_with_colors(points, colors, line_width=5)
        self.shapes.append(shape)

        # triangle
        color1 = (215, 214, 165)
        color2 = (219, 166, 123)
        color3 = (165, 92, 85)
        colors = (color1, color2, color3)
        points = (SCREEN_WIDTH // 2, 500), (SCREEN_WIDTH // 2 - 100, 400), (SCREEN_WIDTH // 2 + 100, 400)
        shape = arc.create_triangles_filled_with_colors(points, colors)
        self.shapes.append(shape)

        # ellipse, gradient between center and outside
        color1 = (69, 137, 133, 127)
        color2 = (7, 67, 88, 127)
        shape = arc.create_ellipse_filled_with_colors(SCREEN_WIDTH // 2, 350, 50, 50,
                                                         inside_color=color1, outside_color=color2)
        self.shapes.append(shape)

    def on_draw(self):
        self.clear()
        self.shapes.draw()
        arc.draw_rectangle_filled(500, 500, 50, 50, (255, 0, 0, 127))


def main():

    MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    arc.run()


if __name__ == "__main__":
    main()