import arcade as arc
# import random as rnd

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
BALL_SIZE = 200


class Ball:
    def __init__(self, x, y, dx, dy, scale):
        self.x, self.y = x, y
        self.dx, self.dy = dx, dy
        # ball radius
        self.radius = (scale * BALL_SIZE) / 2
        # init sprite
        self.sprite = arc.Sprite("ball.png", center_x = x, center_y = y,  scale = scale)

    def draw(self):
        self.sprite.draw()
        
    def move(self):
        # set sprite center
        self.x += self.dx
        self.y += self.dy
        if self.x - self.radius < 0 or self.x + self.radius > SCREEN_WIDTH:
            self.dx *= -1
        if self.y - self.radius < 0 or self.y + self.radius > SCREEN_HEIGHT:
            self.dy *= -1
        # move sprite
        self.sprite.center_x = self.x
        self.sprite.center_y = self.y


class MyGame(arc.Window):
    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "es006 ball")
        arc.set_background_color(arc.color.AMAZON)
        scale = 0.5
        x, y = SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2
        dx, dy = 0, 0
        self.ball = Ball(x, y, dx, dy, scale)

    def on_update(self, dt):
        self.ball.move()

    def on_draw(self):
        self.clear()
        self.ball.draw()

    def on_mouse_press(self, x, y, button, modifiers):
        self.ball.dx = 0.04 * (x - self.ball.x)
        self.ball.dy = 0.04 * (y - self.ball.y)

# main function
def main():
    MyGame()
    arc.run()

if __name__ == "__main__":
    main()