import arcade as arc

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 800

# game window
class Welcome(arc.Window):
    def __init__(self, radius):
        # arc.Window.__init__(self, SCREEN_WIDTH, SCREEN_HEIGHT, "es001")
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "es001")
        arc.set_background_color(arc.color.BLACK)
        self.radius = radius
        arc.start_render()
        arc.start_render()
        arc.draw_circle_filled(
            SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, self.radius, arc.color.RED
        )
        arc.finish_render()

    def on_draw(self):
        pass
        

# Main code entry point
if __name__ == "__main__":
    app = Welcome(150)
    arc.run()

