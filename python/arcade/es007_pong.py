import arcade as arc
import random as rnd

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
BALL_RADIUS = 10
PLATFORM_SIZE = 100


class Ball:
    def __init__(self, x, y, dx, dy):
        self.x, self.y = x, y
        self.dx, self.dy = dx, dy
        
    def draw(self):
        arc.draw_circle_filled(self. x, self.y, BALL_RADIUS, arc.color.WHITE)
        
    def move(self, game, platform):
        self.x += self.dx
        self.y += self.dy
        if self.x - BALL_RADIUS < 0 or self.x + BALL_RADIUS > SCREEN_WIDTH:
            self.dx *= -1
        if self.y + BALL_RADIUS > SCREEN_HEIGHT:
            self.dy *= -1
        if self.hit(platform):
            self.dy *= -1
            game.score += 1
        elif self.y < 0:
            self.x = SCREEN_WIDTH/2
            self.y = SCREEN_HEIGHT/2 - 2 * BALL_RADIUS
            self.dx = 0
            self.dy = 0
           
    def hit(self, platform):
        hit_y = self.y - BALL_RADIUS < BALL_RADIUS 
        hit_x = self.x < platform.x + PLATFORM_SIZE/2 and self.x > platform.x - PLATFORM_SIZE/2
        return hit_x and hit_y


class Platform:
    def __init__(self, x, y):
        self.x, self.y = x, y
        
    def draw(self):
        arc.draw_rectangle_filled(self.x, self.y, PLATFORM_SIZE, BALL_RADIUS, arc.color.WHITE)
    
    def move(self, x):
        if x - PLATFORM_SIZE/2 > 0 and x + PLATFORM_SIZE/2 < SCREEN_WIDTH:
            self.x = x

class MyGame(arc.Window): 
    def __init__(self, score):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "es007 pong")
        arc.set_background_color(arc.color.BLACK)
        self.score = score
        x, y = SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2
        dx, dy = -3, 4
        self.ball = Ball(x, y, dx, dy)
        self.platform = Platform(SCREEN_WIDTH/2, BALL_RADIUS/2)

    def on_update(self, dt):
        self.ball.move(self, self.platform)

    def draw_score_message(self):
        arc.draw_text(f"score: {self.score}", 
            SCREEN_WIDTH - 150, SCREEN_HEIGHT - 50, 
            color = arc.color.WHITE,
            font_size = 16,
            font_name = "monospace"
            )

    def draw_end_message(self):
        arc.draw_text(f"HAI PERSO", 
            SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 
            anchor_x = "center",
            color = arc.color.WHITE,
            font_size = 16,
            font_name = "monospace"
        )

    def on_draw(self):
        self.clear()
        self.ball.draw()
        self.platform.draw()
        self.draw_score_message()
        if self.ball.dx == 0 and self.ball.dy == 0:
            self.draw_end_message()
            

    def on_mouse_motion(self, x, y, dx, dy):
        self.platform.move(x)

def main():
    MyGame(0)
    arc.run()

if __name__ == "__main__":
    main()