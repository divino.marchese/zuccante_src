import arcade as arc
import random as rnd

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

colors = (
    arc.color.ALICE_BLUE, 
    arc.color.AMETHYST, 
    arc.color.RED_PURPLE, 
    arc.color.YELLOW_ROSE,
    arc.color.ORANGE_PEEL,
    arc.color.BLUE_GREEN,
    arc.color.WHITE,
    arc.color.ASH_GREY,
    arc.color.GINGER,
    arc.color.AERO_BLUE,
    arc.color.PINK_LACE
    )

def get_random_color():
    index = rnd.randrange(0, len(colors))
    return colors[index]

class Circle:
    def __init__(self, x, y, radius, dx, dy):
        self.x, self.y = x, y
        self.dx, self.dy = dx, dy
        self.radius = radius
        self.color = get_random_color();

    def draw(self):
        arc.draw_circle_filled(self.x, self.y, self.radius, self.color)

    def change_color(self):
        color = get_random_color()
        while(self.color == color):
            color = get_random_color()
        self.color = color
    
    def move(self):
        self.x += self.dx
        self.y += self.dy
        if self.x - self.radius < 0 or self.x + self.radius > SCREEN_WIDTH:
            self.dx *= -1
            self.change_color()
        if self.y - self.radius < 0 or self.y + self.radius > SCREEN_HEIGHT:
            self.dy *= -1
            self.change_color()


class MyGame(arc.Window):
    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "es002 circle")
        radius = 15
        dx, dy = rnd.randrange(1, 6), rnd.randrange(1, 6) 
        x = rnd.randrange(radius, SCREEN_WIDTH - radius)
        y = rnd.randrange(radius, SCREEN_HEIGHT - radius)
        self.circle = Circle(x, y, radius, dx, dy)

    def on_update(self, dt):
        self.circle.move()

    def on_draw(self):
        self.clear()
        self.circle.draw()

# main function
def main():
    MyGame()
    arc.run()


if __name__ == "__main__":
    main()