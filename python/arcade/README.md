# arcade

Una libreria basata su `pyglet` ed altro, a sua volta che lavora con le OpenGL, per giochi 2D classici *arcade*.

# materiali

[1] "How-To Example Code" [qui](https://api.arcade.academy/en/latest/examples/index.html).  
[2] "How to create a 2D game with Python and the Arcade library" di Paul Vincent Craven per un primo percorso di apprendimtno [qui](https://opensource.com/article/18/4/easy-2d-game-creation-python-and-arcade).  
[3] "Arcade: A Primer on the Python Game Framework" da Real Python  [qui](https://realpython.com/arcade-python-game-framework/).  