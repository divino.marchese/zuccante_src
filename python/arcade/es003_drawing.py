import arcade as arc

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

# game window
class Drawing(arc.Window):
    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "es003 drawing")
        arc.set_background_color(arc.color.WHITE)

    def on_draw(self):
        arc.start_render()

        # draw a grid
        for x in range(0, 601, 120):
            arc.draw_line(x, 0, x, 600, arc.color.ASH_GREY, 2)
        for y in range(0, 601, 200):
            arc.draw_line(0, y, 800, y, arc.color.ASH_GREY, 2)
        
        # draw a point
        arc.draw_text("draw_point", 3, 405, arc.color.BLACK, 10)
        arc.draw_point(60, 495, arc.color.RED, 5)

        # draw points
        arc.draw_text("draw_points", 123, 405, arc.color.BLACK, 10)
        points = ((165, 495),
            (165, 480),
            (165, 465),
            (195, 495),
            (195, 480),
            (195, 465))
        arc.draw_points(points, arc.color.ZAFFRE, 5)

        # draw a line from two points
        arc.draw_text("draw_line", 243, 405, arc.color.BLACK, 10)
        arc.draw_line(270, 495, 300, 450, arc.color.WOOD_BROWN, 3)

        # draw a line betwee each psir of points
        arc.draw_text("draw_lines", 363, 405, arc.color.BLACK, 10)
        points = (
            (390, 450),(450, 450),
            (390, 480),(450, 480),
            (390, 510),(450, 510)
            )
        arc.draw_lines(points, arc.color.BLUE, 3)

        # draw a multi-point line
        arc.draw_text("draw_line_strip", 483, 405, arc.color.BLACK, 10)
        points = ((510, 450),
            (570, 450),
            (510, 480),
            (570, 480),
            (510, 510),
            (570, 510))
        arc.draw_line_strip(points, arc.color.TROPICAL_RAIN_FOREST, 3)

        # draw a polygon
        arc.draw_text("draw_polygon_outline", 3, 207, arc.color.BLACK, 10)
        points = ((30, 240),
            (45, 240),
            (60, 255),
            (60, 285),
            (45, 300),
            (30, 300))
        arc.draw_polygon_outline(points, arc.color.SPANISH_VIOLET, 3)

        # draw a filled in polygon
        arc.draw_text("draw_polygon_filled", 123, 207, arc.color.BLACK, 10)
        points = ((150, 240),
            (165, 240),
            (180, 255),
            (180, 285),
            (165, 300),
            (150, 300))
        arc.draw_polygon_filled(points, arc.color.SPANISH_VIOLET)

        # draw an outline of a circle
        arc.draw_text("draw_circle_outline", 243, 207, arc.color.BLACK, 10)
        arc.draw_circle_outline(300, 285, 18, arc.color.WISTERIA, 3)
        
        # draw a filled in circle
        arc.draw_text("draw_circle_filled", 363, 207, arc.color.BLACK, 10)
        arc.draw_circle_filled(420, 285, 18, arc.color.GREEN_YELLOW)

        # draw an ellipse outline, and another one rotated
        arc.draw_text("draw_ellipse_outline", 483, 207, arc.color.BLACK, 10)
        arc.draw_ellipse_outline(540, 273, 15, 36, arc.color.AMBER, 3)
        # at 45 degrees
        arc.draw_ellipse_outline(540, 336, 15, 36, arc.color.BLACK_BEAN, 3, 45)
        
        # draw a filled ellipse, and another one rotated
        arc.draw_text("draw_ellipse_filled", 3, 3, arc.color.BLACK, 10)
        arc.draw_ellipse_filled(60, 81, 15, 36, arc.color.AMBER)
        # at 45 degrees
        arc.draw_ellipse_filled(60, 144, 15, 36, arc.color.BLACK_BEAN, 45)

        # draw an arc, and another one rotated
        arc.draw_text("draw_arc/filled_arc", 123, 3, arc.color.BLACK, 10)
        arc.draw_arc_outline(150, 81, 15, 36, arc.color.BRIGHT_MAROON, 90, 360)
        arc.draw_arc_filled(150, 144, 15, 36, arc.color.BOTTLE_GREEN, 90, 360, 45)
        
        # draw an rectangle outline
        arc.draw_text("draw_rect", 243, 3, arc.color.BLACK, 10)
        arc.draw_rectangle_outline(295, 100, 45, 65, arc.color.BRITISH_RACING_GREEN)
        arc.draw_rectangle_outline(295, 160, 20, 45, arc.color.BRITISH_RACING_GREEN, 3, 45)
        
        # Draw a filled in rectangle\
        arc.draw_text("draw_filled_rect", 363, 3, arc.color.BLACK, 10)
        arc.draw_rectangle_filled(420, 100, 45, 65, arc.color.BLUSH)
        arc.draw_rectangle_filled(420, 160, 20, 40, arc.color.BLUSH, 45)
        
        # load and draw an image to the screen
        arc.draw_text("draw_bitmap", 483, 3, arc.color.BLACK, 10)
        texture = arc.load_texture("logo.png")
        scale = .15
        # rotation 0 degrees
        arc.draw_scaled_texture_rectangle(540, 120, texture, scale, 0)
        # rotation 45 degrees
        arc.draw_scaled_texture_rectangle(540, 60, texture, scale, 45)


# Main code entry point
if __name__ == "__main__":
    app = Drawing()
    arc.run()


                  

                
