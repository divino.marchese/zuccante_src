import arcade as arc

# set window size
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

arc.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Titolo dell'esercizio")
arc.set_background_color(arc.color.BLACK)

# clear screen and start render process
arc.start_render()


# finish drawing and display the result
arc.finish_render()

# keep the window open until the user hits the 'close' button
arc.run()