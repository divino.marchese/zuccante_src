# appunti RISC V

**[230708]** Prima bozza di appunti per un corso di assembly sulla piattaforma RISC V.

## emulatori

- **Ripes** [qui](https://github.com/mortbopet/Ripes)
- per VSC estensione **Venus** e gestione sintassi RISC V.
- Un elenco di sumulatori ed emulatori [qui](https://www.riscfive.com/risc-v-simulators/).

## esempi per iniziare

Ecco alcuni semplici esempi (vedi anche cartella `src`) . I dati sono nei registri
```
li t0, 2
li t1, 3
add t1, t0, t1
```
Prendiamo i dati dalla memoria (variabili)
```
.data
.word 2, 3
.text
lw t0, 0(gp)
lw t1, 4(gp)
add t1, t0, t1
```
Chiamata a sistema (output su terminale)
```
.data
.word 2, 3
.text
lw t0, 0(gp)
lw t1, 4(gp)
add t1, t0, t1
add a0, a0, t1
addi a7, zero, 1       
ecall
```
Ancora sulle variabili inizializzate
```
.data
x: .word 2
y: .word 3
.text
la t0, x
lw t0, 0(t0)
la t1, y
lw t1, 0(t1)
add t1, t0, t1
```
Dichiarazione di variabili ovvero `.bss` con variabili non inizializzate
```
.bss
y: .word 3 
z: .word 1
.text
la t0, x
lw t0, 0(t0)
la t1, y
lw t1, 0(t1)
add t1, t0, t1
la t2, z
sw t1, 0(t2)
```
"hello world" (stampare del testo)
```
.data
msg: .string "hello world"
.text 
la    a0, msg       # load address
addi  a7, zero, 4   # print string
ecall 
```
"numeri di Fibonacci" tratti da slides
```
# Fibonacci Sequence: 
# print first n numbers

.data
     n: .word 3
     newline: .string "\n"
     msg: .string "fine"
.text
     add t0, zero, zero
     addi t1, zero, 1
     la t3, n           # t3 <- n (the counter)
     lw t3, 0(t3)
fib: add a0, zero, t1     
     addi a7, zero, 1   # print int
     ecall
     la    a0, newline 
     addi  a7, zero, 4   # print string
     ecall
     beq t3, zero, end
     add t2, t1, t0
     mv t0, t1
     mv t1, t2
     addi t3, t3, -1
     j fib
end: la    a0, msg 
     addi  a7, zero, 4   # print string
     ecall
     addi a7, zero, 10 # end (clear screen)
     ecall
```

## funzioni

Prima di arrivare alla trattazione generale, quando possibile, per una funzione usiamo i soli registri!
Ecco una prima versione: `a0` e `a1` sono usati per il ritorno (output), `a0`, `a1`, `a2`, ... per gli argomenti (input)
```
.data
msg: .string "after calling function"
.text
addi a0, zero, 2
addi a1, zero, 3
addi ra, zero, 16      # 16 -> ra (return adress)
j sum
j end
sum: add a0, a0, a1
     jr ra
end: addi a7, zero, 10     # end (clear screen)
     ecall
```
function using registers (final version)
```
msg: .string "after calling function"
.text
addi a0, zero, 2
addi a1, zero, 3
jal ra, sum
j end
sum: add a0, a0, a1
     jr ra
end: addi a7, zero, 10     # end (clear screen)
     ecall  
```
Nelle funzioni si usa lo *stack* il protocollo segue i seguenti passi:
- creazione del frame (aggioranemto del valore di `sp`),
- salvataggio di `ra` nel frame (intesta `sw ra 0(sp)`)
- salvare tutti i registri utilizzati del tipo `s0`, `s1`, ... essi verranno uitiolizzti per portare gli argomenti in modo da non lavorare sui registri a loro dedicati (in modo anche da limitare gli accessi in memoria),
- usare i registri `t0`, `t1` ... per valori temporanei come ad esempio costanti per confronto
- ed infine la procedura di chiusura prevede il ripristino di `ra`, di tutti gli `s0`, `s1`, ... utilizzati.

In tal modo possiamo realizzare chiamate di funzione all'interno di un'altra funzione e chiamate ricorsive.

## materiali

[1] Alcuni manuali e refcard da Shakti [qui](https://holbrook.no/share/doc/riscv/).  
[2] Matariali da Psabi [qui](https://github.com/riscv-non-isa/riscv-elf-psabi-doc).  
[3] Slide del corso di Berkeley [qui](https://inst.eecs.berkeley.edu/~cs61c/su20/pdfs/lectures/) e [qui](https://inst.eecs.berkeley.edu/~cs61c/fa17/).  
[4] Corso Shangai "Computer Architecture" RISC V [qui](https://robotics.shanghaitech.edu.cn/courses/ca/21s/).  
[5] RISC V functions [qui](https://inst.eecs.berkeley.edu/~cs61c/fa17/disc/3/Disc3_Sol.pdf)


