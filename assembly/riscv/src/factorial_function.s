# print n * (n-1) + ... + 1, n>=0
# fact(n) = n * fact(n-1)
# a0 input, a1 output

.data
n: .word 12
.text
la a0, n
lw a0, 0(a0)
jal ra, fact
j finish
fact:       
    addi  sp, sp, -8    # create frame
    sw ra, 0(sp)    
    li t0, 2
    blt a0, t0, base    # 0! and 1! == 1
    sw a0, 4(sp)        # save our n
    addi a0, a0, -1
    jal fact            # fact(n-1)
    lw t0, 4(sp)        # t0 <- n
    mul a1, t0, a1      # fact(n) = n * fact(n-1)
    j done
base:
    li a1, 1
done:
    lw ra, 0(sp)    
    addi sp, sp, 8 
    jr ra           
finish:
    add a0, zero, a1
    addi a7, zero, 1      # print int a0
    ecall
    addi a7, zero, 10     # end  
    ecall 
  
