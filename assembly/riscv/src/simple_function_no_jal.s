# a1, a0 -> a0

.data
msg: .string "after calling function"
.text
addi a0, zero, 2
addi a1, zero, 3
addi ra, zero, 16      # 16 -> ra (return adress)
j sum
j end
sum: add a0, a0, a1
     jr ra
end: addi a7, zero, 10     # end (clear screen)
     ecall