# deiv, rem

.data
newline: .string "\n"
.text
addi t0, zero, 7
addi t1, zero, 2
div t3, t0, t1
rem t4, t0, t1
print:
    add a0, zero, t3
    addi a7, zero, 1      # print int a0
    ecall
    la a0, newline
    addi a7, zero, 4      # print int a0
    ecall
    add a0, zero, t4
    addi a7, zero, 1      # newline  
    ecall
    addi a7, zero, 10     # end  
    ecall 
  