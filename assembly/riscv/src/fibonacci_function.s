# a0 -> a0
# fib(n) = fib(n-1) + fib(n-2), 
# fib(0) = 0, fib(1) = 1


.data
n: .word 2
.text
la a0, n
lw a0, 0(a0)
jal ra, fib
j finish
fib:
    addi sp, sp, -12 # create frame
    sw ra, 0(sp)
    sw a0, 4(sp)
    sw s0, 8(sp)
    li t0, 1
    bgt a0, t0, else
    j done           # fib(0) = 0, fib(1) = 1
else:
    addi a0, a0, -1
    jal fib          # fib(n-1)
    mv s0, a0
    lw a0, 4(sp)
    addi a0, a0, -2
    jal fib          # fib(n-2)
    add a0, s0, a0   # fib(n) = fib(n-1) + fib(n-2)
done:
    lw s0, 8(sp)
    lw ra, 0(sp)
    addi sp, sp, 12
    jr ra         
finish:
    addi a7, zero, 1      # print int a0
    ecall
    addi a7, zero, 10     # end  
    ecall 
  
