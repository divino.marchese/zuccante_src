# a0, a1 -> a0, jal

msg: .string "after calling function"
.text
addi a0, zero, 2
addi a1, zero, 3
jal ra, sum
j end
sum: add a0, a0, a1
     jr ra
end: addi a7, zero, 10     # end (clear screen)
     ecall  