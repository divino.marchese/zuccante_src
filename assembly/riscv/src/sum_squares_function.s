# a0 -> a0
# n² + (n-1)² + ... + 2²

.data
n: .word 4
.text
la a0, n
lw a0, 0(a0)
jal ra, sum_squares
j finish

# function

sum_squares:
    addi sp, sp, −12  # prepare frame
    sw ra, 0(sp)
    sw s0, 4(sp)
    sw s1, 8(sp)
    li t0, 1          # t0 <- 1 
    mv s0, a0         # addi s1, a0, 0
    add s1, zero, zero
loop:
    bge t0, s0, end
    mv a0, s0
    jal square   
    # mul a0, a0, a0  # a macro  
    add s1, s1, a0
    addi s0, s0, -1
    j loop
end:
    mv a0, s1
    lw ra, 0(sp)      # release frame
    lw s0, 4(sp)
    lw s1, 8(sp)
    addi sp, sp, 12
    jr ra

# function square
   
square:
    addi sp, sp, −4   # prepare frame
    sw ra, 0(sp)
    mul a0, a0, a0
    lw ra, 0(sp)
    addi sp, sp, 4
    jr ra
    
# finish

finish: 
  addi a7, zero, 1      # print int a0
  ecall
  addi a7, zero, 10     # finish  
  ecall 
         