.data
     n: .word 3
     newline: .string "\n"
     msg: .string "fine"
.text
     add t0, zero, zero # x0 is zero <- 0 read only
     addi t1, zero, 1
     la t3, n           # t3 <- n (the counter)
     lw t3, 0(t3)
fib: add a0, a0, t1     
     addi a7, zero, 1   # print int
     ecall
     la    a0, newline 
     addi  a7, zero, 4   # print string
     ecall
     beq t3, zero, end
     add t2, t1, t0
     mv t0, t1
     mv t1, t2
     addi t3, t3, -1
     j fib
end: la    a0, msg 
     addi  a7, zero, 4   # print string
     ecall
     # addi a7, zero, 10 # end (clear screen)
     # ecall
               

