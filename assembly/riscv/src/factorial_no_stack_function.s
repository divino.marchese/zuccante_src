# a0 -> a0

.data
n: .word 5
.text
la a0, n
lw a0, 0(a0)
jal ra, fact
j end
fact: 
  add t0, zero, a0
  addi a0, zero, 1
loop:
  beq t0, zero, finish
  mul a0, a0, t0
  addi t0, t0, -1
  j fact
finish:
  jr ra
end: 
  addi a7, zero, 1   # print int
  ecall
  addi a7, zero, 10     # end  
  ecall 
         
