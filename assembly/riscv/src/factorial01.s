# factorial 

.data
n: .word 5
.text
la t0, n
lw t0, 0(t0)
addi a0, zero, 1
fact:
  beq t0, zero, finish
  mul a0, a0, t0
  addi t0, t0, -1
  j fact
finish:
  addi a7, zero, 1      # print int a0
  ecall
  addi a7, zero, 10     # end  
  ecall 
         
